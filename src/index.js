import React from 'react';
import ReactDOM from 'react-dom';

import './scss/general.css'
import './scss/difference.css'

import registerServiceWorker from './registerServiceWorker';
import { HashRouter, Route, Switch } from "react-router-dom";
import App from './App';
import Dashboard from './modules/dashboard/dashboard.component'
import Login from './modules/login/login.component'
import UserManagement from './modules/user-management/user-management.component'
import ForgotPassword from './modules/login/forgot-password.component'
import SetPassword from './modules/login/set-password.component'
import FormM from './modules/form-m/form-m.component'
import Configurations from './modules/configuration/config.component'
import FormMDetails from './modules/form-m/form-m-details.component'
import Audit from './modules/audit/audit.component'
import Error404 from './modules/shared/error-404.component'
import TemplateManagement from './modules/template/template-management.component'
import Correspondence from './modules/correspondence/correspondence.component'
import NewCorrespondence from './modules/correspondence/new-correspondence.component'
import BranchManagement from './modules/branch-management/branch.component'
import PaarProcessing from './modules/paar-processing/paar-processing.component'
import NewShippingDocument from './modules/paar-processing/shipping-document/new-shipping-document.component'
import EditShippingDocument from './modules/paar-processing/shipping-document/edit-shipping-document.component'
import DocumentExaminationView from './modules/paar-processing/document-examination/document-examination-view.component'
import EcdView from './modules/paar-processing/ecd/ecd-view.component'
import NewEcdDocuments from './modules/paar-processing/ecd/new-ecd-documents.component'
import NewDocumentRelease from './modules/paar-processing/document-release/new-document-release.component'
import EditDocumentRelease from './modules/paar-processing/document-release/edit-document-release.component'
import NewDutyPayment from './modules/paar-processing/duty-payment/new-duty-payment.component'
import DutyPaymentView from './modules/paar-processing/duty-payment/duty-payment-view.component';

ReactDOM.render(
  <App>
      <HashRouter>
        <Switch component={App}>          
            <Route exact path="/" component={Login} />
            <Route path="/home" component={Dashboard} />            
            <Route path="/users" component={UserManagement} />
            <Route path="/roles" component={UserManagement} />
            <Route path="/forgot-password" component={ForgotPassword} />
            <Route path="/set-password/:resetToken" component={SetPassword} />
            <Route exact path="/form-m" component={FormM} />
            <Route path="/form-m/:formID" component={FormMDetails} />
            <Route path="/configurations" component={Configurations} />
            <Route path="/audits" component={Audit} />
            <Route exact path="/templates/letter" component={TemplateManagement} />            
            <Route exact path="/templates/email" component={TemplateManagement} />
            <Route exact path="/correspondence" component={Correspondence} />
            <Route exact path="/correspondence/new" component={NewCorrespondence} />
            <Route path="/branch-management" component={BranchManagement} />

            <Route exact path="/paar" component={PaarProcessing} />
            <Route exact path="/examination" component={PaarProcessing} />            
            <Route exact path="/ecd" component={PaarProcessing} />            
            <Route exact path="/duty" component={PaarProcessing} />            
            <Route exact path="/release" component={PaarProcessing} />
            
            <Route exact path="/shipping/document/new" component={NewShippingDocument} />
            <Route exact path="/ecd/new" component={NewEcdDocuments} />
            <Route exact path="/release/initiate" component={NewDocumentRelease} />
            <Route exact path="/duty/new" component={NewDutyPayment} /> 

            <Route exact path="/shipping/document/:shippingDocumentDetailsId" component={EditShippingDocument} />            
            <Route path="/examination/:shippingDocumentDetailsId" component={DocumentExaminationView} /> 
            <Route path="/ecd/:shippingDocumentDetailsId" component={EcdView} /> 
            <Route path="/release/:shippingDocumentDetailsId" component={EditDocumentRelease} />
            <Route path="/duty/:shippingDocumentDetailsId" component={DutyPaymentView} />

            <Route component={Error404} />          
        </Switch>
    </HashRouter>
  </App>, 
  document.getElementById('root'));
registerServiceWorker();

