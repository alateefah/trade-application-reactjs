import React, { Component } from 'react'

//from semantic ui react
import { Table } from 'semantic-ui-react'

class RecentFormMTable extends Component {
    render () {
        return (
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Food</Table.HeaderCell>
                        <Table.HeaderCell>Calories</Table.HeaderCell>
                        <Table.HeaderCell>Protein</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    <Table.Row>
                        <Table.Cell>Apples</Table.Cell>
                        <Table.Cell>200</Table.Cell>
                        <Table.Cell>0g</Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>Orange</Table.Cell>
                        <Table.Cell>310</Table.Cell>
                        <Table.Cell>0g</Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        )
    }
}

export default RecentFormMTable