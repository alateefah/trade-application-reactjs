import React, { Component } from 'react'

//from semantic ui react
import { Segment, Header, Icon } from 'semantic-ui-react'

class Summary extends Component {

    render () {
        return (
            <Segment padded clearing>
                <Header as='h1' floated='left'>
                    <Icon name= {this.props.icon} size="massive" className="ut-teal" />
                </Header>
                <Header as='h1' floated='right' textAlign='right'>
                    <Header.Content>
                        {this.props.value} <Header.Subheader content= {this.props.subheader} />
                    </Header.Content>
                </Header>
            </Segment>
        )
    }
}

export default Summary