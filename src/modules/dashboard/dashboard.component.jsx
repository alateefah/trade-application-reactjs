import React, { Component } from 'react';

//from semantic ui react
import { Header, Grid, Segment, Icon } from 'semantic-ui-react'

//custom
import AppWrapper from './../shared/main/app-wrapper.component'

class Dashboard extends Component {
    render(){
        return (
            <div>
                <AppWrapper> 
                    <Header as='h3'>Home</Header>
                    <Grid columns={4}>
                        <Grid.Row>
                            <Grid.Column width={16}>
                                <Segment padded='very'>
                                    <Header as='h2' icon textAlign='center' color="red">
                                        <Icon name='industry' />
                                        {/* <Header.Content> Welcome </Header.Content> */}
                                    </Header>
                                    <Header as="h4" textAlign='center'>
                                        Welcome to UBA Trade System
                                    </Header>
                                </Segment>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    
                </AppWrapper>
            </div>
        );
    }
}

export default Dashboard