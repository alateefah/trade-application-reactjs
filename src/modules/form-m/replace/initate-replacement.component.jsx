import React, { Component } from 'react'
import { Modal, Header, Button, Form, Message, Table } from 'semantic-ui-react'
import SharedService from './../../../services/shared_service'
import ErrorMessage from './../../shared/message/error.component'
class InitiateReplacement extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            form: props.form,
            modalOpen: false,
            documents: [],
            templates: props.templates,
            recordId: "",
            name: "",
            parentId: props.form,
            formCategory:"FORM_M",
            description: "Approval for form M Replacement",
            initiatorComments: "",
            approvers: "",
            template: "",
            isLoadingForm: false,
            value: "",
            isSearchingUser: false,
            userSearchValue: "",
            users: [],
            showOthers: false,
            formNumber: "",
            comment: "",
            result: "",
            checked: false
        }
    }

    searchFormM = () => { 
        if (this.state.formNumber !== "") {
            this.setState({ isLoadingForm: true, searched: false });
            let params ={
                pageSize: 1,
                pageNumber: 1,
                formType: 'IM',
                formNumber: this.state.formNumber
            }
            SharedService.searchForForms(params)
            .then(response => {       
                this.setState({ isLoadingForm: false });
                if (!response.code) {
                    this.setState ({ searched: true, result: response.list, isLoadingForm: false })
                }
            })
        }
    }

    clearForm = () => {
        this.setState ({
            result: "", 
            searched: false,
            formNumber: "",
            checked: false
        });    
    }

    handleInputChange = (event) => {         
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ [name] : value });          
    }

    save = () => {
        let params = {
            parentId: this.state.checked ? this.state.form: this.state.result[0].id,
            name: "",
            formCategory: "FORM_M",
            type: "FORM_REPLACEMENT",
            description: "Approval for form M Replacement",
            initiatorComments: this.state.initiatorComments,
            recordId: this.state.checked ? this.state.result[0].id: this.state.form
        }
        
        this.setState({ isLoadingForm: true});            
        SharedService.requestApproval(params)
        .then( response => {
            this.setState({ isLoadingForm: false })
            if (response.code) {
                this.setState ({ errorMessage: response.description})                
            } else {            
                this.props.callbackForSuccessfulReplacementRequest();
            }
        })        
        
    }

    toggleNewForm = () => this.setState({ checked: !this.state.checked }) 

    render () {
        const { isLoadingForm, result } = this.state;
        let formDetails = result[0];

        return (     
            <Modal size="small" onClose={this.handleClose} trigger={this.props.trigger} closeIcon>
                <Header content='Request to Replace Form' />
                <Modal.Content>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                    <Form loading={isLoadingForm}>
                        <Form.Input fluid label='Form M' placeholder='Enter Form M Number' 
                            readOnly={formDetails && this.state.searched}
                            value={this.state.formNumber} onChange={this.handleInputChange} name="formNumber" />
                        <a className="helptext" onClick={this.searchFormM}> Search </a>
                        {this.state.searched && result.length === 0 && <Message negative content="Form not found" /> }
                        {this.state.searched && result.length > 0 &&
                            <span>
                                <Message onDismiss={this.clearForm}>
                                    <Message.Content>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Application Number <Header.Subheader>{formDetails.applicationNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Applicant TIN <Header.Subheader>{formDetails.applicantTin}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Name <Header.Subheader>{formDetails.beneficiaryName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Phone <Header.Subheader>{formDetails.beneficiaryPhone}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                                                                           
                                            </Table.Body>
                                        </Table>
                                        <Form.Checkbox label='This is the new form' onChange={this.toggleNewForm} checked={this.state.checked}/>
                                    </Message.Content>
                                </Message>   
                                <Form.TextArea label='Comment' name="initiatorComments" value={this.state.initiatorComments} onChange={this.handleInputChange} placeholder='Comment...' />
                            </span>
                        }
                    </Form>
                </Modal.Content>
                {this.state.searched && result.length > 0 &&
                <Modal.Actions>
                    <Button onClick={this.save} positive content='Save' />                 
                </Modal.Actions>}
            </Modal>    
        )
    }
}

export default InitiateReplacement