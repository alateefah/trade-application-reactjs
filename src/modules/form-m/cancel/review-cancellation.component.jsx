import React, { Component } from 'react'
import { Header, Button, Form, Dropdown, Table, Message, Accordion, Icon, Feed} from 'semantic-ui-react'
import ErrorMessage from './../../shared/message/error.component'
import DocumentDisplayModal from '../../shared/document-display-modal.component';
import AuthenticationService from '../../../services/authentication_service';
import SharedService from '../../../services/shared_service';

class ReviewCancellation extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            templates: props.templates.filter( template => template.purpose === "CANCELLATION_APPROVAL"),
            form: props.form,
            template: "",  
            isLoadingForm: false,
            cancellationDetails: null,
            activeIndex: "",
            currentCancellation: "",
            comment: ""
            
        }
    }

    componentWillMount () { this.getFormCancellations(); }

    getFormCancellations = () => {        
        let params = {
            type: "FORM_CANCELLATION",
            recordId: this.props.form,
            parentId: this.props.form,
        }
        SharedService.getApproval(params)
        .then(response => {
            if (response.code) {
                this.alertError(response.description)
            } else {
               
                this.setState({ 
                    cancellationDetails: response.list,
                    currentCancellation:  response.list.find(cancellationDetail => cancellationDetail.approvalStatus !== "REJECTED")                    
                }, () => {
                    this.setState({ activeIndex: this.state.currentCancellation? this.state.currentCancellation.id: "" })
                })
            }
        })
    }

    validForm = () => {
        this.setState ({ errorMessage: null })
        
        if (this.state.template === null || this.state.template === "") {            
            this.setState ({ errorMessage: "You must select a template to generate a correspondence with" })
            return false;
        }        
        
        return true;
    }

    handleTemplateSelect = (e, { value }) => { this.setState({ template: value }) }

    handleInputChange = (e, { name, value }) => this.setState({ [name]: value }) 
    
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0)
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    reject = (id) => {
        if (this.state.comment === "") {
            this.alertError("Comment required for rejection");
        } else {
            let params = { 
                parentId: id,
                approved: false,
                recordId: id
            }
            this.submit(params);
        }
    }

    approve = (id) => {
        if (this.validForm()) {
            let params = {
                parentId: id,
                approved: true,
                recordId: this.state.template,
                hasCorrespondence: true
            }
            this.submit(params); 
        }
    }

    submit = (params) => {                    
        this.setState({ isLoadingForm: true });
        
        params.type = "FORM_CANCELLATION_II";
        params.url = "cancellation";
        params.initiatorComments = this.state.comment;
        params.formCategory ="FORM_M";

        SharedService.requestApproval(params)
        .then( response => {
            this.setState({ isLoadingForm: false })
            if (response.code) {
                this.alertError(response.description);                
            } else {            
                this.props.callbackForCancellationReview();
            }
        })         
    }

    rejectReview = (id) => {
        if (this.state.comment === "") {
            this.alertError("Comment required for rejection");
        } else {
            let params = { 
                parentId: id,
                approved: false
            }
            this.submitReview(params); 
        }
    }

    approveReview = (id) => {
        let params = {
            parentId: id,
            approved: true
        }
        this.submitReview(params);
    }

    submitReview = (params) => {
        const {cancellationDetails} = this.state;

        let pendingCancellation = cancellationDetails.find(cancellationDetail => cancellationDetail.approvalStatus !== "REJECTED" 
                                                                                        && cancellationDetail.approvalStatus !== "APPROVED");
        let pendingApproval = pendingCancellation.approvals.find(approval => approval.approvalStatus === "PENDING");

        this.setState({ isLoadingForm: true });

        params.approvalId = pendingApproval.id
        params.type = "FORM_CANCELLATION_II";
        params.url = "cancellation";
        params.approverComments = this.state.comment;
        params.formCategory ="FORM_M";  
        params.recordId = pendingApproval.recordId;
        
        SharedService.authorizeApproval(params)
        .then( response => {
            this.setState({ isLoadingForm: false })
            if (response.code) {
                this.alertError(response.description);                
            } else {            
                this.props.callbackForCancellationReview();
            }
        })     
    }

    render () {
        const { templates, isLoadingForm, cancellationDetails, activeIndex} = this.state
        let templateOptions = [], cancellationDetailsAccordions=[];

        if (templates && templates.length > 0) {                  
            templateOptions =  templates.map(template => (
                { key: template.id, text: template.name, value: template.id } 
            ))
        }

        cancellationDetailsAccordions = cancellationDetails &&  cancellationDetails.map((cancellationDetail, index) => {
            return  <Accordion.Accordion key={index}>
                        <Accordion.Title active={activeIndex === cancellationDetail.id} index={cancellationDetail.id} onClick={this.handleClick}>
                            <Icon name='dropdown'/>{"Cancellation by: "+cancellationDetail.createdBy +" ("+cancellationDetail.createdDate+")" }
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === cancellationDetail.id}>
                            <Message>                                
                                <fieldset>
                                    <legend>Uploads</legend>
                                    <Table basic='very' >
                                        <Table.Body>
                                            {cancellationDetail.attachments.map((attachment, index) => {
                                                return <Table.Row key={index}>
                                                            <Table.Cell>{attachment.documentType}</Table.Cell>
                                                            <Table.Cell>
                                                                <DocumentDisplayModal document={attachment} />
                                                            </Table.Cell>
                                                        </Table.Row>
                                                })
                                            }   
                                        </Table.Body>
                                    </Table>                                                   
                                </fieldset>
                                <br/>
                                <Header as="h4">Comment</Header>
                                {cancellationDetail.initiatorComments}
                            </Message>

                            <Feed>
                                {cancellationDetail.approvals && cancellationDetail.approvals.map((approval, index) => {
                                    return  <span className="ui feed" key={index}>
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.createdDate}
                                                    summary={approval.createdBy+" requested form cancellation be "+ approval.initiatorsReview}
                                                    extraText= {approval.initiatorComments}
                                                />
                                                {approval.approvalStatus !== "PENDING" &&
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.approvedDate}
                                                    summary={approval.approvedBy +" "+ approval.approvalStatus+" request."}
                                                    extraText= {approval.approverComments}
                                                />}
                                            </span>
                                    })                                                
                                }               
                            </Feed>

                            {cancellationDetail.status === "CORRESPONDENCE_PENDING_APPROVAL" &&
                            <Message info icon>
                                <Icon name="exclamation triangle" />
                                <Message.Content>
                                    <Message.Header>Notice!</Message.Header>
                                    A Correspondence has been generated for the Form Amendment. Please review in the Correspondences tab.
                                </Message.Content>
                            </Message>
                            }
                            {(cancellationDetail.approvalStatus === "PENDING_AUTHORIZATION" || cancellationDetail.approvalStatus === "PENDING"  || cancellationDetail.approvalStatus === "PENDING_REJECTION" || cancellationDetail.approvalStatus === "PENDING_APPROVAL")
                            && AuthenticationService.hasPermission("AUTHORIZE_AMENDMENT_REQUESTS")                            
                            && <Form loading={isLoadingForm}>
                                    {cancellationDetail.approvalStatus === "PENDING" &&
                                        <Form.Field>
                                            <label> Select Template </label>
                                            <Dropdown fluid selection placeholder='Templates'                                  
                                                options={templateOptions} 
                                                value={this.state.template}
                                                onChange={this.handleTemplateSelect}
                                            />
                                        </Form.Field>
                                    }
                                    <Form.TextArea label='Comment' name="comment" value={this.state.comment} onChange={this.handleInputChange} placeholder='Comment...' />
                                    {AuthenticationService.hasPermission("CREATE_APPROVAL_REQUEST") 
                                    && cancellationDetail.approvalStatus === "PENDING"
                                    &&  <span>
                                            <Button onClick={()=>this.approve(cancellationDetail.id)} positive icon="checkmark" content='Approve' />
                                            <Button onClick={()=>this.reject(cancellationDetail.id)} negative icon="cancel" content='Reject' />
                                        </span>
                                    }

                                    {AuthenticationService.hasPermission("AUTHORIZE_AMENDMENT_REQUESTS") 
                                    && (cancellationDetail.approvalStatus === "PENDING_AUTHORIZATION" || cancellationDetail.approvalStatus === "PENDING_REJECTION" || cancellationDetail.approvalStatus === "PENDING_APPROVAL")
                                    &&  <span>
                                            <Button onClick={()=>this.rejectReview(cancellationDetail.id)} negative icon="cancel" content='Return for correction by inputter' />
                                            <Button onClick={()=>this.approveReview(cancellationDetail.id)} positive icon="checkmark" content='Approve' />                                            
                                        </span>
                                    }
                                </Form>
                            }
                        </Accordion.Content>
                    </Accordion.Accordion>
        })

        return ( 
            <span>  
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                <Accordion fluid styled>
                    {cancellationDetailsAccordions}
                </Accordion> 
            </span> 
        )
    }
}

export default ReviewCancellation