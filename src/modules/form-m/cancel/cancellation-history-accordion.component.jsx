import React, { Component } from 'react'
import { Header, Table, Message, Accordion, Icon, Feed} from 'semantic-ui-react'
import DocumentDisplayModal from '../../shared/document-display-modal.component';

class CancellationHistoryAccordion extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            history: props.history,
            activeIndex: ""           
        }
    }

    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
    }


    render () {
        const { history, activeIndex} = this.state
        let cancellationDetailsAccordions=[];

       cancellationDetailsAccordions =  history.map((cancellationDetail, index) => {
            return  <Accordion.Accordion key={index}>
                        <Accordion.Title active={activeIndex === cancellationDetail.id} index={cancellationDetail.id} onClick={this.handleClick}>
                            <Icon name='dropdown'/>{"Cancellation by: "+cancellationDetail.createdBy +" ("+cancellationDetail.createdDate+")" }
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === cancellationDetail.id}>
                            <Message>                                
                                <fieldset>
                                    <legend>Uploads</legend>
                                    <Table basic='very' >
                                        <Table.Body>
                                            {cancellationDetail.attachments.map((attachment, index) => {
                                                return <Table.Row key={index}>
                                                            <Table.Cell>{attachment.documentType}</Table.Cell>
                                                            <Table.Cell>
                                                                <DocumentDisplayModal document={attachment} />
                                                            </Table.Cell>
                                                        </Table.Row>
                                                })
                                            }   
                                        </Table.Body>
                                    </Table>                                                   
                                </fieldset>
                                <br/>
                                <Header as="h4">Comment</Header>
                                {cancellationDetail.initiatorComments}
                            </Message>

                            <Feed>
                                {cancellationDetail.approvals && cancellationDetail.approvals.map((approval, index) => {
                                    return  <span className="ui feed" key={index}>
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.createdDate}
                                                    summary={approval.createdBy+" requested form cancellation be "+ approval.initiatorsReview}
                                                    extraText= {approval.initiatorComments}
                                                />                                                
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.approvedDate}
                                                    summary={approval.approvedBy +" "+ approval.approvalStatus+" request."}
                                                    extraText= {approval.approverComments}
                                                />
                                            </span>
                                    })
                                                
                                }               
                                                
                            </Feed>
                        </Accordion.Content>
                    </Accordion.Accordion>
        })

        return ( 
            <span>            
                <Accordion fluid styled>
                    {cancellationDetailsAccordions}
                </Accordion> 
                <br/>
            </span>
        )
    }
}

export default CancellationHistoryAccordion