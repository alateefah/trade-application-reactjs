import React, { Component } from 'react'
import { Modal, Header, Button, Form, Table, Icon } from 'semantic-ui-react'
import SharedService from './../../../services/shared_service'
import UploadFile from './../../shared/upload-file.component'
import ErrorMessage from './../../shared/message/error.component'
import CancellationHistoryAccordion from './cancellation-history-accordion.component'

var _ = require("lodash");

class InitiateCancellation extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            form: props.form,
            modalOpen: false,
            documents: [],
            formData: {
                recordId: props.form,
                parentId: props.form,
                name: "",
                description: "Request for Cancellation",
                type: "FORM_CANCELLATION",
                initiatorComments: "",
                extraRecords : [],
                formCategory:"FORM_M",
            },
            template: "",
            isLoadingForm: false,
            value: "",
            isSearchingUser: false,
            userSearchValue: "",
            users: [],
            fileIndex: 0,
            cancellationDetails: null
        }
    }

    componentWillMount () { this.getFormCancellations(); }

    getFormCancellations = () => {        
        let params = {
            type: "FORM_CANCELLATION",
            recordId: this.props.form,
            parentId: this.props.form,
        }
        SharedService.getApproval(params)
        .then(response => {
            if (response.code) {
                this.alertError(response.description)
            } else {               
                this.setState({ cancellationDetails: response.list })
            }
        })
    }

    getFormDocuments = () => { 
        this.setState({ isLoadingForm: true });
        SharedService.getAppParameter('IM_CANCELLATION_DOCUMENTS')
        .then(response => {           
            if (!response.code) {
                this.setState ({ documents: response.value.split(","), isLoadingForm: false })
            }
        })
    }

    validForm = () => {
        const {documents, formData} = this.state;
        this.setState ({ errorMessage: null });

        for (let i = 0; i < documents.length; i++) { 
            
            var a = formData.extraRecords.filter(record => record.documentType === documents[i])             
            
            if (a.length === 0) {
                this.alertError("All document types must have at least one file uploaded.");
                return false;
            }
        }    
        
        return true;
    }

    clearForm = () => {
        this.setState ({
            errorMessage: null, 
            formData: {
                initiatorComments: ""                      
            }
        });    
    }
   
    afterUpload = (data, document) => { 
        let doc = {
            documentName: data.documentName,
            documentType: document,
            documentBinaryType: data.fileExtension
        }

        let newArray = [ ...this.state.formData.extraRecords, doc ];
        this.setState ({ 
            formData: {
                ...this.state.formData,
                extraRecords: newArray,
            },
            fileIndex: this.state.fileIndex + 1            
        }); 
    }

    delete = (e, i) => {
        SharedService.deleteFile(e)
        .then(response => {            
            var newArray = this.state.formData.extraRecords;
            var index = _.findIndex(newArray, function(o) { return o.documentName === e ;})
            newArray.splice(index, 1);
    
            this.setState({ 
                formData: {
                    ...this.state.formData,
                    extraRecords: newArray
                }
            }); 
        })       

    }

    handleInputChange = (e, { value }) => { 
        this.setState({ 
            formData: {
                ...this.state.formData,
                initiatorComments: value
            }
        }) 
    }

    save = () => {
        if (this.validForm()) { 
            this.setState({ isLoadingForm: true});            
            SharedService.requestApproval(this.state.formData)
            .then( response => {
                this.setState({ isLoadingForm: false })
                if (response.code) {
                    this.alertError(response.description)                
                } else {            
                    this.props.callbackForSuccessfulCancellationRequest();
                }
            })        
        }
    }
    
    alertError = (msg) => {
        this.setState({ errorMessage: msg});
        setTimeout(() => this.setState({errorMessage: null}), 4000)
    }

    render () {
        const { documents, isLoadingForm, formData, cancellationDetails } = this.state
        let documentsRow = [];

        if (documents && documents.length > 0) {    
            documentsRow = documents.map((document, i) => {
                let uploaded = formData.extraRecords.filter(record => record.documentType === document);
                
                return <Table.Row key={i}>
                        <Table.Cell collapsing>{document}</Table.Cell>
                        <Table.Cell>
                            {uploaded.map((upload, i) => {
                                return <span key={i}>
                                            <a href={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                            <Icon link name="trash" color="red" style={{float: "right"}} onClick={()=>this.delete(upload.documentName)} />
                                            <br/>
                                        </span>
                                })
                            }                            
                            <UploadFile ref={'row-'+i} info={i} callbackForUpload={(e)=>this.afterUpload(e, document)} />                            
                        </Table.Cell>
                    </Table.Row>
                });
        }

        return (     
            <Modal size="small" onClose={this.handleClose} trigger={this.props.trigger} onOpen={this.getFormDocuments} closeIcon>
                <Header content='Request to Cancel Form' />
                <Modal.Content>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                    {cancellationDetails && <CancellationHistoryAccordion history={cancellationDetails}/>}
                    <Form loading={isLoadingForm}>
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Document Type</Table.HeaderCell>
                                    <Table.HeaderCell>Upload</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{documentsRow}</Table.Body>
                        </Table>                        
                        <Form.TextArea label='Comment' value={this.state.formData.initatorComments} onChange={this.handleInputChange} placeholder='Comment...' />
                    </Form>
                </Modal.Content>
                <Modal.Actions>
                    <Button onClick={this.save} positive content='Save' />                 
                </Modal.Actions>
            </Modal>    
        )
    }
}

export default InitiateCancellation