import React, { Component } from 'react'
import {Table, List, Segment } from 'semantic-ui-react'
import DocumentDisplayModal from './../shared/document-display-modal.component'
import CorrespondenceUpdate from './../correspondence/correspondence-update.component'
import GenerateCorrespondence from './generate-correspondence.component';

class FormCorrespondence extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            correspondences: props.correspondences,
            formId: props.formId,
            templates: props.templates,
            showCreate: false,
            template: "",
            creating: false,
        }
    }

    handleChange = (e, { value }) => this.setState({ value })

    successfulUpdate = () => { this.props.successfulCorrespondenceUpload();  }

    handleSelectChange = (e, val) => { this.setState({ template: val.value }); } 

    create = () => {
        this.setState ({ creating: true})
        let params = {
            formIds: this.state.formId.split(),
            templateId: this.state.template,
            isBatch: false,
        }
        this.props.createCorrespondence(params);
        this.setState ({ creating: false})
    }

    showCreate = () => { this.setState({showCreate: !this.state.showCreate}) }

    render() {
        const { correspondences, showCreate } = this.state
        let correspondencesRows;

        correspondencesRows = correspondences && correspondences.map(correspondence => {
            return  <Table.Row key = {correspondence.id}>                            
                        <Table.Cell>
                            <DocumentDisplayModal document={correspondence} />
                        </Table.Cell>
                        <Table.Cell>{correspondence.createdAt} </Table.Cell>    
                        <Table.Cell> 
                            {correspondence.attachmentStatus === "CORRESPONDENCE_PENDING_APPROVAL" ?
                                <CorrespondenceUpdate correspondence={correspondence.id} successfulUpdate = {this.successfulUpdate} />:
                                correspondence.attachmentStatus
                            }
                        </Table.Cell>
                    </Table.Row>
        })

        return (
            <div> 
                <List size="large">
                    <List.Header as='a' className="ut-pointer" onClick={this.showCreate}>+ New</List.Header>
                </List>
                {showCreate && 
                    <Segment color="grey">                    
                        <GenerateCorrespondence 
                            templates={this.state.templates} 
                            formId= {this.state.formId}
                            successfulCorrespondenceUpload={()=>this.props.successfulCorrespondenceUpload()} 
                            createCorrespondence = {(params)=>this.props.createCorrespondence(params)}
                        />
                    </Segment>
                }
                {correspondences.length <= 0 ?
                    <span style= {{ padding: 5 }}>
                        No Correspondences
                    </span>:
                    <Table celled striped>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell> File</Table.HeaderCell>
                                <Table.HeaderCell> Created</Table.HeaderCell>
                                <Table.HeaderCell> Status</Table.HeaderCell>                            
                            </Table.Row>  
                        </Table.Header>    
                        <Table.Body>{correspondencesRows}</Table.Body>                         
                    </Table>
                }
            </div>
        )
    }
}
export default FormCorrespondence