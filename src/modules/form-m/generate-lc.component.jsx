import React, { Component } from 'react'
import { Form, Button, Checkbox } from 'semantic-ui-react'

class GenerateLC extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            form: props.form,
            checked: false
        }
    }

    toggle = () => this.setState({ checked: !this.state.checked })

    generateLC = () => {
        this.setState({ loading: true})
        let data = {
            formCategory: "FORM_M",
            parentId: this.state.form
        }
        this.props.callbackForLCGeneration(data);
    }

    render () {
        return (
            <Form>
                <Form.Field>
                    <Checkbox label='Generate LC' onChange={this.toggle} checked={this.state.checked} />
                </Form.Field>
                {this.state.checked && <Button type='submit' className="ut-blue-btn" loading={this.state.loading} onClick={this.generateLC}>Generate</Button>}
            </Form>                
        )
    }
}

export default GenerateLC