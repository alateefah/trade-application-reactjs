import React, { Component } from 'react'
import { Form, Radio, Input, Button } from 'semantic-ui-react'
import GenerateCorrespondence from './generate-correspondence.component'

class ExemptedGoodsReview extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            loading: false,
            templates: props.templates,
            formId: props.formId,
            value: null,
            errorMessage: null,
            goodsDetailsId: props.goodsId
        }
    }

    handleChange = (e, { value }) => { this.setState({ value: value }); }

    byPass = () => {
        this.setState ({ byPassing: true })
        let data = {
            recordId: this.state.goodsDetailsId, 
            name: "",
            formCategory: "FORM_M",
            type: "FORM_GOODS",
            description: "Request for Exempted Good bypass",
            initiatorComments: this.state.comment,
            parentId: this.state.formId          
        }
        this.props.callbackForBypass(data);   
    }

    handleInputChange = (event) => { this.setState({comment: event.target.value}); }

    setSelectedUser = (selected) => this.setState({ approvers: selected })    

    clearSelectedUser = () => this.setState({ approvers: "" })

    render () {
        return (
            <span> 
                <Form size='small'>
                    <Form.Group inline>
                        <Form.Field>
                            <Radio name='exemptedReview' value='bypass' checked={this.state.value === 'bypass'} onChange={this.handleChange} label = "Bypass" />
                        </Form.Field>
                        <Form.Field>
                            <Radio name='exemptedReview' value='correspondence' checked={this.state.value === 'correspondence'} onChange={this.handleChange} label="Generate Correspondence"/>       
                        </Form.Field>  
                        <p>&nbsp;</p>                        
                    </Form.Group>  
                    {this.state.value === "bypass" &&                            
                        <Form.Group className="by-pass-details">
                            <Form.Field width={5} control={Input} label='Comment' onChange = {this.handleInputChange} placeholder='Comment' /> 
                            <Form.Field>
                                <label>&nbsp;</label> 
                                <Button type="submit" onClick = {this.byPass} basic size="small" color="black" loading={this.state.byPassing} content="Bypass"/>
                            </Form.Field>                                
                        </Form.Group>
                    }                       
                </Form>  
                {this.state.value === "correspondence"  && 
                    <div className="exempted-correspondence-wrap" >
                        <GenerateCorrespondence templates={this.state.templates} 
                            formId= {this.state.formId}
                            successfulCorrespondenceUpload={()=>this.props.successfulCorrespondenceUpload()} 
                            createCorrespondence = {(params)=>this.props.createCorrespondence(params)}
                        /> 
                    </div> 
                }
            </span>                 
        )
    }
}

export default ExemptedGoodsReview