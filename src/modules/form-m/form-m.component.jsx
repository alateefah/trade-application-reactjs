import React, { Component } from 'react'

//semantic ui
import { Header, Grid, Segment } from 'semantic-ui-react'

//custom
import AppWrapper from './../shared/main/app-wrapper.component'
import FormMList from './form-m-list.component'

class FormM extends Component {
    render () {
        return (            
            <AppWrapper> 
                <Header as='h3'>Form M Management</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment style={{ paddingBottom: 60 }}>
                                <FormMList />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default FormM