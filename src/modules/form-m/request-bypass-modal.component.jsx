import React, { Component } from 'react'
import { Form,  Icon, Button, Modal, Header, Loader } from 'semantic-ui-react'
import DatePicker from 'react-datepicker';
import moment from 'moment';

class RequestBypass extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            document: props.document,
            comment: "",
            modalOpen: false,
            refreshErrorMessage: null,
            dueDate: moment().add(1, 'days')
        }
    }

    handleClose = () => this.setState({ modalOpen: false })

    handleCommentChange = (event) => { this.setState ({ comment: event.target.value}) }

    requestBypass = () => {
        this.setState ({ byPassing: true })
        let data = {
            recordId: this.state.document.recordId,
            name: "",
            formCategory: "FORM_M",
            type: this.state.document.type,
            description: "Re-Request for form M document bypass",
            initiatorComments: this.state.comment,
            parentId: this.state.document.form,
            extraRecords: [
                {
                    documentType: this.state.document.documentType,
                    formInfoId: this.state.document.form,
                    uploadDueDate: this.state.dueDate.toISOString()
                }
            ]
        }
        this.props.callbackForBypass(data);   
    }

    handleDueDate = (date) => { this.setState ({ dueDate: date });  }

    render () {

        return (
            <Modal trigger={this.props.trigger} onClose={this.handleClose} basic size='tiny' closeIcon="cancel">

                {this.state.loadingMessage && <Loader />}
                
                {!this.state.loadingMessage && <Header icon='browser' content='Request Document Bypass' /> } 

                {!this.state.loadingMessage &&
                    <Modal.Content>                       
                        <Form className="request-bypass">
                            <Form.Field>
                                <label>Due Date</label>  
                                <DatePicker className = 'transparent-date' placeholderText='Due Date' dateFormat="YYYY-MM-DD" selected = {this.state.dueDate} onChange = {this.handleDueDate} />
                            </Form.Field>
                            <Form.Field>
                                <label> Comment </label>
                                <Form.Input placeholder='Comment' width={16} onChange = {this.handleCommentChange} className = 'transparent-input'/>
                            </Form.Field>                            
                        </Form>                    
                    </Modal.Content>
                }

                {!this.state.loadingMessage && this.state.dueDate !== null &&
                    <Modal.Actions>
                        <Button color='green' onClick={this.requestBypass} inverted><Icon name='checkmark' /> Request Bypass </Button>
                    </Modal.Actions>  
                }  
            </Modal>                
        )
    }
}

export default RequestBypass