import React, { Component } from 'react'
import { Form, Icon, Button, Segment, Popup, Search } from 'semantic-ui-react'
import FormMService from './../../services/form_m_service'
import BankService from './../../services/bank_service'
import AuthenticationService from './../../services/authentication_service'
import DatePicker from 'react-datepicker'

class FormMSearch extends Component {

    constructor (props) {
        super(props);

        this.state = {
            showMoreFilters: false,
            formData: {
                formType: "",
                applicationNumber: "",
                formNumber: "",
                applicantTin: "",
                beneficiaryName: "",
                status: "",
                bankStatus: "",
                branchCode: "",
                processingStage: ""
            },
            loading: true,
            isSearchingBranch: false,
            branchSearchValue: "",
            branches: [],
            validationDate: null
        }        
        
    }

    componentWillMount () { this.getStatus(); }

    getStatus = () => {
        FormMService.getStatus()
        .then(response => {  
            this.setState({ status: response, loading: false})        
        }) 

    }

    toggleFilter = () => {
        this.setState({ showMoreFilters : !this.state.showMoreFilters })
    }

    handleInputChange = (e, {name, value}) => {           
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    clear = () => {
        this.setState ({             
            formData: {
                formType: "IM",
                applicationNumber: "",
                formNumber: "",
                applicantTin: "",
                beneficiaryName: "",
                status: "",
                bankStatus: "",
                branchCode: "",
                processingStage:""
            },
            validationDate: ""
        }, () => {
            this.props.clearSearch();
        });
    }

    handleSelectChange = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    } 

    validSearch = () => {
        if (this.validFields()) {
            return false;
        }
        return true;
    }

    validFields = () => {
        return ((this.state.formData.applicationNumber === null || this.state.formData.applicationNumber === "")
            && (this.state.formData.formNumber === null || this.state.formData.formNumber === "")
            && (this.state.formData.applicantTin === null || this.state.formData.applicantTin === "")
            && (this.state.formData.beneficiaryName === null || this.state.formData.beneficiaryName === "")
            && (this.state.formData.status === null || this.state.formData.status === "")
            && (this.state.formData.bankStatus === null || this.state.formData.bankStatus === "")
            && (this.state.formData.branchCode === null || this.state.formData.branchCode === "")
            && (this.state.validationDate === null || this.state.validationDate === "")
            && (this.state.formData.processingStage === null || this.state.formData.processingStage === "")
        )
    }

    search = () => {      
        if (this.validSearch()) {
            let params = this.state.formData;
            params.authorizationDate = this.state.validationDate
            this.props.searchParams(params)
        }     
    }

    handleBranchSearch = (e, {value}) => {
        this.setState({ isSearchingBranch: true, branchSearchValue : value })

        if (value.length > 2) {
            this.filterBranch(value);
        }
        
    }

    filterBranch = (value) => {
        let params = {
            pageSize: 5,
            pageNumber: 1,
            searchText: value
        }
        BankService.filterBranches(params)
        .then(response => { 
            if (response.code) {
                return;
            } else {
               this.setState({ branches : response.list, isSearchingBranch: false, })                
            }  
        }) 
    }

    resultRenderer = (params) => {
        return (
            <div key={params.id}>            
                <div className="main">
                    <div className="title">{params.name}</div>
                    {params.address && <div className="desc">{params.address}</div>}
                </div>
            </div>
        )
        
    }

    handleResultSelect = (e, { result }) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                branchCode: result.id
            },
            branchSearchValue : result.name
        })
    }

    handleDateChange = (date) => { this.setState ({ validationDate: date }); }

    render () {
        const { status } = this.state
        let cbnStatus, bankStatus, processingStage;
        
        if (status) 
        {
            cbnStatus = status.cbnStatus && status.cbnStatus.map(bankS => (
                { key: bankS.status, text: bankS.status, value: bankS.status }            
            ));
            bankStatus = status.bankStatus && status.bankStatus.map(cbnS => (
                { key: cbnS.status, text: cbnS.bankStatus, value: cbnS.status }
            )); 
            processingStage = status.processingStage && status.processingStage.map((stage, index) => (
                { key: index, text: stage.status, value: stage.status }
            ));            
        }

        

        return (      
           <span>                
                <Segment padded='very' className='search-wrap'>
                    <Form>
                        <Form.Group>
                            <Form.Input label='Form Number' placeholder='Form Number' width={3} 
                                name = 'formNumber'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.formNumber}
                                id = 'search-form-m-formNumber'
                            />
                            <Form.Input label='Application Number' placeholder='Application Number' width={3} 
                                name = 'applicationNumber'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.applicationNumber}
                                id = 'search-form-m-applicationNumber'
                            />
                            <Form.Input label='Applicant TIN' placeholder='Applicant TIN' width={3}
                                name = 'applicantTin'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.applicantTin}
                                id = 'search-form-m-applicantTin'    
                            />                        
                            <Form.Input label='Beneficiary Name' placeholder='Beneficiary Name' width={4} 
                                name = 'beneficiaryName'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.beneficiaryName}
                                id = 'search-form-m-beneficiaryName'
                            />
                            {!this.state.showMoreFilters &&
                            <Form.Field>
                                <label>&nbsp;</label>
                                <Button onClick={this.search} color="red" content="Search" disabled={this.validFields()} compact/>
                                {this.props.searched && <Button color="grey" onClick={this.clear} content="Clear" compact/> }
                                <span onClick={this.toggleFilter}>
                                    <Popup content='Show Advanced Search' inverted size='tiny' trigger={<Icon name='sliders' size='big' className='tc-teal' link />} />                                    
                                </span>
                            </Form.Field>
                            }
                        </Form.Group>
                        {this.state.showMoreFilters &&
                        <span>                        
                            <Form.Group>
                                <Form.Select label='CBN Status' options={cbnStatus} width={3} placeholder='Select'
                                    name = "status"
                                    onChange = {this.handleSelectChange}
                                    value = {this.state.formData.status}
                                    id = "search-form-m-status"
                                />
                                <Form.Select label='Bank Status' options={bankStatus} width={3} placeholder='Select'
                                    name = "bankStatus"
                                    onChange = {this.handleSelectChange}
                                    value = {this.state.formData.bankStatus}
                                    id = "search-form-m-bank-status"
                                />
                                <Form.Field width={3}> 
                                    <label>Validation Date</label>               
                                    <DatePicker                                
                                        placeholderText='Validation Date'
                                        dateFormat="YYYY-MM-DD"
                                        selected = {this.state.validationDate}
                                        onChange = {this.handleDateChange}     
                                        id = "correspondence-search-validationDate"                               
                                    />
                                </Form.Field>     
                                <Form.Select label='Processing Stage' options={processingStage} width={4} placeholder='Select'
                                    name = "processingStage"
                                    onChange = {this.handleSelectChange}
                                    value = {this.state.formData.processingStage}
                                    id = "search-form-m-bank-processingStage"
                                />
                            </Form.Group>
                            <Form.Group>                            
                                {AuthenticationService.hasPermission('READ_ALL_BRANCHES') &&
                                <Form.Field width={4}>
                                    <label> Bank Branch </label>
                                    <Search fluid type='text'
                                        minCharacters = {3}
                                        placeholder = "Search branch.. (min 3xters)"
                                        loading = {this.state.isSearchingBranch} 
                                        onSearchChange = {this.handleBranchSearch}
                                        value = {this.state.branchSearchValue}
                                        results = {this.state.branches}
                                        resultRenderer = {this.resultRenderer}
                                        onResultSelect={this.handleResultSelect} />
                                </Form.Field>
                                }
                                <Form.Field>
                                    <label>&nbsp;</label>
                                    <Button color="red" onClick={this.search} content="Search" disabled={this.validFields()} compact/>
                                    {this.props.searched && <Button color="grey" onClick={this.clear} content="Clear" compact/> }
                                    <span onClick={this.toggleFilter}>                                
                                        <Popup content='Hide Advanced Search' inverted size='tiny' trigger={<Icon name='sliders' size='big' className='tc-teal' link />} />                                    
                                    </span>
                                </Form.Field>
                            </Form.Group>
                        </span>
                        }
                    </Form>
                </Segment>
           </span>
                                      
        )
    }
    
}


export default FormMSearch