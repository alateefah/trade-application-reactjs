import React, { Component } from 'react'
import { Sidebar, Menu, Icon } from 'semantic-ui-react'
import AuthenticationService from './../../services/authentication_service'
import InitiateAmend from './amend/initate-amend.component';
import InitiateCancellation from './cancel/initate-cancellation.component';
import InitiateReplacement from './replace/initate-replacement.component';

let _ = require("lodash");

class SidebarLeftOverlay extends Component {

    constructor (props) {
        super(props);
        this.state = {
            form: props.form,
            visible : false,
            ammendablesTemplate: _.filter(props.form.availableTemplates, function(o) { return o.purpose === "AMMENDMENT_APPROVAL" }),
            cancellationTemplate: _.filter(props.form.availableTemplates, function(o) { return o.purpose === "CANCELLATION_APPROVAL" })
        }
    }

    toggleVisibility = () => this.setState({ visible: !this.state.visible })

    render () {

        const { visible, form, ammendablesTemplate, cancellationTemplate } = this.state

        return (
            <div>
                <div className={"ut-sidebar-leaf " + (this.state.visible ? 'open' : '')} onClick={this.toggleVisibility}>
                    <Icon name={this.state.visible ? "cancel":"align justify"} />
                </div>
                <Sidebar as={Menu} animation='overlay' width='thin' visible={visible} icon='labeled' vertical inverted className="ut-sidebar">
                    
                    {form.status.toUpperCase() === "REGISTERED" 
                        && AuthenticationService.hasPermission("CREATE_AMENDMENT_REQUEST") 
                        && <InitiateAmend form = {form.id} templates = {ammendablesTemplate}
                            callbackForSuccesfulAmendRequest={()=>this.props.callbackFromSidebar("amend", 6)}
                                trigger={<Menu.Item name='edit'><Icon name='edit' /> Request to Amend </Menu.Item> } />
                    } 

                    <InitiateCancellation form = {form.id} templates = {cancellationTemplate}
                        callbackForSuccessfulCancellationRequest={()=>this.props.callbackFromSidebar("cancellation")}
                            trigger={<Menu.Item name='window close outline'> <Icon name='window close outline' /> Cancellation </Menu.Item> } 
                    />
                    

                    <InitiateReplacement form = {form.id} templates = {ammendablesTemplate}
                        callbackForSuccessfulReplacementRequest={()=>this.props.callbackFromSidebar("replacement")}
                            trigger={ <Menu.Item name='clone'><Icon name='clone' /> Replacement </Menu.Item> } 
                    />
                    
                </Sidebar>
            </div>
        )
  }
}

export default SidebarLeftOverlay
