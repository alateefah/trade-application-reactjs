import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import { Form, Radio, Input, Button } from 'semantic-ui-react'
import UploadAttachment from './../shared/upload-attachment.component';

class MissingDocumentView extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            document: props.document,
            loading: false,
            value: null,
            fileName: "Choose a file",
            isFileUploaded: false,
            uploadedFile: null,
            dueDate: moment().add(1, 'days'),
            errorMessage: null,
            comment: null,
            byPassing: props.loadingUploadButton
        }
    }

    handleChange = (e, { value }) => { this.setState({ value: value }); }

    byPass = () => {
        this.setState ({ byPassing: true })
        let data = {
            name: "",
            formCategory: "FORM_M",
            type: "FORM_M_DOCUMENT_ATTACHMENTS",
            description: "Request for form M document bypass",
            initiatorComments: this.state.comment,
            parentId: this.state.document.form,
            extraRecords: [
                {
                    documentType: this.state.document.document,
                    formInfoId: this.state.document.form,
                    uploadDueDate: this.state.dueDate.toISOString()
                }
            ]
        }
        this.props.callbackForBypass(data);   
    }

    upload = (data) => { this.props.callbackForUpload(data); }
       
    handleInputChange = (event) => { this.setState({comment: event.target.value}); }

    handleDueDate = (date) => { this.setState ({ dueDate: date });  }

    render () {
        return (
            <span> 
                <Form size='small'>
                    <Form.Group inline>
                        <Form.Field>
                            <Radio name='missingDocument' value='upload' checked={this.state.value === 'upload'} onChange={this.handleChange} label = "Upload File" />
                        </Form.Field>
                        <Form.Field>
                            <Radio name='missingDocument' value='bypass' checked={this.state.value === 'bypass'} onChange={this.handleChange} label="Request to bypass"/>       
                        </Form.Field>  
                        <p>&nbsp;</p>                        
                    </Form.Group>  
                    {this.state.value === "upload" && 
                        <UploadAttachment document={this.state.document} callbackForUpload= {this.upload} /> 
                    }                       
                    {this.state.value === "bypass" &&                            
                        <Form.Group className="by-pass-details">
                            <Form.Field>
                                <label>Due Date</label>  
                                <DatePicker placeholderText='Due Date' dateFormat="YYYY-MM-DD" selected = {this.state.dueDate} onChange = {this.handleDueDate} />
                            </Form.Field>
                            <Form.Field width={5} control={Input} label='Comment' onChange = {this.handleInputChange} placeholder='Comment' />
                                {
                                    this.state.dueDate !== null &&
                                    <Form.Field>
                                        <label>&nbsp;</label> 
                                        <Button type="submit" onClick = {this.byPass} basic size="small" color="black" loading={this.state.byPassing} content="Bypass"/>
                                    </Form.Field>
                                }
                        </Form.Group>
                    }                        
                </Form>  
            </span>                 
        )
    }
}

export default MissingDocumentView