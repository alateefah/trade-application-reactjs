import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import { Table, Loader, Pagination } from 'semantic-ui-react'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import NoSearchResult from './../shared/no-search-result.component'
import ErrorMessage from './../shared/message/error.component'
import AuthenticationService from './../../services/authentication_service'
import FormMSearch from './form-m-search.component'
import FormMService from './../../services/form_m_service'


class FormMList extends Component {

    constructor (props) {
        super(props);

        this.state = {
            formType: "IM",
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            formMs: [],
            errorMessage: null,
            refreshErrorMessage: null,
            searched: false,
            searching: false,            
            formData: {
                formNumber: "",
                applicationNumber: "",                
                applicantTin: "",
                beneficiaryName: "",
                status: "",
                bankStatus: "",
                authorizationDate: "",
                processingStage: "",
                bankBranch: ""
            }
        }
    }

    componentWillMount () { 
        this.setState({ loadingMessage : "Loading Form M's" })
        this.getFormMs();
    }

    clearMessages = () => { this.setState ({ refreshErrorMessage: false, searched: false }) }

    getFormMs = () => {  
        this.clearMessages();          
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber
        }
        FormMService.getAll(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    formMs : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber 
                })
            }         
            
        }) 
    }

    handlePageChange = (e, { activePage }) => {         
        this.setState({ pageNumber : activePage }, () => {              
            if (this.state.searched) {
                this.setState({ searchingMessage : "Searching..." })
                this.searchFormM()
            } else {
                this.setState({ loadingMessage : "Loading..." })
                this.getFormMs()
            }
        });          
    }

    refreshAction = () => {
        this.setState({ loadingMessage : "Refreshing..." })
        this.getFormMs();
    }
    
    search = (data) => {
        this.setState ({
            formData: {
                formNumber: data.formNumber,
                applicationNumber: data.applicationNumber,
                applicantTin: data.applicantTin,
                beneficiaryName: data.beneficiaryName,
                status: data.status,
                bankStatus: data.bankStatus,
                processingStage: data.processingStage,
                authorizationDate: data.authorizationDate,
                bankBranch: data.bankBranch
            }            
        }, () => {
            this.searchFormM();
        })
    }

    searchFormM = () => {        
        this.setState({ searching : true })
        let params = this.state.formData;
        params.formType = this.state.formType;
        params.pageNumber = this.state.pageNumber;
        params.pageSize = this.state.pageSize;

        FormMService.search(params)
        .then(response => {   
            this.setState({ searching : false })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    formMs : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber, 
                    searched: true 
                })
            }         
            
        }) 
    }    

    reloadTable = () => {
        this.setState({ loadingMessage : this.state.loadingMessage })  
        this.getFormMs();
    }
    
    render () {
        let rows = this.state.formMs.length <= 0 ?
            <NoSearchResult colSpan={7} /> :
            this.state.formMs.map(formM => {
                return  <Table.Row key={formM.id}>
                            <Table.Cell>{formM.formNumber}</Table.Cell>                            
                            <Table.Cell className='no-left-border'>{formM.applicationNumber}</Table.Cell>
                            <Table.Cell>{formM.applicantTin}</Table.Cell>
                            <Table.Cell>{formM.authorizationDate}</Table.Cell>
                            <Table.Cell>{formM.expiryDate}</Table.Cell>
                            <Table.Cell>{formM.status}</Table.Cell>
                            {AuthenticationService.hasPermission('GET_FORM_M_DETAILS') 
                                && <Table.Cell textAlign='center'><Link to={'/form-m/'+formM.id} form={formM}>view</Link></Table.Cell>
                            }   
                        </Table.Row>
        })

        return (      
            this.state.loadingMessage ?  
                <div className="ut-loader">
                    <Loader active content={this.state.loadingMessage}/> 
                </div>:  
                this.state.refreshErrorMessage ?  
                <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
                <div>
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }                       

                    {AuthenticationService.hasPermission('READ_ALL_FORM_MS') &&
                    <div>
                        {AuthenticationService.hasPermission('SEARCH_FORM_M_INFO') &&<FormMSearch searchParams = {this.search} searched = {this.state.searched} clearSearch= {this.reloadTable} />}
                        {this.state.searching && <Loader style={{top: 85+'%'}} active content={this.state.searchingMessage}/> }
                        {!this.state.searching &&
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='8'>
                                        Showing {this.state.pageNum === 1 ? this.state.pageNumber : this.state.pageNumber*this.state.pageSize - this.state.pageSize+1} - {this.state.pageNumber*this.state.pageSize > this.state.noOfRecords ? this.state.noOfRecords : this.state.pageNumber*this.state.pageSize}  of {this.state.noOfRecords} records 
                                    </Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.HeaderCell>Form Number</Table.HeaderCell>
                                    <Table.HeaderCell>Application Number </Table.HeaderCell>
                                    <Table.HeaderCell>Applicant TIN </Table.HeaderCell>
                                    <Table.HeaderCell>Created Date</Table.HeaderCell>
                                    <Table.HeaderCell>Expiry Date</Table.HeaderCell>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                    {
                                        AuthenticationService.hasPermission('GET_FORM_M_DETAILS') &&
                                        <Table.HeaderCell textAlign='center'> Details </Table.HeaderCell>
                                    }
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{rows}</Table.Body>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='8' textAlign="right">
                                        <Pagination pointing secondary 
                                            activePage={this.state.pageNumber} onPageChange={this.handlePageChange} 
                                            totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                        />  
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>  
                        }        
                    </div>
                    }
                </div>
                                      
        )
    }
    
}


export default FormMList