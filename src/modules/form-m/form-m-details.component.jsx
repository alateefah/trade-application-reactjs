import React, { Component } from 'react'
import { Header, Grid, Segment, Breadcrumb, Loader, Label, Table, Tab, Icon, Menu, Popup, List, Message } from 'semantic-ui-react'
import DocumentDisplayModal from './../shared/document-display-modal.component'
import AppWrapper from './../shared/main/app-wrapper.component'
import Refresh from './../shared/refresh.component'
import UploadAttachment from './../shared/upload-attachment.component'
import MissingDocumentView from './missing-document.component'
import AdditionalInformation from './additional-information.component'
import SuccessMessage from './../shared/message/success.component'
import ErrorMessage from './../shared/message/error.component'
import ReviewRequest from './../shared/review-request-modal.component'
import ApprovalsModal from './../shared/approvals-modal.component'
import FormMService from './../../services/form_m_service'
import SharedService from './../../services/shared_service'
import CorrepondenceService from './../../services/correspondence_service'
import AuthenticationService from './../../services/authentication_service'
import UtilService from './../../services/util_service'
import FormCorrespondence from './form-m-correspondence.component'
import ExemptedGoodsReview from './exempted-good-review'
import GenerateLC from './generate-lc.component'
import RequestBypass from './request-bypass-modal.component'
import Sidebar from './sidebar.component'
import ReviewAmend from  './amend/review-amend.component'
import ReviewCancellation from  './cancel/review-cancellation.component';

let _ = require('lodash');

class FormMDetails extends Component {
    
    constructor (props) {
        super(props);
        
        this.state = {
            formM: props.match.params.formID,
            loadingMessage: null,
            formDetails: [],
            activeIndex: 0,
            errorMessage: null,
            successMessage: null,
            refreshErrorMessage: null,
            documentRejected: false,
            loadingBypassButton:false,
            reviewAgain: false,
            amendmentDetails: null,
            pendingAmendmentId: ""
        }
    }

    componentWillMount () { this.getFormMDetails();}

    getFormMDetails = (index) => {
        this.setState ({loadingMessage: "Loading...", formDetails: null, refreshErrorMessage: null });
        let params = {
            formId: this.state.formM
        }
        FormMService.getForm(params)        
        .then(response => {   
            if (response.code) {
                this.setState({ refreshErrorMessage : response.description }, () => { 
                    this.setState({ loadingMessage : null })
                })
            } else {
                this.setState({ formDetails : response }, () => { 
                    this.setState({ loadingMessage : null})
                })
                if (response.amendments) {
                    let amendments = response.amendments;
                    this.setState({ amendmentDetails: amendments });
                    let pendingAmendment = amendments.find(amendment => amendment.status  === "PENDING" 
                                        || amendment.status  === "PENDING_REJECTION"
                                        || amendment.status  === "PENDING_APPROVAL");
                    if (pendingAmendment) {
                        this.setState({ pendingAmendmentId: pendingAmendment.id})
                    }
                    if (index) {
                        this.setState({ activeIndex: index }, () => {
                        })
                    }
                }
            }               
        }) 
    }

    deleteAttachment = (id) => {
        this.setState({ loadingMessage : "Deleting..." })
        SharedService.deleteAttachment(id)
        .then(response => { 
            this.setState({ loadingMessage : null, activeIndex: 2 }) 
            if (response.code) {  
                this.setState ({ errorMessage: response.description })            
                setTimeout(() => { this.setState ({ errorMessage: null }) }, 5000)                
            } else {       
                this.getFormMDetails();            
                this.setState ({ successMessage: "Document deleted successfully" })
                setTimeout(() => {
                    this.setState ({ successMessage: null })
                }, 5000)
            }
        })
    }

    handleTabChange = (e, { activeIndex }) => this.setState({ activeIndex });
        
    reviewByPass = (data, successIndex, failureIndex) => {
        this.setState({ loadingMessage : "Processing..." })        
        SharedService.authorizeApproval(data)
        .then(response => {  
            if (response.code) {           
                this.setState({ loadingMessage : null })
                this.setState ({ activeIndex: failureIndex, errorMessage: response.description })
                setTimeout (function() { this.setState({ errorMessage: null}); }.bind(this), 4000);
            } else {
                this.getFormMDetails();
                this.setState ({ activeIndex: successIndex, successMessage: "Successful" })
                setTimeout (function() { this.setState({successMessage: null}); }.bind(this), 4000);
            }
        })
        
    }

    uploadAction = (response) => {
        if (response.code) {
            this.setState({ errorMessage: response.description })
            setTimeout(() => this.setState({ errorMessage: null }), 4000); 
        } else {
            this.setState({ loading : true })
            this.getFormMDetails();
            this.setState({ activeIndex: 2, successMessage: "Successfully processed request"});
            setTimeout(() => this.setState({ successMessage: null }), 4000);
        }
    }

    bypassAction = (data, activeIndex) => {  
        SharedService.requestApproval(data)
        .then(response => {  
            this.setState({ loading : false })
            if (response.code) {
                this.alertError(response.description, activeIndex === undefined ? 2 : activeIndex);                        
                setTimeout(() => this.setState({ errorMessage: null }), 4000);
            } else {
                this.alertSuccess("Successfully processed request", activeIndex === undefined ? 2 : activeIndex);
            }
        })
    }    

    createCorrespondence = (params, failureIndex) => {
        this.setState({ loadingMessage : "Creating correspondence..." })
        CorrepondenceService.createCorrespondence(params)
            .then(response => {
                this.setState({ loadingMessage : null })
                if (response.code) {
                    this.setState({ activeIndex: failureIndex ? failureIndex:1, errorMessage: response.description});
                    setTimeout(() => this.setState({ errorMessage: null }), 4000);
                } else {
                    this.alertSuccess("Successfully processed request", 5);
                }
            })
    }

    alertSuccess = (msg, tabIndex) => {
        this.setState({ successMessage: msg});
        this.getFormMDetails();
        this.setState({activeIndex: tabIndex });
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }
    
    alertError = (msg, tabIndex) => {
        this.setState({ errorMessage: msg, activeIndex: tabIndex });
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    reviewAgain = () => { this.setState ({ reviewAgain: !this.state.reviewAgain}) }

    saveAdditionalInfo = (params) => {
        FormMService.updateApplicant(params)
            .then(response => {   
                if (response.code) {
                    this.setState ({ errorMessage: response.description })             
                } else {   
                    this.getFormMDetails();
                    this.setState ({ successMessage: "Details successfully updated" })
                    setTimeout(() => { this.setState ({ successMessage: null }) }, 5000)
                }
            })
    }

    generateLC = (data) => {
        FormMService.generateLC(data)
            .then(response => {   
                if (response.code) {
                    this.setState ({ activeIndex: 6, errorMessage: response.description })       
                    setTimeout(() => { this.setState ({ errorMessage: null }) }, 5000)      
                } else {   
                    this.getFormMDetails();
                    this.setState ({ activeIndex: 0, successMessage: "LC successfully generated"})
                    setTimeout(() => { this.setState ({ successMessage: null }) }, 5000)
                }
            })
    }

    callbackFromSidebar = (action, index) => {
        let msg;

        if (action) {
            msg = action.charAt(0).toUpperCase() + action.slice(1)+" successfully initiated"
        } else {
            msg = "Successful"
        }

        this.alertSuccess(msg, 0);
        this.getFormMDetails(index);
    }

    

    render () {   
        const { formDetails } = this.state;
        let goods, attachments, missingDocuments, supportInfos, panes;

        let closed, pendingApproval, inProcess, replaced, completed;
        let cancellation, cancellationInProcess;
        let amendment;
        let replacement, replacementPendingApproval;


        if (formDetails !== null) { 
            //bank Status
            closed = formDetails.bankStatus === "CLOSED"
            replaced = formDetails.bankStatus === "REPLACED"
            pendingApproval = formDetails.bankStatus === "PENDING_APPROVAL"
            inProcess = formDetails.bankStatus === "IN_PROCESS";
            // approved = formDetails.bankStatus === "APPROVED";      
            completed = formDetails.bankStatus === "COMPLETED";     

            //stages
            cancellation = formDetails.processingStage === "CANCELLATION";
            // cancellationPendingApproval = cancellation && pendingApproval;
            cancellationInProcess = cancellation && inProcess;

            amendment = formDetails.processingStage === "AMEND";

            replacement = formDetails.processingStage === "FORM_REPLACEMENT";
            replacementPendingApproval = replacement && pendingApproval;

            goods = formDetails.formGoodsDetails && formDetails.formGoodsDetails.formGoods &&
                formDetails.formGoodsDetails.formGoods.map(good => {                    
                    return  (good !== "") &&
                        <Table.Row key = {good.id}> 
                                <Table.Cell>
                                    {good.goodStatus === 'PROHIBITED' && 
                                        <Popup trigger={<Icon name='exclamation triangle' color='red'/>} content='Good is prohibited.' on='hover' inverted size='mini' position='left center' /> }
                                    {good.goodStatus === 'EXEMPTED' && 
                                        <Popup trigger={<Icon name='exclamation triangle' color='orange'/>} content='Good is Exempted.' on='hover' inverted size='mini' position='left center' /> }
                                    {good.goodStatus === 'VALID' && 
                                        <Popup trigger={<Icon name='checkmark' color='green'/>} content='Good is Valid.' on='hover' inverted size='mini' position='left center' /> }
                                </Table.Cell>
                                <Table.Cell> {good.hsCode} </Table.Cell>                            
                                <Table.Cell> {good.countryOfOriginName} </Table.Cell>
                                <Table.Cell> {good.description} </Table.Cell>
                                <Table.Cell> {good.fobValue} </Table.Cell>
                                <Table.Cell> {good.freightCharges} </Table.Cell>
                                <Table.Cell> {good.numberOfPackages} </Table.Cell>
                                <Table.Cell> {good.packageTypeName} </Table.Cell>
                                <Table.Cell> {good.quantity} </Table.Cell>
                                <Table.Cell> {good.sectorialPurposeName} </Table.Cell>
                                <Table.Cell> {good.stateOfGoodsName} </Table.Cell>
                                <Table.Cell> {good.unitPrice} </Table.Cell>
                                <Table.Cell> {good.unitName} </Table.Cell>                 
                        </Table.Row>
            })
        
            if (formDetails.attachments !== null) {
                attachments = formDetails.attachments && formDetails.attachments.formAttachment &&
                    formDetails.attachments.formAttachment.map(attachment => {
                        return  <Table.Row key = {attachment.id}>                            
                                    <Table.Cell> 
                                        { AuthenticationService.hasPermission('DELETE_FORM_M_ATTATCHMENTS') 
                                            && attachment.attachmentStatus.trim() === 'DOCUMENT_ATTACHED' 
                                            && !completed 
                                            && !amendment
                                            && !replacement                                            
                                            && <Icon name="trash" color="red" link onClick={() => this.deleteAttachment(attachment.id)}/> 
                                        }
                                        &nbsp;&nbsp;&nbsp;
                                        {attachment.documentType} 
                                    </Table.Cell>                            
                                    <Table.Cell> 
                                        {(attachment.attachmentStatus.trim() === "CBN_PROVIDED" || attachment.attachmentStatus.trim() === "DOCUMENT_ATTACHED") 
                                        && <DocumentDisplayModal document={attachment} />}
                                        <UploadAttachment
                                            document={{document: attachment.documentType, index: attachment.id, form: attachment.formInfoId }} 
                                            callbackForUpload={this.uploadAction} />                                                                        
                                        
                                    </Table.Cell>
                                    <Table.Cell> {attachment.documentDate} </Table.Cell>
                                    <Table.Cell> {attachment.createdAt} </Table.Cell>
                                    <Table.Cell> 
                                        {(attachment.attachmentStatus.trim() === "BYPASS_APPROVED" || attachment.attachmentStatus.trim() === "BYPASS_REJECTED"
                                            || attachment.attachmentStatus.trim() === "BYPASS_PENDING_APPROVAL") &&
                                            <ApprovalsModal 
                                                info = {{ status: "", recordId: attachment.id, type: "FORM_M_DOCUMENT_ATTACHMENTS", parentId: formDetails.id }} 
                                                trigger={<List.Header as='a' className='ut-pointer'>{attachment.attachmentStatus.trim()}</List.Header>}
                                            />
                                        }
                                        {AuthenticationService.hasPermission('AUTHORIZE_APPROVAL_REQUESTS') && attachment.attachmentStatus.trim() === 'BYPASS_PENDING_APPROVAL' && 
                                            <ReviewRequest
                                                trigger = {<Label as='a' style={{float: 'right'}} content="Review Bypass Request"/>}
                                                info = {{ recordId: attachment.id, type: "FORM_M_DOCUMENT_ATTACHMENTS", parentId: formDetails.id, status: "PENDING" }} 
                                                callbackForReview={(data)=>this.reviewByPass(data, 2, 2)}
                                            />                                            
                                        }
                                        {attachment.attachmentStatus.trim() === "BYPASS_REJECTED" && 
                                            <RequestBypass
                                                trigger={
                                                    <div className="ui icon refresh" data-variation="small" data-inverted="" data-position="top center" data-tooltip="Request bypass again" style={{float: 'right'}}>
                                                        <i className="refresh icon"></i>
                                                    </div>
                                                }
                                                document={{documentType: attachment.documentType, recordId: attachment.id, type: "FORM_M_DOCUMENT_ATTACHMENTS", form: formDetails.id }}
                                                callbackForBypass = {this.bypassAction}
                                            />
                                        }
                                    </Table.Cell> 
                                </Table.Row>
                })
        
                missingDocuments = formDetails.attachments.missingAttachments &&
                                    formDetails.attachments.missingAttachments !== [] &&
                                    formDetails.attachments.missingAttachments.map((missingAttachment, i) => {
                        return  (missingAttachment !== "") && 
                                <Table.Row key = {i}>
                                    <Table.Cell> {missingAttachment} </Table.Cell>     
                                    <Table.Cell colSpan={5}>
                                        <MissingDocumentView 
                                            loadingUploadButton={this.state.loadingUploadButton}
                                            document={{document: missingAttachment, index: i, form: formDetails.id }}
                                            callbackForUpload = {this.uploadAction} 
                                            callbackForBypass = {this.bypassAction}
                                        />
                                    </Table.Cell>             
                                </Table.Row>
                })
            }
        
            supportInfos = this.state.formDetails.formSupportInformations &&
                this.state.formDetails.formSupportInformations.map(supportInfo => {
                    return  <Table.Row key = {supportInfo.id}>
                                <Table.Cell> {supportInfo.operation} </Table.Cell>   
                                <Table.Cell> {supportInfo.actor} </Table.Cell>
                                <Table.Cell> {supportInfo.date} </Table.Cell>  
                            </Table.Row>
            })        

            panes = 
            [
                { 
                    menuItem: <Menu.Item key='summary'>Summary Details &nbsp;&nbsp;</Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {formDetails.goodsStatus === "CBN_APPROVAL_REQUIRED" &&
                                <Message info icon>
                                    <Icon name='exclamation triangle' />
                                    <Message.Content>
                                        <Message.Header>Notice!</Message.Header>
                                        This form requires CBN approval. You are required to generate a correspondence with purpose <b>Approval for CB Valid Goods </b>
                                    </Message.Content>
                                </Message>
                            }
                            <Grid columns={2}>
                                <Grid.Column>
                                    <Segment>
                                        <Label as='a' color='grey' ribbon>Applicant</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Applicant RC Number <Header.Subheader>{formDetails.applicantRcNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Applicant TIN <Header.Subheader>{formDetails.applicantTin}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row> 
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Applicant Name <Header.Subheader>
                                                                {formDetails.formApplicant && formDetails.formApplicant.applicantName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                        <Header.Content>
                                                            Applicant Representative <Header.Subheader>{formDetails.applicantRepresentative}</Header.Subheader>
                                                        </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>                                                                        
                                                    <Table.Cell colSpan={2}>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Applicant Address 
                                                                <Header.Subheader>
                                                                    {formDetails.formApplicant && 
                                                                        formDetails.formApplicant.applicantAddress +", "+formDetails.formApplicant.applicantCityCode}
                                                                </Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                                    
                                            </Table.Body>
                                        </Table> 

                                        <Label as='a' color='grey' ribbon>Beneficiary</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Name <Header.Subheader>{formDetails.beneficiaryName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Email <Header.Subheader>{formDetails.beneficiaryEmail}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row> 
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Phone
                                                                <Header.Subheader>{formDetails.beneficiaryPhone}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Beneficiary Address
                                                                <Header.Subheader>{formDetails.beneficiaryAddress}{", "+formDetails.beneficiaryCountryCode}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                                 
                                            </Table.Body>
                                        </Table>

                                        <Label as='a' color='grey' ribbon>Authorization</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Authorization Date
                                                                <Header.Subheader>{formDetails.authorizationDate}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Authorization Representative
                                                                <Header.Subheader>{formDetails.authorizationReperesentative}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                                 
                                            </Table.Body>
                                        </Table>
                                    </Segment>
                                </Grid.Column>
                                <Grid.Column>                                                        
                                    <Segment>
                                        <Label as='a' color='grey' ribbon>Application</Label>                                                            
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Form Number
                                                            <Header.Subheader>{formDetails.formNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Form Type
                                                            <Header.Subheader>{formDetails.formType}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Applicant Representative
                                                            <Header.Subheader>{formDetails.applicantRepresentative}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Inspection Agent
                                                            <Header.Subheader>{formDetails.inspectionAgentName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Status
                                                            <Header.Subheader>{formDetails.status}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                </Table.Row>                                                                    
                                            </Table.Body>                                                                    
                                        </Table>

                                        <Label as='a' color='grey' ribbon>Bank</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Bank Branch
                                                            <Header.Subheader>{formDetails.bankBranch}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Bank Code
                                                                <Header.Subheader>{formDetails.bankCode}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                            Bank Numbering
                                                            <Header.Subheader>{formDetails.bankNumbering}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell></Table.Cell>
                                                </Table.Row>
                                            </Table.Body>
                                        </Table>

                                        <Label as='a' color='grey' ribbon>Others</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Created At
                                                                <Header.Subheader>{formDetails.createdAt}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content>
                                                                Expiry Date
                                                                <Header.Subheader>{formDetails.expiryDate}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                                    
                                            </Table.Body>
                                        </Table>                                                            
                                    </Segment>
                                </Grid.Column>
                            </Grid>
                        </Tab.Pane> 
                },
                { 
                    menuItem: 
                        !replaced &&
                        <Menu.Item key='goods'>Goods Details &nbsp;&nbsp;
                            {(
                                (formDetails.goodsDetailsStatus && formDetails.goodsDetailsStatus !== "VALID") || 
                                (formDetails.goodsStatus !== "BYPASS_APPROVED"                                 
                                    && !closed 
                                    && formDetails.goodsStatus !== "CBN_APPROVAL_REQUIRED" 
                                    && formDetails.goodsStatus !== "CORRESPONDENCE_APPROVED" 
                                    && formDetails.goodsStatus !== "VALID" && formDetails.goodsStatus !== "APPROVED"))
                                    && <Popup inverted size="tiny" content="Requires attention" trigger={<Icon circular inverted color='red' name='warning' size='tiny' />} />
                            }                            
                        </Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false}>
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> } 
                            <Grid>
                                <Grid.Column>
                                    <Segment>
                                        <Table basic='very' fixed>
                                            <Table.Body>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Valid for FX
                                                            <Header.Subheader>{_.capitalize(formDetails.forexStatus.toString())}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Currency
                                                            <Header.Subheader>{formDetails.formGoodsDetails.currencyName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Exchange Rate
                                                            <Header.Subheader>{formDetails.formGoodsDetails.exchangeRate}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Insurance Cost
                                                            <Header.Subheader>{formDetails.formGoodsDetails.insuranceCost}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Payment Mode
                                                            <Header.Subheader>{formDetails.formGoodsDetails.paymentModeName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Custom Office
                                                            <Header.Subheader>{formDetails.formGoodsDetails.customOfficeName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Discharge Port
                                                            <Header.Subheader>{formDetails.formGoodsDetails.dischargePortName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Source of Fund 
                                                            <Header.Subheader>{formDetails.formGoodsDetails.sourceOfFundsName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Terms of Delivery
                                                            <Header.Subheader>{formDetails.formGoodsDetails.termOfDeliveryCode}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Discharge Port
                                                            <Header.Subheader>{formDetails.formGoodsDetails.dischargePortName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>
                                                <Table.Row>                                                                        
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Mode of Transportation
                                                            <Header.Subheader>{formDetails.formGoodsDetails.modeOfTransportName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Proforma Inv Date
                                                            <Header.Subheader>{formDetails.formGoodsDetails.proformaInvDate}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell className={formDetails.formGoodsDetails.proformaStatus === 'DUPLICATE' ?'flag-error':''}>
                                                        <Header as='h4'>
                                                            <Header.Content> Proforma Inv Number
                                                            <Header.Subheader>{formDetails.formGoodsDetails.proformaInvNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Term of Delivery
                                                            <Header.Subheader>{formDetails.formGoodsDetails.termOfDeliveryCode}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Total of CF Value
                                                            <Header.Subheader>{formDetails.formGoodsDetails.totalCfValue}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>     
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Total FOB Value
                                                            <Header.Subheader>{formDetails.formGoodsDetails.totalFobValue}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Total Freight Charges
                                                            <Header.Subheader>{formDetails.formGoodsDetails.totalFreightCharges}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Total Items
                                                            <Header.Subheader>{formDetails.formGoodsDetails.totalItems}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Total Net Weight
                                                            <Header.Subheader>{formDetails.formGoodsDetails.totalNetWeight}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Transfer Mode
                                                            <Header.Subheader>{formDetails.formGoodsDetails.transferModeName}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>     
                                                <Table.Row>
                                                    <Table.Cell colSpan='5'>
                                                        <Header as='h4' >
                                                            <Header.Content> Description
                                                                <Header.Subheader>{formDetails.formGoodsDetails.description}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                </Table.Row>                                                          
                                            </Table.Body>                                                                    
                                        </Table>      
                                        
                                        <Header as="h3" content="Goods" />
                                        <Segment tertiary>
                                            <Table celled striped className="goods-table">
                                                <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell/>
                                                        <Table.HeaderCell>HS Code</Table.HeaderCell>
                                                        <Table.HeaderCell>Country</Table.HeaderCell>
                                                        <Table.HeaderCell>Description</Table.HeaderCell>
                                                        <Table.HeaderCell>FOB Value</Table.HeaderCell>
                                                        <Table.HeaderCell>Freight Charges</Table.HeaderCell>
                                                        <Table.HeaderCell>No. Of Packages</Table.HeaderCell>
                                                        <Table.HeaderCell>Package Type Code</Table.HeaderCell>
                                                        <Table.HeaderCell>Quantity</Table.HeaderCell>
                                                        <Table.HeaderCell>Sectorial Purpose Code</Table.HeaderCell>
                                                        <Table.HeaderCell>State Of Goods Code</Table.HeaderCell>
                                                        <Table.HeaderCell>Unit Price</Table.HeaderCell>
                                                        <Table.HeaderCell>Unit Code</Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Header>
                                                <Table.Body>{goods}</Table.Body>
                                            </Table>
                                        </Segment>
                                        <br/>
                                        {formDetails.formGoodsDetails.goodsStatus === "EXEMPTED" &&
                                            <span>
                                                <Header as="h3" content="Action for Exempted Goods" />
                                                <Segment tertiary>                                                
                                                    <ExemptedGoodsReview 
                                                        templates={_.filter(formDetails.availableTemplates, function(o) { return o.purpose === "EXEMPTED_GOODS_APPROVAL" })} 
                                                        formId= {this.state.formM}
                                                        goodsId = {formDetails.formGoodsDetails.id}
                                                        callbackForBypass = {(e)=>this.bypassAction(e, 1)}
                                                        successfulCorrespondenceUpload={()=>this.alertSuccess("Success", 5)} 
                                                        createCorrespondence = {this.createCorrespondence}
                                                    /> 
                                                </Segment>
                                            </span>
                                        }
                                        {AuthenticationService.hasPermission('AUTHORIZE_GOOD_REQUESTS_BYPASS') &&
                                        formDetails.formGoodsDetails.goodsStatus === "PENDING_APPROVAL" &&
                                            <span>
                                                <Header as="h3" content="Review Exempted Goods Bypass" />
                                                <Segment>
                                                    <ReviewRequest
                                                        trigger ={<Label as='a' content="Approve/Reject Exempted Goods Bypass Request"/>}
                                                        info = {{
                                                            recordId: formDetails.formGoodsDetails.id,
                                                            type: "FORM_GOODS",
                                                            parentId: formDetails.id,
                                                            status: "PENDING",
                                                            url: "goods"
                                                        }} 
                                                        callbackForReview={(data)=>this.reviewByPass(data, 1, 1)}
                                                    />
                                                </Segment>
                                            </span>
                                        }
                                        {formDetails.formGoodsDetails.goodsStatus === "REJECTED" &&
                                            <span>
                                                <Header as="h3" content="Exemption Details" />
                                                <ApprovalsModal 
                                                    info = {{ recordId: formDetails.formGoodsDetails.id, type: "FORM_GOODS", parentId: formDetails.id }} 
                                                    trigger={<List.Header as='a' className='ut-pointer'>EXEMPTED GOODS BYPASS REJECTED</List.Header>} />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <Popup inverted trigger={<Icon name='refresh' link onClick={this.reviewAgain}/>} position="left center" content='Review Again' />
                                                {this.state.reviewAgain && 
                                                    <Segment tertiary>
                                                        <ExemptedGoodsReview 
                                                            templates={_.filter(formDetails.availableTemplates, function(o) { return o.purpose === "EXEMPTED_GOODS_APPROVAL" })} 
                                                            formId= {this.state.formM}
                                                            goodsId = {formDetails.formGoodsDetails.id}
                                                            callbackForBypass = {(e)=>this.bypassAction(e, 1)}
                                                            successfulCorrespondenceUpload={()=>this.alertSuccess("Success", 5)} 
                                                            createCorrespondence = {(data)=>this.createCorrespondence(data, 1)}
                                                        /> 
                                                    </Segment>
                                                }
                                            </span>
                                        }
                                        {formDetails.formGoodsDetails.goodsStatus === "BYPASS_APPROVED" &&
                                            <span>
                                                <Header as="h3" content="Exemption Goods Activity" />
                                                <ApprovalsModal 
                                                    info = {{ recordId: formDetails.formGoodsDetails.id, type: "FORM_GOODS", parentId: formDetails.id }} 
                                                    trigger={<List.Header as='a' className='ut-pointer'>EXEMPTED GOODS BYPASS APPROVED</List.Header>}
                                                />
                                            </span>
                                        }  
                                        {formDetails.formGoodsDetails.goodsStatus === "CORRESPONDENCE_PENDING_APPROVAL" &&
                                        <span>
                                            <Header as="h3" content="Exempted Goods Activity" />
                                            <Message info header="A correspondence has been generated for the exempted goods. Please navigate to the Correspondence tab to review." /> 
                                        </span>
                                        }    
                                        {formDetails.formGoodsDetails.goodsStatus === "CORRESPONDENCE_REJECTED" &&
                                            <span>
                                                <Header as="h3" content="Exempted Good Details" />
                                                <ApprovalsModal 
                                                    info = {{ recordId: formDetails.formGoodsDetails.id, type: "FORM_GOODS", parentId: formDetails.id }} 
                                                    trigger={<List.Header as='a' className='ut-pointer'>CORRESPONDENCE REJECTED</List.Header>} />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <Popup inverted trigger={<Icon name='refresh' link onClick={this.reviewAgain}/>} position="left center" content='Review Again' />
                                                {this.state.reviewAgain && 
                                                    <Segment tertiary>
                                                        <ExemptedGoodsReview 
                                                            templates={_.filter(formDetails.availableTemplates, function(o) { return o.purpose === "EXEMPTED_GOODS_APPROVAL" })} 
                                                            formId= {this.state.formM}
                                                            goodsId = {formDetails.formGoodsDetails.id}
                                                            callbackForBypass = {(e)=>this.bypassAction(e, 1)}
                                                            successfulCorrespondenceUpload={()=>this.alertSuccess("Success", 5)} 
                                                            createCorrespondence = {this.createCorrespondence}
                                                        /> 
                                                    </Segment>
                                                }
                                            </span>
                                        }                                   
                                    </Segment>
                                </Grid.Column>
                            </Grid>
                        </Tab.Pane> 
                },
                { 
                    menuItem: 
                        !closed && !replaced &&
                        <Menu.Item key='attachments'> Attachments &nbsp;&nbsp;
                            {formDetails.attachmentsStatus !== "COMPLETED" 
                                && formDetails.bankStatus !== "REPLACED" 
                                && <Popup inverted size="tiny" content="Section incomplete" trigger={<Icon circular inverted color='red' name='warning' size='tiny' />} />}
                        </Menu.Item>,
                    render: () =>             
                        <Tab.Pane attached={false}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                            <Grid>
                                <Grid.Column>
                                    <Segment>
                                        <Table celled striped>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Document Type</Table.HeaderCell>
                                                    <Table.HeaderCell>Document Name</Table.HeaderCell>
                                                    <Table.HeaderCell>Document Date</Table.HeaderCell>
                                                    <Table.HeaderCell>Created At</Table.HeaderCell>
                                                    <Table.HeaderCell>Document Status</Table.HeaderCell>                                                
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>{attachments}{missingDocuments}</Table.Body>
                                        </Table>
                                    </Segment>
                                </Grid.Column>
                            </Grid>
                        </Tab.Pane> 
                },
                { 
                    menuItem: !replaced && <Menu.Item key='supportingInfo'>Supporting Information </Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false}>
                            <Grid>
                                <Grid.Column>
                                    <Segment>
                                    <Table celled striped>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Action</Table.HeaderCell>
                                                    <Table.HeaderCell>Actor</Table.HeaderCell>
                                                    <Table.HeaderCell>Date</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>{supportInfos}</Table.Body>
                                        </Table>
                                    </Segment>
                                </Grid.Column>
                            </Grid>
                        </Tab.Pane>
                },
                { 
                    menuItem: 
                        !closed && !replaced && 
                        <Menu.Item key='additionalInfo'>Additional Information &nbsp;&nbsp;
                            {formDetails.enrichmentStatus !== "COMPLETED" 
                                && formDetails.bankStatus !== "REPLACED" 
                                && <Popup inverted size="tiny" content="Section incomplete"
                                        trigger={<Icon circular inverted color='red' name='warning' size='tiny' />} />}                                 
                        </Menu.Item>, 
                    render: () => 
                        <Tab.Pane attached={false}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }                    
                            <AdditionalInformation 
                                info = {{
                                    rmEmail: formDetails.rmEmail, 
                                    bmEmail: formDetails.bmEmail,
                                    bomEmail: formDetails.bomEmail,
                                    accountName: formDetails.accountName,
                                    accountNumber: formDetails.accountNumber
                                }} 
                                form={formDetails.id}
                                saveAdditionalInfo={this.saveAdditionalInfo} />                    
                        </Tab.Pane>
                },
                {
                    menuItem: 
                        !closed && !replaced &&
                        <Menu.Item key='correspondence'>Correspondences 
                            &nbsp;&nbsp;
                                {formDetails.goodsStatus === "CBN_APPROVAL_REQUIRED"
                                && formDetails.bankStatus !== "REPLACED" &&
                                <Popup inverted size="tiny" content="Approval for CB correspondence is required" 
                                    trigger={<Icon circular inverted color='red' name='warning' size='tiny' />} />}
                        </Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false} active={formDetails.bankStatus !== "REPLACED"}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }                    
                            <FormCorrespondence 
                                templates={formDetails.availableTemplates}
                                formId= {this.state.formM}
                                correspondences={formDetails.attachments.correspondences} 
                                successfulCorrespondenceUpload={()=>this.alertSuccess("Success", 5)} 
                                createCorrespondence = {this.createCorrespondence}
                            />                
                        </Tab.Pane>
                },
                {
                    menuItem: formDetails.processingStage === "SUBMIT" && 
                                (formDetails.bankStatus === "VALIDATED" || formDetails.bankStatus === "PENDING_APPROVAL")
                                && <Menu.Item key='lc-generation'>LC Generation </Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false} active={formDetails.bankStatus !== "CLOSED" && formDetails.bankStatus !== "REPLACED"}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }    
                            {AuthenticationService.hasPermission('AUTHORIZE_REPLACEMENT_REQUESTS') ?                
                            formDetails.bankStatus === "VALIDATED" && <GenerateLC form = {this.state.formM} callbackForLCGeneration={(data)=>this.generateLC(data)} />:
                            <Message negative header="You do not have the permission to generate LC" />
                            }                                     
                        </Tab.Pane>
                },
                {
                    menuItem: formDetails.amendments &&
                            <Menu.Item key='amend-request'>Amend Request</Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false} active={formDetails.bankStatus !== "CLOSED" && formDetails.bankStatus !== "REPLACED"}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> } 
                            <ReviewAmend 
                                form = {formDetails.id}
                                amendDetails = {formDetails.amendments}
                                templates = {formDetails.availableTemplates}
                                pendingAmendmentId = {this.state.pendingAmendmentId}
                                callbackForAmendReview ={()=>this.callbackFromSidebar(null, 7)}
                            />
                        </Tab.Pane>
                },
                {
                    menuItem: 
                        cancellation &&
                        <Menu.Item key='cancellation-request'>Cancellation Request</Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> } 
                            
                            <ReviewCancellation 
                                form = {formDetails.id}
                                templates = {formDetails.availableTemplates}
                                callbackForCancellationReview ={()=>this.callbackFromSidebar(null, 8)}
                            />
                            {cancellationInProcess &&
                                <Message info icon>
                                    <Icon name='exclamation triangle' />
                                    <Message.Content>
                                        <Message.Header>Notice!</Message.Header>
                                        A Correspondence has been generated for the Form cancellation. Please review in the Correspondences tab.
                                    </Message.Content>
                                </Message>
                            }                   
                            
                        </Tab.Pane>
                },
                {
                    menuItem: 
                        replacement &&
                        <Menu.Item key='replacement-request'>Replacement Request</Menu.Item>,
                    render: () => 
                        <Tab.Pane attached={false}>
                            {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                            {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> } 
                                              
                            {AuthenticationService.hasPermission('AUTHORIZE_REPLACEMENT_REQUESTS') ?
                                replacementPendingApproval  &&
                                <ReviewRequest
                                        trigger = {<Label as='a' content="Review Replacement Request"/>}
                                        info = {{recordId: 0, type: "FORM_REPLACEMENT", parentId: formDetails.id, status: "PENDING", url: "replacement"}} 
                                        callbackForReview={(data)=>this.reviewByPass(data, 0, 10)}
                                />:
                                <Message negative header="You do not have the permission to review this request" />                                  
                            }
                            {replaced
                            && 
                                <Message info icon>
                                    <Icon name="exclamation triangle" /> 
                                    <Message.Content>
                                        <Message.Header> This form has been replaced. </Message.Header>
                                        <Message.List>
                                            <Message.Item>
                                                Click <a href={'#/form-m/'+formDetails.replacementId} target="_blank">here</a> to view new form.
                                            </Message.Item>
                                            <Message.Item>Click <ApprovalsModal 
                                                    info = {{ status: "", recordId: 0, type: "FORM_REPLACEMENT", parentId: formDetails.id }} 
                                                    trigger={<a className="ut-pointer">here</a>}
                                                /> to view request
                                            </Message.Item>
                                        </Message.List>
                                    </Message.Content>
                                </Message>                                    
                            }
                        </Tab.Pane>
                }
            ]
        }

        return (              
            <AppWrapper> 
                {formDetails 
                    && formDetails.processingStage === "SUBMIT"
                    && formDetails.bankStatus.trim().toUpperCase() !== "CLOSED"
                    && !replaced
                    && <Sidebar form={formDetails} callbackFromSidebar = {(data, index) => this.callbackFromSidebar(data, index)} />
                }
                <Header as='h3'>Form M Management</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded style={{ paddingBottom: 60 }}> 
                            {this.state.loadingMessage && <div className="ut-loader"><Loader active content={this.state.loadingMessage}/></div>}
                            {this.state.refreshErrorMessage && <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.getFormMDetails} /> }
                            {!this.state.loadingMessage && !this.state.refreshErrorMessage && this.state.formDetails !== null &&
                                <span>                                                                       
                                    <Breadcrumb size="big">
                                        <Breadcrumb.Section href="#/form-m">Form M</Breadcrumb.Section>
                                        <Breadcrumb.Divider icon='right arrow' />
                                        <Breadcrumb.Section active>{formDetails.applicationNumber}</Breadcrumb.Section>
                                    </Breadcrumb> 
                                    <Header floated="right">
                                        <Label size="large" color={UtilService.statusColor(this.state.formDetails.bankStatus)}>{this.state.formDetails.bankStatus}</Label>
                                    </Header>
                                    <Header floated="right" content={this.state.formDetails.processingStage} />                                                                                                          
                                    <Tab 
                                        activeIndex={this.state.activeIndex} 
                                        menu={{ secondary: true, pointing: true }} 
                                        panes={panes}
                                        style={{ paddingTop: 25 }} 
                                        onTabChange={this.handleTabChange}
                                    />                                    
                                </span>
                            }
                            </Segment>                       

                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default FormMDetails
