import React, { Component } from 'react'
import {Segment, Container, Grid, Form, Input, Button} from 'semantic-ui-react'

import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'

import UtilService from './../../services/util_service'

class AdditionalInformation extends Component {

    constructor (props) {
        super(props);

        this.state = {
            form: props.form,
            applicant: props.info,
            formData: {
                rmEmail: props.info.rmEmail,
                bmEmail: props.info.bmEmail,
                bomEmail: props.info.bomEmail,
                accountNumber: props.info.accountNumber,
                accountName: props.info.accountName,                
            },
            errorMessage: null,
            successMessage: null,
            loading: false
        }
    }

    submitAdditionalInformation = () => {
        if (this.validForm()) {
            this.setState ({ loading: true })
            let params = {
                rmEmail: this.state.formData.rmEmail,
                bmEmail: this.state.formData.bmEmail,
                bomEmail: this.state.formData.bomEmail,
                accountNumber: this.state.formData.accountNumber,
                accountName: this.state.formData.accountName,
                id: this.state.form
            }
            this.props.saveAdditionalInfo(params);
        }

    }

    validForm = () => {

        this.setState ({ errorMessage: null })
        
        let rmEmail = this.state.formData.rmEmail;
        let bmEmail = this.state.formData.bmEmail;
        let bomEmail = this.state.formData.bomEmail;
        let accountNumber = this.state.formData.accountNumber;
        let accountName = this.state.formData.accountName;

        if (rmEmail == null || rmEmail === "") {            
            this.setState ({ errorMessage: "Relationship Manager email required" })
            return false;
        }

        if (!UtilService.validEmail(rmEmail)) {            
            this.setState ({ errorMessage: "Invalid Relationship Manager email format" })
            return false;
        }

        if (bmEmail == null || bmEmail === "") {            
            this.setState ({ errorMessage: "Business Manager email required" })
            return false;
        }

        if (!UtilService.validEmail(bmEmail)) {            
            this.setState ({ errorMessage: "Invalid Business Manager email format" })
            return false;
        }

        if (bomEmail == null || bomEmail === "") {            
            this.setState ({ errorMessage: "Bank Operation Manager email required" })
            return false;
        }

        if (!UtilService.validEmail(bomEmail)) {            
            this.setState ({ errorMessage: "Invalid Bank Operation Manager email format" })
            return false;
        }

        if (accountNumber == null || accountNumber === "") {            
            this.setState ({ errorMessage: "Account Number required" })
            return false;
        }

        if (!UtilService.validAccountNumber(accountNumber)) {            
            this.setState ({ errorMessage: "Invalid account number" })
            return false;
        }

        if (accountName == null || accountName === "") {            
            this.setState ({ errorMessage: "Account name required" })
            return false;
        }

        if (!UtilService.validName(accountName)) {            
            this.setState ({ errorMessage: "Invalid Name format. Alphabets only" })
            return false;
        }
        
        return true;
        
    }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    render () {
        return (
            <Container>          
                <Grid>          
                    <Grid.Column>
                        <Segment>
                            <Form loading={this.state.loading}>   
                                {this.state.errorMessage && <ErrorMessage message = {this.state.errorMessage} /> }
                                {this.state.successMessage && <SuccessMessage message = {this.state.successMessage} /> }   
                                <Form.Group widths='equal'>
                                    <Form.Field control={Input} label='RM Email' placeholder = 'RM Email' 
                                        name = 'rmEmail'
                                        onChange = {this.handleInputChange}
                                        value = {this.state.formData.rmEmail}
                                    />  
                                    <Form.Field control={Input} label='Account Number' placeholder = 'Account Number' 
                                        name = 'accountNumber'
                                        onChange = {this.handleInputChange}
                                        value = {this.state.formData.accountNumber}
                                    />
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field control={Input} label='BM Email' placeholder = 'BM Email' 
                                        name = 'bmEmail'
                                        onChange = {this.handleInputChange}
                                        value = {this.state.formData.bmEmail}
                                    />
                                    <Form.Field control={Input} label='Account Name' placeholder = 'Account Name' 
                                        name = 'accountName'
                                        onChange = {this.handleInputChange}
                                        value = {this.state.formData.accountName}
                                    />
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field control={Input} label='BOM Email' placeholder = 'BOM Email' 
                                        name = 'bomEmail'
                                        onChange = {this.handleInputChange}
                                        value = {this.state.formData.bomEmail}
                                    />
                                </Form.Group>
                                <Form.Group widths='equal'>
                                    <Form.Field>
                                        <Button onClick={this.submitAdditionalInformation} positive content='Save' floated="right"/>
                                    </Form.Field>
                                </Form.Group>
                            </Form>
                        </Segment>
                    </Grid.Column>                        
                </Grid>                
            </Container>
            
        )
    }
}

export default AdditionalInformation