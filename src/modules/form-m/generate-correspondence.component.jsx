import React, { Component } from 'react'
import { Form, Button} from 'semantic-ui-react'

class GenerateCorrespondence extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            formId: props.formId,
            templates: props.templates,
            template: "",
            creating: false,
        }
    }

    handleChange = (e, { value }) => this.setState({ value })

    successfulUpdate = () => { this.props.successfulCorrespondenceUpload();  }
    
    handleSelectChange = (e, val) => { this.setState({ template: val.value }); } 

    create = () => {
        this.setState ({ creating: true})
        let params = {
            formIds: this.state.formId.split(),
            templateId: this.state.template,
            isBatch: false,
        }
        this.props.createCorrespondence(params);
        this.setState ({ creating: false})
    }

    render() {
        const { templates } = this.state
        let templateOptions;

        if (templates.length > 0) {

            templateOptions = templates.map(template => (
                { key: template.id, text: template.name, value: template.id }
            )); 
        }

        return (
            <div>                 
                <Form>
                    <Form.Group >
                        <Form.Select width={6} label='Select Template' search options={templateOptions} 
                            placeholder='Start Typing...' onChange={this.handleSelectChange} />
                        {this.state.template !== "" &&
                            <Form.Field>
                                <label>&nbsp;</label>
                                <Button onClick={this.create} color="red" content='Create' loading={this.state.creating} />
                            </Form.Field>
                        }
                    </Form.Group>
                </Form>
            </div>
        )
    }
}

export default GenerateCorrespondence