import React, { Component } from 'react';
import { Modal, Header, Table, Icon, Form, Checkbox, TextArea, Input, Button, Select } from 'semantic-ui-react';
import SharedService from './../../../services/shared_service';
import FormMService from './../../../services/form_m_service';
import UploadFile from './../../shared/upload-file.component';
import ErrorMessage from './../../shared/message/error.component';
import DatePicker from 'react-datepicker';
import moment from 'moment';

let _ = require("lodash");

class EditAmend extends Component {
    constructor (props) {
        super(props);

        this.state = {
            form: props.form,
            modalOpen: false,
            countries: [],
            ports: [],
            paymentModes: [],
            scanningCompanies: [],
            documents: [],
            uploads: [],
            isLoadingForm: false,
            fileIndex: 0,

            expirySelected: false,
            beneficiarySelected: false,
            paymentModeSelected: false,
            priceSelected: false,
            goodDescriptionSelected: false,
            countryOfSupplySelected: false,
            portOfLoadingSelected: false,
            portOfDischargeSelected: false,
            sizeOfItemsSelected: false,

            expiryDate: moment(),
            beneficiaryName: "",
            beneficiaryAddress: "",
            paymentMode: "",
            price: "",
            goodDescription: "",
            countryOfSupply: "",
            portOfLoading: "",
            portOfDischarge: "",
            scanningCompany: "",
            sizeOfItems: "",
            comment: "",

            amendment: props.amendment
        }
    }

    getLookups = () => {
        const {amendment} = this.state

        this.setState({isLoadingForm: true})
        this.getCountries();
        this.getPorts();
        this.getPaymentModes();
        this.getScanningCompanies();
        this.getAmendementDocuments();

        amendment.amendmentOptions.forEach((amendmentOption) => {

            if (amendmentOption.name === "Expiry Date") {
                this.setState({expirySelected: true, expiryDate: moment(amendmentOption.value) })
            }

            if (amendmentOption.name === "Beneficiary Name" || amendmentOption.name === "Beneficiary Address") {
                
                this.setState({beneficiarySelected: true})
                
                if (amendmentOption.name === "Beneficiary Name") {
                    this.setState ({ beneficiaryName: amendmentOption.value })
                }

                if (amendmentOption.name === "Beneficiary Address") {                    
                    this.setState ({ beneficiaryAddress: amendmentOption.value })
                }
            }

            if (amendmentOption.name === "Payment Mode") {
                this.setState({ paymentModeSelected: true, paymentMode: amendmentOption.value })
            }

            if (amendmentOption.name === "Price") {
                this.setState({ priceSelected: true, price: parseInt(amendmentOption.value, 10) })
            }

            if (amendmentOption.name === "Good Description") {
                this.setState({ goodDescriptionSelected: true,  goodDescription: amendmentOption.value })
            }

            if (amendmentOption.name === "Country of Supply") {
                this.setState({ countryOfSupplySelected: true,  countryOfSupply: amendmentOption.value })
            }

            if (amendmentOption.name === "Port of Loading") {
                this.setState({portOfLoadingSelected: true, portOfLoading: amendmentOption.value })
            }

            if (amendmentOption.name === "Port of Discharge" || amendmentOption.name === "Scanning Company") {

                this.setState({ portOfDischargeSelected: true })
                
                if (amendmentOption.name === "Port of Discharge") {
                    this.setState ({ portOfDischarge: amendmentOption.value })
                }

                if (amendmentOption.name === "Scanning Company") {                    
                    this.setState ({ scanningCompany: amendmentOption.value })
                }
            }

            if (amendmentOption.name === "Size of Items") {
                this.setState({ sizeOfItemsSelected: true, sizeOfItems: amendmentOption.value })
            }
        });

        this.setState({ uploads: amendment.attachments })

        this.setState ({ comment: amendment.comments })
        
    }

    getCountries = () => {
        SharedService.getCountries()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ countries: response.list })
            }
        })
    }

    getPorts = () => {
        SharedService.getPorts()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ ports: response.list })
            }
        })
    }

    getPaymentModes = () => {
        SharedService.getPaymentModes()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ paymentModes: response.list })
            }
        })
    }

    getScanningCompanies = () => {
        SharedService.getScanningCompanies()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ scanningCompanies: response.list })
            }
        })
    }

    getAmendementDocuments = () => { 
        this.setState({ isLoadingForm: true });
        SharedService.getAppParameter('IM_AMENDMENT_DOCUMENTS')
        .then(response => {            
            if (!response.code) {
                this.setState ({ documents: response.value.split(","), isLoadingForm: false })
            }
        })
    }

    handleCheckbox = (e, {name, value}) => {
        this.setState({ [value]: !this.state[value] })
    }

    handleDate = (date) => this.setState({ expiryDate: date})

    handleInputChange = (e, { name, value }) => this.setState({ [name]: value }) 

    handleSelectChange = (e, val) => this.setState({ [val.name] : val.value });  

    afterUpload = (data, document) => { 
        let doc = {
            documentName: data.documentName,
            formInfoId: this.state.form,
            documentType: document,
            documentBinaryType: data.fileExtension,
            documentLink: data.documentLink
        }

        let newArray = [ ...this.state.uploads, doc ];
        this.setState ({ 
            uploads: newArray,
            fileIndex: this.state.fileIndex + 1            
        }); 
    }

    delete = (e, i) => {
        SharedService.deleteFile(e)
        .then(response => {            
            var newArray = this.state.uploads;
            var index = _.findIndex(newArray, function(o) { return o.documentName === e ;})
            newArray.splice(index, 1);
    
            this.setState({ uploads: newArray }); 
        })       
    }

    save = () => {        
        const {expirySelected, beneficiarySelected, paymentModeSelected, priceSelected, goodDescriptionSelected, 
            countryOfSupplySelected, portOfLoadingSelected,  portOfDischargeSelected, sizeOfItemsSelected, expiryDate, documents, uploads} = this.state;
        let amendmentOptions = [], error = false;

        this.setState ({errorMessage: null});

        if (expirySelected) {
            
            if (this.state.expiryDate === null) {
                error = true;
                this.setState({"expiryDate_error": true})
            } else {
                amendmentOptions.push({name: "Expiry Date", value: expiryDate.format("YYYY-MM-DD")});
                this.setState({ "expiryDate_error": false})                
            }
                        
        }

        if (beneficiarySelected) {
            
            if (this.state.beneficiaryName === null || this.state.beneficiaryName === "") {
                error = true;
                this.setState({"beneficiaryName_error": true});
            } else {                
                amendmentOptions.push({name: "Beneficiary Name", value: this.state.beneficiaryName});
                this.setState({ "beneficiaryName_error": false})
            }

            if (this.state.beneficiaryAddress === null || this.state.beneficiaryAddress === "") {
                error = true;
                this.setState({"beneficiaryAddress_error": true});
            } else {                
                amendmentOptions.push({name: "Beneficiary Address", value: this.state.beneficiaryAddress});
                this.setState({ "beneficiaryAddress_error": false})
            }
                        
        }

        if (paymentModeSelected) {
            
            if (this.state.paymentMode === null || this.state.paymentMode === "") {
                error = true;
                this.setState({ "paymentMode_error": true})
            } else {
                amendmentOptions.push({name: "Payment Mode", value: this.state.paymentMode});
                this.setState({ "paymentMode_error": false})                
            }
                        
        }        

        if (priceSelected) {

            if (this.state.price === null || this.state.price === "") {
                error = true;
                this.setState({ "price_error": true})
            } else {
                amendmentOptions.push({name: "Price", value: this.state.price});
                this.setState({ "price_error": false})                
            }

        }

        if (goodDescriptionSelected) {
            if (this.state.goodDescription === null || this.state.goodDescription === "") {
                error = true;
                this.setState({ "goodDescription_error": true})
            } else {
                amendmentOptions.push({name: "Good Description", value: this.state.goodDescription});
                this.setState({ "goodDescription_error": false})                
            }
        }
        
        if (countryOfSupplySelected) {

            if (this.state.countryOfSupply === null || this.state.countryOfSupply === "") {
                error = true;
                this.setState({ "countryOfSupply_error": true})
            } else {
                amendmentOptions.push({name: "Country of Supply", value: this.state.countryOfSupply});
                this.setState({ "countryOfSupply_error": false})                
            }

        }

        if (portOfLoadingSelected) {

            if (this.state.portOfLoading === null || this.state.portOfLoading === "") {
                error = true;
                this.setState({ "portOfLoading_error": true})
            } else {
                amendmentOptions.push({name: "Port of Loading", value: this.state.portOfLoading});
                this.setState({ "portOfLoading_error": false})                
            }

        }

        if (portOfDischargeSelected) {

            if (this.state.portOfDischarge === null || this.state.portOfDischarge === "") {
                error = true;
                this.setState({ "portOfDischarge_error": true})
            } else {
                amendmentOptions.push({name: "Port of Discharge", value: this.state.portOfDischarge});
                this.setState({ "portOfDischarge_error": false})                
            }

            if (this.state.scanningCompany === null || this.state.scanningCompany === "") {
                error = true;
                this.setState({ "scanningCompany_error": true})
            } else {
                amendmentOptions.push({name: "Scanning Company", value: this.state.scanningCompany});
                this.setState({ "scanningCompany_error": false})                
            }

        }

        if (sizeOfItemsSelected) {

            if (this.state.sizeOfItems === null || this.state.sizeOfItems === "") {
                error = true;
                this.setState({ "sizeOfItems_error": true})
            } else {
                amendmentOptions.push({name: "Size of Items", value: this.state.sizeOfItems});
                this.setState({ "sizeOfItems_error": false})                
            }

        }

        if (error) {
            this.alertError("You must enter values for selected Amendment options")
            return false;
        }
        
        for (let i = 0; i < documents.length; i++) { 
                
            var a = uploads.filter(record => record.documentType === documents[i])             
            
            if (a.length === 0) {
                this.alertError("All document types must have at least one file uploaded")
                return false;
            }
        } 

        this.setState({ isLoadingForm: true});
        let params = {
            comments: this.state.comment,
            formId: this.state.form,
            amendmentOptions: amendmentOptions,
            attachments: this.state.uploads,
            previousAmendmentId: this.state.amendment.id
        }
        
        FormMService.amend(params)
        .then(response => {
            this.setState({ isLoadingForm: false});
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.props.callbackForSuccesfulAmendmentUpdate();
            }
        })
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0)
    }

    render () {
        const {countries, ports, paymentModes, scanningCompanies, documents, uploads, isLoadingForm} = this.state;
        let countriesOption=[], portsOption=[], paymentModesOption=[], scanningCompaniesOption=[], 
            documentsRow=[];

        if ( countries.length > 0) {
            countriesOption = countries.map(country => (
                { key: country.code, text: country.name, value: country.name }            
            ));
        }

        if (ports.length > 0) {
            portsOption = ports.map(port => (
                { key: port.code, text: port.name, value: port.name }            
            ));
        }

        if (paymentModes.length > 0) {
            paymentModesOption = paymentModes.map(paymentMode => (
                { key: paymentMode.code, text: paymentMode.name, value: paymentMode.name }            
            ));
        }

        if (scanningCompanies.length > 0) {
            scanningCompaniesOption = scanningCompanies.map(scanningCompany => (
                { key: scanningCompany.code, text: scanningCompany.name, value: scanningCompany.name }            
            ));
        }

        if (documents && documents.length > 0) {    
            documentsRow = documents.map((document, i) => {
                let uploaded = uploads.filter(record => record.documentType === document);
                
                return <Table.Row key={i}>
                        <Table.Cell collapsing>{document}</Table.Cell>
                        <Table.Cell>
                            {uploaded.map((upload, i) => {
                                return <span key={i}>
                                            <a href={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                            <Icon link name="trash" color="red" style={{float: "right"}} onClick={()=>this.delete(upload.documentName)} />
                                            <br/> 
                                        </span>
                                })
                            }                            
                            <UploadFile ref={'row-'+i} info={i} callbackForUpload={(e)=>this.afterUpload(e, document)} />                            
                        </Table.Cell>
                    </Table.Row>
                });
        }

        return (
            <Modal size="small" trigger={this.props.trigger} onOpen={this.getLookups} closeIcon>
                <Header content='Edit Amend Form' />
                <Modal.Content scrolling>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                    <Form loading={isLoadingForm}>
                        <Table basic='very' celled>
                            <Table.Body>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.expirySelected} label="Expiry Date" value="expirySelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.expirySelected}>              
                                            <DatePicker dateFormat="YYYY-MM-DD" selected = {this.state.expiryDate} onChange = {this.handleDate} />
                                        </Form.Field>
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.beneficiarySelected} label="Change of Beneficiary" value="beneficiarySelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.beneficiarySelected} control={Input} placeholder="Beneficiary Name" onChange={this.handleInputChange} value={this.state.beneficiaryName} name="beneficiaryName" error={this.state["beneficiaryName_error"]} /> 
                                        <Form.Field disabled={!this.state.beneficiarySelected} control={TextArea} placeholder="Beneficiary Address" onChange={this.handleInputChange} value={this.state.beneficiaryAddress} name = "beneficiaryAddress" error={this.state["beneficiaryAddress_error"]} />
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.paymentModeSelected} label="Change in Payment Mode" value="paymentModeSelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                    <Form.Field disabled={!this.state.paymentModeSelected} control={Select} placeholder="Select Payment Mode" name ='paymentMode' value={this.state.paymentMode} options={paymentModesOption} onChange = {this.handleSelectChange} error={this.state["paymentMode_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.priceSelected} label="Change in Price" value="priceSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.priceSelected} control={Input} placeholder="Price" onChange={this.handleInputChange} type="number" name="price" value={this.state.price} error={this.state["price_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.goodDescriptionSelected} label="Change in Good Description" value="goodDescriptionSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.goodDescriptionSelected} control={TextArea} placeholder="Change in Good Description" onChange={this.handleInputChange} value={this.state.goodDescription} name="goodDescription" error={this.state["goodDescription_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.countryOfSupplySelected} label="Change in Country of Supply" value="countryOfSupply" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.countryOfSupply} control={Select} placeholder="Select Country of Supply" name ='countryOfSupply' value={this.state.countryOfSupply} options={countriesOption} onChange = {this.handleSelectChange} error={this.state["countryOfSupply_error"]} /> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.portOfLoadingSelected} label="Change in Port of Loading" value="portOfLoading" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.portOfLoading} control={Input} placeholder="Port of Loading" onChange={this.handleInputChange}  name="portOfLoading" value={this.state.portOfLoading} error={this.state["portOfLoading_error"]} /> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.portOfDischargeSelected} label="Change in Port of Discharge" value="portOfDischargeSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.portOfDischargeSelected} control={Select} placeholder="Select Country of Supply" name ='portOfDischarge' value={this.state.portOfDischarge} options={portsOption} onChange = {this.handleSelectChange} error={this.state["portOfDischarge_error"]}/> 
                                        <Form.Field disabled={!this.state.portOfDischargeSelected} control={Select} placeholder="Select Scanning Company" name ='scanningCompany' value={this.state.scanningCompany} options={scanningCompaniesOption} onChange = {this.handleSelectChange} error={this.state["scanningCompany_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} checked={this.state.sizeOfItemsSelected} label="Change in Size of Items" value="sizeOfItemsSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.sizeOfItemsSelected} control={Input} placeholder="Size Of Items" name="sizeOfItems" onChange={this.handleInputChange} type="number" value={this.state.sizeOfItems} error={this.state["sizeOfItems_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                            </Table.Body>
                        </Table>

                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Document Type</Table.HeaderCell>
                                    <Table.HeaderCell>Upload</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{documentsRow}</Table.Body>
                        </Table>    
                        <Form.TextArea label='Comment' value={this.state.comment} name="comment" onChange={this.handleInputChange} placeholder='Comment...' />
                    </Form>
                </Modal.Content>
                <Modal.Actions><Button onClick={this.save} positive content='Save'/></Modal.Actions>
            </Modal>
        )
    }
}

export default EditAmend