import React, { Component } from 'react';
import { Modal, Header, Table, Icon, Form, Checkbox, TextArea, Input, Button, Select } from 'semantic-ui-react';
import SharedService from './../../../services/shared_service';
import FormMService from './../../../services/form_m_service';
import UploadFile from './../../shared/upload-file.component';
import ErrorMessage from './../../shared/message/error.component';
import DatePicker from 'react-datepicker';
import moment from 'moment';

let _ = require("lodash");

class InitiateAmend extends Component {
    constructor (props) {
        super(props);

        this.state = {
            form: props.form,
            modalOpen: false,
            countries: [],
            ports: [],
            paymentModes: [],
            scanningCompanies: [],
            documents: [],
            uploads: [],
            isLoadingForm: false,
            fileIndex: 0,

            expirySelected: false,
            beneficiarySelected: false,
            paymentModeSelected: false,
            priceSelected: false,
            goodDescriptionSelected: false,
            countryOfSupplySelected: false,
            portOfLoadingSelected: false,
            portOfDischargeSelected: false,
            sizeOfItemsSelected: false,

            expiryDate: moment(),
            formData: {
                beneficiaryName: "",
                beneficiaryAddress: "",
                paymentMode: "",
                price: "",
                goodDescription: "",
                countryOfSupply: "",
                portOfLoading: "",
                portOfDischarge: "",
                scanningCompany: "",
                sizeOfItems: "",
                comment: ""
            }
        }
    }

    getLookups = () => {
        this.setState({isLoadingForm: true})
        this.getCountries();
        this.getPorts();
        this.getPaymentModes();
        this.getScanningCompanies();
        this.getAmendementDocuments();
    }

    getCountries = () => {
        SharedService.getCountries()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ countries: response.list })
            }
        })
    }

    getPorts = () => {
        SharedService.getPorts()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ ports: response.list })
            }
        })
    }

    getPaymentModes = () => {
        SharedService.getPaymentModes()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ paymentModes: response.list })
            }
        })
    }

    getScanningCompanies = () => {
        SharedService.getScanningCompanies()
        .then(response => {
            if (response.code) {

            } else {
                this.setState({ scanningCompanies: response.list })
            }
        })
    }

    getAmendementDocuments = () => { 
        this.setState({ isLoadingForm: true });
        SharedService.getAppParameter('IM_AMENDMENT_DOCUMENTS')
        .then(response => {            
            if (!response.code) {
                this.setState ({ documents: response.value.split(","), isLoadingForm: false })
            }
        })
    }

    handleCheckbox = (e, {name, value}) => {
        this.setState({ [value]: !this.state[value] })
    }

    handleDate = (date) => this.setState({ expiryDate: date})

    handleInputChange = (e, { name, value }) => {         
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name]: value
            }
        }) 
    }

    handleSelectChange = (e, val) => {
        console.log(val)
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        }, () => {
            console.log(this.state.formData)
        });  
    }

    afterUpload = (data, document) => { 
        let doc = {
            documentName: data.documentName,
            formInfoId: this.state.form,
            documentType: document,
            documentBinaryType: data.fileExtension,
            documentLink: data.documentLink
        }

        let newArray = [ ...this.state.uploads, doc ];
        this.setState ({ 
            uploads: newArray,
            fileIndex: this.state.fileIndex + 1            
        }); 
    }

    delete = (e, i) => {
        SharedService.deleteFile(e)
        .then(response => {            
            var newArray = this.state.uploads;
            var index = _.findIndex(newArray, function(o) { return o.documentName === e ;})
            newArray.splice(index, 1);
    
            this.setState({ uploads: newArray }); 
        })       
    }

    save = () => {        
        const {expirySelected, beneficiarySelected, paymentModeSelected, priceSelected, 
            goodDescriptionSelected, countryOfSupplySelected, portOfLoadingSelected,
            portOfDischargeSelected, sizeOfItemsSelected, expiryDate, formData, documents, uploads} = this.state;
        let amendmentOptions = [], error = false;

        this.setState ({errorMessage: null});

        if (expirySelected) {
            
            if (this.state.expiryDate === null) {
                error = true;
                this.setState({"expiryDate_error": true})
            } else {
                amendmentOptions.push({name: "Expiry Date", value: expiryDate.format("YYYY-MM-DD")});
                this.setState({ "expiryDate_error": false})                
            }
                        
        }

        if (beneficiarySelected) {
            
            if (formData.beneficiaryName === null || formData.beneficiaryName === "") {
                error = true;
                this.setState({"beneficiaryName_error": true});
            } else {                
                amendmentOptions.push({name: "Beneficiary Name", value: formData.beneficiaryName});
                this.setState({ "beneficiaryName_error": false})
            }

            if (formData.beneficiaryAddress === null || formData.beneficiaryAddress === "") {
                error = true;
                this.setState({"beneficiaryAddress_error": true});
            } else {                
                amendmentOptions.push({name: "Beneficiary Address", value: formData.beneficiaryAddress});
                this.setState({ "beneficiaryAddress_error": false})
            }
                        
        }

        if (paymentModeSelected) {
            
            if (formData.paymentMode === null || formData.paymentMode === "") {
                error = true;
                this.setState({ "paymentMode_error": true})
            } else {
                amendmentOptions.push({name: "Payment Mode", value: formData.paymentMode});
                this.setState({ "paymentMode_error": false})                
            }
                        
        }        

        if (priceSelected) {

            if (formData.price === null || formData.price === "") {
                error = true;
                this.setState({ "price_error": true})
            } else {
                amendmentOptions.push({name: "Price", value: formData.price});
                this.setState({ "price_error": false})                
            }

        }

        if (goodDescriptionSelected) {
            if (formData.goodDescription === null || formData.goodDescription === "") {
                error = true;
                this.setState({ "goodDescription_error": true})
            } else {
                amendmentOptions.push({name: "Good Description", value: formData.goodDescription});
                this.setState({ "goodDescription_error": false})                
            }
        }

        if (countryOfSupplySelected) {
            console.log("hsshh")
            if (formData.countryOfSupply === null || formData.countryOfSupply === "") {
                console.log("hhhe")
                error = true;
                this.setState({ "countryOfSupply_error": true})
            } else {
                console.log("hhh")
                amendmentOptions.push({name: "Country of Supply", value: formData.countryOfSupply});
                this.setState({ "countryOfSupply_error": false})                
            }

        }

        if (portOfLoadingSelected) {

            if (formData.portOfLoading === null || formData.portOfLoading === "") {
                error = true;
                this.setState({ "portOfLoading_error": true})
            } else {
                amendmentOptions.push({name: "Port of Loading", value: formData.portOfLoading});
                this.setState({ "portOfLoading_error": false})                
            }

        }

        if (portOfDischargeSelected) {

            if (formData.portOfDischarge === null || formData.portOfDischarge === "") {
                error = true;
                this.setState({ "portOfDischarge_error": true})
            } else {
                amendmentOptions.push({name: "Port of Discharge", value: formData.portOfDischarge});
                this.setState({ "portOfDischarge_error": false})                
            }

            if (formData.scanningCompany === null || formData.scanningCompany === "") {
                error = true;
                this.setState({ "scanningCompany_error": true})
            } else {
                amendmentOptions.push({name: "Scanning Company", value: formData.scanningCompany});
                this.setState({ "scanningCompany_error": false})                
            }

        }

        if (sizeOfItemsSelected) {

            if (formData.sizeOfItems === null || formData.sizeOfItems === "") {
                error = true;
                this.setState({ "sizeOfItems_error": true})
            } else {
                amendmentOptions.push({name: "Size of Items", value: formData.sizeOfItems});
                this.setState({ "sizeOfItems_error": false})                
            }

        }

        if (error) {
            this.alertError("You must enter values for selected Amendment options")
            return false;
        }
        
        for (let i = 0; i < documents.length; i++) { 
                
            var a = uploads.filter(record => record.documentType === documents[i])             
            
            if (a.length === 0) {
                this.alertError("All document types must have at least one file uploaded")
                return false;
            }
        } 

        this.setState({ isLoadingForm: true});
        let params = {
            comments: this.state.formData.comment,
            formId: this.state.form,
            amendmentOptions: amendmentOptions,
            attachments: this.state.uploads
        }
        
        FormMService.amend(params)
        .then(response => {
            this.setState({ isLoadingForm: false});
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.props.callbackForSuccesfulAmendRequest();
            }
        })
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0)
    }

    render () {
        const {countries, ports, paymentModes, scanningCompanies, documents, uploads, isLoadingForm, formData} = this.state;
        let countriesOption=[], portsOption=[], paymentModesOption=[], scanningCompaniesOption=[], 
            documentsRow=[];

        if ( countries.length > 0) {
            countriesOption = countries.map(country => (
                { key: country.code, text: country.name, value: country.code }            
            ));
        }

        if (ports.length > 0) {
            portsOption = ports.map(port => (
                { key: port.code, text: port.name, value: port.code }            
            ));
        }

        if (paymentModes.length > 0) {
            paymentModesOption = paymentModes.map(paymentMode => (
                { key: paymentMode.code, text: paymentMode.name, value: paymentMode.code }            
            ));
        }

        if (scanningCompanies.length > 0) {
            scanningCompaniesOption = scanningCompanies.map(scanningCompany => (
                { key: scanningCompany.code, text: scanningCompany.name, value: scanningCompany.code }            
            ));
        }

        if (documents && documents.length > 0) {    
            documentsRow = documents.map((document, i) => {
                let uploaded = uploads.filter(record => record.documentType === document);
                
                return <Table.Row key={i}>
                        <Table.Cell collapsing>{document}</Table.Cell>
                        <Table.Cell>
                            {uploaded.map((upload, i) => {
                                return <span key={i}>
                                            <a href={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                            <Icon link name="trash" color="red" style={{float: "right"}} onClick={()=>this.delete(upload.documentName)} />
                                            <br/> 
                                        </span>
                                })
                            }                            
                            <UploadFile ref={'row-'+i} info={i} callbackForUpload={(e)=>this.afterUpload(e, document)} />                            
                        </Table.Cell>
                    </Table.Row>
                });
        }

        return (
            <Modal size="small" trigger={this.props.trigger} onOpen={this.getLookups} closeIcon>
                <Header content='Request to Amend Form' />
                <Modal.Content scrolling>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                    <Form loading={isLoadingForm}>
                        <Table basic='very' celled>
                            <Table.Body>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Expiry Date" value="expirySelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.expirySelected}>              
                                            <DatePicker dateFormat="YYYY-MM-DD" selected = {this.state.expiryDate} onChange = {this.handleDate} />
                                        </Form.Field>
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change of Beneficiary" value="beneficiarySelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.beneficiarySelected}control={Input} placeholder="Beneficiary Name" onChange={this.handleInputChange} value={formData.beneficiaryName} name="beneficiaryName" error={this.state["beneficiaryName_error"]} /> 
                                        <Form.Field disabled={!this.state.beneficiarySelected} control={TextArea} placeholder="Beneficiary Address" onChange={this.handleInputChange} value={formData.beneficiaryAddress} name = "beneficiaryAddress" error={this.state["beneficiaryAddress_error"]} />
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Payment Mode" value="paymentModeSelected" onChange={this.handleCheckbox}/>
                                    </Table.Cell>
                                    <Table.Cell>
                                    <Form.Field disabled={!this.state.paymentModeSelected} control={Select} placeholder="Select Payment Mode" name ='paymentMode' value={formData.paymentMode} options={paymentModesOption} onChange = {this.handleSelectChange} error={this.state["paymentMode_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Price" value="priceSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.priceSelected} control={Input} placeholder="Price" onChange={this.handleInputChange} type="number" name="price" value={formData.price} error={this.state["price_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Good Description" value="goodDescriptionSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.goodDescriptionSelected} control={TextArea} placeholder="Change in Good Description" onChange={this.handleInputChange} value={formData.goodDescription} name="goodDescription" error={this.state["goodDescription_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Country of Supply" value="countryOfSupplySelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.countryOfSupplySelected} control={Select} placeholder="Select Country of Supply" name ='countryOfSupply' value={formData.countryOfSupply} options={countriesOption} onChange = {this.handleSelectChange} error={this.state["countryOfSupply_error"]} /> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Port of Loading" value="portOfLoadingSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.portOfLoadingSelected} control={Input} placeholder="Port of Loading" onChange={this.handleInputChange}  name="portOfLoading" value={formData.portOfLoading} error={this.state["portOfLoading_error"]} /> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Port of Discharge" value="portOfDischargeSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.portOfDischargeSelected} control={Select} placeholder="Select Country of Supply" name ='portOfDischarge' value={formData.portOfDischarge} options={portsOption} onChange = {this.handleSelectChange} error={this.state["portOfDischarge_error"]}/> 
                                        <Form.Field disabled={!this.state.portOfDischargeSelected} control={Select} placeholder="Select Scanning Company" name ='scanningCompany' value={formData.scanningCompany} options={scanningCompaniesOption} onChange = {this.handleSelectChange} error={this.state["scanningCompany_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                                <Table.Row>
                                    <Table.Cell collapsing>
                                        <Form.Field control={Checkbox} label="Change in Size of Items" value="sizeOfItemsSelected" onChange={this.handleCheckbox} />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Form.Field disabled={!this.state.sizeOfItemsSelected} control={Input} placeholder="Size Of Items" name="sizeOfItems" onChange={this.handleInputChange} type="number" value={formData.sizeOfItems} error={this.state["sizeOfItems_error"]}/> 
                                    </Table.Cell>
                                </Table.Row>

                            </Table.Body>
                        </Table>

                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Document Type</Table.HeaderCell>
                                    <Table.HeaderCell>Upload</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{documentsRow}</Table.Body>
                        </Table>    
                        <Form.TextArea label='Comment' value={formData.comment} name="comment" onChange={this.handleInputChange} placeholder='Comment...' />
                    </Form>
                </Modal.Content>
                <Modal.Actions><Button onClick={this.save} positive content='Save'/></Modal.Actions>
            </Modal>
        )
    }
}

export default InitiateAmend