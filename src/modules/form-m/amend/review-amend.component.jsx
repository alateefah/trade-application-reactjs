import React, { Component } from 'react'
import { Header, Button, Form, Dropdown, Table, Message, Grid, Accordion, Icon, Feed, List} from 'semantic-ui-react'
import ErrorMessage from './../../shared/message/error.component'
import DocumentDisplayModal from '../../shared/document-display-modal.component';
import AuthenticationService from '../../../services/authentication_service';
import SharedService from '../../../services/shared_service';
import EditAmend from './edit-amend.component';

class ReviewAmend extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            amendDetails: props.amendDetails,
            templates: props.templates.filter( template => template.purpose === "AMMENDMENT_APPROVAL"),
            checked: false,
            formData: {
                recordId: props.form,
                parentId: "",
                name: "",
                description: "Request for Amendment",
                type: "FORM_AMMENDMENT",
                initiatorComments: "",
                formCategory: "FORM_M",
                approved: ""
            },          
            template: "",  
            isLoadingForm: false,
            activeIndex: props.pendingAmendmentId
            
        }
    }

    handleClose = () => this.setState({ modalOpen: false })

    toggleGenerateCorrespondence = () => { this.setState({ checked: !this.state.checked }) }

    validForm = () => {
        this.setState ({ errorMessage: null })

        if (this.state.checked) { 
            if (this.state.template === null || this.state.template === "") {            
                this.setState ({ errorMessage: "You must select a template to generate a correspondence with" })
                return false;
            }
        }
        
        return true;
    }

    clearForm = () => {
        this.setState ({
            errorMessage: null, 
            formData: {
                template: "",
                initiatorComments: ""                      
            }
        });    
    }

    handleTemplateSelect = (e, { value }) => { this.setState({ template: value }) }

    handleInputChange = (e, { name, value }) => { 
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name]: value 
            }
        }) 
    }
    
    handleClick = (e, titleProps) => {
        const { index } = titleProps
        const { activeIndex } = this.state
        const newIndex = activeIndex === index ? -1 : index
    
        this.setState({ activeIndex: newIndex })
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0)
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    reject = (id) => {
        if (this.state.formData.initiatorComments === "") {
            this.alertError("Comment required for rejection");
        } else {
            this.setState ({ 
                formData: {
                    ...this.state.formData,
                    parentId: id,
                    approved: false
                }
            }, () => { this.submit(id); });
        }
    }

    approve = (id) => {
        this.setState ({ 
            formData: {
                ...this.state.formData,
                parentId: id,
                approved: true
            }
        }, () => { this.submit(id); });
    }

    submit = (id) => {
        if (this.validForm()) {            
            this.setState({ isLoadingForm: true });
            let params = this.state.formData;
            params.recordId = id;
            if (params.approved) {
                if (this.state.checked) {
                    params.recordId = this.state.template;
                } 
            }                
            
            SharedService.requestApproval(params)
            .then( response => {
                this.setState({ isLoadingForm: false })
                if (response.code) {
                    this.alertError(response.description);                
                } else {            
                    this.props.callbackForAmendReview();
                }
            })          
        
        }
    }

    rejectReview = (id) => {
        if (this.state.formData.initiatorComments === "") {
            this.alertError("Comment required for rejection");
        } else {
            this.setState ({ 
                formData: {
                    ...this.state.formData,
                    parentId: id,
                    approved: false
                }
            }, () => { this.submitReview(); });
        }
    }

    approveReview = (id) => {
        this.setState ({ 
            formData: {
                ...this.state.formData,
                parentId: id,
                approved: true
            }
        }, () => { this.submitReview(); });
    }

    submitReview = () => {
        const {amendDetails} = this.state;
        let pendingAmendment = amendDetails.find(amendDetails => 
            amendDetails.status !== "REJECTED" && amendDetails.status !== "APPROVED");
        let pendingApproval = pendingAmendment.approvals.find(approval => approval.approvalStatus === "PENDING");

        this.setState({ isLoadingForm: true });
        let params = this.state.formData;
        params.approvalId = pendingApproval.id
        params.url = "amendment"            
        params.approverComments = params.initiatorComments;   
        
        SharedService.authorizeApproval(params)
        .then( response => {
            this.setState({ isLoadingForm: false })
            if (response.code) {
                this.alertError(response.description);                
            } else {            
                this.props.callbackForAmendReview();
            }
        })     
    }

    render () {
        const { templates, isLoadingForm, amendDetails, activeIndex} = this.state
        let templateOptions = [], amendDetailsAccordions=[], rejectedAmendment;

        if (templates && templates.length > 0) {                  
            templateOptions =  templates.map(template => (
                { key: template.id, text: template.name, value: template.id } 
            ))
        }

        amendDetailsAccordions = amendDetails.map((amendDetail, index) => {
            return  <Accordion.Accordion key={index}>
                        <Accordion.Title active={activeIndex === amendDetail.id} index={amendDetail.id} onClick={this.handleClick}>
                            <Icon name='dropdown'/>{"Amendment By: "+amendDetail.createdBy +" ("+amendDetail.createdOn+")" }
                        </Accordion.Title>
                        <Accordion.Content active={activeIndex === amendDetail.id}>
                            <Message>
                                <Grid columns={2} divided>
                                    <Grid.Column>
                                        <Table basic='very' celled>
                                            <Table.Header>
                                                <Table.Row>
                                                    <Table.HeaderCell>Item </Table.HeaderCell>
                                                    <Table.HeaderCell>New Value</Table.HeaderCell>
                                                </Table.Row>
                                            </Table.Header>
                                            <Table.Body>
                                                {amendDetail.amendmentOptions.map((amendment, index) => {
                                                    return  <Table.Row key={index}>
                                                                <Table.Cell collapsing>{amendment.name}</Table.Cell>
                                                                <Table.Cell>{amendment.value}</Table.Cell>                                                            
                                                            </Table.Row>
                                                })}
                                                
                                            </Table.Body>
                                        </Table>
                                    </Grid.Column>
                                    <Grid.Column>
                                        <fieldset>
                                            <legend>Uploads</legend>
                                            <Table basic='very' >
                                                <Table.Body >
                                                    {amendDetail.attachments.map((attachment, index) => {
                                                        return <Table.Row key={index}>
                                                                    <Table.Cell>{attachment.documentType}</Table.Cell>
                                                                    <Table.Cell>
                                                                        <DocumentDisplayModal document={attachment} />
                                                                    </Table.Cell>
                                                                </Table.Row>
                                                        })
                                                    }   
                                                </Table.Body>
                                            </Table>                                                   
                                        </fieldset>
                                        <br/>
                                        <Header as="h4">Inputter's Comment</Header>
                                        {amendDetail.comments}
                                    </Grid.Column>
                                </Grid>
                            </Message>

                            <Feed>
                                {amendDetail.approvals && amendDetail.approvals.map((approval, index) => {
                                    return  <span className="ui feed" key={index}>
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.createdDate}
                                                    summary={approval.createdBy+" requested form ammendment be "+ approval.initiatorsReview}
                                                    extraText= {approval.initiatorComments}
                                                />
                                                {approval.approvalStatus !== "PENDING" &&
                                                <Feed.Event
                                                    icon="pencil"
                                                    date={approval.approvedDate}
                                                    summary={approval.approvedBy +" "+ approval.approvalStatus+" request."}
                                                    extraText= {approval.approverComments}
                                                />}
                                            </span>
                                    })
                                                
                                }               
                                                
                            </Feed>

                            {amendDetail.status === "CORRESPONDENCE_PENDING_APPROVAL" &&
                            <Message info icon>
                                <Icon name="exclamation triangle" />
                                <Message.Content>
                                    <Message.Header>Notice!</Message.Header>
                                    A Correspondence has been generated for the Form Amendment. Please review in the Correspondences tab.
                                </Message.Content>
                            </Message>
                            }

                            {(amendDetail.status === "PENDING" 
                                || amendDetail.status === "PENDING_REJECTION"
                                || amendDetail.status === "PENDING_APPROVAL")
                            && AuthenticationService.hasPermission("AUTHORIZE_AMENDMENT_REQUESTS")                            
                            && <Form loading={isLoadingForm}>
                                    {amendDetail.status === "PENDING" &&
                                        <span>
                                            <Form.Checkbox label='Generate Correspondence' onChange={this.toggleGenerateCorrespondence} checked={this.state.checked}/>
                                            {this.state.checked &&
                                            <Form.Field>
                                                <label> Select Template </label>
                                                <Dropdown fluid selection placeholder='Templates'                                  
                                                    options={templateOptions} 
                                                    value={this.state.template}
                                                    onChange={this.handleTemplateSelect}
                                                />
                                            </Form.Field>
                                            }
                                        </span>
                                    }
                                    <Form.TextArea label='Comment' name="initiatorComments" value={this.state.formData.initiatorComments} onChange={this.handleInputChange} placeholder='Comment...' />
                                    {AuthenticationService.hasPermission("CREATE_APPROVAL_REQUEST") 
                                    && amendDetail.status === "PENDING"
                                    &&  <span>
                                            <Button onClick={()=>this.approve(amendDetail.id)} positive icon="checkmark" content='Approve' />
                                            <Button onClick={()=>this.reject(amendDetail.id)} negative icon="cancel" content='Reject' />
                                        </span>
                                    }

                                    {AuthenticationService.hasPermission("AUTHORIZE_AMENDMENT_REQUESTS") 
                                    && (amendDetail.status === "PENDING_REJECTION" || amendDetail.status === "PENDING_APPROVAL")
                                    &&  <span>
                                            <Button onClick={()=>this.rejectReview(amendDetail.id)} negative icon="cancel" content='Return for correction by inputter' />
                                            <Button onClick={()=>this.approveReview(amendDetail.id)} positive icon="checkmark" content='Approve' />                                            
                                        </span>
                                    }
                                </Form>
                            }
                        </Accordion.Content>
                    </Accordion.Accordion>
        })

        rejectedAmendment = amendDetails.find(amendDetail => amendDetail.status === "REJECTED");

        return ( 
            <span>  
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />}
                {rejectedAmendment && 
                    <EditAmend 
                        form={this.props.form}
                        amendment={rejectedAmendment} 
                        trigger={<List.Header as='a' className="ut-pointer">Update request</List.Header>} 
                        callbackForSuccesfulAmendmentUpdate = {()=>this.props.callbackForAmendReview()} 
                    />
                }
                <Accordion fluid styled>
                    {amendDetailsAccordions}
                </Accordion> 
            </span> 
        )
    }
}

export default ReviewAmend