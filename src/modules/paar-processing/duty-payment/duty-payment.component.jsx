import React, { Component } from 'react'
import { Table, Loader, Label, Pagination, Header, Icon } from 'semantic-ui-react'
import SuccessMessage from './../../shared/message/success.component'
import Refresh from './../../shared/refresh.component'
import PaarService from './../../../services/paar_service'
import NoSearchResult from './../../shared/no-search-result.component'
import ErrorMessage from './../../shared/message/error.component'
import AuthenticationService from './../../../services/authentication_service'

class DutyPayment extends Component {

    constructor () {
        super();

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            dutyPayments: [],
            errorMessage: null,
            refreshErrorMessage: null,
            loadingMessage: null
        }
        
    }

    componentWillMount () {
        this.setState({ loadingMessage : "Loading..." })
        this.getDutyPayments();
    }

    getDutyPayments = () => {        
        this.setState ({ refreshErrorMessage: false,  searched: false })
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber,
            type: "DUTY_PAYMENT",
            status: ""
        }
        PaarService.getShippingDocuments(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    dutyPayments : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber 
                })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => { 
        this.setState({ pageNumber : activePage }, () => {              
            this.setState({ loadingMessage : "Loading..." })
            this.getDutyPayments();
        });          
    }

    refreshAction = () => {
        this.setState({ loadingMessage : "Loading..." })
        this.getDutyPayments();
    }

    clearAction = () => {
        this.setState({ pageNumber: 1, loadingMessage : "Loading..." }, () => {
            this.getDutyPayments();
        })        
    }

    render () {
        let rows = this.state.dutyPayments.length <= 0 ?
            <NoSearchResult colSpan={6} /> :
            this.state.dutyPayments.map(dutyPayment => {
                return  <Table.Row key={dutyPayment.id}>
                            <Table.Cell>
                                <Label ribbon>{dutyPayment.dutyPaymentRef}</Label>
                            </Table.Cell>                            
                            <Table.Cell>{dutyPayment.consignmentNumber}</Table.Cell>
                            <Table.Cell>{dutyPayment.documentNumber}</Table.Cell>
                            <Table.Cell>{dutyPayment.documentAmount}</Table.Cell>
                            <Table.Cell>{dutyPayment.createdOn } </Table.Cell>
                            <Table.Cell textAlign='center' collapsing>  
                                <a href={"#/duty/"+dutyPayment.id}>view</a>
                            </Table.Cell>                            
                        </Table.Row>
        })

        return (                  
            this.state.loadingMessage ?  
            <div className="ut-loader"><Loader active content={this.state.loadingMessage}/></div>:  
            this.state.refreshErrorMessage ? <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
            <div>
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
      
                {AuthenticationService.hasPermission('ADD_DUTY_PAYMENT_TO_SHIPPING_DOCUMENT_DETAILS') &&
                    <Header as='h4' textAlign='right'>
                        <a href="#/duty/new" id='create-duty-payment' className='ui grey large button'> 
                            <Icon name="book" /> New Duty Payments
                        </a>
                    </Header>
                } 

                {
                    AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') &&
                    <div style={{ paddingTop: 10 }} >
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='6'>
                                        Showing {this.state.pageNum === 1 ? this.state.pageNumber : this.state.pageNumber*this.state.pageSize - this.state.pageSize+1} - {this.state.pageNumber*this.state.pageSize > this.state.noOfRecords ? this.state.noOfRecords : this.state.pageNumber*this.state.pageSize}  of {this.state.noOfRecords} records </Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.HeaderCell>Payment Reference </Table.HeaderCell>
                                    <Table.HeaderCell>Consignment Number</Table.HeaderCell>
                                    <Table.HeaderCell>Document Number </Table.HeaderCell>
                                    <Table.HeaderCell>Amount </Table.HeaderCell>
                                    <Table.HeaderCell>Created On</Table.HeaderCell>
                                    <Table.HeaderCell collapsing>Action</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{rows}</Table.Body>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='6' textAlign="right">
                                        <Pagination pointing secondary 
                                            activePage={this.state.pageNumber} 
                                            onPageChange={this.handlePaginationChange} 
                                            totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                        />  
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>     
                    </div>
                }
            </div>                          
        )
    }
}

export default DutyPayment