import React, { Component } from 'react'
import { Checkbox, Table, Grid, Header, Segment, Breadcrumb, Form, Message, Button, Input, Icon } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component'
import PaarService from './../../../services/paar_service';
import SharedService from './../../../services/shared_service'
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import FormSearch from '../../shared/form-search.component';
import UploadAttachment from '../../shared/upload-attachment.component';

class NewDutyPayment extends Component {

    constructor (props) {
        super (props);

        this.state = {
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            formData: {
                dutyPaymentRef: "",
                id: "",
                dutyPaymentAttachmentId: ""
            },
            isLoadingForm: false,
            errorMessage: null,
            paymentInfo: "",
            upload: ""
        }
    }

    handleInputChange = (event, {name, value}) => this.setState({  
        formData: {
            ...this.state.formData,
            [name] : value
        }
    });  

    search = () => {         
        this.setState({ isLoadingForm: true, searched: true });
        let params = {
            formNumber: this.state.formNumber,
            type: "PAAR",
            status: null
        }
        PaarService.getShippingDetailsByFormNumber(params)
        .then(response => {       
            this.setState({ isLoadingForm: false });
            if (response.code) {
                this.setState ({ searched: false });
                this.alertSuccess(response.description)
            } else {
                this.setState ({ searched: true, result: response })
            }
        })        
    }
        
    handleSelectedFormChange = (e, { value }) => {
        this.setState({ 
            formData: { 
                ...this.state.formData, 
                id: value
            } 
        })
    }

    afterUpload = (data) => {
        this.setState({ 
            upload: data.data, 
            formData: {
                ...this.state.formData, 
                dutyPaymentAttachmentId: data.data.id
            } 
        }) 
    }

    afterUploadDelete = (id) => this.setState({ upload: "" })
    
    submit = () => {
        this.setState({ isLoadingForm: true})
        
        PaarService.addDutyPayment(this.state.formData)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code || response.error) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful!")
                // window.location.href="#/duty/"+this.state.formDetails.id;
                window.location.href="#/duty/"+response.identityValue;
            }
        })
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg });
        this.clearForm();
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    setSelectedForm = (selected) => {
        this.setState({ formNumber: selected }, () => { this.search()})
    }
        
    clearSelectedForm = () => this.setState({ formNumber: "" })
    
    verifyReference = () => {
        const {formData} = this.state

        this.setState({ isLoadingForm: true });
        PaarService.verifyPaymentReference(formData.dutyPaymentRef)
        .then(response => {       
            this.setState({ isLoadingForm: false });
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.setState({ paymentInfo: response.paymentData })
            }
        }) 
    }

    clearReference = () => {
        this.setState ({
            paymentInfo: "",
            formData: {
                ...this.state.formData,
                dutyPaymentRef: "",
            }
        })
    }

    clearForm = () => {

        this.setState ({
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            formData: {
                dutyPaymentRef: "",
                id: "",
                dutyPaymentAttachmentId: ""
            },
            isLoadingForm: false,
            errorMessage: null,
            paymentInfo: "",
            upload: ""
        })
    }

    deleteUpload = (id) => {
        SharedService.deleteAttachment(id)
        .then(response => {
            if (response.code) {

            } else {
                this.setState({upload: "", });
            }
        })
    }

    render () {
        const { result, isLoadingForm, errorMessage, formData, upload} = this.state;   
        let resultOptions;

        resultOptions = result.length > 0 && result.map(ecd => {
                        return  <Table.Row key={ecd.id}>
                                    <Table.Cell>
                                        <Checkbox radio name='selectedForm' value={ecd.id} checked={formData.id === ecd.id}
                                            onChange={this.handleSelectedFormChange} />
                                    </Table.Cell>                            
                                    <Table.Cell className='no-left-border'>{ecd.containerNumber}</Table.Cell>
                                    <Table.Cell>{ecd.courierCompany}</Table.Cell>
                                    <Table.Cell>{ecd.shippingCompany}</Table.Cell>
                                    <Table.Cell>{ecd.documentAmount}</Table.Cell>
                                    <Table.Cell>{ecd.createdOn } </Table.Cell>
                                    <Table.Cell textAlign='center'> 
                                        <a target="_blank" href={"#/shipping/document/"+ecd.id}>view</a>
                                    </Table.Cell>                            
                                </Table.Row>
        })
        
        return (      
            <AppWrapper> 
                <Header as='h3'>Duty Payment</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                        <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/duty">Duty Payment List</Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Add Duty Payment</Breadcrumb.Section>
                                </Breadcrumb>   <br/><br/>
                                {this.state.errorMessage && <ErrorMessage message={errorMessage} /> }
                                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }                                                
                                <Form loading={isLoadingForm}>                                    
                                    <Segment.Group> 
                                        <Segment>                                            
                                            {(!this.state.searched || (this.state.searched && this.state.errorMessage)) &&
                                                <FormSearch selected={this.setSelectedForm} clear={this.clearSelectedForm} width="5"/> }
                                            {resultOptions &&
                                            <Message onDismiss={this.clearForm}>
                                                <Message.Content>
                                                    <br/>
                                                    <Table celled>
                                                        <Table.Header>                                                        
                                                            <Table.Row>
                                                                <Table.HeaderCell></Table.HeaderCell>
                                                                <Table.HeaderCell>Container Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Courier Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Shipping Document </Table.HeaderCell>
                                                                <Table.HeaderCell>Amount </Table.HeaderCell>
                                                                <Table.HeaderCell>Created On</Table.HeaderCell>
                                                                <Table.HeaderCell>Details</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>
                                                        <Table.Body>{resultOptions}</Table.Body>
                                                    </Table>
                                                </Message.Content>
                                            </Message> }
                                        </Segment>
                                        {formData.id !== "" &&
                                        <span>
                                            <Segment>
                                                <Form.Group>
                                                    <Form.Field control={Input} width="6" label="Reference Number" placeholder='Enter Reference Number' onChange={this.handleInputChange} 
                                                        name="dutyPaymentRef" value={formData.dutyPaymentRef} readOnly={this.state.paymentInfo !== ""}/>
                                                    <Form.Field>
                                                        <label>&nbsp;</label>
                                                        {this.state.paymentInfo === "" ?
                                                        <a onClick={this.verifyReference} style={{verticalAlign: "-webkit-baseline-middle"}} className="ut-pointer"> Verify </a>:
                                                        <a onClick={this.clearReference} style={{verticalAlign: "-webkit-baseline-middle"}} className="ut-pointer"> Clear </a>}
                                                    </Form.Field>
                                                </Form.Group>
                                         
                                                {this.state.paymentInfo !== "" &&
                                                    <Form.Field>
                                                        <label> Upload Receipt </label>                                                       
                                                        {upload ?
                                                        <p>
                                                            <a href ={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <Icon name="trash" color="red" link onClick={() => this.deleteUpload(upload.id)}/> 
                                                        </p> 
                                                        :<UploadAttachment document={{document: "Duty Receipt", index: formData.dutyPaymentRef, form: "15" }} callbackForUpload={this.afterUpload} />
                                                        }
                                                    </Form.Field>
                                                }
                                                {upload !== "" && formData.paymentInfo !== "" &&
                                                    <Button size="large" floated='right' color="red" onClick={this.submit}>Submit</Button>
                                                }<p>&nbsp;</p><p>&nbsp;</p>
                                            </Segment>
                                        </span>
                                        }                                        
                                    </Segment.Group>
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default NewDutyPayment