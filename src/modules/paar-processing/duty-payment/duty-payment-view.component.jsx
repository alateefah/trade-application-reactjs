import React, { Component } from 'react';
import { Grid, Header, Segment, Breadcrumb, Table, Label} from 'semantic-ui-react';
import AppWrapper from './../../shared/main/app-wrapper.component';
import PaarService from './../../../services/paar_service';
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import DocumentDisplayModal from '../../shared/document-display-modal.component';

class DutyPaymentView extends Component {

    constructor (props) {
        super(props);

        this.state = {
            refreshMessage: null,
            shippingDocumentDetailsId: props.match.params.shippingDocumentDetailsId,
            shippingDocumentDetails: null,
            loading: true
        }
    }

    componentWillMount () { this.getDutyInfo(); }
    
    getDutyInfo = () => {
        this.setState({ shippingDocumentDetails: null }) ;
        PaarService.getDuty(this.state.shippingDocumentDetailsId)        
        .then(response => {   
            this.setState({ loading: false})  
            if (response.code) {
                this.setState({ refreshMessage : response.description })
            } else {
                this.setState({ shippingDocumentDetails : response })
            }    
            
        }) 
    }
    

    render () {
        const {loading, shippingDocumentDetails} = this.state;          

        return (      
            <AppWrapper> 
                <Header as='h3'>Duty Payment</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>

                            <Segment loading={loading} padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/duty">Duty Payment List  </Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Duty Details</Breadcrumb.Section>
                                </Breadcrumb>   <br/><br/>    
                                                                         
                                
                                {shippingDocumentDetails  && 
                                <Segment.Group>                                         
                                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                                    {shippingDocumentDetails.consignmentNumber &&
                                    <Segment>                                                    
                                        <Header> CONSIGNMENT NUMBER: {shippingDocumentDetails.consignmentNumber} </Header>
                                    </Segment>
                                    }
                                    <Segment>
                                        <Label as='a' color='grey' ribbon>Duty Details</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>  
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Form Number
                                                            <Header.Subheader>{shippingDocumentDetails.formNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Payment Reference
                                                            <Header.Subheader>{shippingDocumentDetails.dutyPaymentRef}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Amount
                                                            <Header.Subheader>{shippingDocumentDetails.dutyDetails.amount+" NGN"}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>                                                
                                                </Table.Row>
                                                <Table.Row><Table.Cell colSpan={3} ></Table.Cell></Table.Row>
                                            </Table.Body>
                                        </Table>
                                            <Label as='a' color='grey' ribbon>Shipping Details</Label>
                                        <Table basic='very' fixed>
                                            <Table.Body>  
                                                <Table.Row>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Invoice Number
                                                            <Header.Subheader>{shippingDocumentDetails.invoiceNumber}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Invoice Date
                                                            <Header.Subheader>{shippingDocumentDetails.invoiceDate}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>
                                                    <Table.Cell>
                                                        <Header as='h4'>
                                                            <Header.Content> Amount
                                                            <Header.Subheader>{shippingDocumentDetails.documentAmount + " ("+shippingDocumentDetails.currencyCode+ ")"}</Header.Subheader>
                                                            </Header.Content>
                                                        </Header>
                                                    </Table.Cell>                                                
                                                </Table.Row>
                                                <Table.Row><Table.Cell colSpan={3} ></Table.Cell></Table.Row>
                                            </Table.Body>
                                        </Table>
                                        <fieldset>                                                
                                            <legend>Uploaded Receipt</legend>   
                                            <br/>
                                            <b>Created:  </b> {shippingDocumentDetails.dutyDetails.attachment.createdAt}<br/><br/>
                                            <DocumentDisplayModal document={shippingDocumentDetails.dutyDetails.attachment} /> 
                                        </fieldset> 
                                    </Segment>                         
                                </Segment.Group>}
                                
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default DutyPaymentView