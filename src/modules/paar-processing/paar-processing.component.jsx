import React, { Component } from 'react'
import { Header, Tab, Grid, Segment } from 'semantic-ui-react'
import AppWrapper from './../shared/main/app-wrapper.component'
import ShippingDocument from './shipping-document/shipping-document.component'
import DocumentExamination from './document-examination/document-examination.component'
import ECD from './ecd/ecd.component'
import DocumentRelease from './document-release/document-release.component'
import AuthenticationService from './../../services/authentication_service'
import DutyPayment from './duty-payment/duty-payment.component';

class PaarProcessing extends Component {
    
    constructor (props) {
        super(props);
        
        this.state = {
            tab: props.location.pathname,
            activeIndex: 0
        }
    }

    componentWillMount = () => {
        const {tab} = this.state;
        
        if (tab === "/examination") {
            this.setState({ activeIndex: 1})
        } else if (tab === "/ecd") {
            this.setState({ activeIndex: 2})
        } else if (tab === "/duty") {
            this.setState({ activeIndex: 3})
        } else if (tab === "/release") {
            this.setState({ activeIndex: 4})        
        } else {
            this.setState({ activeIndex: 0})
        }
    }

    handleTabChange = (e, { activeIndex }) => {
        if (activeIndex === 1) {
            this.props.history.push('/examination');
        } else if (activeIndex === 2) {
            this.props.history.push('/ecd');
        } else if (activeIndex === 3) {
            this.props.history.push('/duty');        
        } else if (activeIndex === 4) {
            this.props.history.push('/release');
        } else {
            this.props.history.push('/paar');
        }
        this.setState({ activeIndex });
    }

    render () {

        let panes = [
            { menuItem: 'PAAR', render: () =>                 
                <Tab.Pane attached={false} id='paar-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') && <ShippingDocument /> }
                </Tab.Pane>                 
            },
            { menuItem: 'Document Examination', render: () => 
                <Tab.Pane attached={false} id='document-examination-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') && <DocumentExamination /> }
                </Tab.Pane> 
            },
            { menuItem: 'ECD', render: () => 
                <Tab.Pane attached={false} id='ecd-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') && <ECD /> }
                </Tab.Pane> 
            },
            { menuItem: 'Duty Payment', render: () => 
                <Tab.Pane attached={false} id='duty-payment-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') && <DutyPayment /> }
                </Tab.Pane> 
            },
            { menuItem: 'Document Release', render: () => 
                <Tab.Pane attached={false} id='document-release-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') && <DocumentRelease /> }
                </Tab.Pane> 
            }
        ]

        return (            
            <AppWrapper> 
                <Header as='h3'>PAAR Processing</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment>
                                <Tab 
                                    menu={{ secondary: true, pointing: true }} 
                                    panes={panes} className="user-mgt-tab"
                                    activeIndex={this.state.activeIndex} 
                                    onTabChange={this.handleTabChange}
                                />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default PaarProcessing