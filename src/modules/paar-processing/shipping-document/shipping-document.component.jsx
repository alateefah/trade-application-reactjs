import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import { Table, Label, Loader, Pagination, Header, Icon, Select, Form, Button } from 'semantic-ui-react'
import Refresh from './../../shared/refresh.component'
import SuccessMessage from './../../shared/message/success.component'
import NoSearchResult from './../../shared/no-search-result.component'
import ErrorMessage from './../../shared/message/error.component'
import AuthenticationService from './../../../services/authentication_service'
import PaarService from './../../../services/paar_service'

class ShippingDocument extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            shippingDocuments: [],
            errorMessage: null,
            refreshErrorMessage: null,
            loadingMessage: null,
            paarStatus: ""
        }
    }

    componentWillMount () { this.getShippingDocuments(); }

    getShippingDocuments = () => {       
        this.setState({ loadingMessage : "Loading..." }) 
        this.setState ({ refreshErrorMessage: false,  searched: false })
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber,
            type: "paar",
            status: this.state.paarStatus
        }
        PaarService.getShippingDocuments(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    shippingDocuments : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber 
                })
            }         
            
        }) 
    }

    filterShippingDocuments = () => {
        this.setState({ pageNumber: 1 }, () => {
            this.getShippingDocuments();
        });
    }

    handlePaginationChange = (e, { activePage }) => { 
        this.setState({ pageNumber : activePage }, () => {              
            // this.setState({ loadingMessage : "Loading..." })
            this.getShippingDocuments();
        });          
    }

    refreshAction = () => {
        this.setState({ loadingMessage : "Loading..." })
        this.getShippingDocuments();
    }

    clearAction = () => {
        this.setState({ pageNumber: 1, loadingMessage : "Loading..." }, () => {
            this.getShippingDocuments();
        })        
    }

    handleSelectChange = (e, {value}) => this.setState({ paarStatus: value }) 

    render () {
        let rows = this.state.shippingDocuments.length <= 0 ?
            <NoSearchResult colSpan={8} /> :
            this.state.shippingDocuments.map(shippingDocument => {
                return  <Table.Row key={shippingDocument.id}>
                            <Table.Cell >
                                <Label ribbon>{shippingDocument.formNumber}</Label>
                            </Table.Cell>                            
                            <Table.Cell >{shippingDocument.consignmentNumber}</Table.Cell>
                            <Table.Cell >{shippingDocument.documentNumber}</Table.Cell>
                            <Table.Cell >{shippingDocument.containerNumber}</Table.Cell>
                            <Table.Cell >{shippingDocument.createdOn }</Table.Cell>
                            <Table.Cell >{shippingDocument.documentAmount}</Table.Cell>
                            <Table.Cell >{shippingDocument.paarStatus }</Table.Cell>
                            <Table.Cell textAlign='center' collapsing>  
                                <Link to={"shipping/document/"+shippingDocument.id}> view </Link>
                            </Table.Cell>                            
                        </Table.Row>
        })
        
        let statusOption = [            
            { key: "PENDING", text: "PENDING", value: "PENDING" },
            { key: "COMPLETED", text: "COMPLETED", value: "COMPLETED"},
            { key: "REJECTED", text: "REJECTED", value: "REJECTED" }
        ]

        return (      
            this.state.loadingMessage ?  
                <div className="ut-loader"><Loader active content={this.state.loadingMessage}/></div>:  
                this.state.refreshErrorMessage ? <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
                <div>
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }

                    {AuthenticationService.hasPermission('CREATE_SHIPPING_DOCUMENT_DETAILS_RECORD') &&
                        <Header as='h4' textAlign='right'>
                            <a href="#/shipping/document/new" id='create-shipping-document' className='ui grey large button'> 
                                <Icon name="book" /> Upload Shipping Documents 
                            </a>
                        </Header>
                    }       
                    {
                        AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') &&
                        <div style={{ paddingTop: 10 }} >
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='8'>                                            
                                            Showing {this.state.pageNum === 1 ? this.state.pageNumber : this.state.pageNumber*this.state.pageSize - this.state.pageSize+1} - {this.state.pageNumber*this.state.pageSize > this.state.noOfRecords ? this.state.noOfRecords : this.state.pageNumber*this.state.pageSize}  of {this.state.noOfRecords} records 
                                        
                                            <Form className="table-filter">
                                                <Form.Group inline>
                                                    <Form.Field>Filter</Form.Field>
                                                    <Form.Field control={Select} options={statusOption} value = {this.state.paarStatus} onChange = {this.handleSelectChange}/>
                                                    <Form.Field><Button content="Go" onClick={this.filterShippingDocuments}/></Form.Field>
                                                </Form.Group>
                                            </Form>
                                        </Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>                                        
                                        <Table.HeaderCell>Form Number</Table.HeaderCell>
                                        <Table.HeaderCell >Consignment Number</Table.HeaderCell>
                                        <Table.HeaderCell>Document Number </Table.HeaderCell>
                                        <Table.HeaderCell >Container Number </Table.HeaderCell>
                                        <Table.HeaderCell >Created On</Table.HeaderCell>
                                        <Table.HeaderCell>Amount </Table.HeaderCell>
                                        <Table.HeaderCell >Status</Table.HeaderCell>
                                        <Table.HeaderCell collapsing>Action</Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>{rows}</Table.Body>
                                <Table.Footer>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='8' textAlign="right">
                                            <Pagination pointing secondary 
                                                activePage={this.state.pageNumber} 
                                                onPageChange={this.handlePaginationChange} 
                                                totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                            />  
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>     
                        </div>
                    }
                </div>                                      
        )
    }
    
}

export default ShippingDocument