import React, { Component } from 'react';
import { Form, Modal, TextArea, Button, Input, Icon } from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import UploadAttachment from './../../shared/upload-attachment.component'
import ErrorMessage from './../../shared/message/error.component';
import SuccessMessage from './../../shared/message/success.component';
import PaarService from './../../../services/paar_service';
import SharedService from './../../../services/shared_service';

class ReviewShippingDocument extends Component {

    constructor (props) {
        super(props);

        this.state = {
            value: "",
            comment: "",
            errorMessage: null,
            loading: false,
            id: props.id,
            issuedDate: moment(),
            paarAttachmentId: "",
            paarNumber:"",
            upload: "",
            pendingNcsApproval: props.pendingNcsApproval,
            open: false
        }        
        
    }

    componentWillReceiveProps(nextProps) {
        this.setState({ pendingNcsApproval: nextProps.pendingNcsApproval });  
    }

    afterUpload = (data) => this.setState({ upload: data.data, paarAttachmentId: data.data.id})  

    handleChange = (e, { value }) => this.setState({ value : value })

    handleCommentChange = (e, { value }) => this.setState({ comment : value })

    reject = () => {
        this.setState({loading: true})
        let params = {
            id: this.state.id,
            paarComment: this.state.comment,
            ncsApprovalId: this.state.pendingNcsApproval.id
        }
        PaarService.NCSDocumentRequery(params)
        .then(response => {
            this.setState({loading: false})
            if (response.code) {
                this.setState({ errorMessage: response.description});
                setTimeout(() => this.setState({ errorMessage: null }), 4000);
            } else {
                this.handleSuccess();
            }
        })
    }

    handleOpen = () => { this.setState({ open: true})}

    handleClose = () => {
        this.setState({ 
            comment: "",
            errorMessage: null,
            loading: false,
            value: "",
            open: false
        })
    }

    approve = () => {
        const {id, paarNumber, paarAttachmentId, issuedDate} = this.state;

        this.setState({loading: true})
        let params = {
            id: id,
            paarNumber: paarNumber,
            paarAttachmentId: paarAttachmentId,
            paarIssuedDate: issuedDate.format('DD-MM-YYYY'),
            ncsApprovalId: this.state.pendingNcsApproval.id
        }

        PaarService.NCSDocumentApproval(params)
        .then(response => {
            this.setState({loading: false})
            if (response.code) {
                this.setState({ errorMessage: response.description});
                setTimeout(() => this.setState({ errorMessage: null }), 4000);
            } else {
                this.handleSuccess();
            }
        })
    }
    
    handleIssuedDateChange = (date) => this.setState ({ issuedDate: date }); 

    handleInputChange = (e, { name, value }) => this.setState({ [name]: value });

    handleSuccess = () => {
        this.handleClose();
        this.props.successfulUpdate();
        
    }

    deleteUpload = (id) => {
        SharedService.deleteAttachment(id)
        .then(response => {
            if (response.code) {

            } else {
                this.setState({paarAttachmentId: "", upload: ""});
            }
        })
    }

    render () {
        const {upload} = this.state
        return (   
            <span>                 
                <Modal onClose={this.handleClose} 
                    trigger={<Button color="red" onClick={this.handleOpen}>{this.props.triggerTitle}</Button>} 
                    open={this.state.open}
                    size='tiny' closeIcon>
                    <Modal.Header>Add NCS Review</Modal.Header>
                    <Modal.Content>
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                        <Form loading={this.state.loading}>
                            <Form.Group inline>
                                <Form.Radio label='Approved' value="true" checked={this.state.value === "true"} onChange={this.handleChange} name='approvalStatus' />
                                <Form.Radio label='Rejected' value="false" checked={this.state.value === "false"} onChange={this.handleChange} name='approvalStatus' />
                            </Form.Group>
                            {this.state.value === "true" &&
                            <span>
                                <div>
                                    <Form.Field control={Input} label="PAAR Number"  placeholder="PAAR Number" 
                                        name = 'paarNumber' value = {this.state.paarNumber} id = 'shipping-doc-paarNumber' onChange = {this.handleInputChange} /> 
                                    <Form.Field>
                                        <label> Approval Attachment</label>
                                        {this.state.paarAttachmentId ?
                                        <p>
                                            <a href ={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <Icon name="trash" color="red" link onClick={() => this.deleteUpload(upload.id)}/> </p> :
                                        <UploadAttachment document={{document: "PAAR Document", index: "1", form: "15" }} callbackForUpload={this.afterUpload} />                                        
                                        }
                                    </Form.Field>
                                    <Form.Field>
                                        <label>Paar Issued Date</label>
                                        <DatePicker                                
                                            placeholderText='Issued Date'
                                            dateFormat="YYYY-MM-DD"
                                            selected={this.state.issuedDate}
                                            onChange={this.handleIssuedDateChange}
                                            id = "shipping-doc-invoiceDate" 
                                        />
                                    </Form.Field>
                                </div>
                            </span> 
                            }
                            {this.state.value === "false" &&
                            <Form.Field>
                                <Form.Field required control={TextArea} rows={5} value={this.state.comment} onChange={this.handleCommentChange} 
                                    label='Reason' placeholder='Reason for rejection...' />
                            </Form.Field>}
                            <br/>
                        </Form>                        
                    </Modal.Content>
                    {this.state.paarAttachmentId !== "" 
                    && this.state.issuedDate !== null
                    && this.state.paarNumber !== ""
                    && <Modal.Actions><Button onClick={this.approve} positive content='Save' /></Modal.Actions>}
                    {this.state.comment !== "" 
                    && <Modal.Actions><Button onClick={this.reject} positive content='Save' /></Modal.Actions>}
                </Modal> 
            </span>        
        )
    }
    
}


export default ReviewShippingDocument