import React, { Component } from 'react'
import { Grid, Header, Segment, Breadcrumb, Form, Input, Icon, Button, Label, Dropdown, Feed } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import UploadRow from './upload-row.component'
import PaarService from './../../../services/paar_service'
import UtilService from './../../../services/util_service'
import ErrorMessage from '../../shared/message/error.component'
import SuccessMessage from '../../shared/message/success.component'
import ReviewShippingDocument from './review-shipping-document.component'
import AuthenticationService from './../../../services/authentication_service'
import SharedService from './../../../services/shared_service'
import DocumentDisplayModal from './../../shared/document-display-modal.component'

var _ = require("lodash")

class EditShippingDocument extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            errorMessage: null,
            successMessage: null,
            refreshErrorMessage: null,
            loadingMessage: null,
            formData: {
                containerNumber: "",
                courierCompany: "",
                documentAmount: "",
                documentNumber: "",
                shippingDocuments: "",
                invoiceNumber: "",
                negotiationBankName: "",
                numberOfContainers: "",
                shippingCompany: "",
                formType: "",
                formId: "",
                consignmentNumber: "",
                paarStatus: "",
                senderReference: "",
                currency: "",
                documentExaminationInitiated: null,
                ecdInitiated: null,
                paarInitiated: null,
                releaseInitiated: null,
            },
            invoiceDate: moment(),
            isLoadingForm: false,
            newRow: "",
            index: 0,
            upload: "",
            copySet: false,
            copySetUploads: [], 
            bankSetUploads: [], 
            advanceSetUploads: [],     
            copySetValue: "Copy Set",
            bankSetValue: "Bank Set",
            advanceSetValue: "Advance Set",
            bankSet: false,
            advanceSet: false,
            documentTypes: [],
            shippingDocumentDetails: null,
            shippingDocumentDetailsId: props.match.params.shippingDocumentDetailsId,
            copySetDocuments: [],
            bankSetDocuments: [],
            advanceSetDocuments: [],
            saving: false,
            currencies: null,
            pendingNcsApproval: null
        }
    }

    componentWillMount () { 
        this.getCurrencies();
        this.getDocumentTypes(); 
        this.getShippingDocumentDetails();
    }
    
    getCurrencies = () => {
        SharedService.getAppParameter('CURRENCIES')
        .then(response => {      
            if (!response.code) {
                let jsonResponse = response.value;
                this.setState ({ 
                    currencies: jsonResponse.split(",")
                })
            }
        })
    }

    getDocumentTypes = () => {
        PaarService.getDocumentTypes()
        .then(response => {
            if (response.code) {
                this.setState({ errorMessage: response.description})
            } else {
                this.setState({ documentTypes: response.list })
            }
        })
    }

    getShippingDocumentDetails = () => {
        this.setState ({ shippingDocumentDetails: null, pendingNcsApproval: null, isLoadingForm: true });
        
        PaarService.getShippingDocumentDetails(this.state.shippingDocumentDetailsId)        
        .then(response => {   
            
            if (response.code) {
                this.setState({ errorMessage : response.description })
            } else {
                this.setState({ 
                    shippingDocumentDetails : response,
                    formData: {
                        containerNumber: response.containerNumber,
                        courierCompany: response.courierCompany,
                        documentAmount: response.documentAmount,
                        documentNumber: response.documentNumber,
                        shippingDocuments: response.shippingDocuments,
                        invoiceNumber: response.invoiceNumber,
                        negotiationBankName: response.negotiationBankName,
                        numberOfContainers: response.numberOfContainers,
                        shippingCompany: response.shippingCompany,
                        formType: response.formType,
                        formId: response.formId,
                        consignmentNumber: response.consignmentNumber ? response.consignmentNumber: "",
                        paarStatus: response.paarStatus,
                        senderReference: response.senderReference,
                        currency: response.currency,
                        documentExaminationInitiated: response.documentExaminationInitiated,
                        ecdInitiated: response.ecdInitiated,
                        paarInitiated: response.paarInitiated,
                        releaseInitiated: response.releaseInitiated,
                    },   
                    paarStatus: response.paarStatus,    
                    invoiceDate: moment(response.invoiceDate),                
                    copySetDocuments: response.shippingDocuments.filter( record => record.documentSet === this.state.copySetValue ),
                    bankSetDocuments: response.shippingDocuments.filter( record => record.documentSet === this.state.bankSetValue ),
                    advanceSetDocuments: response.shippingDocuments.filter( record => record.documentSet === this.state.advanceSetValue ),
                    pendingNcsApproval: response.ncsApprovals.find(approval => approval.approvalStatus === "PENDING")
                })
                this.setState({ isLoadingForm : false })

            }    
            
        }) 
    }

    handleChange = (event, {name, value}) =>  this.setState({  [name] : value });  

    handleInputChange = (event, {name, value}) => {     
        if (name === "documentAmount" || name === "numberOfContainers") {
            if (UtilService.validNumberOnly (value)) {
                this.setState ({ 
                    formData: {
                        ...this.state.formData,
                        [name] : value 
                    }
                });
            }
        } else {    
            this.setState({ 
                formData: {
                    ...this.state.formData,
                    [name] : value 
                }
            });
        }          
    }

    handleInvoiceDateChange = (date) => { this.setState ({ invoiceDate: date }); }

    setUploadSet = (type) => {
        let a = this.state.index
        this.setState({            
            upload: <UploadRow 
                key={a} info={{formInfoId: 15, index: a}} documentTypes={this.state.documentTypes}
                callbackForUpload = {(data)=>this.afterUpload(data, type)}
                callbackForDelete = {(data)=>this.afterUploadDelete(data, type)} />,
            index: this.state.index + 1,
        }, () => {
            this.addMoreUpload(type);
        })
    }

    afterUpload = (data, type) => {
        let shippingDocument = {
            documentSet: type,
            documentTypeId: data.documentType,
            attachmentId: data.attachment.id
        }
        let newArray = [ ...this.state.formData.shippingDocuments, shippingDocument ];
        this.setState({ 
            formData: {
                ...this.state.formData,
                shippingDocuments: newArray
            }
        });
    }

    afterUploadDelete = (id, type) => {

        var newArray = this.state.formData.shippingDocuments;
        var index = _.findIndex(newArray, ["attachmentId", id])
        newArray.splice(index, 1);

        this.setState({ 
            formData: {
                ...this.state.formData,
                shippingDocuments: newArray
            }
        }); 
    }

    addMoreUpload = (type) => {   
        if (type === this.state.copySetValue) {

            let newArray = [ ...this.state.copySetUploads,  this.state.upload];
            this.setState({ copySetUploads: newArray}); 

        } else if (type === this.state.bankSetValue) {

            let newArray = [ ...this.state.bankSetUploads,  this.state.upload];
            this.setState({ bankSetUploads: newArray}); 

        } else if (type === this.state.advanceSetValue) {

            let newArray = [ ...this.state.advanceSetUploads,  this.state.upload];
            this.setState({ advanceSetUploads: newArray}); 

        }
    
    }

    validForm = () => {
        const {formData, shippingDocumentDetails} = this.state
        this.setState ({ errorMessage: null })

        let containerNumber = formData.containerNumber;
        let courierCompany = formData.courierCompany;
        let documentAmount = formData.documentAmount;
        let documentNumber = formData.documentNumber;
        let shippingDocuments = formData.shippingDocuments;
        let invoiceNumber = formData.invoiceNumber;
        let invoiceDate = this.state.invoiceDate;
        let negotiationBankName =formData.negotiationBankName;
        let numberOfContainers = formData.numberOfContainers;
        let shippingCompany = formData.shippingCompany;
        let senderReference = formData.senderReference;
        let consignmentNumber = formData.consignmentNumber;

        if (shippingDocumentDetails.paarStatus === "AWAIT_NCS_APPROVAL" && (consignmentNumber === "" || consignmentNumber === null)) {
            this.alertError("Consignment Number must not be empty");
            return false;
        }

        if ( containerNumber == null || containerNumber === "") {            
            this.alertError("Container Number required");
            return false;
        }
        if ( courierCompany == null || courierCompany === "") {            
            this.alertError("Courier Company required");
            return false;
        }
        if ( documentAmount == null || documentAmount === "") {            
            this.alertError("Document Amount required")
            return false;
        }
        if ( documentNumber == null || documentNumber === "") {            
            this.alertError("Document Number required")
            return false;
        }
        if ( invoiceNumber == null || invoiceNumber === "") {            
            this.alertError("Invoice Number required")
            return false;
        }
        if ( shippingDocuments.length === 0 && this.state.copySetDocuments.length === 0
            && this.state.bankSetDocuments.length === 0 && this.state.advanceSetDocuments.length === 0) {            
                this.alertError("At least 1 shipping document is required")
            return false;
        }
        if ( invoiceDate === null || invoiceDate === "") {            
            this.alertError("Invoice Date required")
            return false;
        }
        if ( negotiationBankName === null || negotiationBankName === "") {            
            this.alertError("Negotiation Bank Name required")
            return false;
        }
        if ( numberOfContainers === null || numberOfContainers === "") {            
            this.alertError("Number Of Containers is required")
            return false;
        }
        if ( shippingCompany === null || shippingCompany === "") {            
            this.alertError("Shipping Company is required")
            return false;
        }
        if ( senderReference == null || senderReference === "") {            
            this.alertError("Sender Reference is required")
            return false;
        }

        return true;
    }

    submit = () => {
        if (this.validForm()) {            
            this.setState ({ isLoadingForm: true })
            let data = this.state.formData;
            data.invoiceDate = this.state.invoiceDate.format('YYYY-MM-DD');
            data.id = this.state.shippingDocumentDetailsId;
            data.currencyCode = data.currency ? data.currency: "NGN"
            
            PaarService.updateShippingDocument(data)
            .then(response => {                
                if (response.code) {
                    this.alertError(response.description)
                } else {
                    this.alertSuccess("Update Successful");
                }   
            })
        } 

    }

    deleteUpload = (id, type) => {
        PaarService.deleteShippingDocumentAttachment(id)
        .then(response => {
            if (response.code) {

            } else {
                this.afterAttachmentDelete(id, type);
            }
        })
    }

    mapdocumentTypeIdToName = (id) => {
        const result = this.state.documentTypes.find( record => record.id === parseInt(id, 10) );
        return result.name;
    }

    afterAttachmentDelete = (id, type) => {        
        let newArray = [];
        if (type === this.state.copySetValue) {
            newArray = this.state.copySetDocuments
        } else if (type === this.state.bankSetValue) {
            newArray = this.state.bankSetValue
        } else if (type === this.state.advanceSetValue) {
            newArray = this.state.advanceSetValue
        }
        var index = _.findIndex(newArray, ["id", id])
        newArray.splice(index, 1);

        this.setState({ copySetDocuments: newArray }); 
    }

    saveConsignmentNumber = () => {
        this.setState({ saving: true })
        let params = {
            id: this.state.shippingDocumentDetailsId,
            consignmentNumber: this.state.formData.consignmentNumber
        }
        PaarService.saveConsigmentNumber(params)
        .then(response => {
            this.setState({ saving: false })
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful")
            }
        })
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg, uploads: [] , copySetUploads: [], advanceSetUploads: [], bankSetUploads: [] });
        this.getShippingDocumentDetails();
        window.scrollTo(0, 0);
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ isLoadingForm: false, errorMessage: msg });
        window.scrollTo(0, 0);
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }
   
    render () {
        const { isLoadingForm, copySetUploads, bankSetUploads, advanceSetUploads, copySetValue, bankSetValue, 
            advanceSetValue, copySetDocuments, bankSetDocuments, advanceSetDocuments, documentTypes, currencies, shippingDocumentDetails, formData, pendingNcsApproval} = this.state;
        let copySetDocumentsOptions, bankSetDocumentsOptions, advanceSetDocumentsOptions, currenciesOptions = [];      

        if (documentTypes.length > 0) {
            if (copySetDocuments && copySetDocuments.length > 0) {
                copySetDocumentsOptions = copySetDocuments.map(copySetDocument => {                    
                    return  <Form.Group key={copySetDocument.id}>
                                <Form.Field width={5}>
                                    {this.mapdocumentTypeIdToName(copySetDocument.attachment.documentType)} 
                                </Form.Field>
                                <Form.Field width={8}>                        
                                    <a href ={copySetDocument.attachment.documentLink} download={copySetDocument.attachment.documentName} target="_blank">{copySetDocument.attachment.documentName}</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {shippingDocumentDetails 
                                        && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "") 
                                        && <Icon name="trash" color="red" link onClick={() => this.deleteUpload(copySetDocument.id, copySetValue)}/> 
                                    }                             
                                </Form.Field>
                            </Form.Group>
                })
            }

            if (bankSetDocuments && bankSetDocuments.length > 0) {
                bankSetDocumentsOptions = bankSetDocuments.map(bankSetDocument => {
                    return  <Form.Group key={bankSetDocument.id}>
                                <Form.Field width={5}>
                                    {this.mapdocumentTypeIdToName(bankSetDocument.attachment.documentType)}
                                </Form.Field>
                                <Form.Field width={8}>                        
                                    <a href ={bankSetDocument.attachment.documentLink} download={bankSetDocument.attachment.documentName} target="_blank">{bankSetDocument.attachment.documentName}</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {shippingDocumentDetails 
                                        && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "")
                                        && <Icon name="trash" color="red" link onClick={() => this.deleteUpload(bankSetDocument.id, bankSetValue)} />
                                    }                                    
                                </Form.Field>
                            </Form.Group>
                })
            }

            if (advanceSetDocuments && advanceSetDocuments.length > 0) {
                advanceSetDocumentsOptions = advanceSetDocuments.map(advanceSetDocument => {
                    return  <Form.Group key={advanceSetDocument.id}>
                                <Form.Field width={5}>
                                    {this.mapdocumentTypeIdToName(advanceSetDocument.attachment.documentType)}
                                </Form.Field>
                                <Form.Field width={8}>                        
                                    <a href ={advanceSetDocument.attachment.documentLink} download={advanceSetDocument.attachment.documentName} target="_blank">{advanceSetDocument.attachment.documentName}</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    {shippingDocumentDetails 
                                    && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "") 
                                    && <Icon name="trash" color="red" link onClick={() => this.deleteUpload(advanceSetDocument.id, advanceSetValue)}/>}
                                </Form.Field>
                            </Form.Group>
                })
            }
        }

        if (currencies && currencies.length > 0) {
            currenciesOptions = currencies.map(currency => (
                { key: currency, text: currency, value: currency }
            ));
        }

        return (      
            <AppWrapper> 
                <Header as='h3'>PAAR Processing</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/paar">PAAR Processing </Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active> Update Shipping Document</Breadcrumb.Section>
                                </Breadcrumb>  
                                
                                <br/><br/>                                      
                                <Form loading={isLoadingForm}>
                                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }

                                    <Segment.Group>       
                                    
                                        <Segment>
                                            {shippingDocumentDetails && <Label as='a' color='grey' ribbon>FORM NUMBER: {shippingDocumentDetails.formNumber}</Label>}
                                            <div className="rhs-status">
                                                {this.state.paarStatus  === "AWAIT_NCS_FEEDBACK"  &&
                                                 AuthenticationService.hasPermission("UPDATE_SHIPPING_DOCUMENT_RECORD_ADD_PAAR_FROM_NCS") &&
                                                 <ReviewShippingDocument 
                                                 triggerTitle="Add NCS Review"
                                                     pendingNcsApproval = {pendingNcsApproval}
                                                     id = {this.state.shippingDocumentDetailsId} 
                                                     successfulUpdate={()=>this.alertSuccess("Update Success")} />
                                                }

                                                {this.state.paarStatus  === "REJECTED" &&
                                                 AuthenticationService.hasPermission("UPDATE_SHIPPING_DOCUMENT_RECORD_ADD_PAAR_FROM_NCS") &&
                                                 <ReviewShippingDocument 
                                                    triggerTitle="Rejected. Review Again" 
                                                    pendingNcsApproval = {pendingNcsApproval}
                                                    id = {this.state.shippingDocumentDetailsId} 
                                                    successfulUpdate={()=>this.alertSuccess("Update Success")} />
                                                }

                                                {this.state.paarStatus  !== "AWAIT_NCS_FEEDBACK"  && this.state.paarStatus  !== "REJECTED" &&
                                                    <Label size="large" color={UtilService.statusColor(this.state.paarStatus)}>{this.state.paarStatus}</Label>}
                                                
                                            </div>           
                                            <Form.Group inline>                                            
                                                <Form.Field width={2}>
                                                    <label>Consignment Number</label>
                                                </Form.Field>
                                                <Form.Field width={6} control={Input} placeholder="Consignment Number" 
                                                    name='consignmentNumber' value={formData.consignmentNumber} id='shipping-doc-consignmentNumber' onChange = {this.handleInputChange} 
                                                />      
                                                {formData.consignmentNumber !== "" && formData.paarStatus === "PENDING" &&
                                                <Form.Field width={2}>
                                                    <Button content="Save" loading={this.state.saving} onClick={this.saveConsignmentNumber} color="teal"/>
                                                </Form.Field>
                                                }                                        
                                            </Form.Group>                                            
                                        </Segment>

                                        <Segment>                                            
                                            <Form.Group widths='equal'>                                                
                                                <Form.Field control={Input} label="Negotiation Bank Name" placeholder="Negotiation Bank Name" 
                                                    name = 'negotiationBankName' value = {formData.negotiationBankName} id = 'shipping-doc-negotiationBank' onChange = {this.handleInputChange} />
                                                <Form.Field control={Input} label="Sender Reference" placeholder="Sender Reference" 
                                                    name = 'senderReference' value = {formData.senderReference} id='shipping-doc-senderReference' onChange = {this.handleInputChange} />
                                                <Form.Field>
                                                    <label> Amount </label>
                                                    <Input label={<Dropdown defaultValue={formData.currency ? formData.currency: "NGN"} options={currenciesOptions} name="currency" onChange={this.handleInputChange} />}
                                                        placeholder="Document Amount"  name = 'documentAmount' labelPosition='right' value = {formData.documentAmount} id = 'shipping-doc-documentAmount' onChange = {this.handleInputChange} />
                                                </Form.Field>
                                            </Form.Group>
                                            <Form.Group widths='equal'>
                                                <Form.Field control={Input} label="Document Number" placeholder="Document Number" 
                                                    name = 'documentNumber' value = {formData.documentNumber} id = 'shipping-doc-documentNumber' onChange = {this.handleInputChange} />
                                                <Form.Field control={Input} label="Invoice Number"  placeholder="Invoice Number" 
                                                    name = 'invoiceNumber' value = {formData.invoiceNumber} id = 'shipping-doc-invoiceNumber' onChange = {this.handleInputChange}/>
                                                <Form.Field>
                                                    <label>Invoice Date</label>
                                                    <DatePicker                                
                                                        placeholderText='Invoice Date'
                                                        dateFormat="YYYY-MM-DD"
                                                        selected={this.state.invoiceDate}
                                                        onChange={this.handleInvoiceDateChange}
                                                        id = "shipping-doc-invoiceDate" 
                                                    />
                                                </Form.Field>
                                            </Form.Group>
                                            <Form.Group >
                                                <Form.Field width={6} control={Input} label="Courier Company"  placeholder="Courier Company" id='shipping-doc-courierCompany'
                                                    name = 'courierCompany' value = {formData.courierCompany} onChange = {this.handleInputChange} />
                                                <Form.Field width={6} control={Input} label="Shipping Company"  placeholder="Shipping Company" 
                                                    name = 'shippingCompany' value = {formData.shippingCompany} id = 'shipping-doc-shippingCompany' onChange = {this.handleInputChange}/>
                                                <Form.Field width={4} control={Input} label="Container Number" placeholder="Container Number" 
                                                    name = 'containerNumber' value = {formData.containerNumber} id = 'shipping-doc-containerNumber' onChange = {this.handleInputChange} />
                                                <Form.Field width={2} control={Input} label="No of Containers"  placeholder="Number of Containers" 
                                                    name = 'numberOfContainers' value = {formData.numberOfContainers} id = 'shipping-doc-numberOfContainers' onChange = {this.handleInputChange} />
                                            </Form.Group>        
                                            
                                            <p>&nbsp;</p>
                                            <Header as='h3'>Uploads</Header> 
                                            <fieldset>
                                                <legend>Copy Set</legend>
                                                <p>&nbsp;</p>
                                                {shippingDocumentDetails 
                                                && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "") 
                                                && <Header as='h5' onClick={() => this.setUploadSet(copySetValue)} className="ut-pointer">
                                                    <Icon name='add circle' color = "green" /> Add
                                                </Header>}     
                                                {copySetDocumentsOptions}                                           
                                                {copySetUploads}
                                            </fieldset>
                                            <br/>
                                            
                                            <fieldset>
                                                <legend>Bank Set</legend>
                                                <p>&nbsp;</p>
                                                {shippingDocumentDetails 
                                                && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "")
                                                &&<Header as='h5' onClick={() => this.setUploadSet(bankSetValue)} className="ut-pointer">
                                                    <Icon name='add circle' color = "green" /> Add
                                                </Header>} 
                                                {bankSetUploads}  
                                                {bankSetDocumentsOptions} 
                                            </fieldset>
                                            <br/>
                                            
                                            <fieldset>
                                                <legend>Advance Set</legend>
                                                <p>&nbsp;</p>
                                                {shippingDocumentDetails 
                                                && (shippingDocumentDetails.documentExaminationStatus === "PENDING" || shippingDocumentDetails.documentExaminationStatus === "")
                                                && <Header as='h5' onClick={() => this.setUploadSet(advanceSetValue)} className="ut-pointer">
                                                    <Icon name='add circle' color = "green" /> Add
                                                </Header>}          
                                                {advanceSetUploads}
                                                {advanceSetDocumentsOptions} 
                                            </fieldset>
                                            <p>&nbsp;</p>                                            
                                            {shippingDocumentDetails 
                                                && shippingDocumentDetails.paarStatus !== "PENDING"
                                                && shippingDocumentDetails.paarStatus !== "AWAIT_NCS_APPROVAL"
                                                && <span>
                                                     
                                                    {shippingDocumentDetails.paarNumber 
                                                        &&  shippingDocumentDetails.paarNumber.length > 0
                                                        &&  <span>
                                                                <Header as='h3'>NCS Approval</Header>
                                                                <b>PAAR Number: </b> {shippingDocumentDetails.paarNumber}<br/>
                                                                <b>PAAR Issued Date: </b> {shippingDocumentDetails.paarIssuedDate}<br/>
                                                                <b>Attachment: </b>
                                                                    <DocumentDisplayModal document={shippingDocumentDetails.ncsPAARAttachment} />
                                                            </span>
                                                    }

                                                    {(shippingDocumentDetails.paarStatus === "REJECTED" 
                                                    || shippingDocumentDetails.ncsApprovals.length > 1) 
                                                        &&  <Feed>
                                                                {shippingDocumentDetails.ncsApprovals 
                                                                    && shippingDocumentDetails.ncsApprovals.map((approval, index) => {
                                                                        return  approval.approvalStatus !== "PENDING" &&
                                                                                <Feed.Event key={index}
                                                                                    icon="pencil"
                                                                                    date={approval.approvedDate}
                                                                                    summary={approval.approvedBy +" "+ approval.approvalStatus+" request."}
                                                                                    extraText= {approval.approverComments}
                                                                                />
                                                                        })
                                                                            
                                                                }           
                                                            </Feed>
                                                    }
                                                </span>
                                            }
                                            <p>&nbsp;</p>
                                            <Button size="large" floated='right' color="red" onClick={this.submit}>Update</Button>
                                            <p>&nbsp;</p><p>&nbsp;</p>
                                        </Segment> 
                                    </Segment.Group>

                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default EditShippingDocument