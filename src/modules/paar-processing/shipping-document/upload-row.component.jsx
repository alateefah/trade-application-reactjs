import React, { Component } from 'react'
import { Form, Icon} from 'semantic-ui-react'
import SharedService from './../../../services/shared_service'
import UploadAttachment from './../../shared/upload-attachment.component'

class UploadRow extends Component {

    constructor (props) {
        super(props);

        this.state = {
            documentTypes: props.documentTypes,
            documentType: "",
            info: props.info, 
            uploaded: false,
            uploadedDocumentType: "",
            documentLink: "",
            documentName: "",
            attachmentId: "",
            uploadedDocumentTypeName: ""
        }
    }

    handleSelectChange = (e, { value }) => {
        this.setState({ documentType: value })        
        const result = this.state.documentTypes.find( record => record.id === value );
        this.setState ({
            uploadedDocumentTypeName: result.name 
        })

    }

    afterUpload = (response) => {
        let data = response.data;
        this.setState({ 
            uploaded: true, 
            documentLink: data.documentLink,
            documentName: data.documentName,
            attachmentId: data.id,
        })
        let params = {
            index: this.state.info.index,
            documentType: this.state.documentType,
            attachment: data
        }
        this.props.callbackForUpload(params);
    }

    deleteAttachment = (id) => {
        this.setState({ loadingMessage : "Deleting..." })
        SharedService.deleteAttachment(id)
        .then(response => { 
            if (response.code) {  
                               
            } else {  
                this.setState ({
                    uploaded: false,
                    uploadedDocumentType: "",
                    documentLink: "",
                    documentName: "",
                    attachmentId: "",
                    documentType: ""
                })
                this.props.callbackForDelete(id);
            }
        })
    }

    render () {
        const { documentTypes, documentType, info, uploaded, documentLink, 
            documentName, attachmentId,  uploadedDocumentTypeName } = this.state;
        let documentTypesOptions = [];   


        if (documentTypes.length > 0) {
            documentTypesOptions = documentTypes.map(documentType => (
                { key: documentType.id, text: documentType.name, value: documentType.id }            
            ));
        }

        return (      
            <Form.Group >
                <Form.Field width={5}>  
                    {uploaded ? 
                    <b>{uploadedDocumentTypeName}</b>: 
                    <Form.Select label='Document Type' options={documentTypesOptions}
                        placeholder='Select Document to upload' name = "documentType"
                        onChange = {this.handleSelectChange} value = {documentType} />
                    }
                </Form.Field>
                
                <Form.Field width={8}>
                    
                    {documentType && !uploaded &&
                    <span>
                        <label>&nbsp;</label>
                        <UploadAttachment
                            document={{document: documentType, index: info.index, form: info.formInfoId }} 
                            callbackForUpload={this.afterUpload} />  
                    </span>
                    }
                    {uploaded &&
                        <span>
                            <a href ={documentLink} download={documentName} target="_blank">{documentName}</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <Icon name="trash" color="red" link onClick={() => this.deleteAttachment(attachmentId)}/>
                        </span>
                    }
                </Form.Field>
                
            </Form.Group>                                                                                
        )
    }
    
}

export default UploadRow