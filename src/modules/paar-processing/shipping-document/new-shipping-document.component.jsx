import React, { Component } from 'react'
import { Checkbox, Table, Grid, Header, Segment, Breadcrumb, Form, Input, 
    Message, Icon, Button, Dropdown, Label } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component'
import SharedService from './../../../services/shared_service'
import DatePicker from 'react-datepicker'
// import moment from 'moment'
import UploadRow from './upload-row.component'
import PaarService from './../../../services/paar_service'
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import FormSearch from '../../shared/form-search.component';
import UtilService from '../../../services/util_service';

var _ = require("lodash")

class NewShippingDocument extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            shippingDocuments: [],
            errorMessage: null,
            successMessage: null,
            refreshErrorMessage: null,
            loadingMessage: null,
            formId: null,
            result: "",
            formNumber: "",
            formData: {
                containerNumber: "",
                courierCompany: "",
                documentAmount: "",
                documentNumber: "",
                shippingDocuments: [],
                invoiceNumber: "",
                negotiationBankName: "",
                numberOfContainers: "",
                shippingCompany: "",
                currencyCode: "",
                senderReference: ""
            },
            invoiceDate: null,
            isLoadingForm: false,
            newRow: "",
            index: 0,
            upload: "",
            copySet: false,
            copySetUploads: [],      
            copySetValue: "Copy Set",
            bankSet: false,
            bankSetUploads: [], 
            bankSetValue: "Bank Set",
            advanceSet: false,
            advanceSetUploads: [],
            advanceSetValue: "Advance Set",
            documentTypes: [],
            currencies: []
        }
    }

    componentWillMount () { this.getCurrencies(); this.getDocumentTypes(); }
    
    getDocumentTypes = () => {
        PaarService.getDocumentTypes()
        .then(response => {
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.setState({ documentTypes: response.list })
            }
        })
    }

    getCurrencies = () => {
        SharedService.getAppParameter('CURRENCIES')
        .then(response => {      
            if (response.code) {
                this.alertError(response.description);
            } else {
                let jsonResponse = response.value;
                this.setState ({ 
                    currencies: jsonResponse.split(",")
                })
            }
        })
    }

    search = () => {         
        this.setState({ isLoadingForm: true, searched: false });
        let params ={
            pageSize: 1,
            pageNumber: 1,
            formType: 'IM',
            formNumber: this.state.formNumber
        }
        SharedService.searchForForms(params)
        .then(response => {       
            this.setState({ isLoadingForm: false });
            if (!response.code) {
                this.setState ({ searched: true, result: response.list, isLoadingForm: false })
            }
        })
        
    }

    handleFormNumberChange = (event, {name, value}) => this.setState({  [name] : value }); 

    handleInputChange = (event, {name, value}) => {      
        if (name === "documentAmount" || name === "numberOfContainers") {
            if (UtilService.validNumberOnly(value)) {
                this.setState ({ 
                    formData: {
                        ...this.state.formData,
                        [name] : value 
                    }
                });
            }
        } else {
            this.setState ({ 
                formData: {
                    ...this.state.formData,
                    [name] : value 
                }
            }); 
        }         
    }

    handleInvoiceDateChange = (date) => { this.setState ({ invoiceDate: date }); }

    setUploadSet = (type) => {
        let a = this.state.index
        this.setState({            
            upload: <UploadRow 
                key={a} 
                info={{formInfoId: 15, index: a}} 
                documentTypes={this.state.documentTypes}
                callbackForUpload = {(data)=>this.afterUpload(data, type)}
                callbackForDelete = {(data)=>this.afterUploadDelete(data, type)} />,
            index: this.state.index + 1,
        }, () => {
            this.addMoreUpload(type);
        })
    }

    handleCopySetChange = () => {
        if (this.state.copySet) {
            this.setState({copySetUploads: []});
            this.emptyUploadSetFromShippingDocument(this.state.copySetValue)
        }
        this.setState({ copySet: !this.state.copySet })
    }

    handleBankSetChange = () => {        
        if (this.state.bankSet) {
            this.setState({bankSetUploads: []});
            this.emptyUploadSetFromShippingDocument(this.state.bankSetValue)
        }
        this.setState({ bankSet: !this.state.bankSet })
    }

    handleAdvanceSetChange = () => {
        if (this.state.advanceSet) {
            this.setState({advanceSetUploads: []});
            this.emptyUploadSetFromShippingDocument(this.state.advanceSetValue)
        }
        this.setState({ advanceSet: !this.state.advanceSet })
    }

    afterUpload = (data, type) => {
        if (!data.code) {
            let shippingDocument = {
                documentSet: type,
                documentTypeId: data.documentType,
                attachmentId: data.attachment.id
            }
            let newArray = [ ...this.state.formData.shippingDocuments, shippingDocument ];
            this.setState({ 
                formData: {
                    ...this.state.formData,
                    shippingDocuments: newArray
                }
            });
        }
    }

    afterUploadDelete = (id, type) => {
        var newArray = this.state.formData.shippingDocuments;
        var index = _.findIndex(newArray, ["attachmentId", id])
        newArray.splice(index, 1);

        this.setState({ 
            formData: {
                ...this.state.formData,
                shippingDocuments: newArray
            }
        }); 
    }

    addMoreUpload = (type) => {   
        if (type === this.state.copySetValue) {

            let newArray = [ ...this.state.copySetUploads,  this.state.upload];
            this.setState({ copySetUploads: newArray}); 

        } else if (type === this.state.bankSetValue) {

            let newArray = [ ...this.state.bankSetUploads,  this.state.upload];
            this.setState({ bankSetUploads: newArray}); 

        } else if (type === this.state.advanceSetValue) {

            let newArray = [ ...this.state.advanceSetUploads,  this.state.upload];
            this.setState({ advanceSetUploads: newArray}); 

        }
    
    }

    validForm = () => {
        this.setState ({ errorMessage: null })
        
        let containerNumber = this.state.formData.containerNumber;
        let courierCompany = this.state.formData.courierCompany;
        let documentAmount = this.state.formData.documentAmount;
        let documentNumber = this.state.formData.documentNumber;
        let shippingDocuments = this.state.formData.shippingDocuments;
        let invoiceNumber = this.state.formData.invoiceNumber;
        let invoiceDate = this.state.invoiceDate;
        let negotiationBankName = this.state.formData.negotiationBankName;
        let numberOfContainers = this.state.formData.numberOfContainers;
        let shippingCompany = this.state.formData.shippingCompany;
        

        if ( containerNumber == null || containerNumber === "") {            
            this.setState ({ errorMessage: "Container Number required" })
            return false;
        }
        if ( courierCompany == null || courierCompany === "") {            
            this.setState ({ errorMessage: "Courier Company required" })
            return false;
        }
        if ( documentAmount == null || documentAmount === "") {            
            this.setState ({ errorMessage: "Document Amount required" })
            return false;
        }
        if ( documentNumber == null || documentNumber === "") {            
            this.setState ({ errorMessage: "Document Number required" })
            return false;
        }
        if ( invoiceNumber == null || invoiceNumber === "") {            
            this.setState ({ errorMessage: "Invoice Number required" })
            return false;
        }
        if ( shippingDocuments.length === 0) {            
            this.setState ({ errorMessage: "At least 1 shipping document is required" })
            return false;
        }
        if ( invoiceDate == null || invoiceDate === "") {            
            this.setState ({ errorMessage: "Invoice Date required" })
            return false;
        }
        if ( negotiationBankName == null || negotiationBankName === "") {            
            this.setState ({ errorMessage: "Negotiation Bank Name required" })
            return false;
        }
        if ( numberOfContainers == null || numberOfContainers === "") {            
            this.setState ({ errorMessage: "Number Of Containers is required" })
            return false;
        }
        if ( shippingCompany == null || shippingCompany === "") {            
            this.setState ({ errorMessage: "Shipping Company is required" })
            return false;
        }

        return true;
    }

    submit = () => {
        if (this.validForm()) {
            this.setState ({ isLoadingForm: true })
            let data = this.state.formData;
            data.invoiceDate = this.state.invoiceDate.format('YYYY-MM-DD');
            data.formType = "IM";
            data.formId = this.state.result[0].id;
            data.currencyCode = data.currencyCode ? data.currencyCode: "NGN"
            
            PaarService.createShippingDocument(data)
            .then(response => {
                this.setState ({ isLoadingForm: false })
                if (response.code) {
                    this.alertError(response.description);
                } else {                    
                    this.setState({ successMessage: "Successful" });
                    window.location.href="#/shipping/document/"+response.identityValue;
                }   
            })
        } 

    }

    clearForm = () => {
        this.setState({ 
            result: "", 
            copySet: false,
            bankSet: false,
            advanceSet: false,
            searched: false,
            formNumber: "",
            formData: {
                containerNumber: "",
                courierCompany: "",
                documentAmount: "",
                documentNumber: "",
                shippingDocuments: [],
                invoiceNumber: "",
                negotiationBankName: "",
                numberOfContainers: "",
                shippingCompany: ""
            }
        })
    }

    emptyUploadSetFromShippingDocument (type) {
        let records = this.state.formData.shippingDocuments.filter(record => record.documentSet !== type);
        this.setState({ 
            formData: {
                ...this.state.formData,
                shippingDocuments: records
            }
        })
    }

    setSelectedForm = (selected) =>  this.setState({ formNumber: selected }, () => { this.search()})
        
    clearSelectedForm = () => this.setState({ formNumber: "" })

    alertError = (msg) => {
        this.setState({ errorMessage: msg})
        window.scrollTo(0,0);
        setTimeout(()=> {this.setState({ errorMessage: null})}, 4000)
    }

    render () {
        const { isLoadingForm, result, copySetUploads, bankSetUploads, advanceSetUploads,
            copySetValue, bankSetValue, advanceSetValue, formData, currencies} = this.state;
        let formDetails = result[0], currenciesOptions;      
        
        if (currencies && currencies.length > 0) {
            currenciesOptions = currencies.map(currency => (
                { key: currency, text: currency, value: currency }
            ));
        }

        
        return (      
            <AppWrapper> 
                <Header as='h3'>PAAR Processing</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                        <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/paar">PAAR Processing </Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>New Shipping Document</Breadcrumb.Section>
                                </Breadcrumb>
                                <br/><br/>
                                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }                                                
                                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                                <Form loading={isLoadingForm}>
                                    <Segment.Group> 
                                        <Segment>
                                            {!this.state.searched &&
                                                <FormSearch selected={this.setSelectedForm} clear={this.clearSelectedForm} width="5"/> 
                                            }                                     
                                            {formDetails && <Label as='a' color='grey' ribbon>FORM NUMBER: {formDetails.formNumber}</Label>}
                                            {this.state.searched && result.length > 0 &&
                                            <Message onDismiss={this.clearForm}>
                                                <Message.Content>                                                    
                                                    <Table basic='very' fixed>
                                                        <Table.Body>
                                                            <Table.Row>                                                                        
                                                                <Table.Cell>
                                                                    <Header as='h4'>
                                                                        <Header.Content>
                                                                            Application Number <Header.Subheader>{formDetails.applicationNumber}</Header.Subheader>
                                                                        </Header.Content>
                                                                    </Header>
                                                                </Table.Cell>
                                                                <Table.Cell>
                                                                    <Header as='h4'>
                                                                        <Header.Content>
                                                                            Applicant TIN <Header.Subheader>{formDetails.applicantTin}</Header.Subheader>
                                                                        </Header.Content>
                                                                    </Header>
                                                                </Table.Cell>
                                                            </Table.Row>
                                                            <Table.Row>                                                                        
                                                                <Table.Cell>
                                                                    <Header as='h4'>
                                                                        <Header.Content>
                                                                            Beneficiary Name <Header.Subheader>{formDetails.beneficiaryName}</Header.Subheader>
                                                                        </Header.Content>
                                                                    </Header>
                                                                </Table.Cell>
                                                                <Table.Cell>
                                                                    <Header as='h4'>
                                                                        <Header.Content>
                                                                            Beneficiary Phone <Header.Subheader>{formDetails.beneficiaryPhone}</Header.Subheader>
                                                                        </Header.Content>
                                                                    </Header>
                                                                </Table.Cell>
                                                            </Table.Row>                                                                                                           
                                                        </Table.Body>
                                                    </Table>
                                                </Message.Content>
                                            </Message> }
                                        </Segment>
                                        {this.state.searched && result.length > 0 &&
                                        <span>
                                            <Segment>
                                                <Form.Group widths='equal'>
                                                    <Form.Field control={Input} label="Negotiation Bank Name" placeholder="Negotiation Bank Name" 
                                                        name = 'negotiationBankName' value = {formData.negotiationBankName} id='shipping-doc-negotiationBank' onChange = {this.handleInputChange} />
                                                    <Form.Field control={Input} label="Sender Reference" placeholder="Sender Reference" 
                                                        name = 'senderReference' value = {formData.senderReference} id='shipping-doc-senderReference' onChange = {this.handleInputChange} />
                                                    <Form.Field>
                                                        <label> Amount </label>
                                                        <Input 
                                                            label={<Dropdown defaultValue="NGN" options={currenciesOptions} name="currencyCode" onChange={this.handleInputChange} />}
                                                            placeholder="Document Amount" 
                                                            name = 'documentAmount' 
                                                            labelPosition='right'
                                                            value = {formData.documentAmount} 
                                                            id = 'shipping-doc-documentAmount' 
                                                            onChange = {this.handleInputChange} />
                                                    </Form.Field>
                                                </Form.Group>
                                                <Form.Group widths='equal'>
                                                    <Form.Field control={Input} label="Document Number" placeholder="Document Number" 
                                                        name = 'documentNumber' value = {this.state.formData.documentNumber} id = 'shipping-doc-documentNumber' onChange = {this.handleInputChange} />
                                                    <Form.Field control={Input} label="Invoice Number"  placeholder="Invoice Number" 
                                                        name = 'invoiceNumber' value = {this.state.formData.invoiceNumber} id = 'shipping-doc-invoiceNumber' onChange = {this.handleInputChange}/>
                                                    <Form.Field>
                                                        <label>Invoice Date</label>
                                                        <DatePicker                                
                                                            placeholderText='Invoice Date'
                                                            dateFormat="YYYY-MM-DD"
                                                            selected={this.state.invoiceDate}
                                                            onChange={this.handleInvoiceDateChange}
                                                            id = "shipping-doc-invoiceDate" 
                                                        />
                                                    </Form.Field>
                                                </Form.Group>
                                                <Form.Group>
                                                    <Form.Field width={6} control={Input} label="Courier Company"  placeholder="Courier Company" 
                                                        name = 'courierCompany' value = {this.state.formData.courierCompany} id = 'shipping-doc-courierCompany' onChange = {this.handleInputChange} />
                                                    <Form.Field width={6} control={Input} label="Shipping Company"  placeholder="Shipping Company" 
                                                        name = 'shippingCompany' value = {this.state.formData.shippingCompany} id = 'shipping-doc-shippingCompany' onChange = {this.handleInputChange}/>
                                                    <Form.Field width={4} control={Input} label="Container Number" placeholder="Container Number" 
                                                        name = 'containerNumber' value = {this.state.formData.containerNumber} id = 'shipping-doc-containerNumber' onChange = {this.handleInputChange} />
                                                    <Form.Field width={2} control={Input} label="No of Containers"  placeholder="Containers" 
                                                        name = 'numberOfContainers' value = {this.state.formData.numberOfContainers} id = 'shipping-doc-numberOfContainers' onChange = {this.handleInputChange} />
                                                    
                                                </Form.Group>        
                                                
                                                <Header as='h3'>Uploads</Header>                                                
                                                <Form.Group widths='equal' inline>
                                                    <Form.Field control={Checkbox} label='Copy Set' checked={this.state.copySet} onChange={this.handleCopySetChange} />
                                                    <Form.Field control={Checkbox} label='Bank Set' checked={this.state.bankSet} onChange={this.handleBankSetChange} />
                                                    <Form.Field control={Checkbox} label='Advance Set' checked={this.state.advanceSet} onChange={this.handleAdvanceSetChange} />
                                                </Form.Group>
                                                {this.state.copySet &&
                                                <fieldset>
                                                    <legend>Copy Set</legend>
                                                    <Header as='h5' onClick={() => this.setUploadSet(copySetValue)} className="ut-pointer">
                                                        <Icon name='add circle' color = "green" /> Add
                                                    </Header>                                                    
                                                    {copySetUploads}
                                                </fieldset>}
                                                <br/>
                                                {this.state.bankSet &&
                                                <fieldset>
                                                    <legend>Bank Set</legend>
                                                    <Header as='h5' onClick={() => this.setUploadSet(bankSetValue)} className="ut-pointer">
                                                        <Icon name='add circle' color = "green" /> Add
                                                    </Header>                                                    
                                                    {bankSetUploads}
                                                </fieldset>}
                                                <br/>
                                                {this.state.advanceSet &&
                                                <fieldset>
                                                    <legend>Advance Set</legend>
                                                    <Header as='h5' onClick={() => this.setUploadSet(advanceSetValue)} className="ut-pointer">
                                                        <Icon name='add circle' color = "green" /> Add
                                                    </Header>                                                    
                                                    {advanceSetUploads}
                                                </fieldset>}
                                                <br/>
                                                {this.state.formData.shippingDocuments.length > 0 &&
                                                <Button size="large" floated='right' color="red" onClick={this.submit}>Submit</Button>
                                                }<p>&nbsp;</p><p>&nbsp;</p>
                                            </Segment>
                                        </span>
                                        }                                        
                                    </Segment.Group>
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default NewShippingDocument