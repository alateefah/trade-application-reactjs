import React, { Component } from 'react'
import { Table, Loader, Label, Pagination, Header, Icon } from 'semantic-ui-react'
import SuccessMessage from './../../shared/message/success.component'
import Refresh from './../../shared/refresh.component'
import PaarService from './../../../services/paar_service'
import NoSearchResult from './../../shared/no-search-result.component'
import ErrorMessage from './../../shared/message/error.component'
import AuthenticationService from './../../../services/authentication_service'

class DocumentRelease extends Component {

    constructor () {
        super();

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            releasedDocuments: [],
            errorMessage: null,
            refreshErrorMessage: null,
            loadingMessage: null
        }
        
    }

    componentWillMount () {
        this.setState({ loadingMessage : "Loading..." })
        this.getExaminedDocuments();
    }

    getExaminedDocuments = () => {        
        this.setState ({ refreshErrorMessage: false,  searched: false })
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber,
            type: "document_release",
            status: ""
        }
        PaarService.getShippingDocuments(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    releasedDocuments : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber 
                })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => { 
        this.setState({ pageNumber : activePage }, () => {              
            this.setState({ loadingMessage : "Loading..." })
            this.getExaminedDocuments();
        });          
    }

    refreshAction = () => {
        this.setState({ loadingMessage : "Loading..." })
        this.getExaminedDocuments();
    }

    clearAction = () => {
        this.setState({ pageNumber: 1, loadingMessage : "Loading..." }, () => {
            this.getExaminedDocuments();
        })        
    }

    render () {
        let rows = this.state.releasedDocuments.length <= 0 ?
            <NoSearchResult colSpan={7} /> :
            this.state.releasedDocuments.map(examinedDocument => {
                return  <Table.Row key={examinedDocument.id}>
                            <Table.Cell>
                                <Label ribbon>{examinedDocument.formNumber}</Label>
                            </Table.Cell>                            
                            <Table.Cell>{examinedDocument.consignmentNumber}</Table.Cell>
                            <Table.Cell>{examinedDocument.documentNumber}</Table.Cell>
                            <Table.Cell>{examinedDocument.documentAmount}</Table.Cell>
                            <Table.Cell>{examinedDocument.createdOn}</Table.Cell>
                            <Table.Cell>{examinedDocument.releaseStatus} </Table.Cell>
                            <Table.Cell textAlign='center'>  <a href={"#/release/"+examinedDocument.id}>view</a></Table.Cell>                            
                        </Table.Row>
        })

        return (                  
            this.state.loadingMessage ?  
            <div className="ut-loader"><Loader active content={this.state.loadingMessage}/></div>:  
            this.state.refreshErrorMessage ? <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
            <div>
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                {AuthenticationService.hasPermission('GET_ALL_ECD_SHIPPING_DETAILS_BY_FORM_NUMBER') &&
                <Header as='h4' textAlign='right'>
                    <a href="#/release/new" id='initiate-document-release' className='ui grey large button'> 
                        <Icon name="book" /> Initiate Document Releases
                    </a>
                </Header>
                } 
                {
                    AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') &&
                    <div style={{ paddingTop: 10 }} >
                        <Table celled>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='7'>
                                        Showing {this.state.pageNum === 1 ? this.state.pageNumber : this.state.pageNumber*this.state.pageSize - this.state.pageSize+1} - {this.state.pageNumber*this.state.pageSize > this.state.noOfRecords ? this.state.noOfRecords : this.state.pageNumber*this.state.pageSize}  of {this.state.noOfRecords} records </Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.HeaderCell>Form Number</Table.HeaderCell>
                                    <Table.HeaderCell >Consignment Number</Table.HeaderCell>
                                    <Table.HeaderCell>Document Number </Table.HeaderCell>
                                    <Table.HeaderCell>Amount </Table.HeaderCell>
                                    <Table.HeaderCell>Created On</Table.HeaderCell>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                    <Table.HeaderCell textAlign='center'>Action</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{rows}</Table.Body>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='7' textAlign="right">
                                        <Pagination pointing secondary 
                                            activePage={this.state.pageNumber} 
                                            onPageChange={this.handlePaginationChange} 
                                            totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                        />  
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>     
                    </div>
                }
            </div>                          
        )
    }
}

export default DocumentRelease