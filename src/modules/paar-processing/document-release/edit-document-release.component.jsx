import React, { Component } from 'react'
import { Table, Grid, Header, Segment, Breadcrumb, Form, TextArea, Label, Button, Feed, Input } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component';
import PaarService from './../../../services/paar_service';
import UtilService from './../../../services/util_service';
import SharedService from './../../../services/shared_service';
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import DocumentDisplayModal from '../../shared/document-display-modal.component';
import UploadAttachment from './../../shared/upload-attachment.component';

class EditDocumentRelease extends Component {

    constructor (props) {
        super (props);

        this.state = {
            isLoadingForm: false,
            errorMessage: null,
            selected: [],
            shippingDocumentDetails: null,
            shippingDocumentDetailsId: props.match.params.shippingDocumentDetailsId,
            copySetValue: "Copy Set",
            bankSetValue: "Bank Set",
            advanceSetValue: "Advance Set",
            advanceSetDocuments: [],
            bankSetDocuments: [],
            advanceAndBankSetDocuments: [],
            reviewComment: "",
            stageOneApprovalValue: null,
            extraRecords: [],
            stageTwoApprovalValue: null,
            initialRequest: null,
            otherApprovals: null,
            currentApproval:null,
            released: [],
            uploads: [],
            fileIndex: 0,
            recipient: "",
            saving: false
        }
    }

    componentWillMount () { this.getDocumentTypes(); }

    getDocumentTypes = () => {
        this.setState ({ isLoadingForm: true });
        PaarService.getDocumentTypes()
        .then(response => {
            if (response.code) {
                this.setState({ errorMessage: response.description})
            } else {
                this.setState({ documentTypes: response.list });
                this.getDocumentForRelease();
            }
        })
    }

    getDocumentForRelease = () => {
        this.setState ({ shippingDocumentDetails: null, isLoadingForm: true });
        
        PaarService.getDocumentForDetails(this.state.shippingDocumentDetailsId)        
        .then(response => {   
            this.setState({ isLoadingForm : false })
            if (response.code) {
               this.alertError(response.description)
            } else {                
                this.setState({ 
                    shippingDocumentDetails : response,                   
                    paarStatus: response.paarStatus, 
                    initialRequest: response.documentReleaseApprovals.find(record => record.type==="DOCUMENT_RELEASE"),
                    otherApprovals: response.documentReleaseApprovals.find(record => record.type==="DOCUMENT_RELEASE_II" && record.approvalStatus !== "PENDING"),
                    currentApproval: response.documentReleaseApprovals.find(record => record.type==="DOCUMENT_RELEASE_II" && record.approvalStatus === "PENDING"),
                })

            }    
            
        }) 
    }
   
    mapdocumentTypeIdToName = (id) => {
        const result = this.state.documentTypes.find( record => record.id === parseInt(id, 10) );
        return result.name;
    }

    clearForm = () => {
        this.setState({             
            isLoadingForm: false,
            errorMessage: null 
        })
    }    

    approve = () => this.setState ({ stageOneApprovalValue: true}, () => { this.submit(); });
    
    reject = () => {
        const {reviewComment} = this.state;
        if (reviewComment === "") {
            this.alertError("Comment required for rejection");
        } else {
            this.setState ({ stageOneApprovalValue: false}, () => { this.submit(); });
        }
    }

    submit = () => {
        const {reviewComment, stageOneApprovalValue, shippingDocumentDetailsId, initialRequest} = this.state
        let params = {
            recordId: shippingDocumentDetailsId,
            formCategory: "FORM_M",
            type: "DOCUMENT_RELEASE",
            approverComments: reviewComment,
            parentId: shippingDocumentDetailsId,
            approved: stageOneApprovalValue,
            url: "release",
            approvalId: initialRequest.id
        }
        this.setState({ isLoadingForm: true});
        SharedService.authorizeApproval(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful");
            }
        })
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg });
        this.clearForm();
        this.getDocumentForRelease();
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0)
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }
   
    handleInputChange = (e, val) => this.setState({ [val.name] : val.value });

    LCApprove = () => {
        this.setState ({ stageTwoApprovalValue: true}, () => { this.LCSubmit(); });
    }

    LCReject = () => {
        this.setState ({ stageTwoApprovalValue: false}, () => { this.LCSubmit(); });
    }

    LCSubmit = () => {
        const {reviewComment, shippingDocumentDetailsId, stageTwoApprovalValue, currentApproval} = this.state
        let params = {
            recordId: currentApproval.id,
            formCategory: "FORM_M",
            type: "DOCUMENT_RELEASE_II",
            approverComments: reviewComment,
            parentId: shippingDocumentDetailsId,
            approved: stageTwoApprovalValue,
            url: "release",
            approvalId: currentApproval.id
        }
        
        this.setState({ isLoadingForm: true})

        SharedService.authorizeApproval(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful");
            }
        })
    }

    upload = (data) => {
        var newArray = this.state.uploads.slice();    
        newArray.push(data.data);   
        this.setState({ uploads: newArray }, () => {
            this.setState({ fileIndex: this.state.fileIndex+1 })
        })
    }

    onInputChange = (e, {name, value}) => this.setState({ [name]: value})

    save = () => {
        
        // this.setState({ errorMessage: null});

        const {recipient, uploads} = this.state;
        if (recipient === null || recipient === "") {
            this.alertError("Recipient required");  
            return;      
        }
        
        if (uploads.length === 0) {            
            this.alertError("At least one upload required");
            return;
        }        
        let params = {
            recipient: this.state.recipient,
            id: this.state.shippingDocumentDetailsId
        }
        this.setState({ saving: true, errorMessage: null});
        PaarService.acknowledgeDocumentPickup(params)
        .then(response => {
            this.setState({saving: false})
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.alertSuccess("Acknowledge Pickup Successful.");
            }
        })
    }

    render () {
        const { isLoadingForm, errorMessage, shippingDocumentDetails, initialRequest, otherApprovals, uploads, fileIndex} = this.state;   
        let documentsOptions, otherApprovalsOptions, acknowledgementUploadsOptions=[];

        if (shippingDocumentDetails && shippingDocumentDetails.shippingDocuments.length > 0) {            
            documentsOptions = shippingDocumentDetails.shippingDocuments.map(shippingDocument => {
                let attachment = shippingDocument.attachment;
                return <Table.Row key={shippingDocument.id}>
                        <Table.Cell>
                            <b>{this.mapdocumentTypeIdToName(attachment.documentType)}</b>
                        </Table.Cell>
                        <Table.Cell textAlign="right">
                            <DocumentDisplayModal document={attachment} />
                        </Table.Cell>
                    </Table.Row>
            })

            acknowledgementUploadsOptions = shippingDocumentDetails.releaseStatus === "COMPLETED"
                && shippingDocumentDetails.acknowledgementUploads.map(acknowledgementUpload => {
                return <Table.Row key={acknowledgementUpload.id}>
                            <Table.Cell collapsing>
                                <DocumentDisplayModal document={acknowledgementUpload} />                                
                            </Table.Cell>
                            <Table.Cell textAlign="right">{acknowledgementUpload.createdAt}</Table.Cell>
                        </Table.Row>
            })
        }
        
        if (otherApprovals) {
            let arr = []
            if (!Array.isArray(otherApprovals)) {
                arr.push(otherApprovals);
            } else {
                arr = otherApprovals;
            }
            otherApprovalsOptions = arr.map(approval => {   
                return  <Feed.Event key={approval.id}
                            icon='pencil'
                            date={approval.approvedDate}
                            summary={approval.approvedBy+" approved document release."}
                            extraText= {approval.approverComments}
                        />
            })
        }

        return (      
            <AppWrapper> 
                <Header as='h3'>Document Release</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                        <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/release">Document Release</Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Edit Document Release</Breadcrumb.Section>
                                </Breadcrumb>  
                                {shippingDocumentDetails &&
                                <Header floated="right">
                                    <Label size="large" color={UtilService.statusColor(shippingDocumentDetails.releaseStatus)}>{shippingDocumentDetails.releaseStatus}</Label>
                                </Header>} <br/><br/>
                                {this.state.errorMessage && <ErrorMessage message={errorMessage} /> }
                                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }                                                
                                <Form loading={isLoadingForm}>   
                                {shippingDocumentDetails &&                                 
                                    <Segment.Group>                                         
                                        <Segment>
                                            <Label as='a' color='grey' ribbon>FORM NUMBER: {shippingDocumentDetails && shippingDocumentDetails.formNumber}</Label>
                                            <Table basic='very' fixed>
                                                <Table.Body>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Negotiation Bank Name
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Sender Reference
                                                                <Header.Subheader>{shippingDocumentDetails.senderReference}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Amount
                                                                <Header.Subheader>{shippingDocumentDetails.documentAmount + " "+ shippingDocumentDetails.currencyCode }</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Document Number
                                                                <Header.Subheader>{shippingDocumentDetails.documentNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Number
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Date
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceDate}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Courier Company
                                                                <Header.Subheader>{shippingDocumentDetails.courierCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Shipping Company
                                                                <Header.Subheader>{shippingDocumentDetails.shippingCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Container Number (Number of Containers)
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber + " ( "+shippingDocumentDetails.numberOfContainers+" ) "}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                    </Table.Row>
                                                </Table.Body>
                                            </Table>
                                        </Segment>
                                        
                                        <Segment>                                         
                                            {documentsOptions && documentsOptions.length > 0 &&
                                            <fieldset>
                                                <legend>Shipping Document Uploads</legend>
                                                <Table>
                                                    <Table.Body>{documentsOptions}</Table.Body>
                                                </Table>
                                                
                                            </fieldset>
                                            }  
                                            <br/>
                                            {shippingDocumentDetails.releaseStatus === "COMPLETED" &&
                                                <fieldset>
                                                    <legend>Pickup Uploads</legend>
                                                    <b>Recipient: </b> {shippingDocumentDetails.recipient}
                                                    <Table >
                                                        <Table.Body>{acknowledgementUploadsOptions}</Table.Body>
                                                    </Table>
                                                    
                                                </fieldset>
                                            }                                                
                                                                                                                                                                             
                                        </Segment>

                                        <Segment>
                                            <Feed>
                                                <Feed.Event
                                                    icon='pencil'
                                                    date={initialRequest.createdDate}
                                                    summary={initialRequest.createdBy+" requested document release."}
                                                    extraText= {initialRequest.initiatorComments}
                                                />
                                                {(shippingDocumentDetails.releaseStatus === "PENDING_LC_APPROVAL" ||
                                                    shippingDocumentDetails.releaseStatus === "APPROVED") &&
                                                <Feed.Event
                                                    icon='pencil'
                                                    date={initialRequest.approvedDate}
                                                    summary={initialRequest.approvedBy+" approved document release."}
                                                    extraText= {initialRequest.approverComments}
                                                />}
                                                {otherApprovalsOptions}
                                            </Feed>
                                            {(shippingDocumentDetails.releaseStatus === "PENDING_APPROVAL" || shippingDocumentDetails.releaseStatus === "PENDING_LC_APPROVAL" ) 
                                                && <Form.Field label="Comment" placeholder="Comment (required for rejection)" control={TextArea} rows={4} onChange={this.handleInputChange} name="reviewComment" value={this.state.reviewComment}/>}
                                            
                                            {shippingDocumentDetails.releaseStatus === "PENDING_APPROVAL" &&
                                            <div className="rhs-status">
                                                <Button size="large" color="red" icon="cancel" basic onClick={this.reject} content = "Reject Request" />
                                                <Button size="large" color="green" icon="checkmark" basic onClick={this.approve} content="Approve Request" />                                                                                                    
                                            </div>
                                            }

                                            {shippingDocumentDetails.releaseStatus === "PENDING_LC_APPROVAL" &&
                                            <span>
                                                <div className="rhs-status">
                                                    <Button size="large" color="red" icon="cancel" basic onClick={this.LCReject} content = "Reject Request" />
                                                    <Button size="large" color="green" icon="checkmark" basic onClick={this.LCApprove} content="Approve Request" />
                                                </div>
                                            </span>
                                            }
                                        </Segment>

                                        {shippingDocumentDetails.releaseStatus === "APPROVED" &&           
                                        <Segment>
                                            <fieldset>
                                                <legend>Pickup Details</legend>
                                                <br/>
                                                <Form.Field width={6} label="Recipient" placeholder="Recipient Name" control={Input} value={this.state.recipient} onChange={this.onInputChange} name="recipient"/>
                                                <Form.Field>
                                                    {uploads.length > 0 && uploads.map(upload => {
                                                        return <span key={upload.id}>
                                                                    <DocumentDisplayModal document={upload} />
                                                                    <br/>
                                                                </span>
                                                    })}
                                                    <UploadAttachment 
                                                        document={{document: "DOCUMENT_RELEASE", index: fileIndex, form: this.state.shippingDocumentDetailsId }}
                                                        callbackForUpload= {this.upload} />
                                                </Form.Field>
                                                <Form.Field control={Button} floated="right" color="teal" content="Save" onClick ={this.save} loading={this.state.saving} />
                                            </fieldset>
                                        </Segment>}                  
                                    </Segment.Group>}
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default EditDocumentRelease