import React, { Component } from 'react'
import { Checkbox, Table, Grid, Header, Segment, Breadcrumb, Form, Message, Button, TextArea } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component';
import PaarService from './../../../services/paar_service';
import SharedService from './../../../services/shared_service';
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import FormSearch from '../../shared/form-search.component';

class NewDocumentRelease extends Component {

    constructor (props) {
        super (props);

        this.state = {
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            isLoadingForm: false,
            errorMessage: null,
            formData: {
                id: ""
            }
        }
    }

    getShippingDocumentDetails = () => {
        this.setState ({ shippingDocumentDetails: null, isLoadingForm: true });
        
        PaarService.getShippingDocumentDetails(this.state.formData.id)        
        .then(response => {   
            this.setState({ isLoadingForm : false })
            if (response.code) {
                this.setState({ errorMessage : response.description })
            } else {
                this.setState({ 
                    shippingDocumentDetails : response,
                    formData: {
                        ...this.state.formData,
                        shippingDocuments: response.shippingDocuments
                    },              
                    advanceAndBankSetDocuments: response.shippingDocuments.filter( record => record.documentSet !== this.state.copySetValue )
                })

            }    
            
        }) 
    }

    handleFormNumberChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({  [name] : value });  
    }

    search = () => {         
        this.setState({ isLoadingForm: true, searched: true });
        let params = {
            formNumber: this.state.formNumber,
            type: "DOCUMENT_EXAMINATION",
            status: "COMPLETED"
        }
        PaarService.getShippingDetailsByFormNumber(params)
        .then(response => {       
            this.setState({ isLoadingForm: false });
            if (response.code) {
                this.setState({errorMessage: response.description})
            } else {
                this.setState ({searched: true, result: response })
            }
        })
    
    }
        
    handleSelectedFormChange = (e, { value }) => {
        this.setState({ 
            formData: { 
                ...this.state.formData, 
                id: value
            } 
        })
    }

    clearForm = () => {
        this.setState({ 
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            formData: {
                id: "",           
            },
            isLoadingForm: false,
            errorMessage: null 
        })
    }    

    submit = () => {
        this.setState({ isLoadingForm: true})
        let params = {
            recordId: this.state.formData.id,
            name: "",
            formCategory: "FORM_M",
            type: "DOCUMENT_RELEASE",
            description: "Request for Document Release",
            initiatorComments: this.state.comment,
            parentId: this.state.formData.id
        }
        SharedService.requestApproval(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code || response.error) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Message has been sent to Trade for approval! ")
            }
        })
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg });
        this.clearForm();
        setTimeout(() => this.setState({ successMessage: null }), 4000);
        window.location.href="#/document-release/"+this.state.formData.id;
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    setSelectedForm = (selected) => {
        this.setState({ formNumber: selected }, () => { this.search() })
    }
        
    clearSelectedForm = () => this.setState({ formNumber: "" })

    render () {
        const { result, isLoadingForm, errorMessage, formData} = this.state;   
        let resultOptions;

        resultOptions = result.length > 0 && result.map(ecd => {
                        return  <Table.Row key={ecd.id}>
                                    <Table.Cell>
                                        <Checkbox radio name='selectedForm' value={ecd.id} checked={formData.id === ecd.id}
                                            onChange={this.handleSelectedFormChange} />
                                    </Table.Cell>                            
                                    <Table.Cell className='no-left-border'>{ecd.containerNumber}</Table.Cell>
                                    <Table.Cell>{ecd.courierCompany}</Table.Cell>
                                    <Table.Cell>{ecd.shippingCompany}</Table.Cell>
                                    <Table.Cell>{ecd.documentAmount}</Table.Cell>
                                    <Table.Cell>{ecd.createdOn } </Table.Cell>
                                    <Table.Cell textAlign='center'> 
                                        <a target="_blank" href={"#/shipping/document/"+ecd.id}>view</a></Table.Cell>                            
                                </Table.Row>
        })
        
        return (      
            <AppWrapper> 
                <Header as='h3'>Document Release</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                        <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/paar-processing/document-release">Document Release</Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>New Document Release</Breadcrumb.Section>
                                </Breadcrumb>   <br/><br/>
                                {this.state.errorMessage && <ErrorMessage message={errorMessage} /> }
                                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }                                                
                                <Form loading={isLoadingForm}>                                    
                                    <Segment.Group> 
                                        <Segment>
                                            {(!this.state.searched || (this.state.searched && this.state.errorMessage)) &&
                                                <FormSearch selected={this.setSelectedForm} clear={this.clearSelectedForm} width="5"/> }                                                                    
                                            {resultOptions &&
                                            <Message onDismiss={this.clearForm}>
                                                <Message.Content>
                                                    <br/>
                                                    <Table celled>
                                                        <Table.Header>                                                        
                                                            <Table.Row>
                                                                <Table.HeaderCell></Table.HeaderCell>
                                                                <Table.HeaderCell>Container Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Courier Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Shipping Document </Table.HeaderCell>
                                                                <Table.HeaderCell>Amount </Table.HeaderCell>
                                                                <Table.HeaderCell>Created On</Table.HeaderCell>
                                                                <Table.HeaderCell>Details</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>
                                                        <Table.Body>{resultOptions}</Table.Body>
                                                    </Table>
                                                </Message.Content>
                                            </Message> 
                                        }                                        
                                        {formData.id !== "" && 
                                        <span>
                                            <Form.Field control={TextArea} label='Comment' placeholder='Any comment...' name="comment" value={this.state.comment} onChange={this.handleChange}/>
                                            <Button size="large" floated='right' color="red" onClick={this.submit}>Request for approval</Button>
                                        </span>
                                        }
                                            <br /><br />
                                        </Segment>
                                        
                                                                                
                                    </Segment.Group>
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default NewDocumentRelease