import React, { Component } from 'react';
import { Form, Modal, TextArea, Button, Icon } from 'semantic-ui-react';
import UploadAttachment from './../../shared/upload-attachment.component'
import ErrorMessage from './../../shared/message/error.component';
import SuccessMessage from './../../shared/message/success.component';
import PaarService from './../../../services/paar_service';
import SharedService from './../../../services/shared_service';

class ReviewDiscrepancyModal extends Component {

    constructor (props) {
        super(props);

        this.state = {
            errorMessage: null,
            successMessage: null,
            loading: false,
            upload: "",
            discrepancy: props.discrepancy,
            comment: ""
            
        }        
        
    }

    handleClose = () => {
        this.setState({ 
            comment: "",
            errorMessage: null,
            loading: false
        })
    }

    afterUpload = (response) => {    
        let data = response.data;
        if (!data.code) {
            this.setState({ upload: data})            
        }        
    }

    save = () => {
        this.setState({loading: true})
        const {discrepancy, upload} = this.state;
        let params = {
            event: "DOCUMENT_DISCREPANCY",
            id: discrepancy.id,
            documentTypeDiscrepanciesId: discrepancy.documentTypeDiscrepanciesId,
            shippingDocumentId: discrepancy.shippingDocumentId,
            shippingDocumentDetailId: discrepancy.shippingDocumentDetailId,
            attachmentId: upload.id,
            comment: this.state.comment
        }
        PaarService.uploadDiscrepancyFile(params)
        .then(response => {
            this.setState({ loading: false})
            if (response.code) {
                this.setState({ errorMessage: response.description})
            } else {
                this.props.callbackForUpload(response);
            }
        })
    }

    handleInputChange = (e, { name, value }) => this.setState({ [name]: value })

    alertError = (msg) =>{
        this.setState({ errorMessage: msg})
        setTimeout(()=>{ this.setState({ errorMessage: null}) }, 4000)
    }

    deleteUpload = (id) => {
        SharedService.deleteAttachment(id)
        .then(response => {
            if (response.code) {

            } else {
                this.setState({upload: "", });
            }
        })
    }

    render () {
        const {upload, discrepancy} = this.state;

        return (                   
            <Modal onClose={this.handleClose} trigger={this.props.modalTrigger}  size='tiny'>
                <Modal.Header> Upload Attachment</Modal.Header>
                <Modal.Content>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                    <Form loading={this.state.loading}>
                        <Form.Field>
                            <label> Discrepancy Attachment</label>
                            {this.state.upload ?
                            <p>
                                <a href ={upload.documentLink} download={upload.documentName} target="_blank">{upload.documentName}</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <Icon name="trash" color="red" link onClick={() => this.deleteUpload(upload.id)}/> 
                            </p> :
                            <UploadAttachment document={{document: "Document Examination", index: discrepancy.id, form: "15" }} callbackForUpload={this.afterUpload} />                                        
                            }
                            </Form.Field>
                            <Form.Field>
                                <Form.Field control={TextArea} rows={5} value={this.state.comment} onChange={this.handleInputChange} name="comment"
                                    label='Comment' placeholder='Comment...' />
                            </Form.Field>
                        <br/>
                    </Form>                        
                </Modal.Content>
                {upload !== "" &&
                <Modal.Actions><Button onClick={this.save} positive content='Save' /></Modal.Actions>    }                
            </Modal>   
        )
    }
    
}


export default ReviewDiscrepancyModal