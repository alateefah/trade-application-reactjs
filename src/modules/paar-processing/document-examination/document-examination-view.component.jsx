import React, { Component } from 'react';
import { Grid, Header, Segment, Breadcrumb, Form, Input, Button, Label, Table, Accordion, Icon, Message, TextArea, Dropdown} from 'semantic-ui-react';
import AppWrapper from './../../shared/main/app-wrapper.component';
import PaarService from './../../../services/paar_service';
import SharedService from './../../../services/shared_service';
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import NewDiscrepanciesActionsModal from './new-discrepancies-actions-modal.component';
import ApprovalsModal from '../../shared/approvals-modal.component';
import DiscrepancyReview from './discrepancy-review.component';
import AuthenticationService from './../../../services/authentication_service';
import ConfirmAction from './../../shared/confirm-action.component'

class DocumentExaminationView extends Component {

    constructor (props) {
        super(props);

        this.state = {
            errorMessage: null,
            successMessage: null,           
            bankAndAdvanceSet: [],
            isLoadingForm: false,
            copySetValue: "Copy Set",
            bankSetValue: "Bank Set",
            advanceSetValue: "Advance Set",
            documentTypes: [],
            shippingDocumentDetails: null,
            shippingDocumentDetailsId: props.match.params.shippingDocumentDetailsId,
            bankSetDocuments: [],
            advanceSetDocuments: [],
            shippingDocumentAndDiscrepancies: [],
            processingAccordionActiveIndex: "",
            selectionAccordionActiveIndex: [],
            stage: "",
            reviewComment: "",
            open: false
        }
    }

    componentWillMount () { this.getDocumentTypes(); }

    getDocumentTypes = () => {
        this.setState ({ isLoadingForm: true });
        PaarService.getDocumentTypes()
        .then(response => {
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.setState({ documentTypes: response.list });
                this.getShippingDocumentDetails();
            }
        })
    }
    
    getShippingDocumentDetails = () => {
        this.setState ({ shippingDocumentDetails: null, isLoadingForm: true });
        
        PaarService.getShippingDocumentDetails(this.state.shippingDocumentDetailsId)        
        .then(response => {   
            this.setState({ isLoadingForm : false })
            if (response.code) {
                this.alertError(response.description)
            } else {
                this.setState({ 
                    shippingDocumentDetails : response,                   
                    stage: response.documentExaminationApprovals.length > 0 ? "UPDATE": "CREATE",
                    paarStatus: response.paarStatus, 
                    bankAndAdvanceSet: response.shippingDocuments.filter( record => record.documentSet !== this.state.copySetValue ),
                    bankSetDocuments: response.shippingDocuments.filter( record => record.documentSet === this.state.bankSetValue ),
                    advanceSetDocuments: response.shippingDocuments.filter( record => record.documentSet === this.state.advanceSetValue )
                })

            }    
            
        }) 
    }
    
    clearForm = () => {
        this.setState ({
            shippingDocumentDetails: null,
            bankSetDocuments: [],
            advanceSetDocuments: [],
        })
    }

    mapdocumentTypeIdToName = (id) => {
        const result = this.state.documentTypes.find( record => record.id === parseInt(id, 10) );
        return result.name;
    }
   
    alertSuccess = (msg) => {
        this.setState({ successMessage: msg });
        this.clearForm();
        this.getShippingDocumentDetails();
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0, 0);
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    saveCallback = (data, id, type) => {
        let selected = data.filter( record => record.selected === true );
        if (selected.length > 0) {
            this.setState({ [id+"_has_selections"]: true, [id+"_selections"]: selected })
        }       
             
    }

    submit = () => { 
        this.closeConfirm();       
        const {shippingDocumentDetails} = this.state
        this.setState({ isLoadingForm: true, })
        let params = {
            id: this.state.shippingDocumentDetailsId,
            event: "DOCUMENT_EXAMINATION",
            shippingDocuments: this.state.bankAndAdvanceSet,
            state: shippingDocumentDetails.documentExaminationStatus === "REJECTED" ? "CREATE": this.state.stage 
        }
        if (params.state === "APPROVE" || params.state === "REJECT") {
            let approval = shippingDocumentDetails.documentExaminationApprovals.find(data => data.approvalStatus === "PENDING")                
            approval.approved = params.state === "APPROVE" ? true:false;
            approval.approverComments = this.state.reviewComment;
            params.approvals = approval;
            
        }
        
        PaarService.requestDiscrepanciesActionsApproval(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.alertSuccess("Action Successful")
            }
        })
        
    }

    handleSetChange = (e, { value }) => this.setState({ value })

    reviewRequest = (data) => {
        this.setState({ isLoadingForm: true})        
        SharedService.authorizeApproval(data)
        .then(response => {  
            this.setState({ isLoadingForm: false})
            if (response.code) {           
                this.alertError(response.description);                
            } else {                
                this.alertSuccess("Review Successful");
            }
        })
        
    }

    handleProcessingAccordionClick = (e, titleProps) => {
        const { index } = titleProps
        const { processingAccordionActiveIndex } = this.state
        const newIndex = processingAccordionActiveIndex === index ? -1 : index
    
        this.setState({ processingAccordionActiveIndex: newIndex })
    }   

    callbackForDiscrepancyUpload = (response) => {
        if (response.code) {
            this.alertError(response.description);
        } else {
            this.alertSuccess("Successful");
        }
    }

    handleInputChange = (e, val) => this.setState({ [val.name] : val.value });
    
    handleCountChange = (e, { name, value }) => {
        const {bankAndAdvanceSet} = this.state

        let id = name.substr(0, name.indexOf('_'));
        let type = name.split("_").pop()

        let index = bankAndAdvanceSet.findIndex((obj => obj.id === parseInt(id, 10) ));

        bankAndAdvanceSet[index][type] = parseInt(value, 10);
    }

    setReviewMode = () => this.setState({ stage: "REVIEW"})
    
    resetView = () => this.setState({ stage: this.state.shippingDocumentDetails.documentExaminationApprovals.length > 0 ? "UPDATE": "CREATE"})

    approve = () => {        
        const {shippingDocumentDetails} = this.state;

        let errorFlag = false
        let shippingDocuments = shippingDocumentDetails.shippingDocuments;

        for (let shippingDocument of shippingDocuments) {
            let selected = shippingDocument.documentTypeDiscrepancies.filter( record => record.selected === true );
        
            for (let item of selected) {                
                let itemAction = item.action || this.state[item.id+"_action"];
                if (itemAction === undefined) {
                    errorFlag = true;
                    this.setState({ [item.id+"_action_error"]: true})
                } else {
                    this.setState({ [item.id+"_action_error"]: false})
                    const result = selected.find( record => record.id === parseInt(item.id, 10) );
                    result.action = item.action || this.state[item.id+"_action"];
                }
            }
        }
        
        if (errorFlag) {
            this.alertError("Select required action for the selected discrepancies");
            return;
        } else {
            this.setState ({ stage: "APPROVE" },  () => { this.submit(); });
        }
        
    }

    disapprove = () => {
        const {reviewComment} = this.state;
        if (reviewComment === "") {
            this.alertError("Comment required for dissapproval")
        } else {
            this.setState ({ stage: "REJECT"}, () => { this.submit(); });
        }
    }

    handleActionChange = (e, {name, value}) => this.setState({ [name] : value }); 
    
    closeConfirm = () => this.setState({ open: false})

    openConfirm = () => { 
        this.setState({ errorMessage: false})

        const {bankAndAdvanceSet} = this.state;
        let errorFlag;
        let shippingDocuments = bankAndAdvanceSet;

        for (let shippingDocument of shippingDocuments) {
            
            let shippingDocumentCopyValue = shippingDocument.copy;
            let shippingDocumentOriginalValue = shippingDocument.original;
            
            if (shippingDocumentCopyValue === undefined || shippingDocumentOriginalValue === undefined)  {
                errorFlag = true;
                this.setState({ [shippingDocument.id+"_group_error"]: true})
            } else {
                this.setState({ [shippingDocument.id+"_group_error"]: false});
                shippingDocument.copy = shippingDocumentCopyValue;
                shippingDocument.original = shippingDocumentOriginalValue; 
            }   
        }

        if (errorFlag) {
            this.alertError("Missing copy/original count in some shipping documents. See highlighted ones")
        } else {
            this.setState({ open : true })
        }
    }

    render () {
        const {isLoadingForm, bankSetDocuments, advanceSetDocuments, documentTypes, shippingDocumentDetails, 
            processingAccordionActiveIndex, selectionAccordionActiveIndex, stage} = this.state;
        let bankSetDocumentsOptions, advanceSetDocumentsOptions, actionsOptions, processingBankOptions, processingAdvanceOptions;      

        actionsOptions = [
            {id: "Applicant to waive", text: "Applicant to waive", value: "Applicant to waive"},
            {id: "Beneficiary to correct", text: "Beneficiary to correct", value: "Beneficiary to correct"},
            {id: "For Information only", text: "For Information only", value: "For Information only"},
            {id: "Ignore", text: "Ignore", value: "Ignore"},
        ]
        
        
        if (shippingDocumentDetails && documentTypes.length > 0 ) {

            if (bankSetDocuments && bankSetDocuments.length > 0) {
                bankSetDocumentsOptions = bankSetDocuments.map(bankSetDocument => { 
                    let selected = bankSetDocument.documentTypeDiscrepancies.filter( record => record.selected === true );                                       
                    return  <span key={bankSetDocument.id}>           
                                <Accordion.Accordion exclusive={false}>
                                    <Accordion.Title active={selectionAccordionActiveIndex.includes(bankSetDocument.id)} index={bankSetDocument.id}>
                                        <Form.Group className={this.state[bankSetDocument.id+"_group_error"] ? "group-error": ""} >
                                            
                                            <Form.Field width={4}>
                                                <b>{this.mapdocumentTypeIdToName(bankSetDocument.attachment.documentType)}</b>
                                            </Form.Field>
                                            <Form.Field width={4}>                        
                                                <a href ={bankSetDocument.attachment.documentLink} download={bankSetDocument.attachment.documentName} target="_blank">{bankSetDocument.attachment.documentName}</a>
                                            </Form.Field>                                
                                            {AuthenticationService.hasPermission("CREATE_SHIPPING_DOCUMENT_DISCREPANCIES_BY_EVENT") 
                                                && stage !== "REVIEW" 
                                                && shippingDocumentDetails.documentExaminationStatus !== "COMPLETED"
                                                &&
                                                <Form.Group>
                                                    <Form.Field>                                                    
                                                        <NewDiscrepanciesActionsModal 
                                                            discrepancies = {bankSetDocument.documentTypeDiscrepancies} 
                                                            callBackOnSave = {(data)=>this.saveCallback(data, bankSetDocument.id, bankSetDocument.documentSet)}
                                                        />
                                                    </Form.Field>
                                                    <Form.Field className="document-count" control={Input} type="number" label="Copy" name={bankSetDocument.id+"_copy"} 
                                                        defaultValue={bankSetDocument.copy} onChange={this.handleCountChange}/>
                                                    <Form.Field className="document-count" control={Input} type="number" label="Original" name={bankSetDocument.id+"_original"} 
                                                        defaultValue={bankSetDocument.original} onChange={this.handleCountChange}/>
                                                </Form.Group>
                                            }
                                            {stage === "REVIEW" &&
                                                <Form.Group>                                                        
                                                    <Form.Field className="document-count" control={Input} readOnly label="Copy" value={bankSetDocument.copy} />
                                                    <Form.Field className="document-count" control={Input} readOnly label="Original" value={bankSetDocument.original} />
                                                </Form.Group>
                                            }

                                        </Form.Group>
                                    </Accordion.Title>
                                    {(this.state[bankSetDocument.id+"_has_selections"] || selected.length > 0) && 
                                    <Accordion.Content active={selectionAccordionActiveIndex.includes(bankSetDocument.id)} >
                                        <Message>
                                            <Table basic='very' celled>
                                                <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell collapsing>Discrepancies</Table.HeaderCell>
                                                        <Table.HeaderCell>Comment</Table.HeaderCell>
                                                        <Table.HeaderCell collapsing>Selected Action</Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Header>
                                                <Table.Body>
                                                    {selected.map((discrepancy, index) => {
                                                        return  <Table.Row key={index}>
                                                                    <Table.Cell collapsing>{discrepancy.name}</Table.Cell>
                                                                    <Table.Cell>{discrepancy.comment}</Table.Cell>
                                                                    <Table.Cell collapsing>
                                                                        {stage === "REVIEW" ?                                                                                
                                                                            <Form.Field>
                                                                                <Dropdown placeholder='Actions' selection onChange={this.handleActionChange} error = {this.state[discrepancy.id+"_action_error"]}  
                                                                                    name={discrepancy.id+"_action"} options={actionsOptions} defaultValue={discrepancy.action} />
                                                                            </Form.Field> :
                                                                            discrepancy.action
                                                                        }</Table.Cell>
                                                                </Table.Row>
                                                        })
                                                    }
                                                </Table.Body>
                                            </Table>
                                        </Message>
                                        <br/>
                                    </Accordion.Content>
                                    }
                                </Accordion.Accordion>
                            </span>

                })
            }

            if (advanceSetDocuments && advanceSetDocuments.length > 0) {
                advanceSetDocumentsOptions = advanceSetDocuments.map(advanceSetDocument => {                    
                    let selected = advanceSetDocument.documentTypeDiscrepancies.filter( record => record.selected === true );
                    return  <span key={advanceSetDocument.id}>
                                <Accordion.Accordion exclusive={false}>
                                    <Accordion.Title active={selectionAccordionActiveIndex.includes(advanceSetDocument.id)} index={advanceSetDocument.id}>
                                        <Form.Group className={this.state[advanceSetDocument.id+"_group_error"] ? "group-error": ""} >
                                            {/* <Form.Field><Icon name='dropdown' /> </Form.Field> */}
                                            <Form.Field width={4}>
                                                <b>{this.mapdocumentTypeIdToName(advanceSetDocument.attachment.documentType)}</b>
                                            </Form.Field>
                                            <Form.Field width={4}>                        
                                                <a href ={advanceSetDocument.attachment.documentLink} download={advanceSetDocument.attachment.documentName} target="_blank">{advanceSetDocument.attachment.documentName}</a>
                                            </Form.Field>                                
                                            {AuthenticationService.hasPermission("CREATE_SHIPPING_DOCUMENT_DISCREPANCIES_BY_EVENT") &&
                                                stage !== "REVIEW" 
                                                && shippingDocumentDetails.documentExaminationStatus !== "COMPLETED"
                                                &&
                                                <Form.Group>
                                                    <Form.Field>                                                    
                                                        <NewDiscrepanciesActionsModal 
                                                            discrepancies = {advanceSetDocument.documentTypeDiscrepancies} 
                                                            callBackOnSave = {(data)=>this.saveCallback(data, advanceSetDocument.id, advanceSetDocument.documentSet)}
                                                        />
                                                    </Form.Field>
                                                    <Form.Field className="document-count" control={Input} type="number" label="Copy" name={advanceSetDocument.id+"_copy"} 
                                                        defaultValue = {advanceSetDocument.copy} onChange={this.handleCountChange}/>
                                                    <Form.Field className="document-count" control={Input} type="number" label="Original" name={advanceSetDocument.id+"_original"} 
                                                        defaultValue = {advanceSetDocument.original} onChange={this.handleCountChange}/>
                                                </Form.Group>
                                            }
                                            {stage === "REVIEW" &&
                                                <Form.Group>                                                        
                                                    <Form.Field className="document-count" control={Input} readOnly label="Copy" value={advanceSetDocument.copy} />
                                                    <Form.Field className="document-count" control={Input} readOnly label="Original" value={advanceSetDocument.original} />
                                                </Form.Group>
                                            }</Form.Group>
                                    </Accordion.Title>
                                    {(this.state[advanceSetDocument.id+"_has_selections"] || selected.length > 0) && 
                                    <Accordion.Content active={selectionAccordionActiveIndex.includes(advanceSetDocument.id)}>
                                        <Message>
                                            <Table basic='very' celled>
                                                <Table.Header>
                                                    <Table.Row>
                                                        <Table.HeaderCell collapsing>Discrepancies</Table.HeaderCell>
                                                        <Table.HeaderCell>Comment</Table.HeaderCell>
                                                        <Table.HeaderCell collapsing>Selected Action</Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Header>
                                                <Table.Body>
                                                    {selected.map((discrepancy, index) => {
                                                        return  <Table.Row key={index}>
                                                                    <Table.Cell collapsing>{discrepancy.name}</Table.Cell>
                                                                    <Table.Cell>{discrepancy.comment}</Table.Cell>
                                                                    <Table.Cell collapsing>
                                                                        {stage === "REVIEW" ?                                                                                
                                                                            <Form.Field>
                                                                                <Dropdown placeholder='Actions' selection onChange={this.handleActionChange} error = {this.state[discrepancy.id+"_action_error"]}  
                                                                                    name={discrepancy.id+"_action"} options={actionsOptions} defaultValue={discrepancy.action}
                                                                                />
                                                                            </Form.Field> :
                                                                            discrepancy.action
                                                                        }</Table.Cell>
                                                                </Table.Row>
                                                        })
                                                    }
                                                </Table.Body>
                                            </Table>
                                        </Message>
                                        <br/>
                                    </Accordion.Content>
                                    }
                                </Accordion.Accordion>
                            </span>                            
                })
            }
        }

        if (shippingDocumentDetails && shippingDocumentDetails.documentExaminationStatus === "PROCESSING") 
        {
                
            processingBankOptions = bankSetDocuments.map((bankSetDocument, i) => {
                return bankSetDocument.shippingDocumentEventDiscrepancies &&                             
                    <Accordion key={i}>
                        <Accordion.Title active={processingAccordionActiveIndex === bankSetDocument.id} index={bankSetDocument.id} onClick={this.handleProcessingAccordionClick}>
                            <Icon name='dropdown' /> {this.mapdocumentTypeIdToName(bankSetDocument.attachment.documentType)}
                        </Accordion.Title>
                        <Accordion.Content active={processingAccordionActiveIndex === bankSetDocument.id}>
                           
                            <Message>
                                <Table basic='very' celled>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell style={{width: 380}}>Discrepancies</Table.HeaderCell>
                                            <Table.HeaderCell collapsing>Expected Action</Table.HeaderCell>
                                            <Table.HeaderCell>Update</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    {bankSetDocument.shippingDocumentEventDiscrepancies.length > 0 ?
                                        <Table.Body>
                                            {bankSetDocument.shippingDocumentEventDiscrepancies.map((discrepancy, index) => {
                                                    return  <DiscrepancyReview 
                                                                key={index} 
                                                                callbackForUpload = {(response)=>this.callbackForDiscrepancyUpload(response)} 
                                                                discrepancy={discrepancy} 
                                                                reviewRequest={this.reviewRequest}
                                                            />
                                                })
                                            }
                                        </Table.Body>:
                                        <Table.Body>
                                            <Table.Cell colSpan={3}>No discrepancies</Table.Cell>
                                        </Table.Body>
                                    }
                                </Table>
                            </Message>
                        </Accordion.Content>
                    </Accordion>
            })
        
            processingAdvanceOptions = advanceSetDocuments.map((advanceSetDocument, i) => {
                return advanceSetDocument.shippingDocumentEventDiscrepancies &&                             
                    <Accordion key={i}>
                        <Accordion.Title active={processingAccordionActiveIndex === advanceSetDocument.id} index={advanceSetDocument.id} onClick={this.handleProcessingAccordionClick}>
                            <Icon name='dropdown' /> {this.mapdocumentTypeIdToName(advanceSetDocument.attachment.documentType)}
                        </Accordion.Title>
                        <Accordion.Content active={processingAccordionActiveIndex === advanceSetDocument.id}>
                            <Message>
                                <Table basic='very' celled>
                                    <Table.Header>
                                        <Table.Row>
                                            <Table.HeaderCell style={{width: 380}}>Discrepancies</Table.HeaderCell>
                                            <Table.HeaderCell collapsing>Expected Action</Table.HeaderCell>
                                            <Table.HeaderCell>Update</Table.HeaderCell>
                                        </Table.Row>
                                    </Table.Header>
                                    {advanceSetDocument.shippingDocumentEventDiscrepancies.length > 0 ?
                                    <Table.Body>
                                        {advanceSetDocument.shippingDocumentEventDiscrepancies.map((discrepancy, index) => {
                                            return  <DiscrepancyReview 
                                                        key={index} 
                                                        callbackForUpload = {(response)=>this.callbackForDiscrepancyUpload(response)} 
                                                        discrepancy={discrepancy} 
                                                        reviewRequest={this.reviewRequest}
                                                    />
                                            })
                                        }
                                    </Table.Body>:
                                    <Table.Body>
                                        <Table.Cell colSpan={3}>No discrepancies</Table.Cell>
                                    </Table.Body>}
                                </Table>
                            </Message>
                        </Accordion.Content>
                    </Accordion>
            })
                
        }
        
        return (      
            <AppWrapper> 
                <Header as='h3'>Document Examination</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment padded style={{ paddingBottom: 60 }} className={stage==="REVIEW" ? "review-mode": ""}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/examination">Document Examination </Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Edit Discrepancies</Breadcrumb.Section>
                                </Breadcrumb>                                 
                                
                                {shippingDocumentDetails 
                                    && AuthenticationService.hasPermission('AUTHORIZE_DOCUMENT_EXAMINATION_REQUESTS') 
                                    && shippingDocumentDetails.documentExaminationStatus === "DOCUMENT_EXAMINATION_APPROVAL_REQUEST" 
                                    && <div className="rhs-float-status">
                                            <Label color='blue' size="large" as='a' content="Review Request" onClick={this.setReviewMode}/>
                                        </div>
                                }
                                {shippingDocumentDetails 
                                    && shippingDocumentDetails.documentExaminationStatus === "REJECTED" 
                                    && <div className="rhs-float-status">
                                            <ApprovalsModal 
                                                info = {{ status: "", recordId: shippingDocumentDetails.id, type: "DOCUMENT_EXAMINATION", parentId: shippingDocumentDetails.id }} 
                                                trigger={<Label color='red' as='a' content="REJECTED"/>}
                                            />
                                        </div>
                                }
                                {shippingDocumentDetails 
                                    && shippingDocumentDetails.documentExaminationStatus === "PROCESSING" 
                                    && <div className="rhs-float-status">
                                            <ApprovalsModal 
                                                info = {{ status: "", recordId: shippingDocumentDetails.id, type: "DOCUMENT_EXAMINATION", parentId: shippingDocumentDetails.id }} 
                                                trigger={<Label color='green' as='a' content="PROCESSING"/>}
                                            />
                                        </div>
                                }
                                {shippingDocumentDetails 
                                    && shippingDocumentDetails.documentExaminationStatus === "COMPLETED" 
                                    && <div className="rhs-float-status">
                                            <Label color='green' as='a' content="COMPLETED"/>
                                        </div>
                                }
                                <br/>
                                <br/>   
                                                                         
                                <Form loading={isLoadingForm}>
                                    <Segment.Group>                                         
                                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                                        {shippingDocumentDetails && shippingDocumentDetails.consignmentNumber &&
                                        <Segment>                                                    
                                            <Header> CONSIGNMENT NUMBER: {shippingDocumentDetails.consignmentNumber} </Header>
                                        </Segment>
                                        }
                                        {shippingDocumentDetails &&
                                        <Segment>
                                            <Label as='a' color='grey' ribbon>FORM NUMBER: {shippingDocumentDetails && shippingDocumentDetails.formNumber}</Label>
                                            <Table basic='very' fixed>
                                                <Table.Body>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Negotiation Bank Name
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Sender Reference
                                                                <Header.Subheader>{shippingDocumentDetails.senderReference}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Amount
                                                                <Header.Subheader>{shippingDocumentDetails.documentAmount + " "+ shippingDocumentDetails.currencyCode }</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Document Number
                                                                <Header.Subheader>{shippingDocumentDetails.documentNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Number
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Date
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceDate}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Courier Company
                                                                <Header.Subheader>{shippingDocumentDetails.courierCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Shipping Company
                                                                <Header.Subheader>{shippingDocumentDetails.shippingCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Container Number (Number of Containers)
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber + " ( "+shippingDocumentDetails.numberOfContainers+" ) "}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                    </Table.Row>
                                                </Table.Body>
                                            </Table>
                                        </Segment>
                                        }
                                        <Segment>                                              
                                            <Header as='h3'>Uploads</Header> 
                                            {(shippingDocumentDetails && shippingDocumentDetails.documentExaminationStatus !== "PROCESSING") &&
                                            <span>                                                                               
                                                {bankSetDocuments.length > 0 &&
                                                <fieldset>
                                                    <legend>Bank Set</legend>
                                                    <br/> {bankSetDocumentsOptions} 
                                                </fieldset>
                                                }
                                                <br/>
                                                
                                                {advanceSetDocuments.length > 0 &&
                                                <fieldset>
                                                    <legend>Advance Set</legend>
                                                    <br/> {advanceSetDocumentsOptions} 
                                                </fieldset>
                                                }
                                                <br/>
                                            </span>
                                            }

                                            {stage === "REVIEW" && 
                                                <Form.Field label="Comment" placeholder="Comment (required for rejection)" control={TextArea} rows={4} onChange={this.handleInputChange} name="reviewComment" value={this.state.reviewComment}/>
                                            }

                                            {(shippingDocumentDetails && 
                                                shippingDocumentDetails.documentExaminationStatus === "PROCESSING") &&
                                                <span>
                                                    
                                                    {processingBankOptions.length > 0 &&
                                                    <fieldset>
                                                        <legend>Bank Set</legend>
                                                        <br/> {processingBankOptions} 
                                                    </fieldset>
                                                    }
                                                    <br/>
                                                        
                                                    {processingAdvanceOptions.length > 0 &&
                                                    <fieldset>
                                                        <legend>Advance Set</legend>
                                                        <br/> {processingAdvanceOptions} 
                                                    </fieldset>
                                                    }
                                                    <br/>
                                                </span>
                                            }

                                            {shippingDocumentDetails 
                                            && shippingDocumentDetails.documentExaminationStatus !== "PROCESSING"
                                            && shippingDocumentDetails.documentExaminationStatus !== "COMPLETED"
                                            &&  <div className="rhs-status">
                                                    {(shippingDocumentDetails.documentExaminationStatus === "PENDING" && stage !== "REVIEW") 
                                                    && AuthenticationService.hasPermission("CREATE_SHIPPING_DOCUMENT_DISCREPANCIES_BY_EVENT") 
                                                    && <span>
                                                            <a href="#/examination">Cancel</a>
                                                            &nbsp;&nbsp;&nbsp;   
                                                            <ConfirmAction 
                                                                header="Confirm Action" 
                                                                confirmAction = {()=>this.submit()} 
                                                                closeAction = {()=>this.closeConfirm()} 
                                                                content = "Are you sure you want to perform this action?" 
                                                                trigger = {<Button size="large" color="red" onClick={()=>this.openConfirm()}>Request for Approval </Button>}
                                                                open = {this.state.open}
                                                            />                
                                                                        
                                                            
                                                        </span>
                                                    }                                                    
                                                    {(shippingDocumentDetails.documentExaminationStatus === "DOCUMENT_EXAMINATION_APPROVAL_REQUEST" 
                                                    || shippingDocumentDetails.documentExaminationStatus === "REJECTED") 
                                                    && stage !== "REVIEW"
                                                    && AuthenticationService.hasPermission("CREATE_SHIPPING_DOCUMENT_DISCREPANCIES_BY_EVENT") 
                                                    &&<span>
                                                            <a href="#/examination">Cancel</a>
                                                            &nbsp;&nbsp;&nbsp;   
                                                            <ConfirmAction 
                                                                header="Confirm Action" 
                                                                confirmAction = {()=>this.submit()} 
                                                                closeAction = {()=>this.closeConfirm()} 
                                                                content = "Are you sure you want to perform this action?" 
                                                                trigger = {
                                                                    <Button size="large" color="red" onClick={()=>this.openConfirm()}>Update &amp; Re-Request for Approval</Button>                                                                                                           
                                                                }
                                                                open = {this.state.open}
                                                            />                                                 
                                                            
                                                        </span>
                                                    }                                                    
                                                    {stage === "REVIEW"
                                                    && AuthenticationService.hasPermission('AUTHORIZE_DOCUMENT_EXAMINATION_REQUESTS') 
                                                    && <span>
                                                            <a className="ut-pointer" onClick={this.resetView}>Exit Review Mode</a>
                                                            &nbsp;&nbsp;&nbsp;
                                                            <Button color='red' onClick={this.disapprove} inverted><Icon name='cancel' /> Return for correction by inputter </Button>
                                                            <Button color='green' onClick={this.approve} inverted><Icon name='checkmark' /> Approve </Button>
                                                        </span>
                                                    }                                             
                                                </div>
                                            }
                                        </Segment>                         
                                    </Segment.Group>
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default DocumentExaminationView