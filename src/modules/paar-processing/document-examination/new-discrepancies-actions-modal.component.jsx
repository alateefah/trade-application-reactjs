import React, { Component } from 'react';
import { Form, Button, Dropdown, Modal, Checkbox, Input } from 'semantic-ui-react';
import ErrorMessage from '../../shared/message/error.component';

class NewDiscrepancyActionModal extends Component {

    constructor (props) {
        super(props);

        this.state = {
            discrepancies: "",
            modalOpen: false,
            selected: []
        }
    }
   
    openModal = () => this.setState ({ modalOpen: true, discrepancies: this.props.discrepancies });  

    handleClose = () => { this.setState ({ modalOpen: false })  }  
    
    handleCheckboxChange = (e, { value }) => {
        const {discrepancies} = this.state   
        
        let index = discrepancies.findIndex((obj => obj.id === value));
        discrepancies[index].selected = !discrepancies[index].selected;

    }

    handleCommentChange = (e, { name, value }) => {
        const {discrepancies} = this.state

        let id = name.substr(0, name.indexOf('_')); 

        let index = discrepancies.findIndex((obj => obj.id === parseInt(id, 10) ));

        discrepancies[index].comment = value;
    }

    handleSelectChange = (e, val) => {
        console.log(val)
        const {discrepancies} = this.state

        let index = discrepancies.findIndex((obj => obj.id === val.name));

        console.log(discrepancies[index])
        discrepancies[index].action = val.value;
        console.log(discrepancies[index])

    }

    save = () => {
        const {discrepancies} = this.state;
        this.handleClose();
        this.props.callBackOnSave(discrepancies);         
    }

    render () {
        const {discrepancies} = this.state;
        let actionsOptions, discrepanciesOptions;      

        actionsOptions = [
            {id: "Applicant to waive", text: "Applicant to waive", value: "Applicant to waive"},
            {id: "Beneficiary to correct", text: "Beneficiary to correct", value: "Beneficiary to correct"},
            {id: "For Information only", text: "For Information only", value: "For Information only"},
            {id: "Ignore", text: "Ignore", value: "Ignore"},
        ]

        if (discrepancies && discrepancies.length) {
            discrepanciesOptions = discrepancies.map(discrepancy => {
                return  <Form.Group key={discrepancy.id}>     
                            <Form.Field width={6} control={Checkbox} label={discrepancy.name} value={discrepancy.id} 
                                onChange={this.handleCheckboxChange} defaultChecked={discrepancy.selected} />
                            <Form.Field width={6} control={Input} placeholder="Comment" onChange={this.handleCommentChange} 
                                defaultValue ={discrepancy.comment} name={discrepancy.id+"_comment"} />
                            <Form.Field width={4}>
                                <Dropdown placeholder='Actions' selection onChange={this.handleSelectChange} 
                                    name={discrepancy.id} options={actionsOptions} 
                                    defaultValue={discrepancy.action}
                                />
                            </Form.Field> 
                        </Form.Group>
            })
        }

        return (      
            <span> 
                <a className="ut-pointer" onClick={this.openModal} >Add/View Discrepancies </a>
                <Modal size='large' open={this.state.modalOpen}>
                    <Modal.Header> Select Applicable Discrepancies </Modal.Header>
                    <Modal.Content scrolling> 
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        <Form>  
                            {discrepanciesOptions}
                        </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <a onClick={this.handleClose} className="ut-pointer">Cancel</a>
                        &nbsp;&nbsp;&nbsp;
                        <Button color="red" content="Save" onClick={this.save} />
                    </Modal.Actions>
                </Modal>
            </span>          
        )
    }
    
}

export default NewDiscrepancyActionModal