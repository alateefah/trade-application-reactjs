import React, { Component } from 'react'
import { Table, Icon, Label, Button } from 'semantic-ui-react';
import DocumentDisplayModal from '../../shared/document-display-modal.component';
import ReviewRequest from '../../shared/review-request-modal.component';
import AuthenticationService from './../../../services/authentication_service';
import ApprovalsModal from '../../shared/approvals-modal.component';
import ReviewDiscrepancyModal from './review-discrepancy-modal.component';

class DiscrepancyReview extends Component {
    state = {discrepancy: this.props.discrepancy, loading: false}

    render () {
        const {discrepancy, loading} = this.state;

        return (  
            loading ? 
            <Table.Row>
                <Table.Cell >{discrepancy.name}</Table.Cell>
                <Table.Cell collapsing>{discrepancy.action}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Table.Cell>
                <Table.Cell ><Icon loading name='spinner' size='large' /></Table.Cell>
            </Table.Row>:
            <Table.Row>
                <Table.Cell >{discrepancy.name}</Table.Cell>
                <Table.Cell collapsing>{discrepancy.action}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</Table.Cell>
                <Table.Cell>
                    {discrepancy.attachment ?
                        <span>
                            <DocumentDisplayModal document={discrepancy.attachment} floated="left"/>                                        
                            {AuthenticationService.hasPermission('AUTHORIZE_DOCUMENT_DISCREPANCY_REQUESTS') &&
                            discrepancy.approvalStatus === "PENDING" &&
                            <ReviewRequest
                                trigger = {<Label as="a" content="Review Request" style={{float: "right"}} />}
                                info = {{ recordId: discrepancy.id, 
                                            type: "DOCUMENT_DISCREPANCY", 
                                            parentId: discrepancy.shippingDocumentDetailId, 
                                            status: "PENDING",
                                            url: "documentdiscrepancy" 
                                        }} 
                                callbackForReview={(data)=>this.props.reviewRequest(data)} /> 
                            }

                            {/* {discrepancy.approvalStatus === "REJECTED" && 
                                <ReviewDiscrepancyModal 
                                    modalTrigger={<Popup trigger={<Icon as="i" link color="teal" name="refresh" style={{float: "right"}} />} size='tiny' content='Upload new discrepancy waiver' inverted />}
                                    // modalTrigger={<Icon link color="teal" name="refresh" style={{float: "right"}} />}
                                    discrepancy={discrepancy} callbackForUpload={(response)=>this.props.callbackForUpload(response)}/>
                            } */}

                            {discrepancy.approvalStatus === "REJECTED" && 
                            <ReviewDiscrepancyModal 
                                // modalTrigger={<Popup trigger={<Icon as="i" link color="teal" name="refresh" style={{float: "right"}} />} size='tiny' content='Upload new discrepancy waiver' inverted />}
                                modalTrigger={<Icon link color="teal" name="refresh" style={{float: "right"}} />}
                                discrepancy={discrepancy} callbackForUpload={(response)=>this.props.callbackForUpload(response)}/>
                            }

                            {discrepancy.approvalStatus !== "PENDING" &&
                            <ApprovalsModal 
                                info = {{ status: "", recordId: discrepancy.id, type: "DOCUMENT_DISCREPANCY", parentId: discrepancy.shippingDocumentDetailId }} 
                                trigger={<Label color={discrepancy.approvalStatus === "REJECTED"? 'red': 'green'} as='a' content={discrepancy.approvalStatus} style={{float: "right"}}/>}
                            />}

                            
                            
                        </span> :discrepancy.action !== "Ignore" &&
                        <ReviewDiscrepancyModal modalTrigger={<Button color="teal" size="small" content="Upload Review"/>} discrepancy={discrepancy} callbackForUpload={(response)=>this.props.callbackForUpload(response)}/>              
                    }
                </Table.Cell>
            </Table.Row>            
        )
    }
}

export default DiscrepancyReview