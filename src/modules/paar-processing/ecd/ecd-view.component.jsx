import React, { Component } from 'react';
import { Grid, Header, Segment, Breadcrumb, Form, Table, Label, Icon, Checkbox, Button, Popup} from 'semantic-ui-react';
import AppWrapper from './../../shared/main/app-wrapper.component';
import PaarService from './../../../services/paar_service';
import AuthenticationService from './../../../services/authentication_service';
import SharedService from './../../../services/shared_service';
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import DocumentDisplayModal from '../../shared/document-display-modal.component';
import ReviewRequest from '../../shared/review-request-modal.component';
import ApprovalsModal from '../../shared/approvals-modal.component';
import RequestApprovalReviewModal from '../../shared/request-approval-review-modal.component';
import UploadRow from './upload-row.component';
import ChangeAttachmentModal from './change-attachment-modal.component';

let _ = require("lodash");

class EcdView extends Component {

    constructor (props) {
        super(props);

        this.state = {
            errorMessage: null,
            successMessage: null,
            refreshErrorMessage: null,
            isLoadingForm: false,
            shippingDocumentDetails: null,
            shippingDocumentDetailsId: props.match.params.shippingDocumentDetailsId,
            saving: false,
            acknowledgeds: [],
            upload: "",
            uploads: [],
            index: 0,
            newEcds: [],
            approvalModal: false,
            documentTypes: []
        }
    }

    componentWillMount () { this.getEcdDocumentTypes(); }
    
    getEcdDocumentTypes = () => {
        SharedService.getAppParameter("ECD_DOCUMENTS_LIST")
        .then(response => {
            this.setState({ isLoadingForm : false })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState ({ documentTypes: response.value.split(",") })
                this.getEcdDocumentDetails();
            }  
        })
    }

    getEcdDocumentDetails = () => {
        this.setState({shippingDocumentDetails: null, isLoadingForm: true, newEcds: [] }) ;
        PaarService.getEcdDocumentDetails(this.state.shippingDocumentDetailsId)        
        .then(response => {   
            this.setState({ isLoadingForm: false})  
            if (response.code) {
                this.setState({ errorMessage : response.description })
            } else {
                this.setState({ shippingDocumentDetails : response })
            }    
            
        }) 
    }

    reviewRequest = (data) => {
        this.setState({ isLoadingForm: true})        
        SharedService.authorizeApproval(data)
        .then(response => {  
            this.setState({ isLoadingForm: false})
            if (response.code) {           
                this.alertError(response.description);                
            } else {                
                this.alertSuccess("Review Successful");
            }
        })
        
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg, newEcds: [], uploads: [], acknowledgeds: [] });
        this.getEcdDocumentDetails();
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    toggle = (e, { value }) => {
        let acknowledgeds = this.state.acknowledgeds;
        if (acknowledgeds.includes(value)) {
            var index = acknowledgeds.indexOf(value);
            acknowledgeds.splice(index, 1);
            this.setState({ acknowledgeds: acknowledgeds }); 
        } else {    
            let newArray = [ ...this.state.acknowledgeds, value ];
            this.setState({ acknowledgeds: newArray }); 
        }
    }

    acknowledgeReceipt = () => {
        this.setState({ isLoadingForm: true})
        let params = {
            id: this.state.shippingDocumentDetailsId,
            ecdsAcknowledged: this.state.acknowledgeds
        }
        PaarService.acknowledgeReceipt(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.alertSuccess("Successful")
            }
        })
    }
    
    deleteEcd = (id) => {
        this.setState({ isLoadingForm: true})        
        PaarService.deleteEcd(id)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.alertSuccess("Successful")
            }
        })
    }

    requestApproval = (data) => {
        this.setState({ isLoadingForm: true })
        SharedService.requestApproval(data)
        .then (response => {
            this.setState({ isLoadingForm: false})
            if (response.code) {
                this.alertError(response.description);
            } else {
                this.alertSuccess("Successful");
            }
        })
    }

    setUpload = () => {
        this.setState({            
            upload: <UploadRow key={this.state.index}  info={{formInfoId: 15, index: this.state.index}} 
                        documentTypes= {this.state.documentTypes}
                        callbackForUpload = {(data)=>this.afterUpload(data)}
                        callbackForDelete = {(data)=>this.afterUploadDelete(data)} />
        }, () => {
            let newArray = [ ...this.state.uploads,  this.state.upload];
            this.setState({ uploads: newArray, index: this.state.index + 1}); 
        })
    }

    afterUpload = (data) => {
        let newArray = [ ...this.state.newEcds, data ];
        
        this.setState({ newEcds: newArray })

    }

    afterUploadDelete = (id) => {
        var newArray = this.state.newEcds;
        var index = _.findIndex(newArray, ["attachmentId", id])
        newArray.splice(index, 1);

        this.setState({ newEcds: newArray });
    } 
    
    addNewEcds = () => {
        this.setState({ isLoadingForm: true})
        let params = {
            id: this.state.shippingDocumentDetailsId,
            ecds: this.state.newEcds
        }
        PaarService.createEcd(params)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code || response.error) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful! ")
            }
        })

    }

    clearAll = () => {
        this.setState({ shippingDocumentDetails: null, isLoadingForm: true, acknowledgeds: []  }) }

    render () {
        const {isLoadingForm, acknowledgeds, uploads, newEcds, shippingDocumentDetails} = this.state;  
        let ecdOptions;

        if (shippingDocumentDetails !== null) {
            ecdOptions = shippingDocumentDetails.ecds.map(ecd => {
                let attachment = ecd.attachment;
                return  <Table.Row key={ecd.id}>
                            <Table.Cell collapsing textAlign='center'>

                                {ecd.status === 'PENDING_ACKNOWLEDGEMENT' &&
                                    <Checkbox label='Acknowledge Receipt' value={ecd.id} onChange={this.toggle} />
                                }

                                {ecd.status === 'ACKNOWLEDGED' 
                                && AuthenticationService.hasPermission("UPDATE_ECD_ATTATCHMENTS") 
                                &&  <RequestApprovalReviewModal ecd={ecd}
                                        trigger={<Label as='a' content="Request Approval" />}
                                        callbackForApprovalRequest = {(data)=>this.requestApproval(data)}
                                    />
                                }

                                {ecd.status === 'PENDING_APPROVAL' &&
                                AuthenticationService.hasPermission("AUTHORIZE_ECD_REQUESTS") &&
                                    <ReviewRequest
                                        trigger = {<Label color='blue' as='a' content="Review Approval Request"/>}
                                        info = {{ 
                                            recordId: ecd.id, 
                                            type: "ECD_PROCESSING", 
                                            parentId: ecd.shippingDetailId, 
                                            status: "PENDING",
                                            url: "ecd"
                                        }} 
                                        callbackForReview={(data)=>this.reviewRequest(data)}
                                    /> 
                                }

                                {ecd.status === 'PENDING_REJECTION' &&
                                AuthenticationService.hasPermission("AUTHORIZE_ECD_REQUESTS") &&
                                    <ReviewRequest
                                        trigger = {<Label color='red' as='a' content="Review Rejection Request"/>}
                                        info = {{ 
                                            recordId: ecd.id, 
                                            type: "ECD_PROCESSING", 
                                            parentId: ecd.shippingDetailId, 
                                            status: "PENDING",
                                            url: "ecd"
                                        }} 
                                        callbackForReview={(data)=>this.reviewRequest(data)}
                                    /> 
                                }

                                {(ecd.status === 'APPROVAL_REJECTED' ||  ecd.status === 'REJECTED') &&                             
                                    <ApprovalsModal 
                                        info = {{ status: "", recordId: ecd.id, type: "ECD_PROCESSING", parentId: ecd.shippingDetailId }} 
                                        trigger={<Label color='red' as='a' content={ecd.status.replace(/[_-]/g, " ")}/>}
                                    />
                                }
                                
                                {ecd.status === 'APPROVED' && 
                                    <ApprovalsModal 
                                        info = {{ status: "", recordId: ecd.id, type: "ECD_PROCESSING", parentId: ecd.shippingDetailId }} 
                                        trigger={<Label color='green' as='a' content={ecd.status} />}
                                    />}

                                {ecd.status === 'PENDING' && 
                                    <ApprovalsModal 
                                        info = {{ status: "", recordId: ecd.id, type: "ECD_PROCESSING", parentId: ecd.shippingDetailId }} 
                                        trigger={<Label color='red' as='a' content="REJECTED"/>}
                                    />
                                }
                            </Table.Cell> 
                            <Table.Cell collapsing>{attachment.documentType}</Table.Cell>
                            <Table.Cell collapsing> 
                                <DocumentDisplayModal document={attachment} />
                                &nbsp;&nbsp;&nbsp;
                            </Table.Cell>
                            <Table.Cell>{ecd.comments}</Table.Cell>
                            <Table.Cell collapsing textAlign='right'>                                
                                {AuthenticationService.hasPermission("DELETE_ECD_SHIPPING_DETAILS_BY_ID") &&
                                ecd.status === 'PENDING_ACKNOWLEDGEMENT' &&
                                    <Popup
                                        trigger={<Icon name="trash" color="red" onClick={()=>this.deleteEcd(ecd.id)}/>}
                                        content='Delete'
                                        inverted
                                    />                            
                                }
                                {AuthenticationService.hasPermission("DELETE_ECD_SHIPPING_DETAILS_BY_ID") &&
                                (ecd.status === 'REJECTED' || ecd.status === "APPROVAL_REJECTED" || ecd.status === 'PENDING') &&                            
                                <ChangeAttachmentModal documentTypes = {this.state.documentTypes} id={ecd.id} completeUpload={()=>this.getEcdDocumentDetails()} /> 
                                }
                            </Table.Cell>
                        </Table.Row>
            })
        }

        return (      
            <AppWrapper> 
                <Header as='h3'>ECD</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/ecd">ECD List  </Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Edit ECD</Breadcrumb.Section>
                                </Breadcrumb>   <br/><br/>    
                                                                         
                                <Form loading={isLoadingForm}>
                                {shippingDocumentDetails  && 
                                    <Segment.Group>                                         
                                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                                        {shippingDocumentDetails.consignmentNumber &&
                                        <Segment>                                                    
                                            <Header> CONSIGNMENT NUMBER: {shippingDocumentDetails.consignmentNumber} </Header>
                                        </Segment>
                                        }
                                        <Segment>
                                            <Label as='a' color='grey' ribbon>FORM NUMBER: {shippingDocumentDetails && shippingDocumentDetails.formNumber}</Label>
                                            <Table basic='very' fixed>
                                                <Table.Body>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Negotiation Bank Name
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Sender Reference
                                                                <Header.Subheader>{shippingDocumentDetails.senderReference}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Amount
                                                                <Header.Subheader>{shippingDocumentDetails.documentAmount + " ( "+ shippingDocumentDetails.currencyCode +" )"}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Document Number
                                                                <Header.Subheader>{shippingDocumentDetails.documentNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Number
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceNumber}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Invoice Date
                                                                <Header.Subheader>{shippingDocumentDetails.invoiceDate}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>                                                
                                                    </Table.Row>
                                                    <Table.Row>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Courier Company
                                                                <Header.Subheader>{shippingDocumentDetails.courierCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Shipping Company
                                                                <Header.Subheader>{shippingDocumentDetails.shippingCompany}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                        <Table.Cell>
                                                            <Header as='h4'>
                                                                <Header.Content> Container Number (Number of Containers)
                                                                <Header.Subheader>{shippingDocumentDetails.containerNumber + " ( "+shippingDocumentDetails.numberOfContainers+" ) "}</Header.Subheader>
                                                                </Header.Content>
                                                            </Header>
                                                        </Table.Cell>
                                                    </Table.Row>
                                                </Table.Body>
                                            </Table>
                                        </Segment>
                                        
                                        <Segment> 
                                            <Header as='h3'>Uploads</Header> 
                                            
                                            <Table celled compact definition> 
                                                <Table.Header fullWidth>                                                
                                                    <Table.Row>
                                                        <Table.HeaderCell />
                                                        <Table.HeaderCell>Document Type</Table.HeaderCell>
                                                        <Table.HeaderCell>Document</Table.HeaderCell>
                                                        <Table.HeaderCell>Comments </Table.HeaderCell>
                                                        <Table.HeaderCell />
                                                    </Table.Row>
                                                </Table.Header>
                                                <Table.Body>
                                                    {ecdOptions}
                                                </Table.Body>
                                                {acknowledgeds.length > 0 &&
                                                <Table.Footer>
                                                    <Table.Row>
                                                        <Table.HeaderCell />
                                                        <Table.HeaderCell colSpan='4'>
                                                            <Button size='small' color="teal" onClick={this.acknowledgeReceipt}>Acknowledge Selected</Button>
                                                        </Table.HeaderCell>
                                                    </Table.Row>
                                                </Table.Footer>
                                                }                                                
                                            </Table>   
                                            <Header as='h5' onClick={() => this.setUpload()} className="ut-pointer">
                                                <Icon name='add circle' color = "green" /> Add more
                                            </Header>   
                                            {uploads}
                                            {newEcds.length > 0 &&
                                                <Button color="red" content="Submit" onClick={this.addNewEcds} />
                                            }
                                        </Segment>                         
                                    </Segment.Group>
                                }
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default EcdView