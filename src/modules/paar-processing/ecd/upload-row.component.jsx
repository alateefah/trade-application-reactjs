import React, { Component } from 'react'
import { Form, Icon, Button} from 'semantic-ui-react'
import SharedService from './../../../services/shared_service'
import UploadAttachment from './../../shared/upload-attachment.component'

class UploadRow extends Component {

    constructor (props) {
        super(props);

        this.state = {
            documentTypes: props.documentTypes,
            documentType: "",
            info: props.info, 
            uploaded: false,
            uploadedDocumentType: "",
            documentLink: "",
            documentName: "",
            attachmentId: "",
            comments: "",
            saved: false
        }
    }

    handleSelectChange = (e, { value }) => {
        this.setState({ documentType: value })      
    }

    afterUpload = (response) => {
        let data = response.data;
        this.setState({ 
            uploaded: true, 
            documentLink: data.documentLink,
            documentName: data.documentName,
            attachmentId: data.id,
        })      
    }

    save = () => {
        this.setState({ saving: true})
        let params = {
            attachmentId: this.state.attachmentId,
            comments: this.state.comments
        }
        this.props.callbackForUpload(params);
        this.setState({ saving: false, saved: true})
    }

    deleteAttachment = (id) => {
        this.setState({ loadingMessage : "Deleting..." })
        SharedService.deleteAttachment(id)
        .then(response => { 
            if (response.code) {  
                               
            } else {  
                this.setState ({
                    uploaded: false,
                    uploadedDocumentType: "",
                    documentLink: "",
                    documentName: "",
                    attachmentId: "",
                    documentType: ""
                })
                this.props.callbackForDelete(id);
            }
        })
    }

    handleInputChange = (e, {name, value}) => this.setState({ [name]: value}); 

    render () {
        const { documentTypes, documentType, info, uploaded, documentLink, 
            documentName, attachmentId, saved } = this.state;
        let documentTypesOptions = [];   


        if (documentTypes.length > 0) {
            documentTypesOptions = documentTypes.map((documentType, index) => (
                { key: index, text: documentType, value: documentType }            
            ));
        }

        return (      
            <Form.Group>
                <Form.Field width={5}>  
                    {uploaded ? 
                    <b>{documentType}</b>: 
                    <Form.Select label='Document Type' options={documentTypesOptions}
                        placeholder='Select Document to upload' name = "documentType"
                        onChange = {this.handleSelectChange} value = {documentType} />
                    }
                </Form.Field>
                
                <Form.Field width={10}>                    
                    {documentType && !uploaded &&
                    <span>
                        <label>&nbsp;</label>
                        <UploadAttachment
                            document={{document: documentType, index: info.index, form: info.formInfoId }} 
                            callbackForUpload={this.afterUpload} />                          
                        
                    </span>
                    }
                    {uploaded &&
                        <span>
                            <Form.Field>
                                <a href ={documentLink} download={documentName} target="_blank">{documentName}</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <Icon name="trash" color="red" link onClick={() => this.deleteAttachment(attachmentId)}/>
                            </Form.Field>
                            <Form.Group>
                                <Form.Input readOnly={saved} label="Comment" width={10} onChange={this.handleInputChange} name="comments" value={this.state.comments}/>
                                <Form.Field>
                                    <label>&nbsp;</label>
                                    {!saved &&
                                    <Button content ="Save" color="teal" onClick={this.save} />}
                                </Form.Field>
                            </Form.Group>                            
                        </span>
                    }
                </Form.Field>
                
            </Form.Group>                                                                                
        )
    }
    
}

export default UploadRow