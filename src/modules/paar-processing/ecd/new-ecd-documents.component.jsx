import React, { Component } from 'react'
import { Checkbox, Table, Grid, Header, Segment, Breadcrumb, Form, Message, Button, Icon } from 'semantic-ui-react'
import AppWrapper from './../../shared/main/app-wrapper.component'
import PaarService from './../../../services/paar_service'
import SharedService from './../../../services/shared_service'
import ErrorMessage from '../../shared/message/error.component';
import SuccessMessage from '../../shared/message/success.component';
import UploadRow from './upload-row.component';
import FormSearch from '../../shared/form-search.component';

var _ = require("lodash")

class NewEcdDocuments extends Component {

    constructor (props) {
        super (props);

        this.state = {
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            formData: {
                ecds: [],
                id: ""
            },
            isLoadingForm: false,
            errorMessage: null,
            index: 0,
            upload: "",
            uploads: [],
            documentTypes: []
        }
    }

    componentWillMount () { this.getEcdDocumentTypes(); }
    
    getEcdDocumentTypes = () => {
        SharedService.getAppParameter("ECD_DOCUMENTS_LIST")
        .then(response => {
            this.setState({ isLoadingForm : false })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState ({ documentTypes: response.value.split(",") })
            }  
        })
    }

    setUpload = () => {
        this.setState({            
            upload: <UploadRow key={this.state.index}  info={{formInfoId: 15, index: this.state.index}} 
                        documentTypes={this.state.documentTypes}
                        callbackForUpload = {(data)=>this.afterUpload(data)}
                        callbackForDelete = {(data)=>this.afterUploadDelete(data)} />
        }, () => {
            let newArray = [ ...this.state.uploads,  this.state.upload];
            this.setState({ uploads: newArray, index: this.state.index + 1}); 
        })
    }

    handleFormNumberChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({  [name] : value });  
    }

    search = () => {         
        this.setState({ isLoadingForm: true, searched: true });
        let params = {
            formNumber: this.state.formNumber,
            type: "PAAR"
        }
        PaarService.getShippingDetailsByFormNumber(params)
        .then(response => {       
            this.setState({ isLoadingForm: false });
            if (response.code) {
                this.setState ({ searched: false })
                this.alertError(response.description)
            } else {
                this.setState ({ searched: true, result: response })
            }
        })        
    }
        
    handleSelectedFormChange = (e, { value }) => {
        this.setState({ 
            formData: { 
                ...this.state.formData, 
                id: value
            } 
        })
    }

    clearForm = () => {
        this.setState({ 
            formNumber: "",
            searched: false,
            result: [],
            formDetails: null,
            formData: {
                ecds: [],
                id: ""
            },
            isLoadingForm: false,
            errorMessage: null 
        })
    }    

    afterUpload = (data) => {
        let newArray = [ ...this.state.formData.ecds, data ];
        
        this.setState({
            formData: {
                ...this.state.formData,
                ecds: newArray
            }
        })

    }

    afterUploadDelete = (id) => {
        var newArray = this.state.formData.ecds;
        var index = _.findIndex(newArray, ["attachmentId", id])
        newArray.splice(index, 1);

        this.setState({ 
            formData: {
                ...this.state.formData,
                ecds: newArray
            }
        });
    } 
    
    submit = () => {
        this.setState({ isLoadingForm: true})
        PaarService.createEcd(this.state.formData)
        .then(response => {
            this.setState({ isLoadingForm: false})
            if (response.code || response.error) {
                this.alertError(response.description)
            } else {
                this.alertSuccess("Successful!");
                window.location.href="#/ecd/"+response.identityValue;
            }
        })
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg });
        this.clearForm();
        window.scrollTo(0,0);
        setTimeout(() => this.setState({ successMessage: null }), 4000);
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg });
        window.scrollTo(0,0);
        setTimeout(() => this.setState({ errorMessage: null }), 4000);
    }

    setSelectedForm = (selected) => {
        this.setState({ formNumber: selected }, () => { this.search() })
    }
        
    clearSelectedForm = () => this.setState({ formNumber: "" })
    
    render () {
        const { result, isLoadingForm, errorMessage, formData, uploads} = this.state;   
        let resultOptions;

        resultOptions = result.length > 0 && result.map(ecd => {
                        return  <Table.Row key={ecd.id}>
                                    <Table.Cell>
                                        <Checkbox radio name='selectedForm' value={ecd.id} checked={formData.id === ecd.id}
                                            onChange={this.handleSelectedFormChange} />
                                    </Table.Cell>                            
                                    <Table.Cell className='no-left-border'>{ecd.containerNumber}</Table.Cell>
                                    <Table.Cell>{ecd.courierCompany}</Table.Cell>
                                    <Table.Cell>{ecd.shippingCompany}</Table.Cell>
                                    <Table.Cell>{ecd.documentAmount}</Table.Cell>
                                    <Table.Cell>{ecd.createdOn } </Table.Cell>
                                    <Table.Cell textAlign='center'>  <a target="_blank" href={"#/shipping/document/"+ecd.id}>view</a></Table.Cell>                            
                                </Table.Row>
        })
        
        return (      
            <AppWrapper> 
                <Header as='h3'>ECD</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                        <Segment padded style={{ paddingBottom: 60 }}>  
                                <Breadcrumb size="big">
                                    <Breadcrumb.Section href="#/ecd">ECD List</Breadcrumb.Section>
                                    <Breadcrumb.Divider icon='right arrow' />
                                    <Breadcrumb.Section active>Upload ECD Documents</Breadcrumb.Section>
                                </Breadcrumb>   <br/><br/>
                                {this.state.errorMessage && <ErrorMessage message={errorMessage} /> }
                                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }                                                
                                <Form loading={isLoadingForm}>                                    
                                    <Segment.Group> 
                                        <Segment>
                                            {(!this.state.searched || (this.state.searched && this.state.errorMessage)) &&
                                                <FormSearch selected={this.setSelectedForm} clear={this.clearSelectedForm} width="5"/> 
                                            }
                                            {resultOptions &&
                                            <Message onDismiss={this.clearForm}>
                                                <Message.Content>
                                                    <br/>
                                                    <Table celled>
                                                        <Table.Header>                                                        
                                                            <Table.Row>
                                                                <Table.HeaderCell></Table.HeaderCell>
                                                                <Table.HeaderCell>Container Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Courier Number </Table.HeaderCell>
                                                                <Table.HeaderCell>Shipping Document </Table.HeaderCell>
                                                                <Table.HeaderCell>Amount </Table.HeaderCell>
                                                                <Table.HeaderCell>Created On</Table.HeaderCell>
                                                                <Table.HeaderCell>Details</Table.HeaderCell>
                                                            </Table.Row>
                                                        </Table.Header>
                                                        <Table.Body>{resultOptions}</Table.Body>
                                                    </Table>
                                                </Message.Content>
                                            </Message> }
                                        </Segment>
                                        {formData.id !== "" &&
                                        <span>
                                            <Segment>
                                                <fieldset>
                                                    <legend>Uploads</legend>
                                                    <Header as='h5' onClick={() => this.setUpload()} className="ut-pointer">
                                                        <Icon name='add circle' color = "green" /> Add
                                                    </Header>                                                    
                                                    {uploads}
                                                </fieldset>                                               
                                                
                                                {this.state.formData.ecds.length > 0 &&
                                                <Button size="large" floated='right' color="red" onClick={this.submit}>Submit</Button>
                                                }<p>&nbsp;</p><p>&nbsp;</p>
                                            </Segment>
                                        </span>
                                        }                                        
                                    </Segment.Group>
                                </Form>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default NewEcdDocuments