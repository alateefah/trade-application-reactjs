import React, { Component } from 'react'
import { Modal, Icon, Loader, Form } from 'semantic-ui-react';
import PaarService from './../../../services/paar_service';
import ErrorMessage from '../../shared/message/error.component';
import UploadRow from './upload-row.component';

class ChangeAttachmentModal extends Component {

    constructor (props) {
        super (props);

        this.state = {
            id: props.id,
            loading: false,
            errorMessage: null,
            uploaded: false,
            modalOpen: false
        }
    }

    handleOpen = () => this.setState({ modalOpen: true})

    handleClose = () => this.setState({ modalOpen: false})

    afterUpload = (data) => {
        // this.setState({ loading: true })
        data.id = this.state.id;
        data.status = "PENDING_ACKNOWLEDGEMENT"
        PaarService.updateEcdAttachment(data)
        .then(response => {
            this.setState({ loading: false, uploaded:true})
            if (response.code) {

            } else {
                this.handleClose();
                this.props.completeUpload();
            }
        })
    }

    afterUploadDelete = (id) => this.setState({ uploaded: false })
    
    render () {      
        const {modalOpen, errorMessage, loading} = this.state 
                             
        return (
            <Modal trigger={<Icon name="refresh" color="teal" />} open={modalOpen} onOpen={this.handleOpen} onClose={this.handleClose} closeIcon>
                <Modal.Header>Update Document </Modal.Header>
                {errorMessage && <ErrorMessage message={errorMessage}/>}
                <Modal.Content>
                    {loading && <Loader />} 
                    {!loading &&
                    <Form>
                        <UploadRow 
                            documentTypes = {this.props.documentTypes}
                            key={this.state.index}  info={{formInfoId: 15, index: this.state.index}} 
                            callbackForUpload = {(data)=>this.afterUpload(data)}
                            callbackForDelete = {(data)=>this.afterUploadDelete(data)} />
                    </Form>}
                </Modal.Content>
            </Modal>
        )
    }
    
}

export default ChangeAttachmentModal