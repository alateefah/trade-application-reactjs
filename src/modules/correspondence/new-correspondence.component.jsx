import React, { Component } from 'react'
import { Header, Grid, Segment, Breadcrumb, Loader, Dimmer } from 'semantic-ui-react'
import AppWrapper from './../shared/main/app-wrapper.component'
import Refresh from './../shared/refresh.component'
import FormMCorrespondenceSearch from './correspondence-form-m-search.component'
import CorrespondenceFormMSearchList from './correspondence-form-m-search-list.component'
import SuccessMessage from './../shared/message/success.component'
import ErrorMessage from './../shared/message/error.component'
import AuthenticationService from './../../services/authentication_service'
import CorrespondenceService from './../../services/correspondence_service'
// import FormMService from './../../services/form_m_service'

class NewCorrespondence extends Component {
    
    constructor (props) {
        super(props);
        
        this.state = {
            // pageNumber: 1,
            // pageSize: 10,
            formMResult: null,
            searched: false,
            successMessage: null,
            errorMessage: null,
            formData: {
                applicantTin: "",
                bankStatus: "",
                formType: "",
                goodStatus: "",
                template: "",
                pageNumber: 1,
                pageSize: 10,
            }
        }
    }

    preSearch = (data) => {
        console.log(data)
        this.setState ({
            formData: {
                applicantTin: data.applicantTin,
                bankStatus: data.bankStatus,
                formType: data.formType,
                goodStatus: data.goodStatus,
                template: data.template,
                pageNumber: 1,
                pageSize: 10,
            }
        }, () => {
            this.search();
        })
    }

    search = () => {
        this.setState({ formMResult: null, loadingMessage : "Loading..." })        
        CorrespondenceService.searchforForms(this.state.formData)
        .then(response => {   
            this.setState({ loadingMessage : null, searched: true })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ formMResult: response })
            }         
            
        }) 
    }

    create = (params) => {        
        this.setState({ loadingMessage : "Creating..." })
        CorrespondenceService.createCorrespondence(params)
        .then(response => {   
            this.setState({ loadingMessage : null })           
            if (response.code) {
                this.alertError(response.description) 
            }  else {
                this.search();
                this.alertSuccess("Correspondence successfully created. ") 
            }      
        }) 
    }

    alertSuccess = (msg) => {
        this.setState({ successMessage: msg })
        setTimeout(() => { 
            this.setState({ successMessage: null })
        }, 5000);
        
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg })
        setTimeout(() => { 
            this.setState({ errorMessage: null })
        }, 5000);
        
    }

    handlePaginationChange = (pageNumber) => {
        this.setState ({ 
            formData: {
                ...this.state.formData,
                pageNumber: pageNumber 
            }
        }, () => {
            this.search();
        });
        
    }

    render () {  
        return (              
            <AppWrapper> 
                <Header as='h3'>Correspondence</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded style={{ paddingBottom: 60 }}>

                                {this.state.refreshErrorMessage && 
                                <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> }
                            
                                <span>                                                                       
                                    <Breadcrumb size="big">
                                        <Breadcrumb.Section href="#/correspondence">Correspondences</Breadcrumb.Section>
                                        <Breadcrumb.Divider icon='right arrow' />
                                        <Breadcrumb.Section active>New</Breadcrumb.Section>
                                    </Breadcrumb>     

                                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  }                                                                
                                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }  

                                    {AuthenticationService.hasPermission('SEARCH_FORM_M_INFO') && 
                                    <FormMCorrespondenceSearch searchParams = {this.preSearch} searched = {this.state.searched} 
                                        clearSearch= {() => this.setState({ formMResult: null, searched: false })} 
                                    />}
                                    
                                    {this.state.formMResult !== null 
                                        && AuthenticationService.hasPermission('SEARCH_FORM_M_INFO') 
                                        &&  <CorrespondenceFormMSearchList 
                                                formMs={this.state.formMResult} 
                                                template = {this.state.formData.template} 
                                                createParams = {this.create}
                                                handlePagination={this.handlePaginationChange}
                                            /> 
                                    }      

                                    {this.state.loadingMessage && 
                                    <Dimmer active inverted>
                                        <Loader inverted>Loading</Loader>
                                    </Dimmer>
                                    }
                                
                                </span>
                            </Segment>                       

                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>                                      
        )
    }
    
}

export default NewCorrespondence