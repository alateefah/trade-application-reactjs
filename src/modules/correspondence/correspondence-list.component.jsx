import React, { Component } from 'react'
import { Loader, Dimmer } from 'semantic-ui-react'
import CorrespondenceService from './../../services/correspondence_service'
import AuthenticationService from './../../services/authentication_service.js'
import CorrespondenceSearch from './correspondence-search.component'
import CorrespondenceSearchList from './correspondence-search-list.component'
import ErrorMessage from './../shared/message/error.component'

class CorrespondenceList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loadingMessage: false,
            correspondences: null,
            errorMessage: null,
            refreshErrorMessage: null,
            
            searched: false,
            formData: {
                fromDate: "",
                toDate: "",
                status: "",
                templateId: "",
                
            }
        }
    }

    preSearch = (data) => {
        this.setState ({
            formData: {
                fromDate: data.startDate,
                toDate: data.endDate,
                status: data.status,
                templateId: data.template.toString(),
                pageNumber: 1,
                pageSize: 10,
            }
        }, () => {
            this.search();
        })
    }

    search = () => { 
        this.setState({ correspondences: null, loadingMessage : "Loading..." })
        CorrespondenceService.searchForCorrespondences(this.state.formData)
        .then(response => {   
            this.setState({ loadingMessage : null, searched: true  })
            if (response.code) {
                this.alertError(response.description)
            }  else {
                this.setState({ correspondences: response })
            } 
        }) 
    }

    handlePaginationChange = (pageNumber) => {
        this.setState ({ 
            formData: {
                ...this.state.formData,
                pageNumber: pageNumber 
            }
        }, () => {
            this.search();
        });
        
    }

    alertSuccess = (msg) => {
        this.getCorrespondences();
        this.setState({ successMessage: msg })
        setTimeout(() => { 
            this.setState({ successMessage: null })
        }, 5000);
        
    }

    alertError = (msg) => {
        this.setState({ errorMessage: msg })
        setTimeout(() => { 
            this.setState({ errorMessage: null })
        }, 5000);
        
    }

    render () {
        const { correspondences } = this.state;
        return (     
            <span>
                {this.state.errorMessage &&  <ErrorMessage message = {this.state.errorMessage} /> }
                {/* {AuthenticationService.hasPermission('GENERATE_CORRESPONDENCE') && 
                    <a href="#/correspondence/new" className="ui right floated button tc-blue-btn">Create Correspondence</a>} */}
                    
                <CorrespondenceSearch searchParams = {this.preSearch} searched = {this.state.searched} 
                                clearSearch= {() => this.setState({ correspondences: null, searched: false })} />
                
                {correspondences !== null &&
                    AuthenticationService.hasPermission('GET_ALL_CORRESPONDENCES') &&                    
                    <CorrespondenceSearchList 
                        correspondences={correspondences} 
                        handlePagination={this.handlePaginationChange} 
                        successfulCorrespondenceUpdate = {this.search} />
                }

                {this.state.loadingMessage && 
                <Dimmer active inverted>
                    <Loader inverted>Loading</Loader>
                </Dimmer>
                }
            </span>
                          
        )
    }
    
}

export default CorrespondenceList