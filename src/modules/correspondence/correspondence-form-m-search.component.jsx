import React, { Component } from 'react'
import { Form, Button, Segment } from 'semantic-ui-react'
import FormMService from './../../services/form_m_service'
import CorrespondenceService from './../../services/correspondence_service'

class FormMCorrespondenceSearch extends Component {

    constructor (props) {
        super(props);

        this.state = {
            showMoreFilters: false,
            formData: {
                formType: "",
                applicantTin: "",
                bankStatus: "",
                goodStatus: "",
                template: ""
            },
            loading: true,
            formTypes: [
                {key: "IM", text: "FORM M", value: "IM"}
            ],
            status: [],
            templates: []
        }        
        
    }

    componentWillMount () { this.getStatus(); }

    getStatus = () => {
        FormMService.getStatus()
        .then(response => {  
            if (!response.code) {
                this.setState({ status: response});
                this.getTemplates()    
            }    
        }) 

    }

    getTemplates = () => {
        let params = {pageSize:100, pageNumber:1, type: "LETTER"}
        CorrespondenceService.getAllTemplates(params)
        .then(response => {
            if (!response.code) {
                this.setState({ templates: response.list, loading: false })
            }
        })
    }

    handleInputChange = (event) => {         
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    clear = () => {
        this.setState ({             
            formData: {
                applicantTin: "",
                beneficiaryName: "",
                goodStatus: "",
                template: ""
            }
        }, () => {
            this.props.clearSearch();
        });
    }

    handleSelectChange = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    } 

    validSearch = () => {
        if (this.validFields()) {
            return false;
        }
        return true;
    }

    validFields = () => {
        return ((this.state.formData.formType === null || this.state.formData.formType === "")
            || (this.state.formData.template === null || this.state.formData.template === "")
        )
    }

    search = () => {        
        if (this.validSearch()) {
            this.props.searchParams(this.state.formData)
        } else {
            console.log("Joker")
        }      
    }

    render () {
        const { status, templates } = this.state
        let goodsStatus, bankStatuses, templateOptions;
        
        if (status) 
        {
            goodsStatus = status.goodsStatus && status.goodsStatus.map(goodS => (
                { key: goodS.status, text: goodS.goodsStatus, value: goodS.status }            
            ));

            bankStatuses = status.bankStatus && status.bankStatus.map(bankS => (
                { key: bankS.status, text: bankS.bankStatus, value: bankS.status }            
            ));  
        
        }

        if (templates.length > 0) {
            
            templateOptions =  templates.map(template => (
                { key: template.id, text: template.name, value: template.id } 
            ))
        }

        return (      
           <span>                
                <Segment padded='very' className='search-wrap'>
                    <Form loading={this.state.loading}>
                        <Form.Group widths='equal'>    
                            {this.state.formTypes &&
                            <Form.Select required label='Form Type' options={this.state.formTypes} fluid 
                                placeholder='Select'
                                name = "formType" onChange = {this.handleSelectChange} id = "correspondence-search-formType"
                                value = {this.state.formData.formType}  
                            />  }                          
                            <Form.Input label='Applicant TIN' placeholder='Applicant TIN' fluid
                                name = 'applicantTin'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.applicantTin}
                                id = 'correspondence-search-applicantTin'    
                            />                        
                            {bankStatuses &&
                            <Form.Select label='Bank Status' options={bankStatuses} fluid 
                                placeholder='Select'
                                name = "bankStatus" onChange = {this.handleSelectChange}
                                value = {this.state.formData.bankStatus}  id = "correspondence-search-bankStatus"
                            />}  
                            {goodsStatus &&
                            <Form.Select label='Good Status' options={goodsStatus} fluid placeholder='Select'
                                    name = "goodStatus" onChange = {this.handleSelectChange}
                                    value = {this.state.formData.goodStatus}  id = "correspondence-search-goodStatus"
                            />  }                               
                        </Form.Group>   
                        {templateOptions &&
                        <Form.Select label='Template' search options={templateOptions} fluid placeholder='Select'
                                name = "template" onChange = {this.handleSelectChange} required
                                value = {this.state.formData.template}  id = "correspondence-search-template"
                        />  } 
                        <Form.Field>
                            <Button color="red" onClick={this.search} content="Search" disabled={this.validFields()} />
                            {this.props.searched && <Button color="grey" onClick={this.clear} content="Clear" /> }                                
                        </Form.Field>
                    </Form>
                </Segment>
           </span>
                                      
        )
    }
    
}


export default FormMCorrespondenceSearch