import React, { Component } from 'react'
import { Table, Pagination } from 'semantic-ui-react'
import NoSearchResult from './../shared/no-search-result.component'
import DocumentDisplayModal from './../shared/document-display-modal.component'
import CorrespondenceUpdate from './correspondence-update.component'

class CorrespondenceSearchList extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: props.correspondences.currentPageNumber,
            pageSize: props.correspondences.currentPageSize,
            noOfRecords: props.correspondences.noOfRecords,
            loading: true,
            correspondences: props.correspondences.list            
        }
    }

    componentWillMount () {}

    handlePaginationChange = (e, { activePage }) => {
        this.props.handlePagination(activePage);
    }

    successfulUpdate = () => {
        this.props.successfulCorrespondenceUpdate();
    }

    render () {
        const { correspondences, pageSize, pageNumber, noOfRecords } = this.state;
        let rows;

        correspondences.length <= 0 ?
            rows =  <NoSearchResult colSpan="4" />:
            rows =  correspondences.map(correspondence => {
                return  <Table.Row key={correspondence.attachmentId}>
                            <Table.Cell textAlign="center">
                                {correspondence.formInfos.map(correspondenceForm => {  
                                    return <span key={correspondenceForm.id} ><a href={"#/form-m/"+correspondenceForm.id} target="_blank">{correspondenceForm.formNumber}</a> <br/></span>   
                                })}                                  
                            </Table.Cell>
                            <Table.Cell>{correspondence.attachment.createdAt}</Table.Cell>                           
                            <Table.Cell> {<DocumentDisplayModal document={correspondence.attachment} /> }</Table.Cell>
                            <Table.Cell collapsing>
                                {correspondence.attachment.attachmentStatus === 'CORRESPONDENCE_PENDING_APPROVAL' ?
                                <CorrespondenceUpdate 
                                    correspondence={correspondence.attachmentId} 
                                    successfulUpdate = {this.successfulUpdate} />:
                                correspondence.attachment.attachmentStatus}
                            </Table.Cell> 
                        </Table.Row>
        })

        return (    

            <span>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell colSpan='4'>
                                Showing {pageNumber === 1 ? 
                                    pageNumber : pageNumber*pageSize - pageSize+1} - 
                                    {pageNumber*pageSize > noOfRecords ? noOfRecords : pageNumber*pageSize}  of {noOfRecords} records 
                            </Table.HeaderCell>
                        </Table.Row>
                        <Table.Row>
                            <Table.HeaderCell>Form Numbers</Table.HeaderCell>
                            <Table.HeaderCell>Created </Table.HeaderCell>
                            <Table.HeaderCell>view </Table.HeaderCell>
                            <Table.HeaderCell>Update </Table.HeaderCell>                            
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>{rows}</Table.Body>
                    <Table.Footer>  
                        <Table.Row>
                            <Table.HeaderCell colSpan='4' textAlign="right">                      
                                <Pagination 
                                    pointing 
                                    secondary 
                                    defaultActivePage={pageNumber} 
                                    onPageChange={this.handlePaginationChange} 
                                    totalPages={Math.ceil(noOfRecords/pageSize)} 
                                />
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>  

                
            </span>        
                                      
        )
    }
    
}


export default CorrespondenceSearchList