import React, { Component } from 'react'
import { Table, Checkbox, Button, Pagination } from 'semantic-ui-react'
// import CorrespondenceService from './../../services/correspondence_service'
import NoSearchResult from './../shared/no-search-result.component'

class CorrespondenceFormMSearchList extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: props.formMs.currentPageNumber,
            pageSize: props.formMs.currentPageSize,
            noOfRecords: props.formMs.noOfRecords,
            loading: true,
            formMsList: props.formMs.list,
            applicantTin: "",
            beneficiaryName: "",
            goodStatus: "",
            allChecked: false,
            selectedForms: [],
            templates: [],           
            template: props.template,
            isBatch: false
            
        }
    }

    handleCheckIt = (e, { value }) => {
        if (this.isSelected(value)) {

            let newArray = this.state.selectedForms;
            var index = newArray.indexOf(value)
            newArray.splice(index, 1);
            this.setState({ selectedForms: newArray})

        } else {

            let newArray = this.state.selectedForms.slice();    
            newArray.push(value);   
            this.setState({ selectedForms: newArray})

        }
    }

    isSelected = (value) => {
        return this.state.selectedForms.includes(value);
    }

    create = () => {
        if (this.validFields()) {
            let params = {
                formIds: this.state.selectedForms,
                templateId: this.state.template,
                isBatch: this.state.isBatch,
            }
            this.props.createParams(params);            
        }
    }

    handleSelectChange = (e, val) =>  {
        this.setState({ template : val.value });  
    }

    toggleBatch = () => { this.setState ({ isBatch: !this.state.isBatch }) }
    
    validFields = () => {
        return (this.state.selectedForms.length > 0 && this.state.template !== null && this.state.template !== "")
    }

    handlePaginationChange = (e, { activePage }) => {
        this.props.handlePagination(activePage);
    }

    render () {
        const {formMsList, pageSize, pageNumber, noOfRecords} = this.state;
        let rows = [];



        formMsList.length === 0 ?
            rows =  <NoSearchResult colSpan="7" />:
            rows =  formMsList.map(formM => {
                    return  <Table.Row key={formM.id}>
                                <Table.Cell>
                                    <Checkbox value={formM.id} onChange={this.handleCheckIt} checked={this.state.selectedForms.indexOf(formM.id) > -1}/>                                
                                </Table.Cell>
                                <Table.Cell>{formM.formNumber}</Table.Cell>                            
                                <Table.Cell className='no-left-border'>{formM.applicationNumber}</Table.Cell>
                                <Table.Cell>{formM.beneficiaryName}</Table.Cell>
                                <Table.Cell>{formM.createdAt}</Table.Cell>
                                <Table.Cell>{formM.expiryDate}</Table.Cell>
                                <Table.Cell>{formM.status}</Table.Cell> 
                            </Table.Row>
        })

        return (    

            <span>
                <Table celled>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell colSpan='8'>
                                Showing {pageNumber === 1 ? 
                                    pageNumber : pageNumber*pageSize - pageSize+1} - 
                                    {pageNumber*pageSize > noOfRecords ? noOfRecords : pageNumber*pageSize}  of {noOfRecords} records 
                                    
                                    {this.state.selectedForms.length > 0 &&
                                        <span style={{float: 'right'}}>
                                            {this.state.selectedForms.length > 1 && <Checkbox label='Is Batch ?' onChange={this.toggleBatch} />} &nbsp;&nbsp;&nbsp;&nbsp;
                                            <Button float="right" className="ut-blue-btn" content="Create Correspondence" onClick={this.create} />
                                        </span>
                                    }
                                    
                            </Table.HeaderCell>
                        </Table.Row>
                        <Table.Row>
                            <Table.HeaderCell> {/* <Checkbox onChange={this.handleCheckAll} /> */} </Table.HeaderCell>
                            <Table.HeaderCell>Form Number</Table.HeaderCell>
                            <Table.HeaderCell>Application Number </Table.HeaderCell>
                            <Table.HeaderCell>Applicant Name </Table.HeaderCell>
                            <Table.HeaderCell>Created </Table.HeaderCell>
                            <Table.HeaderCell>Expiry</Table.HeaderCell>
                            <Table.HeaderCell>Status</Table.HeaderCell>                            
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>{rows}</Table.Body>
                    <Table.Footer>  
                        <Table.Row>
                            <Table.HeaderCell colSpan='8' textAlign="right">                      
                                <Pagination 
                                    pointing 
                                    secondary 
                                    defaultActivePage={pageNumber} 
                                    onPageChange={this.handlePaginationChange} 
                                    totalPages={Math.ceil(noOfRecords/pageSize)} 
                                />
                            </Table.HeaderCell>
                        </Table.Row>
                    </Table.Footer>
                </Table>  

                
            </span>        
                                      
        )
    }
    
}


export default CorrespondenceFormMSearchList