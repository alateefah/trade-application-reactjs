import React, { Component } from 'react'
import { Header, Grid, Segment } from 'semantic-ui-react'
import AppWrapper from './../shared/main/app-wrapper.component'
import CorrespondenceList from './correspondence-list.component'

class Correspondence extends Component {
    render () {
        return (            
            <AppWrapper> 
                <Header as='h3'>Correspondence</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded style={{ paddingBottom: 60 }}>                                
                                <CorrespondenceList />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default Correspondence