import React, { Component } from 'react'
import { Form, Button, Segment } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import moment from 'moment'
import CorrespondenceService from './../../services/correspondence_service'

class CorrespondenceSearch extends Component {

    constructor (props) {
        super(props);

        this.state = {
            formData: {
                template: "",
                status: "",
            },
            startDate: null, //moment().subtract(1, 'days'),
            endDate: null, //moment(),
            loading: true,
            status: [
                { key: "approved", text: "Approved", value: "approved" },
                { key: "pending", text: "Pending", value: "pending" },
                { key: "rejected", text: "Rejected", value: "rejected" }  
            ],
            templates: []
        }        
        
    }

    componentWillMount () { this.getTemplates(); }

    getTemplates = () => {
        this.setState({ loading: true}) 
        let params = {
            pageSize: 100,
            pageNumber: 1,
            type: "LETTER"
        }
        CorrespondenceService.getAllTemplates(params)
        .then(response => {  
            this.setState({ templates: response.list, loading: false})        
        }) 

    }

    handleInputChange = (event) => {         
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    clear = () => {
        this.setState ({             
            formData: {
                template: "",
                status: "",
                startDate: moment().subtract(1, 'days'),
                endDate: moment()
            }
        }, () => {
            this.props.clearSearch();
        });
    }

    handleSelectChange = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    } 

    validSearch = () => {
        if (this.validFields()) {
            return false;
        }

        return true;
    }

    validFields = () => {
        return ((this.state.formData.template === null || this.state.formData.template === "")
            && (this.state.formData.status === null || this.state.formData.status === "")
            && (this.state.startDate === null || this.state.startDate === "")
            && (this.state.endDate === null || this.state.endDate === "")
        )
    }

    search = () => {        
        if (this.validSearch()) {
            let params = {
                startDate: this.state.startDate === null ? "": this.state.startDate.format('YYYY-MM-DD') + 'T00:00:00.000Z',
                endDate: this.state.endDate === null ? "": this.state.endDate.format('YYYY-MM-DD') + 'T00:00:00.000Z',
                template: this.state.formData.template,
                status: this.state.formData.status,
            }
            this.props.searchParams(params)
        } else {
            console.log("Joker")
        }      
    }

    handleChangeStart = (date) => { this.setState ({ startDate: date }); }

    handleChangeEnd = (date) => { this.setState ({ endDate: date });  }

    render () {
        const { status, templates } = this.state
        let allTemplates;               

        if (templates.length > 0) 
        {
            allTemplates = templates && templates.map(template => (
                { key: template.id, text: template.name, value: template.id }            
            ));           
        }

        return (      
           <span>                
                <Segment padded='very' className='search-wrap'>
                    <Form loading={this.state.loading}>
                        <Form.Group>  
                            {allTemplates &&
                            <Form.Select label='Template' options={allTemplates} width={4} placeholder='Select Template'
                                    name = "template" onChange = {this.handleSelectChange} search
                                    value = {this.state.formData.template}  id = "correspondence-search-template"
                            />}
                            <Form.Select label='Correspondence Status' options={status} width={3} placeholder='Select'
                                    name = "status" onChange = {this.handleSelectChange}
                                    value = {this.state.formData.status} id = "correspondence-search-goodStatus"
                            />                          
                            <Form.Field width={3}> 
                                <label>Start Date</label>               
                                <DatePicker                                
                                    placeholderText='Start Date'
                                    dateFormat="YYYY-MM-DD"
                                    selected = {this.state.startDate}
                                    selectsStart
                                    startDate = {this.state.startDate}
                                    endDate = {this.state.endDate}
                                    onChange = {this.handleChangeStart}     
                                    id = "correspondence-search-starDate"                               
                                />
                            </Form.Field>
                            <Form.Field width={3}> 
                                <label>End Date </label>               
                                <DatePicker                                
                                    placeholderText='End Date'
                                    dateFormat="YYYY-MM-DD"
                                    selected={this.state.endDate}
                                    selectsEnd
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChange={this.handleChangeEnd}
                                    id = "correspondence-search-endDate" 
                                />
                            </Form.Field>   
                            <Form.Field>
                                <label>&nbsp;</label>
                                <Button color="red" onClick={this.search} content="Search" disabled={this.validFields()}  />
                                {this.props.searched && <Button color="grey" onClick={this.clear} content="Clear" /> }                                
                            </Form.Field>
                        </Form.Group>                        
                    </Form>
                </Segment>
           </span>
                                      
        )
    }
    
}


export default CorrespondenceSearch