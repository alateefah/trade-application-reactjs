import React, { Component } from 'react'
import { Form, Modal, List, TextArea } from 'semantic-ui-react'
import UploadFile from './../shared/upload-file.component'
import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'
import CorrepondenceService from './../../services/correspondence_service'

class CorrespondenceUpdate extends Component {

    constructor (props) {
        super(props);

        this.state = {
            value: "false",
            comment: "",
            correspondence: props.correspondence,
            errorMessage: null,
            loading: false
        }        
        
    }

    afterUpload = (data) => {
        this.setState({loading: true})
        data.approved = this.state.value;
        data.comments = this.state.comment;
        CorrepondenceService.updateCorrespondence(data)
        .then(response => {
            this.setState({loading: false})
            if (response.code) {
                this.setState({ errorMessage: response.description});
                setTimeout(() => this.setState({ errorMessage: null }), 4000);
            } else {
                this.setState({ successMesage: "Upload Succesful. Refreshing..."});
                this.props.successfulUpdate();
            }
        })
    }

    handleChange = (e, { value }) => {
        this.setState({ value : value })
    }

    handleCommentChange = (e, { value }) => {
        this.setState({ comment : value })
    }

    render () {

        return (   
            <span>                 
                <Modal trigger={<List.Header as='a' className='ut-pointer'>Update</List.Header>}  size='mini'>
                    <Modal.Header>Update correspondence</Modal.Header>
                    <Modal.Content>
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                        <Form loading={this.state.loading}>
                            <Form.Group inline>
                                <Form.Radio label='Approved' value="true" checked={this.state.value === "true"} onChange={this.handleChange} name='approvalStatus' />
                                <Form.Radio label='Rejected' value="false" checked={this.state.value === "false"} onChange={this.handleChange} name='approvalStatus' />
                            </Form.Group>
                            {this.state.value === "false" &&
                            <Form.Field>
                                <Form.Field required control={TextArea} value={this.state.comment} onChange={this.handleCommentChange} 
                                    label='Reason' placeholder='Reason for rejection...' />
                            </Form.Field>}
                            <br/>
                        </Form>
                        <UploadFile info={this.state.correspondence} callbackForUpload={this.afterUpload} /> 
                    </Modal.Content>
                </Modal> 
            </span>        
        )
    }
    
}


export default CorrespondenceUpdate