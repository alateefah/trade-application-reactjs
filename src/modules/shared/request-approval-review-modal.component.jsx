import React, { Component } from 'react'
import { Form,  Icon, Button, Modal, Message } from 'semantic-ui-react'

class RequestApprovalReviewModal extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            comment: "",
            modalOpen: false,
            ecd: props.ecd,
            errorMessage: null
        }
    }

    handleClose = () => this.setState({ modalOpen: false, comment: "", errorMessage: null })

    handleOpen = () => this.setState({ modalOpen: true })

    approve = () => {
        const {ecd} =  this.state;
        
        let params = {
            recordId: ecd.id,
            name: "",
            formCategory: "FORM_M",
            type: "ECD_PROCESSING",
            description: "Approval for ECD Document",
            initiatorComments: this.state.comment,
            parentId: ecd.shippingDetailId,
            approved: true
        }
        this.handleClose();
        this.props.callbackForApprovalRequest(params);
        
    }

    disapprove = () => {
        const {ecd, comment} =  this.state;

        if (comment) {    
            let params = {
                recordId: ecd.id,
                name: "",
                formCategory: "FORM_M",
                type: "ECD_PROCESSING",
                description: "Approval for ECD Document",
                initiatorComments: this.state.comment,
                parentId: ecd.shippingDetailId,
                approved: false
            }
            this.handleClose();
            this.props.callbackForApprovalRequest(params);   
        } else {
            this.setState({ errorMessage: "Reason for rejection required."})
        }     
    }

    handleCommentChange = (event) => { this.setState ({ comment: event.target.value}) }

    render () {          
        const {errorMessage, modalOpen} = this.state;

        return (
            <Modal                 
                open={modalOpen}
                onOpen={this.handleOpen}
                onClose={this.handleClose}
                trigger={this.props.trigger} basic size='small' closeIcon="cancel"
            >   
                {errorMessage && <Message error content={errorMessage} />}         
                <Modal.Content>
                    <Form>
                        <Form.Group>
                            <Form.Input placeholder='Comment (required of rejection)' width={16} onChange = {this.handleCommentChange} className = 'transparent-input'/>
                        </Form.Group>
                    </Form>                    
                </Modal.Content>            
                <Modal.Actions>
                    <Button color='green' onClick={this.approve} inverted><Icon name='checkmark' /> Approve </Button>
                    <Button color='red' onClick={this.disapprove} inverted><Icon name='cancel' /> Reject </Button>
                </Modal.Actions>                  
            </Modal>                
        )
    }
}

export default RequestApprovalReviewModal