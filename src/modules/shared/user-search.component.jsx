import React, { Component } from 'react'
import {Form, Search} from 'semantic-ui-react'
import UserService from './../../services/user_service'

class UserSearch extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            isSearchingUser: false,
            approvers: "",
            userSearchValue: "",
            width: props.width? props.width: ""
        }        
    }

    handleUserSearch = (e, {value}) => {
        this.setState({ isSearchingUser: true, userSearchValue : value })
        // console.log(value)
        if (!value) {
            this.props.clear();
        } else if (value.length > 2) {
            this.filterUser(value);
        }
        
    }

    filterUser = (value) => {
        let params = {
            searchText: value,
            pageNumber: 1,
            pageSize: 5
        }
        UserService.search(params)
        .then(response => { 
            if (response.code) {
                return;
            } else {
               this.setState({ users : response.list, isSearchingUser: false })                
            }  
        }) 
    }

    resultRenderer = (result) => {
        return (
            <div className="result" key={result.id}>
                <div className="content">
                    <div className="title">{result.emailAddress}</div>
                </div>
            </div>  
        )
        
    }
    
    handleResultSelect = (e, { result }) => {
        this.setState({ 
            approvers: result.emailAddress,
            userSearchValue : result.emailAddress
        })
        this.props.selected(result.emailAddress);
    }

    render () {
        return (
            <Form.Field width={this.state.width}>
                <label> Authorizer </label>
                <Search type='text' fluid
                    placeholder="Start typing..."
                    minCharacters = {3}
                    loading = {this.state.isSearchingUser} 
                    onSearchChange = {this.handleUserSearch}
                    value = {this.state.userSearchValue}
                    results = {this.state.users}
                    resultRenderer = {this.resultRenderer}
                    onResultSelect={this.handleResultSelect}
                />
            </Form.Field>  
        
        )
    }
}

export default UserSearch