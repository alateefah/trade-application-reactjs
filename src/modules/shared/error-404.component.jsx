import React, { Component } from 'react'
import { Link } from "react-router-dom"

//semantic ui
import { Container } from 'semantic-ui-react'

class Error404 extends Component {

    render() {    
        return (            
            <Container textAlign='center' style={{ marginTop: '4em' }}>
                404 Component  <br />
                Not Found <br/>
                <Link to='/home'> Back to Dashboard</Link>            
            </Container>   
        )
    }
}

export default Error404