import React, { Component } from 'react'
import { Modal, Image} from 'semantic-ui-react'
import PdfViewer from './../shared/pdf-viewer.component'

class DocumentDisplayModal extends Component {

    constructor (props) {
        super(props);

        this.state = {
            document: props.document,
            documentType: props.document.documentBinaryType,
            url: props.document.documentLink,
            name: props.document.documentName,
            loading : false,
            isImageFile: false,
            isPdfFile: false
        }
    }

    loadFile = () => {
        
        if (this.state.documentType.trim() === 'jpg' ||this.state.documentType.trim() === 'jpeg' || this.state.documentType.trim() === 'png') {
            this.setState({ isImageFile: true });
            return;
        }
        
        if (this.state.documentType.trim() === 'pdf') {
            this.setState({ isPdfFile: true });
            return;
        }
    }
    
    render () {
        const {documentType} = this.state;
        return (       
            documentType.trim() !== 'jpg' && documentType.trim() !== 'jpeg' 
                && documentType.trim() !== 'png' && documentType.trim() !== 'pdf' ?
            <a href ={this.state.url} download={this.state.name} target="_blank">{this.state.name}</a>:
            <Modal closeIcon trigger={<a className='ut-pointer' onClick={this.loadFile}>{this.state.name}</a>}>                
                <Modal.Content scrolling>                    
                    <Modal.Description>                            
                        {this.state.isImageFile && <Image src={this.state.url} alt={this.state.name} /> }
                        {this.state.isPdfFile && <PdfViewer src={this.state.url} name={this.state.name} /> }                                
                    </Modal.Description>                    
                </Modal.Content>
            </Modal>
           
        )       
        
    }
}

export default DocumentDisplayModal
