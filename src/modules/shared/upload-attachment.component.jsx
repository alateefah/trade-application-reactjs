import React, { Component } from 'react'
import _ from 'lodash'
import { Icon, Popup, Label, Button } from 'semantic-ui-react'
import SharedService from './../../services/shared_service'

class UploadAttachment extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            document: props.document,
            fileName: "Choose a file",
            isFileUploaded: false,
            uploadedFile: null,
            uploading: false,
            error: null,
            success: null,
            acceptedFileExtensions: ['jpg','jpeg', 'tiff', 'tif', 'gif', 'png','pdf']
        }        
    }

    readFile = (event) => {
        this.setState ({ 
            fileName: event.target.files[0].name,
            uploadedFile: event.target.files[0]
        })
    }

    clearFile = (event) => {
        event.target.value = null;
        this.setState({ uploadedFile: null, fileName: "Choose a file", errorMessage: null })
    }

    upload = (event) => {
        if (this.isValidFile()) {
            this.setState({ uploading: true})
            var date = new Date().toISOString().slice(0,10); 
            let data = {
                byPassed: "false",
                documentType: this.state.document.document,
                documentDate: date,
                formInfoId: this.state.document.form,
                upload: this.state.uploadedFile
            }
            SharedService.uploadAttachment (data)
            .then(response => {  
                this.setState({ uploading: false})
                if (!data.code) {
                    this.setState({ uploadedFile: null, fileName: "Choose a file", errorMessage: null })
                    this.props.callbackForUpload(response);
                }
            })       
        }
    }
       
    isValidFile = () => {        
        if (this.state.uploadedFile === null) {
            return false;
        }

        const file = this.state.uploadedFile;

        const fileSize = file.size;
        const fileType = file.type;
        const fileExtension = file.name.substr((file.name.lastIndexOf('.') +1));

        const maxByteSize = 5000000;    
        const acceptedFiles = ['image/jpg', 'image/jpeg','image/tiff', 'image/tif', 'image/gif', 'image/png', 'application/pdf'];        
        // const acceptedFileExtensions = ['jpg','jpeg', 'tiff', 'tif', 'gif', 'png','pdf'];
        
        if (fileSize > maxByteSize) {      
            this.setState({errorMessage: "File too large."})      
            return false;
        }

        if(!_.includes(acceptedFiles, fileType)) {  
            this.setState({errorMessage: "Invalid file type."})          
            return false;
        }

        if(!_.includes(this.state.acceptedFileExtensions, fileExtension.toLowerCase())) {          
            this.setState({errorMessage: "Invalid file extension."})  
            return false;
        }

        return true;
    }

    render () {
        return ( 
            <div>       
                <input type="file" name="file" id={"upload_"+this.state.document.index} 
                    className="ut-input-file" accept="image/*, application/pdf"  onChange={this.readFile} onClick={this.clearFile}/>
                <label htmlFor={"upload_"+this.state.document.index} className="ui teal label"> <Icon name="upload" /> {this.state.fileName}</label> 
                <Popup trigger={<Icon name='question circle' />} inverted position = 'top center' content ={'Only '+ this.state.acceptedFileExtensions.join(", ")+ ' allowed with maximum size of 5MB'} />
                {
                    this.state.uploadedFile !== null &&
                    <Button type="submit" content="Upload File" onClick = {this.upload} basic size="small" color="black" loading={this.state.uploading} />
                }
                {this.state.errorMessage && <Label basic color='red' pointing="left">{this.state.errorMessage}</Label>}
            </div>
        )
    }
}

export default UploadAttachment