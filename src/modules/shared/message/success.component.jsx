import React, { Component } from 'react'

import { Message } from 'semantic-ui-react'

class SuccessMessage extends Component {
  
    render() {    
        return (
            <Message positive>
                <Message.Header>{this.props.message}</Message.Header>                
            </Message>    
        )
    }
}

export default SuccessMessage