import React, { Component } from 'react'

import { Message } from 'semantic-ui-react'

class ErrorMessage extends Component {
  
    render() {    
        return (
            <Message negative>
                <Message.Header>{this.props.message}</Message.Header>                
            </Message>    
        )
    }
}

export default ErrorMessage