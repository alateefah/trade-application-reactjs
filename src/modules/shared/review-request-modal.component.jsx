import React, { Component } from 'react'
import { Form,  Icon, Button, Modal, Header, Loader, Item } from 'semantic-ui-react'
import SharedService from './../../services/shared_service'
import Refresh from './refresh.component'

let _ =  require ('lodash');

class ReviewRequest extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            info: props.info,
            comment: "",
            modalOpen: false,
            refreshErrorMessage: null,
            approvalsInfo: null,
            approvalId: null,
            errorMessage: null
        }
    }

    handleClose = () => this.setState({ modalOpen: false })

    handleOpen= () => {
        this.setState({ modalOpen: true }); 
        this.getApprovalComments();
    }

    approve = () => {
        let record =  this.state.info.recordId;
        
        if (record === 0) {
            record = this.state.approvalsInfo[_.lastIndexOf(this.state.approvalsInfo)-1].id;
        } 
        let data = {
            recordId: record,
            formCategory: "FORM_M",
            type: this.state.info.type,
            approverComments: this.state.comment,
            approvalId: this.state.approvalId,
            parentId: this.state.info.parentId,
            approved: true,
            url: this.state.info.url            
        }
        this.handleClose();
        this.props.callbackForReview(data);
        
    }

    disapprove = () => {
        let record =  this.state.info.recordId;

        if (record === 0) {
            record = this.state.approvalsInfo[_.lastIndexOf(this.state.approvalsInfo)-1].id;
        } 

        let data = {   
            recordId: record,
            formCategory: "FORM_M",
            type: this.state.info.type,
            approverComments: this.state.comment,
            approvalId: this.state.approvalId,
            parentId: this.state.info.parentId, 
            approved: false,
            url: this.state.info.url 
        }
        this.handleClose();
        this.props.callbackForReview(data);
        
    }

    handleCommentChange = (event) => { this.setState ({ comment: event.target.value}) }

    getApprovalComments = () => {  
        this.setState({ loadingMessage : "Loading comments" })
        SharedService.getApproval (this.state.info)
        .then(response => {  
            if (response.code) {
                this.setState({ refreshErrorMessage : response.description }, () => { 
                    this.setState({ loadingMessage : null })
                })
            } else {
                let pending = _.findIndex(response.list, function(o) { return o.approvalStatus === 'PENDING'; });
                this.setState({ approvalsInfo : response.list, approvalId: response.list[pending].id }, () => { 
                    this.setState({ loadingMessage : null })
                })
            }    
            
        }) 
    }

    refreshAction = () => { this.getApprovalComments(); }

    render () {
        const { approvalsInfo, modalOpen } = this.state;
        let feeds = [];

        if (approvalsInfo && approvalsInfo.length > 0) {
            feeds = approvalsInfo.map(approval => {
                return  (approval !== "") &&
                    <Item.Group key={approval.id} className="approval">
                        <Item>                                    
                            <Item.Content>
                                <Item.Header as='a'>{approval.createdBy}</Item.Header>
                                <Item.Meta><span className='price'>{approval.createdDate}</span></Item.Meta>
                                <Item.Description>{approval.initiatorComments ? approval.initiatorComments: "No comment"}</Item.Description> 
                            </Item.Content>
                        </Item>      
                        {approval.approvalStatus !== "PENDING" &&      
                        <Item className="approver">                    
                            <Item.Content>
                                <Item.Header as='a'>{approval.approvedBy}</Item.Header>
                                <Item.Meta><span className='price'>{approval.approvedDate}</span></Item.Meta>
                                <Item.Description>{approval.approverComments ? approval.approverComments: "No comment"}</Item.Description>
                            </Item.Content>
                        </Item> }
                    </Item.Group>
                })
        }   

        return (

            <Modal trigger={this.props.trigger} onOpen={this.handleOpen} open={modalOpen}
                onClose={this.handleClose} basic size='small' closeIcon="cancel">

                {this.state.refreshErrorMessage && <Refresh message={this.state.refreshErrorMessage} refreshAction ={this.getApprovalComments} />}
                {this.state.loadingMessage && <Loader />}              
                {!this.state.refreshErrorMessage && !this.state.loadingMessage && <Header icon='browser' content='Review Request' /> } 

                {!this.state.refreshErrorMessage && !this.state.loadingMessage &&
                    <Modal.Content>
                        {feeds.length > 0 && feeds}
                        <br/><br/>
                        <Form>
                            <Form.Group>
                                <Form.Input placeholder='Comment (required of rejection)' width={16} onChange = {this.handleCommentChange} className = 'transparent-input'/>
                            </Form.Group>
                        </Form>                    
                    </Modal.Content>
                }

                {!this.state.refreshErrorMessage && !this.state.loadingMessage &&
                    <Modal.Actions>
                        <Button color='green' onClick={this.approve} inverted><Icon name='checkmark' /> Approve </Button>
                        <Button color='red' onClick={this.disapprove} inverted><Icon name='cancel' /> Reject </Button>
                    </Modal.Actions>  
                }  
            </Modal>                
        )
    }
}

export default ReviewRequest