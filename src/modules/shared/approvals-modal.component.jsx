import React, { Component } from 'react'
import { Modal, Header, Dimmer, Loader, Feed, Label, Popup } from 'semantic-ui-react'
import SharedService from './../../services/shared_service'

class ApprovalsModal extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            info: props.info,
            loadingMessage: null,
            approvalsInfo: null,
            modalOpen: false,
            showUserSearch: false,
            formData: {
                approvalId: ""
            }
        }
    }

    handleClose = () => this.setState({ modalOpen: false })

    getApprovalComments = () => {
        this.setState({ loadingMessage : "Loading comments...", showUserSearch: false })
        SharedService.getApproval(this.state.info)
        .then(response => {  
            this.setState({ loadingMessage : null})
            if (response.code) {
                this.setState({ refreshErrorMessage : response.description }, () => { 
                    this.setState({ loadingMessage : null })
                })
            } else {
                this.setState({ 
                    approvalsInfo : response.list, 
                    formData: {
                        ...this.state.formData,
                        approvalId: response.list[0].id
                    } 
                }, () => { 
                    this.setState({ loadingMessage : null })
                })
            }    
            
        }) 
    }

   
    render () {
        const { approvalsInfo } = this.state;
        let feeds = [], initiatiorAvi, approverAvi;

        initiatiorAvi = <Popup inverted position='left center'
                            trigger={<Label.Group size='huge' circular><Label as='a' content="IN" /></Label.Group>}
                            content='Initiator'
                            on='hover'
                        />

        approverAvi = <Popup inverted position='left center'
                        trigger={<Label.Group size='huge' circular><Label as='a' content="AP" /></Label.Group>}
                        content='Approver'
                        on='hover'
                    />

        if (approvalsInfo && approvalsInfo.length > 0) {
            feeds = approvalsInfo.map(approval => {
                return  (approval !== "") &&
                
                    <Feed  key={approval.id} className = "approvals-feed">
                        <Feed className="comment-feeds left">
                            <Feed.Event icon = {initiatiorAvi} 
                                date={approval.createdBy} 
                                extraText={approval.createdDate}
                                summary={approval.initiatorComments ? approval.initiatorComments: "No comment"} />
                        </Feed>
                        {approval.approvalStatus !== "PENDING" &&    
                        <Feed className="comment-feeds right">                    
                            <Feed.Event icon = {approverAvi}
                                date={approval.approvedBy} 
                                extraText={approval.approvedDate}
                                summary={approval.approverComments ? approval.approverComments: "No comment"} />
                        </Feed>
                        }                    
                    </Feed>
                })
        }

        return (
            <Modal trigger={this.props.trigger} onOpen={this.getApprovalComments} onClose={this.handleClose} basic size='small' closeIcon="cancel">
                
                {!this.state.refreshErrorMessage && !this.state.loadingMessage && <Header icon='browser' content='Approval Requests' /> }          
            
                {this.state.loadingMessage && <Dimmer active><Loader /></Dimmer>}

                {!this.state.refreshErrorMessage && !this.state.loadingMessage &&
                <Modal.Content>
                    {feeds.length > 0 && feeds}                                   
                </Modal.Content>}
            </Modal>                
        )
    }
}

export default ApprovalsModal