import React, { Component } from 'react'

//semantic ui
import { Container, Header, Icon } from 'semantic-ui-react'

class Refresh extends Component {

    render() {    
        return (            
            <Container textAlign='center' style={{ marginTop: '4em' }}>
                <Icon name='repeat' size='big' color='red' onClick={this.props.refreshAction} />
                <Header size='small' content={this.props.message} />                 
            </Container>   
        )
    }
}

export default Refresh