import React, { Component } from 'react'
import {Form, Search} from 'semantic-ui-react'
import SharedService from './../../services/shared_service'

class FormSearch extends Component {

    constructor (props) {
        super (props) ;
        
        this.state = {
            isSearchingForm: false,
            approvers: "",
            formSearchValue: "",
            width: props.width ? props.width: ""
        }        
    }

    handleFormSearch = (e, {value}) => {
        this.setState({ formSearchValue : value })
        if (!value) {
            this.props.clear();
        } else if (value.length > 2) {            
            this.filterForm(value);
        }
        
    }

    filterForm = (value) => {
        this.setState({isSearchingForm: true})
        let params = {
            formNumber: value,
            pageNumber: 1,
            pageSize: 10,
            formType: "IM"
        }
        SharedService.searchForForms(params)
        .then(response => { 
            if (response.code) {
                return;
            } else {
               this.setState({ forms : response.list, isSearchingForm: false })                
            }  
        }) 
    }

    resultRenderer = (result) => {
        return (
            <div className="result" key={result.formNumber}>
                <div className="content">
                    <div className="title">{result.formNumber}</div>
                </div>
            </div>  
        )
        
    }
    
    handleResultSelect = (e, { result }) => {
        this.setState({ 
            forms: result.formNumber,
            formSearchValue : result.formNumber
        })
        this.props.selected(result.formNumber);
    }

    render () {
        return (
            <Form.Field width={this.state.width}>                
                <Search type='text' fluid
                    placeholder="Start typing form number...(min 3 xters)"
                    minCharacters = {3}
                    loading = {this.state.isSearchingForm} 
                    onSearchChange = {this.handleFormSearch}
                    value = {this.state.formSearchValue}
                    results = {this.state.forms}
                    resultRenderer = {this.resultRenderer}
                    onResultSelect={this.handleResultSelect}
                />
            </Form.Field>  
        
        )
    }
}

export default FormSearch