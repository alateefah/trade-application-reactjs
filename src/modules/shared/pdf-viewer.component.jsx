import React, { Component } from 'react'

class PdfViewer extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            fileName: props.name,
            src: props.src
        }
    }

    render () {
        return (
            <embed src={this.state.src} type="application/pdf" width="900" height="800"/>
        )
    }
}

export default PdfViewer