import React, { Component } from 'react'

//from semantic ui react
import { Container } from 'semantic-ui-react'

// custom 
import TCHeader  from './../header/tc-header.component'

class AppWrapper extends Component {

    render() {    
        return (            
            <div>     
                <TCHeader />       
                <Container fluid style={{ marginTop: '10em' }}>  
                    {this.props.children}
                </Container>                
            </div>
        )
    }
}

export default AppWrapper