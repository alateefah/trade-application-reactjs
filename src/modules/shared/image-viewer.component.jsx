import React, { Component } from 'react'
import { Modal, List, Image } from 'semantic-ui-react'

class ImageViewer extends Component {

    constructor (props) {
        super(props);

        this.state = {
            fileName: props.name,
            src: props.src
        }
    }

    render () {
        return (
            <Modal trigger={<List.Header as='a' className='tc-pointer'>{this.state.fileName}</List.Header>} closeIcon>
                <Modal.Description>
                    <Modal.Content >
                        <Modal.Description>
                            <Image src={this.state.src} fluid />
                        </Modal.Description>
                    </Modal.Content>
                </Modal.Description>
            </Modal>
        )
    }
}

export default ImageViewer