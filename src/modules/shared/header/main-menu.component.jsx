import React, { Component } from 'react'
import { Menu, Icon, Dropdown, Grid } from 'semantic-ui-react'
import NavLink from "./nav_link";
import AuthenticationService from './../../../services/authentication_service'

class  MainMenu extends Component {
    
    render() {    
        const trigger = (
            <span><Icon name='wordpress forms' /> Trade Forms</span>
        )

        const settingsTrigger = (
            <span><Icon name='settings' /> Settings</span>
        )

        const correspondenceTrigger = (
            <span><Icon name='book' /> Correspondences </span>
        )

        const templateTrigger = (
            <span><Icon name='book' /> Templates</span>
        )

        return (              
            <span>
                <Grid centered className="menu-grid">
                    <Menu>
                        {/* <NavLink to="/home">
                            <Icon name='dashboard' /> Home
                        </NavLink> */}
                        {
                            (AuthenticationService.hasPermission('READ_ALL_USERS') || AuthenticationService.hasPermission('READ_ALL_ROLES')) &&
                            <NavLink to="/users">
                                <Icon name='users' /> User Management
                            </NavLink>
                        }
                        {
                            (AuthenticationService.hasPermission('READ_ALL_FORM_MS') ||
                            AuthenticationService.hasPermission('READ_CORRESPONDENCE_TEMPLATES')) &&
                            <Dropdown trigger={trigger} item className="ut-dropdown-menu">
                                <Dropdown.Menu> 
                                    {AuthenticationService.hasPermission('READ_ALL_FORM_MS') 
                                    && <NavLink to="/form-m" > <Icon name='book' /> Form M </NavLink> } 
                                    
                                    <Dropdown.Item>
                                        <Dropdown trigger={correspondenceTrigger}>   
                                            <Dropdown.Menu>
                                                {AuthenticationService.hasPermission('GENERATE_CORRESPONDENCE') &&
                                                <NavLink to="/correspondence/new" > 
                                                    New 
                                                </NavLink> } 
                                                {AuthenticationService.hasPermission('READ_CORRESPONDENCE_TEMPLATES') &&
                                                <NavLink to="/correspondence" > 
                                                    List 
                                                </NavLink>}
                                            </Dropdown.Menu>
                                        </Dropdown>
                                    </Dropdown.Item> 

                                    {AuthenticationService.hasPermission('READ_ALL_SHIPPING_DETAILS') 
                                    && <NavLink to="/paar" > <Icon name='book' /> PAAR Processing </NavLink> } 

                                </Dropdown.Menu>
                            </Dropdown>
                        }                    
                        {AuthenticationService.hasPermission('GET_ALL_AUDIT_TRAILS') 
                            && <NavLink to="/audits"> <Icon name='file text outline' /> Audit Trail </NavLink> }
                        {
                            (AuthenticationService.hasPermission('READ_CORRESPONDENCE_TEMPLATES') ||
                                AuthenticationService.hasPermission('READ_APPLICATION_PARAMETER') ||
                                AuthenticationService.hasPermission('READ_ALL_BRANCHES')) 
                            && 
                            <Dropdown trigger={settingsTrigger} item style={{paddingTop: 13}}>
                                <Dropdown.Menu>
                                    {AuthenticationService.hasPermission('READ_APPLICATION_PARAMETER') 
                                        && <NavLink to="/configurations"> <Icon name="configure" /> Configurations </NavLink>}
                                    
                                    {AuthenticationService.hasPermission('READ_CORRESPONDENCE_TEMPLATES') &&
                                        <Dropdown.Item>
                                            <Dropdown trigger={templateTrigger}>   
                                                <Dropdown.Menu>
                                                    <NavLink to="/templates/letter" > Letter </NavLink>
                                                    <NavLink to="/templates/email" > Email </NavLink> 
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        </Dropdown.Item>
                                    }
                                       
                                    {AuthenticationService.hasPermission('READ_ALL_BRANCHES') 
                                        && <NavLink to="/branch-management"><Icon name="random" />  Branch Management </NavLink>}
                                </Dropdown.Menu>
                            </Dropdown>
                        }
                    </Menu>
                </Grid>  
            </span>
        )
    }
}

export default MainMenu