import React, { Component } from 'react'

//semantic ui
import { Modal, Button, Form, Input, Dropdown } from 'semantic-ui-react'

//error 
import ErrorMessage  from './../../shared/message/error.component.jsx'
import SuccessMessage from './../../shared/message/success.component'

//api
import UserService from './../../../services/user_service'
import AuthenticationService from './../../../services/authentication_service'


class ChangePasswordModal extends Component {
    
    constructor() {
        super();
          
 		this.state= {
            modalOpen: false,
            formData: {
                oldPassword: '',
                newPassword: ''
            },
            isUpdating : false,
            successMessage: null,
            errorMessage: null,
            newPasswordType : 'password'
        };
        
        this.openChangePasswordModal = this.openChangePasswordModal.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.clearForm = this.clearForm.bind(this);
        this.validForm = this.validForm.bind(this)
        this.changePassword = this.changePassword.bind(this)
    }

    openChangePasswordModal () {
        this.setState({ modalOpen : true });        
    }

    handleInputChange(event) { 
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });      
    }

    clearForm () {       
        this.setState({ 
            formData: {
                oldPassword: '',
                newPassword: ''                     
            }
        });    
        
    }

    changePassword () {
        this.setState ({errorMessage: null })
        if (this.validForm()) {
            this.setState ({ isUpdating : true })
            let params = {
                username: AuthenticationService.getUserId(),
                oldPassword: this.state.formData.oldPassword,  
                newPassword: this.state.formData.newPassword
            }
            UserService.updatePassword(params)
            .then( response => {  
                if (response.code) {
                    this.clearForm()
                    this.setState ({ isUpdating : false, errorMessage: response.description})                
                } else {
                    this.clearForm()
                    this.setState ({ isUpdating : false })
                    this.setState ({ successMessage: "Password Updated Successfully!" })
                    setTimeout(() => {
                        this.setState ({ successMessage: null });
                    }, 5000);
                }
            })
        }
    }

    validForm () {
        this.setState ({ errorMessage: null })

        if (this.state.formData.oldPassword == null || this.state.formData.oldPassword === "") {
            this.setState ({ errorMessage: "Old password required"})
            return false;
        }

        if (this.state.formData.newPassword == null || this.state.formData.newPassword === "") {
            this.setState ({ errorMessage: "New password required"})
            return false;
        }

        return true;
    }

    handleClose = () => {
        this.setState ({errorMessage: null })
        this.clearForm();
        this.setState({ modalOpen: false })
    }
    
    togglePassword = () => {
        this.setState({
            newPasswordType: this.state.newPasswordType === 'input' ? 'password' : 'input'
        })
    }
    render () {       

        return (
            <span>
                <Dropdown.Item text='Change Password' className= "tc-pointer" onClick={this.openChangePasswordModal} icon="lock" />
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Change Password </Modal.Header>
                    <Modal.Content>
                    {this.state.errorMessage && 
                        <ErrorMessage message={this.state.errorMessage} />
                    }
                    {this.state.successMessage && 
                        <SuccessMessage message={this.state.successMessage} />
                    }
                    <Form id='change-password-form'>      
                                          
                        <Form.Field control={Input} label='Old Password' 
                            placeholder='Type in your old password ...' type='password'
                            name = 'oldPassword' 
                            onChange = {this.handleInputChange} 
                            value = {this.state.formData.oldPassword}
                            id = 'old-password-field'
                        />  
                        <div className="field">
                            <label>New Password </label>
                            <div className="ui icon input fluid">
                                <input label='New Password' id = 'new-password-field'
                                    placeholder='Type in your new password ...'  type={this.state.newPasswordType}
                                    name = 'newPassword' 
                                    onChange = {this.handleInputChange} 
                                    value = {this.state.formData.newPassword}
                                />
                                <i className="eye link icon" onClick={this.togglePassword}></i>                      
                            </div>
                        </div>
                    </Form>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button positive icon="key" labelPosition='left' content='Change password' onClick={this.changePassword} loading={this.state.isUpdating}/>                         
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </span>
        )
    }
}

export default ChangePasswordModal