import React, { Component } from 'react'

import { Menu, Container, Dropdown } from 'semantic-ui-react';

//custom 
import MainMenu  from './main-menu.component'
import logo from './../../../images/logo.png'; 
import ChangePasswordModal from './change-password-modal.component'

//service
import AuthenticationService from './../../../services/authentication_service.js'
import UserService from './../../../services/user_service.js'

class TCHeader extends Component {
    constructor (props) {
        super(props)
        this.state = {
            isLogOut: false,
            trigger: null
        }
        this.logOut = this.logOut.bind(this);
    }

    componentDidMount () {
        this.setState({ 
            trigger: 
                AuthenticationService.hasSession() ?
                    <span>
                        <span className="avatar image ut-user-icon"> { AuthenticationService.getUserId().charAt(0) } </span>
                        <span>{AuthenticationService.getUserId()} </span>
                    </span>: 
                    AuthenticationService.goToLoginPage()
        })
    }
    
    logOut () {
        UserService.logout()
        .then((response)=> {
            AuthenticationService.goToLoginPage();              
        })

    }
    
    render() {   
        
       return (   
            <header className="topNav">         
                <Menu  className="upper" borderless>
                    <Container fluid>                        
                        <Menu.Menu >
                            <Dropdown item pointing='top right' trigger={this.state.trigger}>
                                <Dropdown.Menu>                                    
                                    {!AuthenticationService.isLdap() && 
                                        <ChangePasswordModal /> 
                                    }
                                    <Dropdown.Item text='Logout' onClick={this.logOut} icon="sign out" />
                                </Dropdown.Menu>
                            </Dropdown>
                        </Menu.Menu>
                        <Menu.Item as='a' header position='right'>  
                            <img alt="trade connect logo" className="ut-logo"  src={logo} /> 
                        </Menu.Item>
                        
                    </Container>
                </Menu>
                <MainMenu />
            </header>
        )
    }
}

export default TCHeader