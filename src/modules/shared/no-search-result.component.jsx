import React, { Component } from 'react'

//semantic ui
import { Table } from 'semantic-ui-react'

class NoSearchResult extends Component {

    render() {    
        return (            
            <Table.Row>
                <Table.Cell colSpan={this.props.colSpan} textAlign = 'center' content='No Search Result'/>       
            </Table.Row>  
        )
    }
}

export default NoSearchResult