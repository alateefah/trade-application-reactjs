import React, { Component } from 'react'
import {Icon, Popup, Button, Label} from 'semantic-ui-react'
import _ from 'lodash'
import SharedService from './../../services/shared_service'

class UploadFile extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            info: props.info,
            fileName: "Choose a file",
            isFileUploaded: false,
            uploadedFile: null,
            uploading: false,
            error: null,
            success: null,
            acceptedFileExtensions: ['jpg','jpeg', 'tiff', 'tif', 'gif', 'png','pdf']
        }        
    }

    isValidFile = () => {        
        if (this.state.uploadedFile === null) {
            return false;
        }

        const file = this.state.uploadedFile;

        const fileSize = file.size;
        const fileType = file.type;
        const fileExtension = file.name.substr((file.name.lastIndexOf('.') +1));

        const maxByteSize = 5000000;    
        const acceptedFiles = ['image/jpg', 'image/jpeg','image/tiff', 'image/tif', 'image/gif', 'image/png', 'application/pdf'];       
        // const acceptedFileExtensions =['jpg','jpeg', 'tiff', 'tif', 'gif', 'png','pdf'];
        
        if (fileSize > maxByteSize) {      
            this.setState({errorMessage: "File too large."})      
            return false;
        }

        if(!_.includes(acceptedFiles, fileType)) {  
            this.setState({errorMessage: "Invalid file type."})          
            return false;
        }

        if(!_.includes(this.state.acceptedFileExtensions, fileExtension.toLowerCase())) {          
            this.setState({errorMessage: "Invalid file extension."})  
            return false;
        }

        return true;
    }

    readFile = (event) => {
        this.setState ({ 
            fileName: event.target.files[0].name,
            uploadedFile: event.target.files[0]
        })
    }

    clearFile = (event) => {
        event.target.value = null;
        this.setState({ uploadedFile: null, fileName: "Choose a file", errorMessage: null })
    }

    upload = (event) => {
        if (this.isValidFile()) {
            this.setState({ uploading: true})
            let data = {
                upload: this.state.uploadedFile
            }
            SharedService.upload(data)
            .then(response => {  
                this.setState({ uploading : false })
                if (!response.code) {
                    response.id = this.state.info;
                    this.props.callbackForUpload(response);
                    this.setState({ uploadedFile: null, fileName: "Choose a file", errorMessage: null })
                }
            })
        }
    }

    render () {
        return ( <span>       
            <input type="file" name="file" id={"upload_"+this.state.info} className="ut-input-file" accept="image/*, application/pdf" onChange={this.readFile} onClick={this.clearFile} style={{width: 0}}/>
            <label htmlFor={"upload_"+this.state.info} className="ui teal label"> <Icon name="upload" /> {this.state.fileName}</label> 
            <Popup trigger={<Icon name='question circle' />} inverted position = 'top center' content ={'Only '+ this.state.acceptedFileExtensions.join(", ")+ ' allowed with maximum size of 5MB'} />
            {
                this.state.uploadedFile !== null &&
                <Button type="submit" content="Upload File" onClick = {this.upload} basic size="small" color="black" loading={this.state.uploading} />
            }
            {this.state.errorMessage && <Label basic color='red' pointing="left">{this.state.errorMessage}</Label>}
        </span>
        )
    }
}

export default UploadFile