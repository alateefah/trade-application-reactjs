import React, { Component } from 'react'
import { Modal, Button, Icon, Header } from 'semantic-ui-react'

class ConfirmAction extends Component {
    
    render() {    
        return (  
            <Modal trigger = {this.props.trigger} basic size='small' open={this.props.open}>
                <Header icon='archive' content={this.props.header} />
                <Modal.Content>
                    <p>{this.props.content}</p>
                </Modal.Content>
                <Modal.Actions>
                    <Button  color='red' inverted onClick={this.props.closeAction}>
                        <Icon name='remove' /> No
                    </Button>
                    <Button color='green' inverted onClick={this.props.confirmAction}>
                        <Icon name='checkmark' /> Yes
                    </Button>
                </Modal.Actions>
            </Modal>
        )
    }
}

export default ConfirmAction