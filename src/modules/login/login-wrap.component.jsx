import React, { Component } from 'react'

// semantic ui react
import { Segment, Header } from 'semantic-ui-react';

// custom
import logo from './../../images/logo.png'; 


class LoginWrap extends Component {

    constructor(props) {
        super(props)
        this.state = {
            errorMessage: null
        }
    }
    render () {        

        return (
            <div>
                <div className="account-pages"></div>
                <div className="wrapper-page">
                    <Segment padded='very'>
                        <Header as='h3' icon textAlign='center'>
                            <img src={logo} alt="UBA Trade Logo" className="ut-logo large" />
                        </Header>                        
                        <div className="clearfix-20"></div>
                        {this.props.children}
                    </Segment>
                </div>
            </div>
        );
    }
}

export default LoginWrap