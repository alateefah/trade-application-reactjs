import React, { Component } from 'react'

// semantic ui react
import { Form, Icon, Button, Input } from 'semantic-ui-react';

// custom
import ErrorMessage from './../shared/message/error.component'
import LoginWrap from './login-wrap.component'

//api
import LoginService from './../../services/login_service.js'
import AuthenticationService from './../../services/authentication_service.js'

class Login extends Component {
    constructor (props) {
        
        super(props);

        this.state = {
            formSubmitted: false,
            invalidForm: false,
            errorMessage: null,
            formData: {
                username: '',
                password: '' 
            },
            usernameError: false,
            passwordError: false,
            isSubmitting: false  
        }    

    }

    handleInputChange = (event) => { 
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });      
    }

    login = () => {             
        if (this.validForm()) {    
            this.setState({ isSubmitting: true})
            LoginService.login(this.state.formData) 
                .then((response) => {  
                    this.setState({ isSubmitting: false });
                    if (response.code) {
                        this.setState ({ errorMessage : response.description })
                    } else {
                        AuthenticationService.setSession(response.AccessToken, response.Username, 
                                response.authType, response.Role, response.Id, response.Permissions); 
                    }    
                    
                })   
        }  
    }

    validForm = () => {        
        if (this.state.formData.username == null || this.state.formData.username === "") {            
            this.setState ({ errorMessage: "Username required", usernameError: true })
            return false;
        }

        if (this.state.formData.password == null || this.state.formData.password === "") {
            this.setState ({ errorMessage: "Password required", passwordError: true})
            return false;
        }

        return true;
    }

    removeUsernameError = () => {
        if (this.state.usernameError) {
            this.setState ({ usernameError: false })
        }
    }

    removePasswordError = () => {
        if (this.state.passwordError) {
            this.setState ({ passwordError: false })
        }
    }


    render () {        
        return (
            <LoginWrap>
                {this.state.errorMessage && 
                    <ErrorMessage message={this.state.errorMessage} />
                }
                <Form onSubmit={this.login} error>
                    <Form.Field>
                        <Input 
                            icon='user' iconPosition = 'left'                                     
                            name = 'username'
                            placeholder='Username'
                            className = 'borderless'
                            onChange = {this.handleInputChange}
                            value = {this.state.username}
                            error = {this.state.usernameError}
                            onKeyDown = {this.removeUsernameError}
                        />
                    </Form.Field>
                    <div className="clearfix-20"></div>
                    <Form.Field>
                        <Input type = 'password' 
                            icon = 'key' iconPosition = 'left'
                            name = 'password'
                            placeholder = 'Password' 
                            className = 'borderless' 
                            onChange = {this.handleInputChange}
                            value = {this.state.password}
                            error = {this.state.passwordError}
                            onKeyDown = {this.removePasswordError}
                        />
                    </Form.Field>
                    <div className="clearfix-20"></div>
                    <Button type='submit' fluid size='large' className='ut-red-btn' loading={this.state.isSubmitting}>
                        Login
                    </Button>
                    <div className="clearfix-20"></div>
                </Form>
                <div className="clearfix-10"></div>
                <Form.Field disabled>
                    <a href="#/forgot-password" className="ut-link">
                        <Icon name='lock' /> Forgot your password?
                    </a>
                </Form.Field>
            </LoginWrap>
        );
    }
}

export default Login