import React, { Component } from 'react';
import { Message, Form, Button, Icon } from 'semantic-ui-react';
import LoginWrap from './login-wrap.component'
import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'
import UserService from './../../services/user_service'

class SetPassword extends Component {
    constructor (props) {
        super(props)
        this.state = {
            resetToken: props.match.params.resetToken,
            errorMessage: null,
            formData: {
                password: ""
            },
            isSetting: false,
            successMessage: null,
            passwordType: 'password'
        }

        this.setPassword = this.setPassword.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    setPassword () {   
        this.setState ({ isSetting: true })
        if (this.state.formData.password === null || this.state.formData.password === "" ) {
            this.setState ({ errorMessage: "Password required" })
            return false;
        }
        let params = {
            token: this.state.resetToken,
            password: this.state.formData.password
        }
        
        UserService.setPassword(params)
        .then((response) => {
            if (response.code) {
                this.setState ({ isSetting: false, errorMessage: response.description })
            } else {
                this.setState ({ isSetting: false, successMessage: "Your password has been set and account activated."
            })
            }
        })

        
    }

    handleInputChange(event) { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    togglePassword = () => {
        this.setState({
            passwordType: this.state.passwordType === 'input' ? 'password' : 'input'
        })
    }

    render() {
        return (
            <LoginWrap>
                {this.state.successMessage &&
                    <div  className='center-aligned'>                                              
                        <Icon size='massive' color='green' name='checkmark box' />
                        <SuccessMessage message={this.state.successMessage}/>  
                        <a rel="stylesheet" href="#/"> <Icon name='arrow left' /> Go to Login </a>
                    </div>
                }
                {this.state.errorMessage &&
                    <div className='center-aligned'>
                        <Icon.Group size='huge'>
                            <Icon size='big' color='red' name='dont' />
                            <Icon color='black' name='user' />
                        </Icon.Group>
                        <ErrorMessage message={this.state.errorMessage}/>
                        <a rel="stylesheet" href="#/" className="tc-link"> <Icon name='arrow left' /> Back to Login </a>
                    </div>
                } 
                {!this.state.errorMessage && !this.state.successMessage && 
                    <div>
                        <Message info content ="Please set your password to access UBA Trade." />                        
                        <div className="clearfix-20"></div>
                        <Form>
                            <div className="field">
                                <label>Password </label>
                                <div className="ui icon input fluid">
                                    <input label='New Password' id = 'new-password-field'
                                        placeholder='Type in your new password ...'  type={this.state.passwordType}
                                        name = 'password' 
                                        onChange = {this.handleInputChange} 
                                        value = {this.state.formData.password}
                                    />
                                    <i className="eye link icon" onClick={this.togglePassword}></i>                      
                                </div>
                            </div>

                            <div className="clearfix-20"></div>
                            <Button type='submit' fluid size='large' className='ut-red-btn' onClick={this.setPassword} loading={this.state.isSetting}>
                                Set Password
                            </Button>
                            <div className="clearfix-20"></div>
                        </Form>
                        <div className="clearfix-10"></div>
                    </div>
                }
            </LoginWrap>
        );
    }
}

export default SetPassword