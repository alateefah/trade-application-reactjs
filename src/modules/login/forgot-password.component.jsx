import React, { Component } from 'react';
import { Form, Icon, Button, Message } from 'semantic-ui-react';
import LoginWrap from './login-wrap.component'
import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'
import UserService from './../../services/user_service'

class ForgotPassword extends Component {
    constructor (props) {
        super(props)
        this.state = {
            errorMessage: null,
            successMessage: null,
            formData: {
                email: ""
            },
            isSending: false
        }

        this.sendResetPasswordEmail = this.sendResetPasswordEmail.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
    }

    sendResetPasswordEmail () {   
        this.setState ({ errorMessage: null })
        if (this.state.formData.email === null || this.state.formData.email === "" ) {
            this.setState ({ errorMessage: "Email or Username required" })
            return false;
        }
        let params = {
            user: this.state.formData.email
        }
        this.setState ({ isSending: true })
        UserService.sendResetPasswordEmail(params)
        .then((response) => {
            if (response.code) {
                this.setState ({ isSending: false, errorMessage: response.description })
                setTimeout(() => { this.setState ({ errorMessage: null }) }, 5000)
            } else {
                this.setState ({ 
                    isSending: false, 
                    successMessage: "An email containing instructions on how to reset your password has been sent.",
                    formData: {
                        email: ''
                    }
                })
                setTimeout(() => { this.setState ({ successMessage: null }) }, 5000)
            }
           
        })

        
    }

    handleInputChange(event) { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }


    render() {
        return (
            <LoginWrap>
                <Message info content = "Enter your email address and instructions will be sent to you!" />
                <div className="clearfix-20"></div>
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage}/> }
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage}/> }                
                <Form>
                    <Form.Field>
                        <input placeholder='Email or Username' size='huge' 
                            className="borderless-input" name = 'email' onChange = {this.handleInputChange} value = {this.state.formData.email} />
                    </Form.Field>
                    <div className="clearfix-20"></div>
                    <Button type='submit' fluid size='large' className='ut-red-btn' loading={this.state.isSending} onClick={this.sendResetPasswordEmail}> Send </Button>
                    <div className="clearfix-20"></div>
                </Form>
                <div className="clearfix-10"></div>
                <Form.Field disabled>
                    <a href="#/" className="ut-link">
                        <Icon name='arrow left' /> Back to Login
                    </a>
                </Form.Field>
            </LoginWrap>
        );
    }
}

export default ForgotPassword