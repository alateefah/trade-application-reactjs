import React, { Component } from 'react'
import { Table, Loader, Header, Icon, Popup, Pagination } from 'semantic-ui-react'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import NoSearchResult from './../shared/no-search-result.component'
import ErrorMessage from './../shared/message/error.component'
import AuthenticationService from './../../services/authentication_service'
import EditConfigModal from './edit-config-modal.component'
import ConfirmAction from './../shared/confirm-action.component'
import ConfigService from './../../services/config_service'
import NewConfigModal from './new-config.component'
import ConfigSearch from './config-search.component'

class ConfigList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loading: true,
            configs: [],
            errorMessage: null,
            refreshErrorMessage: null,
            loadingMessage: "Loading config's...",
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            open: false,
            confirmMessage: null,
            configId: null,
            searchParam: ""
        }
    }

    componentWillMount () {
        this.getConfigs();
    }

    clearMessages = () => this.setState ({ refreshErrorMessage: false})

    getConfigs = () => {  
        this.clearMessages(); 
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber
        }
        ConfigService.getAll(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ configs: response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber })
            }         
            
        }) 
    }

    preSearch = (value) => {
        this.setState({searched: true,searchParam: value, pageNumber: 1, loadingMessage : "Searching..."}, () => {
            // this.setState({ loadingMessage : "Searching..." })
            this.searchConfigs();
        })
    }

    searchConfigs = () => {
        let searchParam = this.state.searchParam;
        if (searchParam !== null || searchParam !==""){
            this.setState({loadingMessage:this.state.loadingMessage })
            let params = {
                searchParam: this.state.searchParam,
                pageNumber: this.state.pageNumber,
                pageSize: this.state.pageSize
            }
            ConfigService.search(params)
            .then(response => {
                this.setState({ loadingMessage : null })
                if (response.code) {
                    this.setState ({ refreshErrorMessage: response.description })
                } else {
                    this.setState ({configs: response.list, noOfRecords: response.noOfRecords, 
                        pageSize: response.currentPageSize, pageNumber: response.pageNumber, searched: true })
                    }
            })    
        }           
    }

    clear = () => {
        this.setState({ loadingMessage: "Loading...", searchParam: "", searched: false, pageNumber : 1 }, () => {
            this.getConfigs();
        });
    }

    recordUpdated = () => {
        this.setState({ loading : true })
        this.getConfigs();
        this.setState ({ successMessage: "Configuration updated successfully." })
        setTimeout(() => {
            this.setState ({ successMessage: null });
        }, 5000);
    }

    refreshAction = () => {
        this.setState({ loadingMessage : this.state.loadingMessage })
        this.getConfigs();
    }

    delete = () => {
        this.setState({ loadingMessage : "Deleting..." })
        let params = {
            key: this.state.configId
        }
        ConfigService.delete(params)
        .then(response => { 
            if (response.code) {  
                this.setState({ open: false, loadingMessage : null, errorMessage: "Could not delete configuration. "+ response.description })
                setTimeout(() => {
                    this.setState ({ errorMessage: null });
                }, 5000);       
            } else {
                this.setState({ open: false, loadingMessage : null, successMessage: "Configuration deleted successfully." })
                this.getConfigs();
                setTimeout(() => {
                    this.setState ({ successMessage: null });
                }, 5000);
            }
        })
    }

    handlePageChange = (e, {activePage}) => { 
        this.setState({ loadingMessage : "Loading..." })
        this.setState({ pageNumber : activePage }, () => {     
            this.getConfigs();            
        });          
    }

    openConfirm = (config) => { 
        this.setState({            
            confirmMessage: "Are you sure you want to delete configuration with key: "+ config.key,
            configId: config.applicationParameterId,
            open : true
        })
    }   
    
    closeConfirm = () => { this.setState({ open : false })  }

    render () {
        let rows = this.state.configs.length <= 0 ?
            <NoSearchResult colSpan={3} /> :
            this.state.configs.map(config => {
                return  <Table.Row key={config.key}>
                            <Table.Cell singleLine>{config.key}</Table.Cell> 
                            <Table.Cell singleLine>{config.value}</Table.Cell>                            
                            <Table.Cell singleLine>{config.description}</Table.Cell>
                            <Table.Cell>{config.createdBy}</Table.Cell>
                            <Table.Cell collapsing >
                                <EditConfigModal config = {config} recordUpdated = {this.recordUpdated} />
                                <ConfirmAction 
                                    header="Confirm delete ?" 
                                    confirmAction = {()=>this.delete()} 
                                    closeAction = {()=>this.closeConfirm()} 
                                    content = {this.state.confirmMessage} 
                                    trigger = {
                                        <Popup inverted position='right center' content='Delete Config ?' size='tiny'  
                                            trigger={<Icon id={'confirm-delete-'+config.key} name = "trash" color = "red" link onClick={()=>this.openConfirm(config)} /> }
                                        />                                                                                                           
                                    }
                                    open = {this.state.open}
                                />
                            </Table.Cell>
                        </Table.Row>
        })

        return (      
            this.state.loadingMessage ?  
                <div className="ut-loader">
                    <Loader active content={this.state.loadingMessage}/> 
                </div>:  
                this.state.refreshErrorMessage ?  
                <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
                <div>
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }                       
                    {AuthenticationService.hasPermission('READ_APPLICATION_PARAMETER') &&
                    <div>
                        <ConfigSearch search = {(e)=>this.preSearch(e)} searchParam={this.state.searchParam} 
                            searched={this.state.searched} clear={this.clear} />
                        <Header as='h3' textAlign='right'>
                            <NewConfigModal newRecord = { this.getConfigs }/>
                        </Header>
                        <Table celled fixed>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell width={4}>Key</Table.HeaderCell>
                                    <Table.HeaderCell width={5}>Value </Table.HeaderCell>
                                    <Table.HeaderCell width={4}>Description</Table.HeaderCell>
                                    <Table.HeaderCell width={2}>Created By</Table.HeaderCell>
                                    <Table.HeaderCell width={1} textAlign='center'>Action </Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{rows}</Table.Body>
                            <Table.Footer>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5' textAlign="right">
                                        <Pagination pointing secondary 
                                            activePage={this.state.pageNumber} 
                                            onPageChange={this.handlePageChange} 
                                            totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                        /> 
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table>
                    </div>
                    }
                </div>
                                      
        )
    }
    
}


export default ConfigList