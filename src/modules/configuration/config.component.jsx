import React, { Component } from 'react'

//semantic ui
import { Header, Grid, Segment } from 'semantic-ui-react'

//custom
import AppWrapper from './../shared/main/app-wrapper.component'
import ConfigList from './config-list.component'

class Config extends Component {
    render () {
        return (            
            <AppWrapper> 
                <Header as='h3'>Configurations</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded="very" style={{ paddingBottom: 60 }} >                                
                                <ConfigList />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default Config