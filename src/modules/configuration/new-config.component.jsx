import React, { Component } from 'react'


//semantic ui
import { Modal, Button, Form, Input, TextArea } from 'semantic-ui-react'

//custom
import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'

//api
import ConfigService from './../../services/config_service'

class NewConfigModal extends Component {

    constructor() {
        super();
          
 		this.state = {
            modalOpen: false,
            formData: {
                ckey: '',
                cvalue: '',
                description: ''
            },
            errorMessage: null,
            successMessage: null,
            isCreating: null
        };
        
    }

    handleClose = () => {
        this.clearForm();
        this.setState({ modalOpen: false, errorMessage: null, successMessage: null })
    }

    openNewConfigModal = () => {
        this.setState({ modalOpen : true })
    }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    validForm = () => {
        this.setState ({ errorMessage: null })

        let key = this.state.formData.ckey;
        let value = this.state.formData.cvalue;

        if ( key == null || key === "") {            
            this.setState ({ errorMessage: "Configuration key required" })
            return false;
        }

        if ( value == null || value === "") {            
            this.setState ({ errorMessage: "Configuration value required" })
            return false;
        }


        return true;
    }

    createConfig = () => {
        if (this.validForm()) {
            this.setState({ isCreating : true });
            let params = {
                key: this.state.formData.ckey,
                value: this.state.formData.cvalue,
                description: this.state.formData.description
            }            
            ConfigService.create(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isCreating : false, errorMessage: response.description})                
                } else {
                    this.clearForm()
                    this.setState({ isCreating : false });
                    this.props.newRecord();
                    this.setState ({ successMessage: "Config Added!" })
                    setTimeout(() => {
                        this.setState ({ successMessage: null });
                    }, 5000);
                }
            })
        }
    }

    clearForm = () => {        
        this.setState({ 
            formData: {
                ckey : '',
                cvalue: '',
                description: ''            
            }
        });    
    }

    render () {      

        return (
            <div>
                <Button id='create-config' icon='configure' color='grey' content='Create' size='large' className="ut-blue-btn" onClick={this.openNewConfigModal} />
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Create Config </Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                        <Form>     
                            <Form.Field control={Input} label='Configuration Key' placeholder = 'Configuration key' 
                                name = 'ckey'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.ckey}
                                id = 'create-config-form-name'
                            />                         
                            <Form.Field control={TextArea} label='Configuration Value' placeholder = 'Configuration Value' 
                                name = 'cvalue'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.cvalue}
                                id = 'create-config-form-value'
                            />     
                            <Form.Field control={Input} label='Description' placeholder = 'Description' 
                                name = 'description'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.description}
                                id = 'create-config-form-description'
                            />
                        </Form>                   
                    </Modal.Content>
                    <Modal.Actions>
                        <Button icon="configure" labelPosition='left' onClick={this.createConfig} positive content='Create Config' loading = {this.state.isCreating} />
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </div>
        )
    }
}

export default NewConfigModal