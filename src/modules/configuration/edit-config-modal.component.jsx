import React, { Component } from 'react'


//semantic ui
import { Modal, Button, Form, Input, Icon, Popup, TextArea } from 'semantic-ui-react'

//custom
import ErrorMessage from './../shared/message/error.component'
import SuccessMessage from './../shared/message/success.component'

//api
import ConfigService from './../../services/config_service'

class EditConfigModal extends Component {

    constructor() {
        super();
          
 		this.state = {
            modalOpen: false,
            isUpdating: false,
            formData: {
                ckey: '',
                cvalue: '',
                description: '',
                id: ''
            },
            errorMessage: null,
            successMessage: null
        };

    }

    handleClose = () => {
        this.clearForm()
        this.setState({ modalOpen: false })
    }

    openEditConfigModal = () => {
        let config = this.props.config;
        
        this.setState ({
            formData: {
                id: config.applicationParameterId,
                ckey: config.key,
                cvalue: config.value,
                description: config.description
            }
        })
        this.setState({ modalOpen : true })
        
    }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    validForm = () => {
        this.setState ({ errorMessage: null })
        
        let ckey = this.state.formData.ckey;
        let cvalue = this.state.formData.cvalue;
        let description = this.state.formData.description;

        if ( ckey === null || ckey === "") {            
            this.setState ({ errorMessage: "Config name required" })
            return false;
        }

        if ( cvalue === null || cvalue === "") {            
            this.setState ({ errorMessage: "Config value required" })
            return false;
        }

        if ( description === null || description === "") {            
            this.setState ({ errorMessage: "Description value required" })
            return false;
        }

        return true;
    }

    updateConfig = () => {
        if (this.validForm()) {
            this.setState({ isUpdating: true });
            let params = {
                id: this.state.formData.id,
                key: this.state.formData.ckey,
                value: this.state.formData.cvalue,
                description: this.state.formData.description
            }   
            ConfigService.update(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isUpdating: false })
                    this.setState ({ errorMessage: response.description})                
                } else {            
                    this.setState({ isUpdating: false, modalOpen: false })
                    this.props.recordUpdated();
                }
                
            })
        }
    }

    clearForm = () => {
        this.setState({ 
            errorMessage: null,
            formData: {
                ckey: '',
                cvalue: '',
                description: ''
            }
        });    
    }

    render () {       

        return (
            <span>
                <Popup trigger={<Icon id={'edit-config-'+this.props.config.ckey} color="teal" className='ut-pointer' name='edit' onClick={this.openEditConfigModal} />} 
                    size='tiny' position='left center' content='Update config details' inverted /> 
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> {this.state.formData.ckey}</Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && 
                            <ErrorMessage message={this.state.errorMessage} />
                        }
                        {this.state.successMessage && 
                            <SuccessMessage message={this.state.successMessage} />
                        }
                        <Form loading={this.state.isLoadingForm}>                            
                            <Form.Field control={TextArea} label='Config Value' placeholder = 'Config Value' 
                                name = 'cvalue'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.cvalue}
                            />     
                            <Form.Field control={Input} label='Description' placeholder = 'Description' 
                                name = 'description'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.description}
                            />
                        </Form>                   
                    </Modal.Content>
                    <Modal.Actions>
                        <Button icon="users" labelPosition='left' onClick={this.updateConfig} positive 
                            content='Update config' loading={this.state.isUpdating}/>
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </span>
        )
    }
}

export default EditConfigModal