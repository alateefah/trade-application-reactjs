import React, { Component } from 'react'
import { Input, Button } from 'semantic-ui-react'

class ConfigSearch extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            searchParam: this.props.searchParam,
            loadingMessage: "Searching...",
            searched: this.props.searched
        }
    }

    handleSearchChange = (event) => {
        this.setState({ searchParam: event.target.value })
    }

    search = () => {
        this.setState({ searched: true }, () => {
            this.props.search(this.state.searchParam)
        })
    }

    clearAction = () => {
        this.setState({ searched: false, searchParam: "" }, () => {
            this.props.clear();
        })
    }

    render () {
        
        return (  
                <Input type='text' placeholder='Search... (min 3 xters)' action id='search-input' size='large' style ={{ width: 300, float: 'left' }}>
                    <input name={this.state.searchParam} value={this.state.searchParam} onChange = {this.handleSearchChange} />
                    <Button icon='search' onClick = {this.search}  disabled={this.state.searchParam.length <= 2} />                        
                    {this.state.searched && <Button onClick = {this.clearAction} content='Clear' className='left-border' id='clear-search-btn'/> }
                </Input>            
        )
    }
    
}

export default ConfigSearch