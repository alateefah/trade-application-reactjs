import React, { Component } from 'react'
import { Modal, Button, Form, Input, TextArea } from 'semantic-ui-react'
import ErrorMessage  from './../shared/message/error.component.jsx'
import SuccessMessage  from './../shared/message/success.component.jsx'
import BankService from './../../services/bank_service'
import UtilService from './../../services/util_service'

class NewBranchModal extends Component {

    constructor () {
        super();
          
 		this.state = {
            modalOpen: false,
            errorMessage: false,  
            loadingMessage: false,  
            formData: {
                branchCode: "",
                branchName: "",
                address: "",
                emails: ""
            }
        };
        
    }

    openModal = () => { this.setState ({ modalOpen: true })  }
    
    handleClose = () => { this.setState ({ modalOpen: false }); this.clearForm();  }

    create = () => {        
        
        if (this.validForm() ) {
            this.setState ({ loadingMessage: true })
            let params = {
                name: this.state.formData.branchName,
                code: this.state.formData.branchCode,
                status: true,
                email: this.state.formData.emails,
                address: this.state.formData.address
            }
            BankService.createBranch(params)
            .then(response => {
                this.setState ({ loadingMessage: false })
                if (response.code) {
                    this.setState ({ errorMessage: response.description})
                } else {
                    this.clearForm();
                    this.props.newRecord();
                    this.setState ({ successMessage: "Branch successfully added!" })
                    setTimeout(() => {
                        this.setState ({ successMessage: null });
                    }, 5000);               
                }
            })
        }
    }

    clearForm = () => {
        this.setState ({
            errorMessage: null,
            formData: {
                branchCode: "",
                branchName: "",
                address: "",
                emails: ""
            }
        })
    }

    validForm = () => {
        this.setState ({ errorMessage: null})

        let emails = this.state.formData.emails;

        if (this.state.formData.branchCode === null || this.state.formData.branchCode === "") {
            this.setState ({ errorMessage: "Branch code required" });
            return false;
        }

        if (this.state.formData.branchName === null || this.state.formData.branchName === "") {
            this.setState ({ errorMessage: "Branch name required" });
            return false;
        }

        if (this.state.formData.address === null || this.state.formData.address === "" ) {
            this.setState ({ errorMessage: "Address required" });
            return false;
        }

        if (emails === null || emails  === "") {
            this.setState ({ errorMessage: "At least one email required" });
            return false;
        }

        var emailsArray = emails.split(',');

        for (var i = 0; i < emailsArray.length; i++) {
            if (!UtilService.validEmail(emailsArray[i])) {
                this.setState ({ errorMessage: "Invalid Email address: "+ emailsArray[i] });
                return false;
            }
        }

        return true;
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
       
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });  
    }
    
    render () {    
       
        return (             
            <div>
                <Button icon='random' floated="right" id='add-branch-button' content='Create Branch' size='large' color="grey" onClick={this.openModal} />
                <Modal size='tiny' open={this.state.modalOpen}>
                   <Modal.Header> Create Branch </Modal.Header>
                    <Modal.Content>  
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  } 
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }                      
                        
                        <Form loading={this.state.loadingMessage}>  
                            <Form.Field control={Input} label='Branch Code' placeholder='Branch Code'
                                name = 'branchCode' onChange = {this.handleInputChange} value = {this.state.formData.branchCode}
                                id = 'create-branch-form-branchCode' />  
                            <Form.Field control={Input} label='Branch Name' placeholder='Branch Name'
                                name = 'branchName' onChange = {this.handleInputChange} value = {this.state.formData.branchName}
                                id = 'create-branch-form-branchName' />
                            <Form.Field control={TextArea} label='Address' placeholder='Branch Address' id = 'create-branch-form-address'
                                name = 'address' value = {this.state.formData.address} onChange = {this.handleInputChange} />   
                            <Form.TextArea label='Email Address(es) (comma seprated)' placeholder='Email Address(es) (comma seprated)' id = 'create-branch-form-emails'
                                name = 'emails' value = {this.state.formData.emails} onChange = {this.handleInputChange} />                 
                        </Form>      
                                     
                    </Modal.Content>                    
                    {!this.state.loadingMessage &&
                    <Modal.Actions>
                        <Button icon="random" labelPosition='left' onClick={this.create} positive content='Create Branch' />
                        <Button negative content='Cancel' onClick={this.handleClose} />
                    </Modal.Actions> 
                    }               
                </Modal>
            </div>
                
        )
    }
}

export default NewBranchModal