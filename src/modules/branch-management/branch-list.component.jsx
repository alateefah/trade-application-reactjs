import React, { Component } from 'react'
import { Table, Pagination, Loader, Popup, Icon } from 'semantic-ui-react'
import BankService from './../../services/bank_service'
import NewBranchModal from './new-branch-modal.component'
import AuthenticationService from './../../services/authentication_service.js'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import ErrorMessage from './../shared/message/error.component'
import EditBranchModal from './edit-branch.component'
import ConfirmAction from './../shared/confirm-action.component'
import SearchBranch from './branch-search.component'

class BranchList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loadingMessage: true,
            branches: [],
            errorMessage: null,
            refreshErrorMessage: null,
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            confirmMessage: null,
            branchId: null,
            open: false,
            searched: false,
            searchText: ""
        }
    }

    componentWillMount () { this.setState({ loadingMessage : "Loading branches..." }); this.getBranchLists(); }

    getBranchLists = () => {               
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber
        }
        BankService.getBranches(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ branches: response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => {
        this.setState({ loadingMessage : "Loading..." });
        this.setState({ pageNumber: activePage }, () => {
            if (this.state.searched) {
                this.search();
            } else {
                this.getBranchLists();
            }
        })
    }

    recordUpdated = () => {
        this.setState({ loadingMessage : "Loading..." });
        this.alertSuccess("Branch updated successfully.");
    }

    delete = () => {
        this.closeConfirm();
        this.setState({ loadingMessage : "Deleting Branch..." })
        BankService.deleteBranch(this.state.branchId)
        .then(response => {   
            if (response.code) {
                this.setState ({ loadingMessage : null, errorMessage: response.description })
                setTimeout(() => {
                    this.setState ({ errorMessage: null });
                }, 5000);
            }  else {
                this.alertSuccess("Branch deleted successfully");
            }         
            
        }) 
    }

    alertSuccess = (msg) => {
        this.setState ({ successMessage: msg })
        this.getBranchLists();
        setTimeout(() => {
            this.setState ({ successMessage: null });
        }, 5000);
    }

    closeConfirm = () => { this.setState({ open : false })  }

    openConfirm = (branch) => { 
        this.setState({            
            confirmMessage: "Are you sure you want to delete branch: "+ branch.name,
            branchId: branch.id,
            open : true
        })
    } 

    preSearch = (value) => {
        this.setState({searched: true,searchText: value, pageNumber: 1}, () => {
            this.setState({ loadingMessage : "Searching..." })
            this.search();
        })
    }

    search = () => {
        let params = {
            pageSize: 10,
            searchText: this.state.searchText,
            pageNumber: this.state.pageNumber
        }
        BankService.filterBranches(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                return;
            } else {
               this.setState({  branches: response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber });
            }  
        }) 
    }

    clear = () => {
        this.setState({ loadingMessage: "Loading...", searchText: "", searched: false, pageNumber : 1 }, () => {
            this.getBranchLists();
        });
    }

    render () {
        const { branches } = this.state;

        let branchesRows;
        
        if (branches.length > 0) 
        {
            branchesRows = branches.map(branch => (
                <Table.Row key={branch.id}>
                    <Table.Cell collapsing>{branch.code}</Table.Cell>
                    <Table.Cell collapsing>{branch.name}</Table.Cell>
                    <Table.Cell>{branch.address}</Table.Cell>
                    <Table.Cell collapsing textAlign='center'>
                        <EditBranchModal branch={branch} recordUpdated = {()=>this.recordUpdated()}/>
                        <ConfirmAction 
                            header="Confirm delete ?" 
                            confirmAction = {()=>this.delete()} 
                            closeAction = {()=>this.closeConfirm()} 
                            content = {this.state.confirmMessage} 
                            trigger = {
                                <Popup inverted position='right center' content='Delete Branch ?' size='tiny'  
                                    trigger={<Icon id={'confirm-delete-'+branch.id} name = "trash" color = "red" link onClick={()=>this.openConfirm(branch)} /> }
                                />                                                                                                           
                            }
                            open = {this.state.open}
                        />            
                    </Table.Cell>
                </Table.Row>           
            ));
        }
        
        return (      
            <span>                
                {this.state.refreshErrorMessage &&  <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.getTemplateLists} /> }
                {this.state.loadingMessage && <Loader active inline='centered' />}
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                {
                    !this.state.loadingMessage && !this.state.refreshErrorMessage &&
                    <span>
                        {
                        AuthenticationService.hasPermission('READ_ALL_BRANCHES') && 
                        <SearchBranch search = {(e)=>this.preSearch(e)} searchText={this.state.searchText} 
                            searched={this.state.searched} clear={this.clear} /> }
                        {AuthenticationService.hasPermission('CREATE_BRANCH') && <NewBranchModal newRecord = {this.getBranchLists} /> }
                        <div style={{ paddingTop: 1, clear: 'both' }}></div>
                        <Table celled striped>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell collapsing>Branch Code</Table.HeaderCell>
                                    <Table.HeaderCell>Name</Table.HeaderCell>
                                    <Table.HeaderCell>Address</Table.HeaderCell>
                                    <Table.HeaderCell collapsing>Action</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{branchesRows}</Table.Body>
                            <Table.Footer fullWidth>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5' textAlign="right"> 
                                        <Pagination pointing secondary defaultActivePage={this.state.pageNumber} onPageChange={this.handlePaginationChange} 
                                            totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} />
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table> 
                    </span>
                }             
            </span>
                                      
        )
    }
    
}

export default BranchList