import React, { Component } from 'react'
import { Header, Grid, Segment } from 'semantic-ui-react'
import AppWrapper from './../shared/main/app-wrapper.component'
import BranchList from './branch-list.component'

class BranchManagement extends Component {
    render () {
        return (            
            <AppWrapper> 
                <Header as='h3'>Branch Management</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded="very" style={{ paddingBottom: 60 }}>                                
                                <BranchList />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default BranchManagement