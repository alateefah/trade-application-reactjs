import React, { Component } from 'react'
import { Input, Button } from 'semantic-ui-react'

class BranchSearch extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            searchText: this.props.searchText,
            searched: this.props.searched
        }
    }

    handleSearchChange = (event) => {
        this.setState({ searchText: event.target.value })
    }

    search = () => {
        this.setState({ searched: true }, () => {
            
            this.props.search(this.state.searchText)
        })
    }

    clearAction = () => {
        this.setState({ searched: false, searchText: "" }, () => {
            this.props.clear();
        })
    }

    render () {
        
        return (  
                <Input type='text' placeholder='Search... (min 3 xters)' action id='search-input' size='large' style ={{ width: 300, float: 'left' }}>
                    <input name={this.state.searchText} value={this.state.searchText} onChange = {this.handleSearchChange} />
                    <Button icon='search' onClick = {this.search}  disabled={this.state.searchText.length <= 2} />                        
                    {this.state.searched && <Button onClick = {this.clearAction} content='Clear' className='left-border' id='clear-search-btn'/> }
                </Input>            
        )
    }
    
}

export default BranchSearch