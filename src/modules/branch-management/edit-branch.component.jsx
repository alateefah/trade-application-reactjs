import React, { Component } from 'react'
import { Modal, Button, Form, Icon, TextArea, Input, Popup } from 'semantic-ui-react'
import ErrorMessage  from './../shared/message/error.component.jsx'
import BankService from './../../services/bank_service'
import UtilService from './../../services/util_service'

class EditBranchModal extends Component {

    constructor (props) {
        super(props);
          
 		this.state = {
            modalOpen: false,
            branch: props.branch,
            formData: {
                id: '',
                branchCode: '',
                address: '',
                branchName: '',
                emails: ''
            },
            loading: false     
        };
        
    }

    closeModal = () => { this.setState({ errorMessage:null, modalOpen: false }); }

    openModal = () => { 
        this.setState ({            
             formData: {
                id: this.props.branch.id,
                branchCode: this.props.branch.code,
                address: this.props.branch.address,
                branchName: this.props.branch.name,
                emails: this.props.branch.email
            },
            modalOpen: true
        })
    }

    update = () => {    
        if (this.validForm() ) {
            this.setState ({ loading: true })
            let params = {
                id:  this.state.formData.id,
                name: this.state.formData.branchName,
                code: this.state.formData.branchCode,
                status: true,
                email: this.state.formData.emails,
                address: this.state.formData.address
            }
            BankService.updateBranch(params)
            .then(response => {
                this.setState ({ loading: false })
                if (response.code) {
                    this.setState ({ errorMessage: response.description})
                } else {
                    this.setState ({ modalOpen : false }) 
                    this.props.recordUpdated();
                                
                }
            })
        }
    }

    validForm = () => {
        this.setState ({ errorMessage: null})

        if (this.state.formData.branchCode === null || this.state.formData.branchCode === "") {
            this.setState ({ errorMessage: "Branch code required" });
            return false;
        }

        if (this.state.formData.branchName === null || this.state.formData.branchName === "") {
            this.setState ({ errorMessage: "Branch name required" });
            return false;
        }

        if (this.state.formData.address === null || this.state.formData.address === "" ) {
            this.setState ({ errorMessage: "Address required" });
            return false;
        }
        if (this.state.formData.emails === null || this.state.formData.emails  === "") {
            this.setState ({ errorMessage: "At least one email required" });
            return false;
        }
        
        var emailsArray = this.state.formData.emails.split(',');

        for (var i = 0; i < emailsArray.length; i++) {
            if (!UtilService.validEmail(emailsArray[i])) {
                this.setState ({ errorMessage: "Invalid Email address: "+ emailsArray[i] });
                return false;
            }
        }

        return true;
    }

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
       
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });  
    }

    render () {    

        return (             
            <span> 
                <Popup inverted position="top left"
                    trigger={<Icon name = 'edit' link onClick={this.openModal} color="teal" />}
                    content='Update Branch'
                />
                
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> {this.state.formData.branchName} </Modal.Header>
                    <Modal.Content>  
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  }   
                        <Form loading={this.state.loading}>  
                            <Form.Field control={Input} label='Branch Code' placeholder='Branch Code'
                                name = 'branchCode' onChange = {this.handleInputChange} value = {this.state.formData.branchCode}
                                id = 'create-branch-form-branchCode' />  
                            <Form.Field control={Input} label='Branch Name' placeholder='Branch Name'
                                name = 'branchName' onChange = {this.handleInputChange} value = {this.state.formData.branchName}
                                id = 'create-branch-form-branchName' />
                            <Form.Field control={TextArea} label='Address' placeholder='Branch Address' id = 'create-branch-form-address'
                                name = 'address' value = {this.state.formData.address} onChange = {this.handleInputChange} />   
                            <Form.Field control={TextArea} label='Email Address(es) (comma seprated)' placeholder='Email Address(es) (comma seprated)' id = 'create-branch-form-emails'
                                name='emails' value = {this.state.formData.emails} onChange = {this.handleInputChange} />                           
                        </Form>    
                    </Modal.Content>
                    {!this.state.loading &&
                    <Modal.Actions>
                        <Button  icon="book" labelPosition='left' onClick={this.update} positive content='Update Branch' />
                        <Button negative content='Cancel' onClick={this.closeModal}/>
                    </Modal.Actions> 
                    }               
                </Modal>
            </span>                
        )
    }
}

export default EditBranchModal