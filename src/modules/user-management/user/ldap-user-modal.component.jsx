import React, { Component } from 'react'

//semantic ui
import { Modal, Button, Form, Select, Input, Message, List, Search } from 'semantic-ui-react'

//error 
import ErrorMessage  from './../../shared/message/error.component.jsx'
import SuccessMessage from './../../shared/message/success.component'

//api
import UserService from './../../../services/user_service'
import RoleService from './../../../services/role_service'
import BankService from './../../../services/bank_service'

class LdapUserModal extends Component {
    
    constructor() {
        super();
          
 		this.state= {
            roles:[],
            userTypes: [],
            modalOpen: false,
            isLoadingBranches: false, 
            value: '', 
            results: [],
            isLoadingForm: false,
            branches: [],
            searchUserView: true,
            errorMessage: null,
            formData: {
                username: '',
                email: '',
                userRole: '',
                firstName: '',
                jobTitle: ''
            },
            isImporting : false,
            successMessage: null,
            branchDisplayValue: ''
        };
                
    }

    handleClose = () => {
        this.clearForm();
        this.setState({ modalOpen: false })        
    }

    getAvailableRoles = () => {
        RoleService.getAvailableRoles()
            .then(response => {   
                if (response.code) {

                } else {
                   this.setState({ roles : response, isLoadingForm: false })
                }                       
                
            }) 
    }

    getUserTypes = () => {
        this.setState({ isLoadingForm : true })
        UserService.getUserTypes ()
            .then(response => {          
                if (response.status) {

                } else {                    
                    this.setState({ userTypes: response })                    
                }                        
                
            }) 
    }

    openLdapUserModal = () => { this.setState({ loadingOptions: true, modalOpen : true }); }

    handleInputChange = (event) => { 
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });      
    }

    importUser = () => {
        this.setState ({ errorMessage: null })
        let username = this.state.formData.username;
        if (username === null || username === "") {
            this.setState ({ errorMessage: "Username is required" })
            return false;
        }
        
        this.setState ({ isImporting: true })
        UserService.importUser(username)
        .then(response => {     
            this.setState({ isImporting: false})
            if (response.code) {
                this.setState ({ errorMessage : response.description })
            }  else {
                this.setState({ 
                    formData: {
                        firstName : response.firstName,
                        lastName: response.lastName,
                        email: response.emailAddress,
                        jobTitle: response.jobTitle,    
                        username: response.emailAddress.split('@')[0]                    
                    }
                });    
                this.setState ({ searchUserView: false })
                this.getUserTypes();
                this.getAvailableRoles();   
            }                     
            
        })
    }

    clearForm = () => {
        this.setState({ 
            formData: {
                firstName : '',
                lastName: '',
                email: '',
                jobTitle: '',
                username: ''                      
            },
            searchUserView: true, 
            errorMessage: null,
            branchDisplayValue: '' 
        })
    }

    handleSelectChange = (e, val) =>  {        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    }

    addUser = () => {
        if (this.validForm()) {
            this.setState({ isLoadingForm: true });
            let params = {
                username: this.state.formData.username,
                emailAddress: this.state.formData.email,              
                authType: "LDAP",
                roleId: this.state.formData.role,
                branchId: this.state.formData.branch,
                userTypeId: this.state.formData.type
            }
            
            UserService.createUser(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isLoadingForm: false, errorMessage: response.description})                
                } else {            
                    this.clearForm()
                    this.setState({ isLoadingForm: false, successMessage: "User Added!" })
                    setTimeout(() => {
                        this.setState ({ successMessage: null });
                    }, 5000);
                    this.props.newRecord();
                }
            })
        }
    }

    validForm = () => {
        this.setState ({ errorMessage: null })

        if (this.state.formData.type == null || this.state.formData.type === "") {
            this.setState ({ errorMessage: "User type required"})
            return false;
        }

        if (this.state.formData.role == null || this.state.formData.role === "") {
            this.setState ({ errorMessage: "Role Required"})
            return false;
        }

        if (this.state.formData.branch == null || this.state.formData.branch === "") {
            this.setState ({ errorMessage: "Branch required"})
            return false;
        }

        return true;
    }

    handleSearchChange = (event) => {
        let val = event.target.value;

        this.setState({ branchDisplayValue : val })

        if (val.length > 2) {
            this.filterBranches(val);
        }
        
    }

    resultRenderer = (params) => {
        return (
            <div key={params.id}>            
                <div className="main">
                    <div className="price">{params.code}</div>
                    <div className="title">{params.name}</div>
                    {params.address && <div className="desc">{params.address}</div>}
                </div>
            </div>
        )
        
    }

    branchSelected = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                branch : val.result.id
            },
            branchDisplayValue : val.result.name        
        })
    }

    filterBranches = (value) => {
        let params = {
            pageSize: 5,
            searchText: value,
            pageNumber: 1
        }
        BankService.filterBranches(params)
        .then(response => {   
            if (response.code) {
                return;
            } else {
               this.setState({ filteredBranches : response.list })                
            }  
        }) 
    }

    render () {       
        let userTypes, roles;

        if (this.state.userTypes !== []) {
            userTypes =this.state.userTypes.map(uTypes => (
                { key: uTypes.id, text: uTypes.name, value: uTypes.id }            
            ));
        }

        if (this.state.roles !== []) {
            roles =this.state.roles.map(role => (
                { key: role.id, text: role.name, value: role.id }            
            ));
        }

        return (
            <div>
                <Button id='import-user-btn' icon='download' color="grey" content='Import' size='large' className="ut-blue-btn" onClick={this.openLdapUserModal}/>
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Import User </Modal.Header>
                    <Modal.Content>
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  }
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                    {
                    this.state.searchUserView ? 
                    <Form loading={this.state.isImporting}>
                        <Form.Field control={Input} 
                            label='Username' name = 'username' placeholder = 'john.doe' onChange = {this.handleInputChange}/>
                    </Form>:
                    <Form loading={this.state.isLoadingForm}>
                        <Message onDismiss={this.clearForm} >
                            <Message.Content>
                            <List>
                                <List.Item icon='user' content = {this.state.formData.firstName + " " + this.state.formData.lastName} />
                                <List.Item icon='id badge' content = {this.state.formData.jobTitle} />
                                <List.Item icon='mail' content = {this.state.formData.email} />
                            </List>
                            </Message.Content>
                        </Message>
                        
                        <Form.Field control={Select} label='User Type' options={userTypes} 
                            placeholder='Select user type' 
                            name = 'type'
                            onChange = {this.handleSelectChange}
                            value = {this.state.type}
                            id = 'import-user-form-usertype'
                        />                  
                        <Form.Field control={Select} label='Assign Role' options={roles} 
                            placeholder='Assign role'
                            name = 'role'
                            onChange = {this.handleSelectChange}
                            value = {this.state.role} 
                            id = 'import-user-form-role'
                        /> 
                        <Form.Field>
                            <label>Bank Branch</label>
                            <Search 
                                onSearchChange={this.handleSearchChange}
                                type='text'
                                results={this.state.filteredBranches} 
                                fluid
                                placeholder='Search branch... (min. of 3 xters)'
                                minCharacters={3} 
                                resultRenderer = {this.resultRenderer}
                                onResultSelect = {this.branchSelected} 
                                value = {this.state.branchDisplayValue}
                                id = 'import-user-form-branch'
                            /> 
                        </Form.Field>                            
                    </Form>
                    }
                    </Modal.Content>
                    <Modal.Actions>
                        {
                        this.state.searchUserView ? 
                        <Button positive icon="download" labelPosition='left' content='Import user' onClick={this.importUser} /> :
                        <Button positive  icon="add user" labelPosition='left' content='Create user' onClick={this.addUser}/>
                        }
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </div>
        )
    }
}

export default LdapUserModal