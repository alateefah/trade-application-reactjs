import React, { Component } from 'react'

//semantic ui
import { Modal, Button, Form, Select, Icon, Message, List, Popup, Search } from 'semantic-ui-react'

//custom
import ErrorMessage from './../../shared/message/error.component'
import SuccessMessage from './../../shared/message/success.component'

//api
import UserService from './../../../services/user_service'
import RoleService from './../../../services/role_service'
import BankService from './../../../services/bank_service'

class EditUserModal extends Component {

    constructor(props) {
        super(props);
          
 		this.state = {
            roles:[],
            userTypes: [],
            modalOpen: false,
            isLoadingBranches: false, 
            value: '', 
            isLoadingForm: false,
            branches: [],
            formData: {
                username: '',
                email: '',
                branch: '',
                type: '',
                role: '',
                id: ''
            },
            errorMessage: null,
            successMessage: null,
            filteredBranches : []
        };
    }

    handleClose = () => {
        this.setState({ errorMessage: null, modalOpen: false })
    }

    getAvailableRoles = () => {
        RoleService.getAvailableRoles()
            .then(response => {   
                if (response.code) {

                } else {
                   this.setState({ roles : response, isLoadingForm : false })   
                }                        
                
            }) 
    }

    getUserTypes = () => {
        UserService.getUserTypes ()
            .then(response => {          
                if (response.status) {

                } else {                    
                    this.setState({ userTypes :  response })                    
                }                        
                
            }) 
    }

    openEditUserModal = () => {        
        let user = this.props.user;
        this.setState ({
            formData: {
                username: user.username,
                email: user.emailAddress,
                authType: user.authType,
                role: user.role ? user.role.id: "",
                branch: user.branch ? user.branch.id: "",
                type: user.userType ? user.userType.id: "",
                id: user.id
            },            
            branchDisplayValue: user.branch ? user.branch.name: ""
        })
        this.setState({ isLoadingForm: true , modalOpen : true })
        this.getUserTypes();
        this.getAvailableRoles();
    }

    handleSelectChange = (e, val) =>  {

        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    }

    validForm = () => {

        this.setState ({ errorMessage: null })
       
        if (this.state.formData.type == null || this.state.formData.type === "") {
            this.setState ({ errorMessage: "User type required"})
            return false;
        }

        if (this.state.formData.role == null || this.state.formData.role === "") {
            this.setState ({ errorMessage: "Role Required"})
            return false;
        }

        if (this.state.formData.branch == null || this.state.formData.branch === "") {
            this.setState ({ errorMessage: "Branch required"})
            return false;
        }

        return true;
    }

    updateUser = () => {
        if (this.validForm()) {
            this.setState({ isLoadingForm: true });
            let params = {
                id: this.state.formData.id,
                roleId: this.state.formData.role,
                branchId: this.state.formData.branch,
                userTypeId: this.state.formData.type
            }
            UserService.update(params)
            .then( response => {
                if (response.code) {
                this.setState({ isLoadingForm: false })
                this.setState ({ errorMessage: response.description})                
                } else {
                    this.setState({ isLoadingForm: false,  modalOpen : false})
                    this.props.recordUpdated();
                }
            })
        }
    }

    handleSearchChange = (event) => {
        let val = event.target.value;

        this.setState({ branchDisplayValue : val })

        if (val.length > 2) {
            this.filterBranches(val);
        }
        
    }

    resultRenderer = (params) => {
        return (
            <div key={params.id}>            
                <div className="main">
                    <div className="price">{params.code}</div>
                    <div className="title">{params.name}</div>
                    {params.address && <div className="desc">{params.address}</div>}                    
                </div>
            </div>
        )
        
    }

    branchSelected = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                branch : val.result.id
            },
            branchDisplayValue : val.result.name        
        })
    }

    filterBranches = (value) => {
        let params = {
            pageSize: 5,
            searchText: value,
            pageNumber: 1
        }
        BankService.filterBranches(params)
        .then(response => {   
            if (response.code) {
                return;
            } else {
               this.setState({ filteredBranches : response.list })                
            }  
        }) 
    }

    render () {       
        let userTypes, roles;

        if (this.state.userTypes !== []) {
            userTypes =this.state.userTypes.map(uTypes => (
                { key: uTypes.id, text: uTypes.description, value: uTypes.id }            
            ));
        }

        if (this.state.roles !== []) {
            roles =this.state.roles.map(role => (
                { key: role.id, text: role.name, value: role.id }            
            ));
        }

        return (
            <span>
                <Popup trigger={<Icon className='ut-pointer' color='teal' name='edit' id={'edit-user-'+this.state.formData.username} onClick={this.openEditUserModal} />} content='Update user details' inverted />
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Edit User </Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />   }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                        <Form loading={this.state.isLoadingForm}>  
                            <Message>
                                <Message.Content>
                                    <List>
                                        <List.Item icon='users' content = {this.state.formData.username} />
                                        <List.Item icon='mail' content = {this.state.formData.email} />
                                    </List>
                                </Message.Content>
                            </Message>
                            <Form.Field control={Select} label='User Type' options={userTypes} 
                                placeholder='Select user type' 
                                name = 'type'
                                onChange = {this.handleSelectChange}
                                value = {this.state.formData.type}
                                id = 'edit-user-form-usertype'
                            />                       
                            <Form.Field control={Select} label='Assign Role' options={roles} 
                                placeholder='Assign role' 
                                name = 'role'
                                onChange = {this.handleSelectChange}
                                value = {this.state.formData.role}
                                id = 'edit-user-form-role'
                            />                            
                            <Form.Field>
                                <label>Bank Branch</label>
                                <Search 
                                    onSearchChange={this.handleSearchChange}
                                    type='text'
                                    results={this.state.filteredBranches} 
                                    fluid
                                    placeholder='Search branch... (min. of 3 xters)'
                                    minCharacters={3} 
                                    resultRenderer = {this.resultRenderer}
                                    onResultSelect = {this.branchSelected} 
                                    value = {this.state.branchDisplayValue}
                                    id = 'create-user-form-branch'
                                /> 
                            </Form.Field>                              
                        </Form>                    
                    </Modal.Content>
                    <Modal.Actions>
                        <Button  icon="add user" labelPosition='left' onClick={this.updateUser} positive content='Update' />
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </span>
        )
    }
}

export default EditUserModal