import React, { Component } from 'react'


//semantic ui
import { Modal, Button, Form, Select, Input, Search } from 'semantic-ui-react'

//custom
import ErrorMessage from './../../shared/message/error.component'
import SuccessMessage from './../../shared/message/success.component'

//api
import UserService from './../../../services/user_service'
import RoleService from './../../../services/role_service'
import BankService from './../../../services/bank_service'
import UtilService from './../../../services/util_service'

class NewUserModal extends Component {

    constructor() {
        super();
          
 		this.state = {
            roles:[],
            userTypes: [],
            modalOpen: false,
            isLoadingBranches: false, 
            value: '', 
            isLoadingForm: false,
            branches: [],
            formData: {
                username: '',
                email: '',
                branch: '',
                type: '',
                role: ''
            },
            errorMessage: null,
            successMessage: null,
            filteredBranches: [],
            branchDisplayValue: ''
        };
        
    }

    openNewUserModal = () => {
        this.setState({ isLoadingForm: true, modalOpen : true })
        this.getUserTypes();
        this.getAvailableRoles();
    }

    handleClose = () => {
        this.clearForm();
        this.setState({ modalOpen: false })
    }

    getAvailableRoles = () => {
        RoleService.getAvailableRoles()
        .then(response => {   
            if (response.code) {

            } else {    
                this.setState({ roles : response, isLoadingForm : false })
            }                        
            
        }) 
    }

    getUserTypes = () => {
        UserService.getUserTypes ()
            .then(response => {          
                if (response.status) {

                } else {                    
                    this.setState({ userTypes : response })                    
                }                        
                
            }) 
    }    

    filterBranches = (value) => {
        let params = {
            pageSize: 5,
            searchText: value,
            pageNumber: 1
        }
        BankService.filterBranches(params)
        .then(response => {   
            if (response.code) {
                return;
            } else {
               this.setState({ filteredBranches : response.list })                
            }  
        }) 
    }

    handleInputChange = (event) => {         
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    handleSelectChange = (e, val) =>  {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    }

    validForm = () => {
        this.setState ({ errorMessage: null })

        let username = this.state.formData.username;
        if (username == null || username === "") {            
            this.setState ({ errorMessage: "Username required" })
            return false;
        }

        if (!UtilService.validAlphaNumericRegExp(username)) {            
            this.setState ({ errorMessage: "Username must be at least 5 characters and cannot start with number or special character" })
            return false;
        }

        if (this.state.formData.email == null || this.state.formData.email === "") {
            this.setState ({ errorMessage: "Email required"})
            return false;
        }

        var emailRegExp = /\S+@\S+\.\S+/;
        if (!emailRegExp.test(this.state.formData.email)) {
            this.setState ({ errorMessage: "Invalid email format"})
            return false;
        }

        if (this.state.formData.type == null || this.state.formData.type === "") {
            this.setState ({ errorMessage: "User type required"})
            return false;
        }

        if (this.state.formData.role == null || this.state.formData.role === "") {
            this.setState ({ errorMessage: "Role Required"})
            return false;
        }

        if (this.state.formData.branch == null || this.state.formData.branch === "") {
            this.setState ({ errorMessage: "Branch required"})
            return false;
        }

        return true;
    }

    addUser = () => {
        if (this.validForm()) {
            this.setState({ isLoadingForm: true });
            let params = {
                username: this.state.formData.username,
                emailAddress: this.state.formData.email,              
                authType: "DB",
                roleId: this.state.formData.role,
                branchId: this.state.formData.branch,
                userTypeId: this.state.formData.type
            }
            UserService.createUser(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isLoadingForm: false })
                    this.setState ({ errorMessage: response.description})                
                } else {            
                    this.clearForm()
                    this.setState({ isLoadingForm: false })
                    this.props.newRecord();
                    this.setState ({ successMessage: "User Added!" })
                    setTimeout(() => {
                        this.setState ({ successMessage: null });
                    }, 5000);
                }
            })
        }
    }

    clearForm = () => {
        this.setState ({
            errorMessage: null, 
            formData: {
                username : '',
                email: '',
                branch: '',
                type: '',
                role: ''                      
            },            
            branchDisplayValue: '' 
        });    
    }

    handleSearchChange = (event) => {
        let val = event.target.value;

        this.setState({ branchDisplayValue : val })

        if (val.length > 2) {
            this.filterBranches(val);
        }
        
    }

    resultRenderer = (params) => {
        return (
            <div key={params.id}>            
                <div className="main">
                    <div className="price">{params.code}</div>
                    <div className="title">{params.name}</div>
                    {params.address && <div className="desc">{params.address}</div>}
                </div>
            </div>
        )
    }

    branchSelected = (e, val) => {
        this.setState({ 
            formData: {
                ...this.state.formData,
                branch : val.result.id
            },
            branchDisplayValue : val.result.name        
        })
    }

    render () {      
        
        let userTypes, roles;

        if (this.state.userTypes !== []) {

            userTypes = this.state.userTypes.map(uTypes => (
                { key: uTypes.id, text: uTypes.description, value: uTypes.id }            
            ));
        }

        if (this.state.roles !== []) {
            
            roles = this.state.roles.map(role => (
                { key: role.id, text: role.name, value: role.id }            
            ));
        }

        return (
             
            <div>
                <Button icon='add user' id='add-user-button' color="grey" content='Create' size='large' onClick={this.openNewUserModal} />
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Create User </Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                        <Form loading={this.state.isLoadingForm}>
                            <Form.Group widths='equal'>
                                <Form.Field control={Input} label='Username' placeholder='Username'
                                    name = 'username' onChange = {this.handleInputChange} value = {this.state.formData.username} id = 'create-user-form-username' />
                                <Form.Field control={Select} label='User Type' options={userTypes} placeholder='Select user type' 
                                    name = 'type' onChange = {this.handleSelectChange} value = {this.state.formData.type} id = 'create-user-form-usertype' />
                            </Form.Group>                              
                            <Form.Field control={Input} label='Email' placeholder = 'Email' 
                                name = 'email' onChange = {this.handleInputChange} value = {this.state.formData.email} id = 'create-user-form-email' />                        
                            <Form.Field control={Select} label='Assign Role' options={roles} 
                                placeholder='Assign role' name = 'role' onChange = {this.handleSelectChange} value = {this.state.formData.role} id = 'create-user-form-role' />  
                            <Form.Field>
                                <label>Bank Branch</label>
                                <Search 
                                    onSearchChange={this.handleSearchChange}
                                    type='text'
                                    results={this.state.filteredBranches} 
                                    fluid
                                    placeholder='Search branch... (min. of 3 xters)'
                                    minCharacters={3} 
                                    resultRenderer = {this.resultRenderer}
                                    onResultSelect = {this.branchSelected} 
                                    value = {this.state.branchDisplayValue}
                                    id = 'create-user-form-branch'
                                /> 
                            </Form.Field>                         
                        </Form>                    
                    </Modal.Content>
                    <Modal.Actions>
                        <Button icon="add user" labelPosition='left' onClick={this.addUser} positive content='Create user' />
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </div>
                
        )
    }
}

export default NewUserModal