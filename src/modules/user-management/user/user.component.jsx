import React, { Component } from 'react'
import { Button, Table, Label, Checkbox, Loader, Input, List, Icon, Popup, Pagination } from 'semantic-ui-react'
import NewUserModal from './new-user-modal.component'
import LdapUserModal from './ldap-user-modal.component'
import EditUserModal from './edit-user-modal.component'
import Refresh from './../../shared/refresh.component'
import SuccessMessage from './../../shared/message/success.component'
import NoSearchResult from './../../shared/no-search-result.component'
import ErrorMessage from './../../shared/message/error.component'
import AuthenticationService from './../../../services/authentication_service'
import UserService from './../../../services/user_service'

class User extends Component {

    constructor (props) {
        super(props);

        this.state = {
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            loading: true,
            users: [],
            errorMessage: null,
            refreshErrorMessage: null,
            searchText: '',
            searched: false,
            loadingMessage: "Loading users...",
            loadingUsersMessage: "Loading users...",
            searchingMessage: "Searching...",
            resendingActivationMessage:"Resending activation email...",
            unlockingUserMessage: "Unlocking user..."
        }
    }

    componentWillMount () {
        this.setState({ loadingMessage : this.state.loadingUsersMessage })
        this.getUsers();
    }

    getUsers = () => {        
        this.setState ({ refreshErrorMessage: false,  searched: false, searchText: '' })
        UserService.getUsersList(this.state.pageNumber, this.state.pageSize)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code === 1008) {
                return;
            }
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ 
                    users : response.list, 
                    noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, 
                    pageNumber: response.currentPageNumber 
                })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => { 
        this.setState({ pageNumber : activePage }, () => {              
            if (this.state.searched) {
                this.searchUser();
            } else {
                this.setState({ loadingMessage : "Loading..." })
                this.getUsers();
            }
        });          
    }

    newRecordInserted = () => { this.getUsers();  }

    refreshAction = () => {
        this.setState({ loadingMessage : this.state.loadingUsersMessage })
        this.getUsers();
    }

    recordUpdated = () => {
        this.setState ({ loadingMessage : this.state.loadingUsersMessage })
        this.getUsers();
        this.setState ({ successMessage: "User updated successfully." })
        setTimeout(() => {
            this.setState ({ successMessage: null });
        }, 5000);
    }

    search = () => {
        this.setState({ pageNumber : 1 }, () => {
            this.searchUser();
        })
    }

    searchUser = () => {
        let searchText = this.state.searchText;
        
        if ( (searchText !== null || searchText !== '') && searchText.length > 3) {
            this.setState({ loadingMessage : this.state.searchingMessage })
            let params = {
                searchText: searchText,
                pageNumber: this.state.pageNumber, 
                pageSize: this.state.pageSize
            }

            UserService.search(params)
            .then(response => {   
                this.setState({ loadingMessage : null })
                if (response.code) {
                    this.setState ({ refreshErrorMessage: response.description })
                }  else {
                    this.setState ({ 
                        users : response.list, 
                        noOfRecords: response.noOfRecords, 
                        pageSize: response.currentPageSize, 
                        pageNumber: response.currentPageNumber,
                        searched: true
                    })
                }   
            }) 
        }
    }

    handleSearchChange = (event) => { this.setState({ searchText: event.target.value }); }

    resendActivationEmail = (username) => {
        this.setState({ loadingMessage : this.state.resendingActivationMessage })
        let params = {
            username: username
        }
        UserService.resendActivationEmail(params)
        .then(response => {   
            this.setState({ loadingMessage : null })            
            this.setState ({ errorMessage: response.description })
            setTimeout(() => {
                this.setState ({ errorMessage: null })
            }, 5000)             
        }) 
        .catch (error => {
            this.setState({ loadingMessage : null }) 
            this.setState ({ successMessage: "Mail Successfully sent" })
            setTimeout(() => {
                this.setState ({ successMessage: null })
            }, 5000)
        })
    }

    updateEnable = (id, value) => {
        let selected = document.getElementById(id);
        let val = value ? 0 : 1

        selected.classList.add("disabled");
        let params = {
            id: id,
            is_enabled: val
        }

        UserService.updateEnable (params)
        .then(response => {   
            selected.checked = value ? 1 : 0
            this.setState ({ errorMessage: response.description })
            //selected.classList.remove("disabled"); 
            setTimeout(() => {
                this.setState ({ errorMessage: null })
            }, 5000)             
        }) 
        .catch (error => { 
            selected.checked = val  
            //selected.classList.remove("disabled");            
        })
    }

    unlockAccount = (id) => {
        this.setState({ loadingMessage : this.state.unlockingUserMessage })
        let params = {
            id: id
        }
        UserService.unlock(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {            
                this.setState ({ errorMessage: response.description })
                setTimeout(() => {
                    this.setState ({ errorMessage: null })
                }, 5000)             
            } else {
                this.getUsers();
                this.setState ({ successMessage: "User unlocked" })
                setTimeout(() => {
                    this.setState ({ successMessage: null })
                }, 5000)
            }
        })
    }

    clearAction = () => {
        this.setState({ pageNumber: 1, loadingMessage : this.state.loadingUsersMessage }, () => {
            this.getUsers();
        })        
    }

    render () {
        let rows = this.state.users.length <= 0 ?
            <NoSearchResult colSpan={7} /> :
            this.state.users.map(person => {
                return  <Table.Row key={person.id}>
                            <Table.Cell>
                                <Label ribbon>{person.authType}</Label>
                            </Table.Cell>                            
                            <Table.Cell className='no-left-border'>{person.username}</Table.Cell>
                            <Table.Cell>{person.emailAddress}</Table.Cell>
                            <Table.Cell>{person.role ? person.role.name : null } </Table.Cell>
                            <Table.Cell>
                                
                                {
                                    person.status ? "Active": 
                                    AuthenticationService.hasPermission('RESEND_PASSWORD') &&
                                    <List.Header as='a' className='ut-pointer' onClick = {() => this.resendActivationEmail(person.username)}>
                                        Resend Link
                                    </List.Header>
                                }   
                            </Table.Cell>
                            <Table.Cell>
                                {
                                    person.status &&
                                    <Checkbox toggle 
                                        defaultChecked={person.is_enabled}
                                        onChange={() => this.updateEnable(person.id, person.is_enabled)}
                                        id = {person.id}                               
                                    />
                                }
                            </Table.Cell>
                                <Table.Cell textAlign='center'>    
                                    {
                                        AuthenticationService.hasPermission('UPDATE_USER') &&                        
                                        <span><EditUserModal user = {person} recordUpdated = {this.recordUpdated} /> &nbsp;&nbsp;</span>
                                    }
                                    {   
                                        person.is_locked && AuthenticationService.hasPermission('UNLOCK_USER') && 
                                        <Popup trigger={<Icon className='ut-pointer' name='unlock alternate' onClick={() => this.unlockAccount(person.id)} />} content='Unlock Account' inverted /> 
                                    }                          
                                </Table.Cell>
                            
                        </Table.Row>
        })

        return (      
            this.state.loadingMessage ?  
                <div className="ut-loader">
                    <Loader active content={this.state.loadingMessage}/> 
                </div>:  
                this.state.refreshErrorMessage ?  
                <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
                <div>
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />  }
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                    {AuthenticationService.hasPermission('SEARCH_USER') && 
                        <Input type='text' placeholder='Search... (min 3 xters)' action id='search-input' size='large' style ={{ width: 300 }}>
                            <input name={this.state.searchText} value={this.state.searchText} onChange = {this.handleSearchChange} />
                            <Button icon='search' onClick = {this.search} className='tc-blue-btn' disabled={this.state.searchText.length <= 2} />                        
                            {this.state.searched && <Button onClick = {this.clearAction} content='Clear' color='grey' className='left-border' id='clear-search-btn'/> }
                        </Input> 
                    }                       
                    {AuthenticationService.hasPermission('CREATE_USER') &&
                        <Button.Group floated='right'>
                            <NewUserModal newRecord = {this.newRecordInserted} />
                            <Button.Or />
                            <LdapUserModal newRecord = {this.newRecordInserted} />
                        </Button.Group>
                    }
                    {
                        AuthenticationService.hasPermission('READ_ALL_USERS') &&
                        <div style={{ paddingTop: 10 }} >
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='9'>
                                            Showing {this.state.pageNum === 1 ? this.state.pageNumber : this.state.pageNumber*this.state.pageSize - this.state.pageSize+1} - {this.state.pageNumber*this.state.pageSize > this.state.noOfRecords ? this.state.noOfRecords : this.state.pageNumber*this.state.pageSize}  of {this.state.noOfRecords} records </Table.HeaderCell>
                                    </Table.Row>
                                    <Table.Row>
                                        <Table.HeaderCell></Table.HeaderCell>
                                        <Table.HeaderCell>Username </Table.HeaderCell>
                                        <Table.HeaderCell>Email </Table.HeaderCell>
                                        <Table.HeaderCell>Role </Table.HeaderCell>
                                        <Table.HeaderCell>
                                            Active 
                                            <Popup
                                                trigger={<Icon name='question circle' />}
                                                content ='Have user set password for account?'
                                                inverted
                                                position = 'top center'
                                            />
                                        </Table.HeaderCell>
                                        <Table.HeaderCell>
                                            Enable
                                            <Popup
                                                trigger={<Icon name='question circle' />}
                                                content ='Control user access to portal'
                                                inverted
                                                position = 'top center'
                                            />
                                        </Table.HeaderCell>
                                        {(AuthenticationService.hasPermission('UPDATE_USER') || AuthenticationService.hasPermission('UNLOCK_USER') ) &&
                                            <Table.HeaderCell textAlign='center'>Edit </Table.HeaderCell>
                                        }
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>{rows}</Table.Body>
                                <Table.Footer>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='7' textAlign="right">
                                            <Pagination pointing secondary 
                                                activePage={this.state.pageNumber} 
                                                onPageChange={this.handlePaginationChange} 
                                                totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                            />  
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>     
                        </div>
                    }
                </div>                                      
        )
    }
    
}

export default User