import React, { Component } from 'react'

//semantic ui
import { Header, Tab, Grid, Segment } from 'semantic-ui-react'

//custom
import AppWrapper from './../shared/main/app-wrapper.component'
import User from './user/user.component'
import Role from './role/role.component'

import AuthenticationService from './../../services/authentication_service'

class UserManagement extends Component {

    constructor (props) {
        super(props);
        
        this.state = {
            path: props.match.path,
            activeIndex: 0
        }
    }

    componentWillMount = () => {
        if (this.state.path === "/roles") {
            this.setState({ activeIndex: 1})
        } else {
            this.setState({ activeIndex: 0})
        }
    }

    handleTabChange = (e, { activeIndex }) => {
        if (activeIndex === 1) {
            this.props.history.push('/roles');
        } else {
            this.props.history.push('/users');
        }
        this.setState({ activeIndex });
    }
    
    render () {

        let panes = [
            { menuItem: 'Users', render: () => 
                
                <Tab.Pane attached={false} id='user-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_USERS') && <User /> }
                </Tab.Pane> 
                
            },
            { menuItem: 'Roles', render: () => 

                <Tab.Pane attached={false} id='role-tab'>
                    {AuthenticationService.hasPermission('READ_ALL_ROLES') && <Role /> }
                </Tab.Pane> 

            }
        ]

        return (            
            <AppWrapper> 
                <Header as='h3'>{this.state.activeIndex === 0 ? "User ": "Role "} Management</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment>
                                <Tab menu={{ secondary: true, pointing: true }} 
                                    panes={panes}                                     
                                    activeIndex={this.state.activeIndex} 
                                    onTabChange={this.handleTabChange} className="user-mgt-tab"/>
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default UserManagement