import React, { Component } from 'react'


//semantic ui
import { Modal, Button, Form, Input, Icon, Popup } from 'semantic-ui-react'

//custom
import ErrorMessage from './../../shared/message/error.component'
import SuccessMessage from './../../shared/message/success.component'

//api
import RoleService from './../../../services/role_service'
import UtilService from './../../../services/util_service'

class EditRoleModal extends Component {

    constructor() {
        super();
          
 		this.state = {
            modalOpen: false,
            isUpdating: false,
            formData: {
                name: '',
                description: '',
                id: ''
            },
            errorMessage: null,
            successMessage: null
        };

    }

    handleClose = () => {
        this.clearForm()
        this.setState({ modalOpen: false })
    }

    openEditRoleModal = () => {
        let role = this.props.role;
        
        this.setState ({
            formData: {
                name: role.name,
                description: role.description,
                id: role.id
            },
            modalOpen : true 
        })
        
    }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    validForm = () => {
        this.setState ({ errorMessage: null })
        
        let name = this.state.formData.name;

        if ( name == null || name === "") {            
            this.setState ({ errorMessage: "Role name required" })
            return false;
        }

        if (!UtilService.validAlphaNumericRegExp(name)) {            
            this.setState ({ errorMessage: "Role name must be at least 5 characters and cannot start with number or special character" })
            return false;
        }
        
        return true;
    }

    updateRole = () => {
        if (this.validForm()) {
            this.setState({ isUpdating: true });
            let params = {
                name: this.state.formData.name,
                description: this.state.formData.description,
                id: this.state.formData.id
            }
            
            RoleService.update(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isUpdating: false })
                    this.setState ({ errorMessage: response.description})                
                } else {
                    this.setState({ isUpdating: false, modalOpen: false })
                    this.props.recordUpdated();
                }
            })
        }
    }

    clearForm = () => {
        this.setState({ 
            formData: {
                name : '',
                description: '',
                id: ''      
            }
        });    
    }

    render () {       

        return (
            <span>
                <Popup trigger={<Icon id={'edit-role-'+this.props.role.name} color="teal" className='ut-pointer' name='edit' onClick={this.openEditRoleModal} />} 
                    position='left center' content='Update role details' inverted /> 
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Edit Role </Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} />}
                        <Form loading={this.state.isLoadingForm}>                            
                            <Form.Field control={Input} label='Role Name' placeholder = 'Role name' 
                                name = 'name'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.name}
                            />     
                            <Form.Field control={Input} label='Description' placeholder = 'Description' 
                                name = 'description'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.description}
                            />
                        </Form>                   
                    </Modal.Content>
                    <Modal.Actions>
                        <Button icon="users" labelPosition='left' onClick={this.updateRole} positive 
                            content='Update role' loading={this.state.isUpdating}/>
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </span>
        )
    }
}

export default EditRoleModal