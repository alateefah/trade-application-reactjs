import React, { Component } from 'react'

//semantic ui
import { Header, Table, Loader } from 'semantic-ui-react'

//custom
import NewRoleModal from './new-role-modal.component'
import EditRoleModal from './edit-role-modal.component'
import RolePermissionsModal from './role-permissions-modal.component'
import SuccessMessage from './../../shared/message/success.component'
import Refresh from './../../shared/refresh.component'


//api
import RoleService from './../../../services/role_service'

import AuthenticationService from './../../../services/authentication_service'

class Role extends Component {

    constructor () {
        super();

        this.state = {
            loading: true,
            roles: []
        }
        
    }

    componentWillMount () {
        this.setState({ loading : true })
        this.getRoles();
    }

    getRoles = () => {                
        RoleService.getAvailableRoles()
        .then(response => {   
            this.setState({ loading : false })
            if (response.code) {
                this.setState ({ errorMessage: response.description })
            }  else {
                this.setState({ roles : response })
            }         
            
        }) 
    }

    recordUpdated = () => {
        this.setState({ loading : true })
        this.getRoles();
        this.setState ({ successMessage: "Role updated successfully." })
        setTimeout(() => {
            this.setState ({ successMessage: null });
        }, 5000);
    }

    refreshAction = () => {
        this.setState({ loading : true })
        this.getRoles();
    }

    render () {
        let rows = this.state.roles.map(role => {
            return  <Table.Row key={role.id} id={"row_"+role.name}>                           
                        <Table.Cell width={5}>{role.name}</Table.Cell>
                        <Table.Cell width={9}>{role.description}</Table.Cell>
                        <Table.Cell textAlign='center' width={2}>
                            {role.name !== 'ROLE_ADMIN' &&  
                            AuthenticationService.hasPermission('UPDATE_ROLE')  && 
                            <span>  
                                <EditRoleModal role = {role} recordUpdated = {this.recordUpdated} /> 
                                &nbsp;&nbsp;&nbsp;&nbsp;
                            </span>
                            }    
                            {AuthenticationService.hasPermission('ASSIGN_PERMISSIONS_TO_ROLE')  && <RolePermissionsModal role = {role}/> }                                                    
                        </Table.Cell>
                    </Table.Row>
        })

        return (                  
            this.state.loading ?  
            <div className="ut-loader"><Loader active content="Loading roles..." /></div>:   
            this.state.errorMessage ?  
                <Refresh message ={this.state.errorMessage} refreshAction={this.refreshAction} /> :      
                <div>                
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                    <Header as='h3' textAlign='right'>
                        {AuthenticationService.hasPermission('CREATE_ROLE') && <NewRoleModal newRecord = { this.getRoles } />}
                    </Header>
                    <Table celled>
                        <Table.Header>
                            <Table.Row>
                                <Table.HeaderCell>Name</Table.HeaderCell>
                                <Table.HeaderCell>Description </Table.HeaderCell>
                                <Table.HeaderCell textAlign='center'>Edit </Table.HeaderCell>
                            </Table.Row>
                        </Table.Header>
                        <Table.Body>{rows}</Table.Body>
                    </Table>
                </div>                               
        )
    }
}

export default Role