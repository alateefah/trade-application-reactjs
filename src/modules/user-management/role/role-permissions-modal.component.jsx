import React, { Component } from 'react'
import { Modal, Button, Icon, Popup, Segment } from 'semantic-ui-react'
// import DualListBox from 'react-dual-listbox'
// import 'react-dual-listbox/lib/react-dual-listbox.css'
import RoleService from './../../../services/role_service'
import ErrorMessage from './../../shared/message/error.component'
import ListBox from 'react-listbox'
import 'react-listbox/dist/react-listbox.css'

class RolePermissionsModal extends Component {

    constructor(props) {
        super(props);
          
 		this.state = {
            modalOpen: false,            
            errorMessage: null,
            successMessage: null,
            role: props.role.id,
            permissions: [],
            rolePermissions: [],
            loadingPermissions: false,
            loadingRolePermissions: false,
            assigningPermissions: false
        };

    }

    openRolePermissionsModal = () => {
        this.getPermissions();
        this.getRolePermissions(this.state.role);
        this.setState({ modalOpen : true }) 
        
    }

    handleClose = () => { this.setState({ modalOpen: false })  }

    getRolePermissions = (roleId) => {
        this.setState({ loadingRolePermissions: true })
        RoleService.getRolePermissions({ roleId: roleId })
        .then(response => {   
            if (response.code) {
                this.setState ({ errorMessage: response.description })
            }  else {                
                this.setState ({ rolePermissions : response, loadingRolePermissions: false })
            }  
        }) 
    }

    getPermissions = () => {
        this.setState({ loadingPermissions: true })
        RoleService.getAllPermissions()
        .then(response => {   
            if (response.code) {
                this.setState ({ errorMessage: response.description })
            }  else {
                this.setState({ permissions : response, loadingPermissions: false })                
            }         
           
        }) 
    }

    assignPermissions = (selected) => {
        this.setState ({ errorMessage: null, assigningPermissions: true})
        let params = {
            permissionIds: selected,
            roleId: this.state.role
        }
        RoleService.assignPermissions(params)
        .then(response => {                
            this.getRolePermissions(params.roleId);
            this.setState({ assigningPermissions : false, errorMessage: response.description }) 
        }) 
    }

    render () {       
        const { permissions, rolePermissions } = this.state
        let permissionList = [] , rolePermissionList = [], dualbox;

        if (permissions.length > 0) {
            permissionList = permissions.map(permission => (
                { value: permission.id, label: permission.name }            
            ));
        }

        if (rolePermissions.length > 0) {
            rolePermissions.map(rolePermission => (
                rolePermissionList.push(rolePermission.id)        
            ));
        }

        if (!this.state.loadingPermissions && !this.state.loadingRolePermissions && !this.state.assigningPermissions) {
            dualbox = <ListBox
                        options = {permissionList} 
                        selected={rolePermissionList} 
                        onChange={(selected) =>  this.assignPermissions(selected)}
                        
                    />
        }

        return (
            <span>
                <Popup position = 'right center' trigger={<Icon className='ut-pointer' id={this.props.role.name+'-permissions'} name='key' onClick={this.openRolePermissionsModal} />} content='Manage Permissions' inverted />
                <Modal size='small' open={this.state.modalOpen}>
                    <Modal.Header> Role Permissions for {this.props.role.name} </Modal.Header>
                    <Modal.Content>   
                        {this.state.errorMessage &&
                            <ErrorMessage message={this.state.errorMessage} />
                        }
                        <Segment loading = {this.state.loadingPermissions || this.state.loadingRolePermissions || this.state.assigningPermissions} >                                   
                            {dualbox}                                                            
                        </Segment> 
                    </Modal.Content>
                    <Modal.Actions>
                        <Button negative content='Exit' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </span>
        )
    }
}

export default RolePermissionsModal