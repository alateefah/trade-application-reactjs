import React, { Component } from 'react'
import { Modal, Button, Form, Input } from 'semantic-ui-react'
import ErrorMessage from './../../shared/message/error.component'
import SuccessMessage from './../../shared/message/success.component'
import RoleService from './../../../services/role_service'
import UtilService from './../../../services/util_service'

class NewRoleModal extends Component {

    constructor() {
        super();
          
 		this.state = {
            modalOpen: false,
            formData: {
                name: '',
                description: ''
            },
            errorMessage: null,
            successMessage: null,
            isCreating: null
        };
        
        this.openNewRoleModal = this.openNewRoleModal.bind(this)
        this.createRole = this.createRole.bind(this)
        this.handleInputChange = this.handleInputChange.bind(this)
        this.clearForm = this.clearForm.bind(this)
    }

    handleClose = () => {
        this.clearForm()
        this.setState({ modalOpen: false })
    }

    openNewRoleModal () {
        this.setState({ modalOpen : true })
    }

    handleInputChange(event) { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    validForm () {
        this.setState ({ errorMessage: null })

        let name = this.state.formData.name;

        if ( name == null || name === "") {            
            this.setState ({ errorMessage: "Role name required" })
            return false;
        }

        if (!UtilService.validAlphaNumericRegExp(name)) {            
            this.setState ({ errorMessage: "Role name must be at least 5 characters and cannot start with number or special character" })
            return false;
        }

        return true;
    }

    createRole () {
        if (this.validForm()) {
            this.setState({ isCreating : true });
            let params = {
                name: this.state.formData.name,
                description: this.state.formData.description
            }            
            RoleService.create(params)
            .then( response => {
                if (response.code) {
                    this.setState({ isCreating : false, errorMessage: response.description})                
                } else {
                    this.clearForm()
                    this.setState({ isCreating : false });
                    this.props.newRecord();
                    this.setState ({ successMessage: "Role Added!" })
                    setTimeout(() => { this.setState ({ successMessage: null }); }, 5000);
                }
            })
        }
    }

    clearForm = () => {
        this.setState({ 
            formData: {
                name : '',
                description: ''            
            }
        });    
    }

    render () {      

        return (
            <div>
                <Button id='create-role' icon='users' color='grey' content='Create' size='large' className="ut-blue-btn" onClick={this.openNewRoleModal} />
                <Modal size='tiny' open={this.state.modalOpen}>
                    <Modal.Header> Create Role </Modal.Header>
                    <Modal.Content>                    
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                        {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                        <Form>                            
                            <Form.Field control={Input} label='Role Name' placeholder = 'Role name' 
                                name = 'name'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.name}
                                id = 'create-role-form-name'
                            />     
                            <Form.Field control={Input} label='Description' placeholder = 'Description' 
                                name = 'description'
                                onChange = {this.handleInputChange}
                                value = {this.state.formData.description}
                                id = 'create-role-form-description'
                            />
                        </Form>                   
                    </Modal.Content>
                    <Modal.Actions>
                        <Button  icon="tasks" labelPosition='left' onClick={ this.createRole } positive 
                            content='Create role' loading = {this.state.isCreating} />
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions>                
                </Modal>
            </div>
        )
    }
}

export default NewRoleModal