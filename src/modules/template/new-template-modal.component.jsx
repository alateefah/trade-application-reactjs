import React, { Component } from 'react'
import { Modal, Button, Form, Input } from 'semantic-ui-react'
import { Editor } from '@tinymce/tinymce-react'
import ErrorMessage  from './../shared/message/error.component.jsx'
import CorrespondenceService from './../../services/correspondence_service'
import SharedService from './../../services/shared_service'

class NewTemplateModal extends Component {

    constructor () {
        super();
          
 		this.state = {
            modalOpen: false,
            letterInitialTemplate: '<div style="padding: 30px; 10px;"> <div style="text-align: right; "><b>TS/FM/UBA/<span class="correspondence-value" contenteditable="false">$data.getReferenceDate()</span>/<span class="correspondence-value" contenteditable="false">$data.getLetterNumber()</span></b></div> <div><span class="correspondence-value" contenteditable="false">$data.getDate()</span></div> <br/> <div>The Director, </div> <div>Trade & Exchange Department, </div> <div>Central Bank of Nigeria, </div> <div>Abuja. </div> <br/> <div>Dear Sir, </div> <br/> <div><b><u>SUBMISSION OF FORMS M FOR ITEMS NOT VALID FOR FOREIGN EXCHANGE</u></b></div> <br/> <div>In line with your letter with reference number TED/FEM/FPC/GEN/01/<span class="correspondence-value" contenteditable="false">$data.getReferenceNumber()</span> dated <span class="correspondence-value" contenteditable="false">$data.getApplicationDate()</span> on Not Valid for Foreign Exchange Forms M, we hereby submit the under listed Form M with the source of funds and eviend of source if funds for your approval: </div> <br/> <!-- #set( $count = 1 ) --> <table align="center" border=1 cellspacing=0 cellpadding=7 width="700px" style="font-size: 15px;"> <tr style="font-size: 13px;"> <th width="40px" style="text-align: center;" class="hidden">SN</th> <th width="200px" style="text-align: left;">CUSTOMER NAME</th> <th width="130px" style="text-align: left;">MF NO.</th> <th width="100px" style="text-align: left;">AMT </th> <th width="50px" style="text-align: left;">FCY </th> <th width="200px" style="text-align: left;">SOURCE OF FUND</th> </tr> <!-- #foreach( $correspondenceData in $data.getCorrespondenceDataList() ) --> <tr > <td style="text-align: center;" class="hidden">$count</td> <td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getBeneficiaryName()</span></td> <td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormNumber()</span></td> <td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormGoodsDetails().getTotalCfValue()</span></td> <td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormGoodsDetails().getCurrencyCode()</span></td> <td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormGoodsDetails().getSourceOfFundsName()</span></td> </tr> <!-- #set( $count = $count + 1 )#end --> </table> <br/> <div>Attached are copies of Form M, Proforma Invoice, Insurance certificate, 2017 Quota Allocation approval for importation of frozen fish, Customer’s letter and statement of account.</div> <br/> <div>Kindly approve to enable our customer commence importation.</div> <br/> <div>Yours faithfully,</div> <br />FOR: <b>UNITED BANK FOR AFRICA PLC</b><br/><br/><br/><br/><br/><br/><br/> <div><span><b>AUTHORIZED SIGNATORY</b></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span><b>AUTHORIZED SIGNATORY</b></span></div> </div>',
            templateData: "",
            formData: {
                templateName: "",
                templateDescription: "",
                type: "LETTER",
                purpose: ""               
            },
            loadingMessage: false,
            correspondenceParams: [],
            errorMessage: null,
            templatePurposes: []
        };
        
    }

    openNewTemplateModal = () => {  
        this.setState({ loadingMessage : true, templateData: this.state.letterInitialTemplate })
        this.getCorrespondenceParameter();
        this.getTemplatePurpose();
        this.setState({ modalOpen : true });  
    }

    handleEditorChange = (e) => {   this.setState ({ templateData: e.target.getContent() })  }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
       
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    create = () => {        
        if (this.validFields()) {
            this.setState({ loadingMessage: true })
            let params = {
                name: this.state.formData.templateName,
                purpose: this.state.formData.purpose,
                type: this.state.formData.type,
                description: this.state.formData.templateDescription,
                templateData: this.state.templateData
            }
            CorrespondenceService.createTemplate(params) 
            .then(response => {  
                this.setState({ loadingMessage: null })
                if (response.code) {
                    this.setState({ errorMessage: response.description})
                } else {
                    this.handleClose();
                    this.props.callbackForSuccessfulTemplateCreation();
                    
                }
            })
           
        }        
    }

    handleClose = () => { 
        this.setState({ 
            errorMessage: null,
            modalOpen: false, 
            formData: {
                templateName: "",
                templateDescription: "",
                type: "LETTER",
                purpose: ""               
            }
        }) 
    }

    getCorrespondenceParameter = () => {  
        SharedService.getAppParameter("CORRESPONDENCE_TEMPLATE_PLACE_HOLDERS")
        .then(response => {   
            this.setState({ loadingMessage : false })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState ({ correspondenceParams: JSON.parse(response.value.replace(/\\/g, "")) })
            }         
            
        }) 
    }

    handleSelectChange = (e, val) =>  {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    }

    validFields = () => {
        this.setState ({ errorMessage: null });

        if (this.state.formData.templateName == null || this.state.formData.templateName === "") {
            this.setState ({ errorMessage: "Enter template name"})
            return false;
        }
        
        if (this.state.formData.purpose == null || this.state.formData.purpose === "") {
            this.setState ({ errorMessage: "Select template purpose"})
            return false;
        }

        if (this.state.formData.templateDescription == null || this.state.formData.templateDescription === "") {
            this.setState ({ errorMessage: "Enter template description"})
            return false;
        }

        if (this.state.templateData == null || this.state.templateData === "") {
            this.setState ({ errorMessage: "Template required"})
            return false;
        }

        return true;       

    }    

    getTemplatePurpose = () => {
        SharedService.getAppParameter("TEMPLATE_PURPOSE_LIST")
        .then(response => {   
            this.setState({ loadingMessage : false })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState ({ templatePurposes: JSON.parse(response.value.replace(/\\/g, "")) })
            }         
            
        })
    }

    render () {    
        let { correspondenceParams, templatePurposes } = this.state
        let purposes, letterEditor;

        if (templatePurposes) {
            purposes = templatePurposes.map(templatePurpose => (
                { key: templatePurpose.value, text: templatePurpose.key, value: templatePurpose.value }            
            ));
        }
        
        if (correspondenceParams.length) {
            
            letterEditor = <Editor                                    
                initialValue= {this.state.letterInitialTemplate}
                init = {{
                    branding: false,
                    plugins: 'image table, code',
                    menu: { 
                        edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
                        format: {title: 'Format', items: 'bold italic underline superscript subscript | removeformat'},
                        table: {title: 'Table', items: 'customtable | deletetable | column'}
                    },
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code | correspondenceItems | customTable | code',
                    height: 350,
                    table_toolbar: "",
                    table_toolbar_enabled: false,
                    content_style: '.mce-content-body {font-size:12px} .correspondence-value {font-family: monospace; font-size: 12px;  color: maroon; background-color: #ececec;} .hidden {display: none;}',
                    font_formats: 'font-family: Calibri, Candara, Segoe, Optima, Arial, sans-serif',
                    setup: function (editor) {             
                        editor.addButton('correspondenceItems', {
                            type: 'listbox',
                            text: 'Correspondence Items',
                            icon: false,
                            onselect: function (e) {                                
                                editor.insertContent("<span class='correspondence-value' contenteditable='false'>"+this.value()+"</span>");
                                this.value('');
                            },
                            values: correspondenceParams
                        })
                        editor.addButton('customTable', {
                            text: 'New Table',
                            icon: false,
                            onclick: function (e) {    
                                editor.insertContent('<br/> <!-- #set( $count = 1 ) --><table align="center" border=1 cellspacing=0 cellpadding=7 width="750px" style="margin-left: -15px; font-size: 15px;">	<tr style="font-size: 13px;">		<th width="40px" style="text-align: center;" class="hidden">SN</th>		<th width="200px" style="text-align: left;">CUSTOMER NAME</th>		<th width="130px" style="text-align: left;">MF NO.</th>		<th width="100px" style="text-align: left;">AMT </th>		<th width="50px" style="text-align: left;">FCY </th><th width="200px" style="text-align: left;">SOURCE OF FUND</th>	</tr>	<!-- #foreach( $correspondenceData in $data.getCorrespondenceDataList() ) -->	<tr >	<td style="text-align: center;" class="hidden">$count</td>	<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getBeneficiaryName()</span></td><td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormNumber()</span></td>	<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getTotalCfValue()</span></td>		<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getCurrencyCode()</span></td>		<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getSourceOfFundsName()</span></td>	</tr>	<!-- #set( $count = $count + 1 )#end --></table><br/>');
                            }
                        })
                    }
                }}
                onChange={this.handleEditorChange} />    
                
        }

        return (             
            <div>
                <Button icon='book' id='add-template-button' content='Create Template' floated="right" size='large' color = "grey" onClick={this.openNewTemplateModal} />
                <Modal size='large' open={this.state.modalOpen} onClose={this.handleClose}>
                    <Modal.Header> Create Template </Modal.Header>
                    <Modal.Content>  
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  }    
                        <Form loading={this.state.loadingMessage}>  
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='Template Name' placeholder='Template Name' name = 'templateName' onChange = {this.handleInputChange} value = {this.state.formData.templateName}  />
                                <Form.Select fluid label='Purpose' options={purposes} placeholder='Template Purpose' name="purpose" onChange={this.handleSelectChange} />
                            </Form.Group>                       
                            <Form.Field control={Input} label='Description' onChange = {this.handleInputChange} value = {this.state.formData.templateDescription} placeholder = 'Template Description' name = 'templateDescription' />    
                            <Form.Field className = "template-editor">
                                {letterEditor}
                            </Form.Field>                         
                        </Form>      
                    </Modal.Content>                    
                    {!this.state.loadingMessage &&
                    <Modal.Actions>
                        <Button  icon="book" labelPosition='left' onClick={this.create} positive content='Create template' />
                        <Button negative content='Cancel' onClick={this.handleClose} />
                    </Modal.Actions> 
                    }               
                </Modal>
            </div>
                
        )
    }
}

export default NewTemplateModal