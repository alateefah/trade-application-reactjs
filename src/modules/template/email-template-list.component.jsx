import React, { Component } from 'react'
import { Table, Pagination, Loader } from 'semantic-ui-react'
import CorrespondenceService from './../../services/correspondence_service'
import AuthenticationService from './../../services/authentication_service.js'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import ErrorMessage from './../shared/message/error.component'
import EditTemplateModal from './edit-template.component'

class EmailTemplateList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loadingMessage: true,
            templates: [],
            errorMessage: null,
            refreshErrorMessage: null,
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0
        }
    }

    componentWillMount () { this.getTemplateLists(); }

    getTemplateLists = () => {   
        this.setState({ templates: [], loadingMessage : "Loading templates..." })      
        let params = {
            pageSize: this.state.pageSize,

            pageNumber: this.state.pageNumber,
            type: "EMAIL"
        }
        CorrespondenceService.getAllTemplates(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ templates: response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => {
        this.setState({ pageNumber: activePage }, () => {
            this.getTemplateLists();
        })
    }

    alertSuccess = (msg) => {
        this.getTemplateLists();
        this.setState({ successMessage: msg })
        setTimeout(() => { 
            this.setState({ successMessage: null })
        }, 5000);
        
    }

    render () {
        const { templates } = this.state;

        let templateRows;
        
        if (templates !== []) 
        {
            templateRows = templates.map(template => (
                <Table.Row key={template.id}>
                    <Table.Cell collapsing>
                    {AuthenticationService.hasPermission('UPDATE_CORRESPONDENCE_TEMPLATES') ?
                        <EditTemplateModal template={template} 
                            callbackForSuccessfulTemplateUpdate = {()=> {this.alertSuccess("Update successful!"); this.getTemplateLists()} }/>:
                        template.name
                    }
                    </Table.Cell>
                    <Table.Cell>{template.description}</Table.Cell>                 
                </Table.Row>           
            ));
        }
        
        return (      
            <span>
                
                {this.state.refreshErrorMessage &&  <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.getTemplateLists} /> }
                {this.state.loadingMessage && <Loader active inline='centered' />}
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                {
                    !this.state.loadingMessage && !this.state.refreshErrorMessage &&
                    <span>
                        <div style={{ paddingTop: 1, clear: 'both' }}></div>
                        <Table celled striped>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5'>Email Templates Repository</Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.HeaderCell>Name</Table.HeaderCell>
                                    <Table.HeaderCell>Description</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{templateRows}</Table.Body>
                            <Table.Footer fullWidth>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5' textAlign="right"> 
                                        <Pagination 
                                            pointing 
                                            secondary 
                                            defaultActivePage={this.state.pageNumber} 
                                            onPageChange={this.handlePaginationChange} 
                                            totalPages={this.state.noOfRecords/this.state.pageSize} 
                                        />
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table> 
                    </span>
                }             
            </span>
                                      
        )
    }
    
}

export default EmailTemplateList