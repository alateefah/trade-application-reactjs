import React, { Component } from 'react'
import { Modal, Button, Form, Input, List } from 'semantic-ui-react'
import { Editor } from '@tinymce/tinymce-react'
import ErrorMessage  from './../shared/message/error.component.jsx'
import CorrespondenceService from './../../services/correspondence_service'
import SharedService from './../../services/shared_service'
import RefreshErrorMessage from './../shared/refresh.component';

class EditTemplateModal extends Component {

    constructor (props) {
        super(props);
          
 		this.state = {
            modalOpen: false,
            templateData: null,
            template: props.template,
            formData: {
                templateName: "",
                templateId: props.template.id,                
                templateDescription: "",
                fileName: props.template.fileName,
                formCategory: props.template.formCategory,   
                type: props.template.type,     
                auditable: props.template.auditableId,       
            },
            loading: false,
            errorMessage: null,
            correspondenceParams: [],
            refreshErrorMessage: null
        };
        
    }

    openEditTemplateModal = () => {  
        this.setState({ loading : true, modalOpen : true })        
        this.getTemplate();
    }

    handleEditorChange = (e) => { this.setState ({ templateData: e.target.getContent() })  }

    handleInputChange = (event) => { 
        
        const target = event.target;
        const value = target.value;
        const name = target.name;
       
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    update = () => {        
        if (this.validFields()) {
            this.setState({ loading: true })
            let params = {
                name: this.state.formData.templateName,
                id: this.state.formData.templateId,
                description: this.state.formData.templateDescription,
                fileName: this.state.formData.fileName,
                templateData: this.state.templateData,
            }
            CorrespondenceService.updateTemplate(params) 
            .then(response => {  
                this.setState({ loading: false })
                if (response.code) {
                    this.setState({ errorMessage: response.description})
                } else {
                    this.handleClose();
                    this.props.callbackForSuccessfulTemplateUpdate();
                    
                }
            })
           
        }        
    }

    handleClose = () => { this.setState({  errorMessage: null, modalOpen: false }) }

    getCorrespondenceParameter = () => {  
        SharedService.getAppParameter("CORRESPONDENCE_TEMPLATE_PLACE_HOLDERS")
        .then(response => {   
            this.setState({ loading : false })            
            if (response.code) {
                // this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState ({ correspondenceParams: JSON.parse(response.value.replace(/\\/g, "")) })
            }         
            
        }) 
    }

    handleSelectChange = (e, val) =>  {
        this.setState({ 
            formData: {
                ...this.state.formData,
                [val.name] : val.value
            }
        });  
    }

    validFields = () => {
        this.setState ({ errorMessage: null });

        if (this.state.formData.templateName == null || this.state.formData.templateName === "") {
            this.setState ({ errorMessage: "Enter template name"})
            return false;
        }
        
        if (this.state.formData.templateDescription == null || this.state.formData.templateDescription === "") {
            this.setState ({ errorMessage: "Enter template description"})
            return false;
        }

        if (this.state.templateData == null || this.state.templateData === "") {
            this.setState ({ errorMessage: "Template required"})
            return false;
        }
        return true;       

    }    

    getTemplate = () => {
        CorrespondenceService.getTemplate(this.state.formData.templateId)
        .then(response => {      
            if (response.code) {
                this.setState({ loading : false, refreshErrorMessage: response.description })
            }  else {                
                this.setState ({ 
                    templateData: response.templateData,
                    formData: {
                        ...this.state.formData,
                        templateName: response.name, 
                        templateDescription: response.description,
                        type: response.type
                    }
                })
                this.getCorrespondenceParameter();
            }         
            
        }) 
    }

    render () {    
        let { correspondenceParams } = this.state
        let editor;
        
        if (correspondenceParams.length && this.state.templateData !== null) {   
            editor = <Editor                                    
                initialValue= {this.state.templateData}
                init = {{
                    branding: false,
                    plugins: 'table, code',                    
                    menu: { 
                        edit: {title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall'},
                        format: {title: 'Format', items: 'bold italic underline superscript subscript | removeformat'},
                        table: {title: 'Table', items: 'customtable | deletetable | column'}
                    },
                    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | correspondenceItems | customTable | code',
                    height: 350,
                    content_style: '.mce-content-body {font-size:12px} .correspondence-value {font-family: monospace; font-size: 12px;  color: maroon; background-color: #ececec;} .hidden {display: none;}',
                    font_formats: 'font-family: Calibri, Candara, Segoe, Optima, Arial, sans-serif',                    
                    setup: function (editor) {             
                        editor.addButton('correspondenceItems', {
                            type: 'listbox',
                            text: 'Correspondence Items',
                            icon: false,
                            onselect: function (e) {                                
                                editor.insertContent("<span class='correspondence-value' contenteditable='false'>"+this.value()+"</span>");
                                this.value('');
                            },
                            values: correspondenceParams
                        })
                        editor.addButton('customTable', {
                            text: 'New Table',
                            icon: false,
                            onclick: function (e) {    
                                editor.insertContent('<br/> <!-- #set( $count = 1 ) --><table align="center" border=1 cellspacing=0 cellpadding=7 width="750px" style="margin-left: -15px; font-size: 15px;">	<tr style="font-size: 13px;">		<th width="40px" style="text-align: center;" class="hidden">SN</th>		<th width="200px" style="text-align: left;">CUSTOMER NAME</th>		<th width="130px" style="text-align: left;">MF NO.</th>		<th width="100px" style="text-align: left;">AMT </th>		<th width="50px" style="text-align: left;">FCY </th><th width="200px" style="text-align: left;">SOURCE OF FUND</th>	</tr>	<!-- #foreach( $correspondenceData in $data.getCorrespondenceDataList() ) -->	<tr >	<td style="text-align: center;" class="hidden">$count</td>	<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getBeneficiaryName()</span></td><td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormNumber()</span></td>	<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getTotalCfValue()</span></td>		<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getCurrencyCode()</span></td>		<td> <span class="correspondence-value" contenteditable="false">$correspondenceData.getFormMGoodsDetails().getSourceOfFundsName()</span></td>	</tr>	<!-- #set( $count = $count + 1 )#end --></table><br/>');
                            }
                        })
                    }
                }}
                onChange={this.handleEditorChange} />   
        }

        return (             
            <span>                 
                <List.Header as='a' className='ut-pointer' onClick={this.openEditTemplateModal}>{this.state.template.name} </List.Header>
                <Modal size='large' open={this.state.modalOpen} onClose={this.handleClose}>
                    <Modal.Header> {this.state.formData.templateName} </Modal.Header>
                    <Modal.Content>  
                        {this.state.refreshErrorMessage && <RefreshErrorMessage message={this.state.refreshErrorMessage} />  }
                        {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} />  }                     
                        {!this.state.refreshErrorMessage &&
                        <Form loading={this.state.loading}>  
                            <Form.Input fluid label='Template Name' placeholder='Template Name' 
                                    name = 'templateName' onChange = {this.handleInputChange} 
                                    readOnly = {this.state.formData.type === "EMAIL"}
                                    value = {this.state.formData.templateName}  
                                />                   
                            <Form.Field control={Input} label='Description' placeholder = 'Template Description' 
                                name = 'templateDescription'  onChange = {this.handleInputChange} value = {this.state.formData.templateDescription} 
                            />    
                            <Form.Field className = "template-editor">
                                {editor}
                            </Form.Field>                         
                        </Form>      
                        }           
                    </Modal.Content>
                    
                    {!this.state.loading && !this.state.refreshErrorMessage &&
                    <Modal.Actions>
                        <Button  icon="book" labelPosition='left' onClick={this.update} positive content='Update template' />
                        <Button negative content='Cancel' onClick={this.handleClose}/>
                    </Modal.Actions> 
                    }               
                </Modal>
            </span>                
        )
    }
}

export default EditTemplateModal