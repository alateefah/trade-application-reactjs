import React, { Component } from 'react'
import { Table, Icon, Pagination, Loader, Popup } from 'semantic-ui-react'
import CorrespondenceService from './../../services/correspondence_service'
import NewTemplateModal from './new-template-modal.component'
import AuthenticationService from './../../services/authentication_service.js'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import ErrorMessage from './../shared/message/error.component'
import EditTemplateModal from './edit-template.component'
import ConfirmAction from './../shared/confirm-action.component'

class LetterTemplateList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loadingMessage: true,
            templates: [],
            errorMessage: null,
            refreshErrorMessage: null,
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            open: false,
            confirmMessage: null,
            templateId: null
        }
    }

    componentWillMount () { this.getTemplateLists(); }

    getTemplateLists = () => {   
        this.setState({ templates: [], loadingMessage : "Loading templates..." })      
        let params = {
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber,
            type: "LETTER"
        }
        CorrespondenceService.getAllTemplates(params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ templates: response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber })
            }         
            
        }) 
    }

    handlePaginationChange = (e, { activePage }) => {
        this.setState({ pageNumber: activePage }, () => {
            this.getTemplateLists();
        })
    }

    alertSuccess = (msg) => {
        this.getTemplateLists();
        this.setState({ successMessage: msg })
        setTimeout(() => { 
            this.setState({ successMessage: null })
        }, 5000);
        
    }

    alertError = (msg) => {
        console.log(msg)
        this.setState({ errorMessage: msg })
        setTimeout(() => { 
            this.setState({ errorMessage: null })
        }, 5000);
        
    }

    delete = () => {
        this.closeConfirm();
        this.setState({ loadingMessage : "Deleting..." })
        CorrespondenceService.delete(this.state.templateId)
        .then(response => { 
            this.setState({ loadingMessage : null }); 
            if (response.code) {                  
                this.alertError(response.description);            
            } else {                                        
                this.alertSuccess("Template successfully deleted. ")
            }
        }) 
    }

    closeConfirm = () => { this.setState({ open : false })  }

    openConfirm = (template) => { 
        this.setState({            
            confirmMessage: "Are you sure you want to delete template: "+ template.name,
            templateId: template.id,
            open : true
        })
    } 

    render () {
        const { templates } = this.state;

        let templateRows;
        
        if (templates !== []) 
        {
            templateRows = templates.map(template => (
                <Table.Row key={template.id}>
                    <Table.Cell collapsing>
                        {AuthenticationService.hasPermission('UPDATE_CORRESPONDENCE_TEMPLATES') ?
                        <EditTemplateModal template={template} 
                            callbackForSuccessfulTemplateUpdate = {()=>{this.alertSuccess("Update successful!"); this.getTemplateLists()}}/>:
                        template.name
                        }
                    </Table.Cell>
                    <Table.Cell>{template.description}</Table.Cell>
                    <Table.Cell collapsing>{template.purpose}</Table.Cell>
                    <Table.Cell collapsing textAlign='center'>   
                        <ConfirmAction 
                            header="Confirm delete ?" 
                            confirmAction = {()=>this.delete()} 
                            closeAction = {()=>this.closeConfirm()} 
                            content = {this.state.confirmMessage} 
                            trigger = {
                                <Popup inverted position='right center' content='Delete Config ?' size='tiny'  
                                    trigger={<Icon id={'confirm-delete-'+template.id} name = "trash" color = "red" link onClick={()=>this.openConfirm(template)} /> } />                                                                                                           
                            }
                            open = {this.state.open}
                        />                     
                    </Table.Cell>
                </Table.Row>           
            ));
        }
        
        return (      
            <span>
                
                {this.state.refreshErrorMessage &&  <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.getTemplateLists} /> }
                {this.state.loadingMessage && <Loader active inline='centered' />}
                {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }
                {
                    !this.state.loadingMessage && !this.state.refreshErrorMessage &&
                    <span>
                        {AuthenticationService.hasPermission('CREATE_CORRESPONDENCE_TEMPLATE') && 
                          <NewTemplateModal callbackForSuccessfulTemplateCreation={() => this.alertSuccess("Template successfully created")} />
                        }
                        <div style={{ paddingTop: 1, clear: 'both' }}></div>
                        <Table celled striped>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5'>Letter Templates Repository</Table.HeaderCell>
                                </Table.Row>
                                <Table.Row>
                                    <Table.HeaderCell>Name</Table.HeaderCell>
                                    <Table.HeaderCell>Description</Table.HeaderCell>
                                    <Table.HeaderCell>purpose</Table.HeaderCell>
                                    <Table.HeaderCell collapsing>Delete</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>{templateRows}</Table.Body>
                            <Table.Footer fullWidth>
                                <Table.Row>
                                    <Table.HeaderCell colSpan='5' textAlign="right"> 
                                        <Pagination 
                                            pointing 
                                            secondary 
                                            defaultActivePage={this.state.pageNumber} 
                                            onPageChange={this.handlePaginationChange} 
                                            totalPages={this.state.noOfRecords/this.state.pageSize} 
                                        />
                                    </Table.HeaderCell>
                                </Table.Row>
                            </Table.Footer>
                        </Table> 
                    </span>
                }             
            </span>
                                      
        )
    }
    
}

export default LetterTemplateList