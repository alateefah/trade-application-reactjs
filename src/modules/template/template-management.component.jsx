import React, { Component } from 'react'
import { Header, Tab, Grid, Segment } from 'semantic-ui-react'
import AppWrapper from './../shared/main/app-wrapper.component'
import LetterTemplateList from './letter-template-list.component'
import EmailTemplateList from './email-template-list.component'
import AuthenticationService from './../../services/authentication_service'

class TemplateManagement extends Component {
    
    constructor (props) {
        super(props);
        
        this.state = {
            tab: props.location.pathname,
            activeIndex: 0
        }
    }

    componentWillMount = () => {
        const {tab} = this.state;

        if (tab === "/templates/email") {
            this.setState({ activeIndex: 1})
        } else if (tab === "/templates/letter") {
            this.setState({ activeIndex: 0})
        } 
    }

    handleTabChange = (e, { activeIndex }) => {
        if (activeIndex === 0) {
            this.props.history.push('/templates/letter');
        } else if (activeIndex === 1) {
            this.props.history.push('/templates/email');
        }
        this.setState({ activeIndex });
    }

    render () {

        let panes = [
            { menuItem: 'Letters', render: () => 
                
                <Tab.Pane attached={false} id='letter-tab'>
                    <LetterTemplateList /> 
                </Tab.Pane> 
                
            },
            { menuItem: 'Emails', render: () => 

                <Tab.Pane attached={false} id='email-tab'>
                    <EmailTemplateList /> 
                </Tab.Pane> 

            }
        ]

        return (            
            <AppWrapper> 
                <Header as='h3'>Template Management</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column width={16}>
                            <Segment>
                                {AuthenticationService.hasPermission('CREATE_CORRESPONDENCE_TEMPLATE') && 
                                    <Tab 
                                        menu={{ secondary: true, pointing: true }} 
                                        panes={panes} 
                                        className="user-mgt-tab"
                                        activeIndex={this.state.activeIndex} 
                                        onTabChange={this.handleTabChange}
                                    />
                                }
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default TemplateManagement