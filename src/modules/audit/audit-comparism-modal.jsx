import React, { Component } from 'react'
import { Modal, List, Loader, Dimmer, Segment, Label, Message} from 'semantic-ui-react'

import AuditService from './../../services/audit_service'

// import JsonDiffReact from 'jsondiffpatch-for-react';
import Diff from './../shared/vendor/react-diff';
import moment from 'moment'

class AuditComparismModal extends Component {

    constructor (props) {
        super(props);
        this.state = {
            id: props.audit.id,
            resource: props.audit.resource,
            auditableId: props.audit.auditableId,
            loading : true,
            result: null,
            pageSize: 10,
            pageNumber: 0,
            left: null,
            right: null,
            title: props.audit.title
        }
    }
    
    getComparism = () => {        
        let params = {
            auditableId: this.state.auditableId,
            resource: this.state.resource,
            id: this.state.id,
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber
        }
        AuditService.compare(params)
            .then(response => {         
                if (response.code) {
                    this.setState ({ refreshErrorMessage: response.description })
                }  else {
                    this.setState({ result: response.list, left: response.list[0], right: response.list[1] ? response.list[1]: null })
                    
                }
                this.setState({ loading : false })
        })     
    }

    render () {
        return (            
            <Modal trigger={<List.Header as='a' className='ut-pointer' onClick={this.getComparism}>view </List.Header>} size='large' closeIcon>
                <Modal.Header>{this.state.title}</Modal.Header>
                <Modal.Content>
                    {this.state.loading ?
                        <Dimmer active inverted>
                            <Loader inverted content='Loading' />
                        </Dimmer>:
                        <Modal.Description>
                            <Segment.Group>
                                <Segment.Group horizontal>
                                    <Segment>
                                        <Label color='green' ribbon>Current</Label>
                                        {this.state.result !== null &&
                                        <Message>
                                            {this.state.title === "User logout" &&
                                            <Message.Header>{this.state.left.detail}</Message.Header>}
                                            <p>
                                                {moment(this.state.left.createdDate).format("MMM Do YYYY, h:mm:ss A")}
                                                <span className="actor-name"> {this.state.left.actor} </span>
                                            </p>
                                        </Message>}
                                    </Segment>
                                    {this.state.right !== null &&
                                    <Segment>
                                        <Label color='red' ribbon='right'>Previous</Label>
                                        {this.state.result !== null &&
                                        <Message>
                                            {this.state.title === "User logout"  &&
                                            <Message.Header>{this.state.right.detail}</Message.Header>}
                                            <p>
                                                {moment(this.state.right.createdDate).format("MMM Do YYYY, h:mm:ss A")}
                                                <span className="actor-name"> {this.state.right.actor} </span>
                                            </p>
                                        </Message>}                                            
                                    </Segment>  }                               
                                </Segment.Group>
                                <Segment>
                                    <pre>
                                        <Diff type="json" inputA={this.state.right ? this.state.right.auditable: ""} inputB={this.state.left.auditable} />
                                    </pre>
                                </Segment>
                            </Segment.Group>
                        </Modal.Description>
                    }
                </Modal.Content>
            </Modal>
           
        )       
        
    }
}

export default AuditComparismModal