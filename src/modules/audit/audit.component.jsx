import React, { Component } from 'react'

//semantic ui
import { Header, Grid, Segment } from 'semantic-ui-react'

//custom
import AppWrapper from './../shared/main/app-wrapper.component'
import AuditList from './audit-list.component'

class Audit extends Component {
    render () {
        return (            
            <AppWrapper> 
                <Header as='h3'>Audit</Header>
                <Grid>
                    <Grid.Row>
                        <Grid.Column>
                            <Segment padded style={{ paddingBottom: 60 }}>                                
                                <AuditList />
                            </Segment>
                        </Grid.Column>                    
                    </Grid.Row>
                </Grid>  
            </AppWrapper>            
        )
    }
}

export default Audit