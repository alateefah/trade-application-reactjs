import React, { Component } from 'react'
import { Table, Loader, Pagination } from 'semantic-ui-react'
import moment from 'moment'
import AuditSearch from './audit-search.component'
import AuditComparismModal from './audit-comparism-modal'
import Refresh from './../shared/refresh.component'
import SuccessMessage from './../shared/message/success.component'
import NoSearchResult from './../shared/no-search-result.component'
import ErrorMessage from './../shared/message/error.component'
import AuthenticationService from './../../services/authentication_service'
import AuditService from './../../services/audit_service'

class AuditList extends Component {

    constructor (props) {        
        super(props);

        this.state = {
            loading: true,
            audits: [],
            errorMessage: null,
            refreshErrorMessage: null,
            loadingMessage: "Loading audit...",
            pageNumber: 1,
            pageSize: 10,
            noOfRecords: 0,
            searched: false,
            searching: false,
            searchingMessage: "Searching ...",
            search: "",
            startDate: "",
            endDate: "",
        }
    }

    componentWillMount () {
        this.getAudits();
    }

    clearMessages = () => {
        this.setState ({ refreshErrorMessage: false, searched: false})
    }

    getAudits = () => {  
        this.clearMessages(); 
        let params = {
            pageNumber: this.state.pageNumber,
            pageSize: this.state.pageSize
        }
        AuditService.get (params)
        .then(response => {   
            this.setState({ loadingMessage : null })
            if (response.code) {
                this.setState ({ refreshErrorMessage: response.description })
            }  else {
                this.setState({ audits : response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber})
            }         
            
        }) 
    }
    
    refreshAction = () => {
        this.setState({ loadingMessage : this.state.loadingMessage })
        this.getAudits();
    }

    handlePageChange = (e, {activePage} ) => { 
        
        this.setState({ pageNumber : activePage }, () => {              
            if (this.state.searched) {
                this.searchAudit();
            } else {
                this.setState({ loadingMessage : "Loading..." })
                this.getAudits()
            }
        });          
    }   

    search = (data) => {
        this.setState ({
            search: data.search,
            startDate: data.startDate.format('YYYY-MM-DD') + 'T00:00:00.000Z', //data.startDate.toISOString(),
            endDate: data.endDate.format('YYYY-MM-DD') + 'T23:59:59.000Z', //data.endDate.toISOString(),
            pageNumber: 1
        }, () => {
            this.searchAudit();
        })
        
    }

    searchAudit = () => {         
        this.setState({ searching : true })
        let params = {
            search:this.state.search,
            startDate: this.state.startDate,
            endDate: this.state.endDate,
            pageSize: this.state.pageSize,
            pageNumber: this.state.pageNumber
        }
        AuditService.search (params)
        .then(response => {   
            console.log(response)
            this.setState({ searching : false})                
            if (response.code) {
                this.setState ({ errorMessage: response.description })
            }  else {
                this.setState({ audits : response.list, noOfRecords: response.noOfRecords, 
                    pageSize: response.currentPageSize, pageNumber: response.currentPageNumber, searched: true })
            }         
            
        }) 
        
    }
      
    clearSearch = () => {
        this.setState({ pageNumber: 1, loadingMessage : "Searching" }, () => {
            this.getAudits();
        })
        
    }

    render () {
        let rows = this.state.audits.length <= 0 ?
            <NoSearchResult colSpan={3} /> :
            this.state.audits.map(audit => {
                return  <Table.Row key={audit.id}>
                            <Table.Cell>{audit.actor}</Table.Cell>
                            <Table.Cell>{audit.auditableAction}</Table.Cell>
                            <Table.Cell>{audit.detail}</Table.Cell>
                            <Table.Cell>
                                {moment(audit.createdDate).format("MMM Do YYYY, h:mm:ss A")}                               
                            </Table.Cell> 
                            <Table.Cell>
                                <AuditComparismModal audit={{resource: audit.resource, id:audit.id, auditableId:audit.auditableIdOut, title: audit.detail}} />
                            </Table.Cell>
                        </Table.Row>
        })

        return (      
            this.state.loadingMessage ?  
                <div className="ut-loader">
                    <Loader active content={this.state.loadingMessage}/> 
                </div>:  
                this.state.refreshErrorMessage ?  
                <Refresh message = {this.state.refreshErrorMessage} refreshAction = {this.refreshAction} /> :
                <div>
                    {this.state.successMessage && <SuccessMessage message={this.state.successMessage} /> }
                    {this.state.errorMessage && <ErrorMessage message={this.state.errorMessage} /> }                       
                    {AuthenticationService.hasPermission('GET_ALL_AUDIT_TRAILS') &&
                    <div>       
                        {AuthenticationService.hasPermission('GET_ALL_AUDIT_TRAILS') &&
                            <AuditSearch searchParams = {this.search} searched = {this.state.searched} clearSearch= {this.clearSearch} /> 
                        }    
                        {this.state.searching && <Loader style={{top: 85+'%'}} active content={this.state.searchingMessage}/> }
                        {!this.state.searching &&            
                            <Table celled>
                                <Table.Header>
                                    <Table.Row>                                    
                                        <Table.HeaderCell>Changed by </Table.HeaderCell>
                                        <Table.HeaderCell>Change Type </Table.HeaderCell>
                                        <Table.HeaderCell>Description </Table.HeaderCell>
                                        <Table.HeaderCell>Updated </Table.HeaderCell>
                                        <Table.HeaderCell>More </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Header>
                                <Table.Body>{rows}</Table.Body>
                                <Table.Footer>
                                    <Table.Row>
                                        <Table.HeaderCell colSpan='7' textAlign="right">
                                            <Pagination pointing secondary 
                                                activePage={this.state.pageNumber} 
                                                onPageChange={this.handlePageChange} 
                                                totalPages={Math.ceil(this.state.noOfRecords/this.state.pageSize)} 
                                            />
                                        </Table.HeaderCell>
                                    </Table.Row>
                                </Table.Footer>
                            </Table>
                        }
                    </div>
                    }
                </div>
                                      
        )
    }
    
}


export default AuditList