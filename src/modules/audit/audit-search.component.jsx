import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'react-datepicker/dist/react-datepicker.css';

//semantic ui
import { Form, Segment, Button } from 'semantic-ui-react'

class AuditSearch extends Component {

    constructor (props) {
        super(props);

        this.state = {
            search: "",
            startDate: moment().subtract(1, 'days'),
            endDate: moment()
        }        
        
    }

    handleInputChange = (event) => {         
        const target = event.target;
        const value = target.value;
        const name = target.name;
        
        this.setState({ 
            formData: {
                ...this.state.formData,
                [name] : value
            }
        });          
    }

    clear = () => {
        this.setState ({             
            search: "",
            startDate: moment().subtract(1, 'days'),
            endDate: moment()
        }, () => {
            this.props.clearSearch();
        });
        
    }

    handleInputChange = (event) => { this.setState({ search : event.target.value }); }

    handleChangeStart = (date) => { this.setState ({ startDate: date }); }

    handleChangeEnd = (date) => { this.setState ({ endDate: date });  }

    validSearch = () => {      
        if (!this.validFields()) {
            return true;
        }

        return false;
    }

    validFields = () => {
        return ((this.state.search === null || this.state.search === "")
            && (this.state.startDate === null || this.state.startDate === "")
            && (this.state.endDate === null || this.state.endDate === ""));
    }

    search = () => { 
        if (this.validSearch()) {
            this.props.searchParams(this.state)
        }
    }

    render () {
        return (      
           <span>                
                <Segment padded='very' className='search-wrap'>
                    <Form>
                        <Form.Group>
                            <Form.Input label='Keyword' placeholder='Type in keywords' width={4} 
                                name = 'searchParam' onChange = {this.handleInputChange}  value = {this.state.search}
                                id = "audit-search-search-param" 
                            />     
                            <Form.Field width={4}> 
                                <label>Start Date</label>               
                                <DatePicker                                
                                    placeholderText='Start Date'
                                    dateFormat="YYYY-MM-DD"
                                    selected = {this.state.startDate}
                                    selectsStart
                                    startDate = {this.state.startDate}
                                    endDate = {this.state.endDate}
                                    onChange = {this.handleChangeStart}     
                                    id = "audit-search-start-date"                               
                                />
                            </Form.Field>
                            <Form.Field width={4}> 
                                <label>End Date </label>               
                                <DatePicker                                
                                    placeholderText='End Date'
                                    dateFormat="YYYY-MM-DD"
                                    selected={this.state.endDate}
                                    selectsEnd
                                    startDate={this.state.startDate}
                                    endDate={this.state.endDate}
                                    onChange={this.handleChangeEnd}
                                    id = "audit-search-end-date" 
                                />
                            </Form.Field>  
                            <Form.Field>
                                <label>&nbsp;</label>
                                <Button type='submit' color="red" onClick={() => this.search()} disabled={this.validFields()} content="Search" />
                                {this.props.searched && <Button color="grey" onClick={() => {this.clear()} }>Clear</Button> }
                                
                            </Form.Field>                         
                        </Form.Group>                        
                    </Form>
                </Segment>
           </span>
                                      
        )
    }
    
}


export default AuditSearch