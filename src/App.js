import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';

class App extends Component {
  render() {
    return (  
      <div>
        { this.props.children }
      </div>
    );
  }
}

export default App;
