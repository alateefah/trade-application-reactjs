import request from './request_service';
import UrlService from './url_service';

class BankService {

    static getBranches (params) {
        let options = Object();
        options.url = UrlService.GET_BRANCHES;
        options.params = null;
        options.authenticated = true;
        options.headers = {
            'pagesize': params.pageSize,
            'pagenumber': params.pageNumber
        }

        return request.get(options);
    }   

    static filterBranches (params) {
        let options = Object();
        options.url = UrlService.FILTER_BRANCHES;
        options.params = null;
        options.authenticated = true;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber,
            'branchName': params.searchText
        }

        return request.get(options);
    }   

    static createBranch (params) {
        let options = Object();
        options.url = UrlService.BRANCH;
        options.params = params;
        options.authenticated = true;

        return request.post(options);
    }

    static updateBranch (params) {
        let options = Object();
        options.url = UrlService.BRANCH+"/"+params.id;
        options.params = params;
        options.authenticated = true;

        return request.put(options);
    }

    static deleteBranch (branchId) {
        let options = Object();
        options.url = UrlService.BRANCH+"/"+branchId;
        options.params = null;
        options.authenticated = true;

        return request.delete(options);
    }
}

export default BankService;