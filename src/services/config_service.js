import request from './request_service';
import UrlService from './url_service';

class ConfigService {

    static getAll (params) {
        let options = Object();

        options.url = UrlService.GET_CONFIGS;
        options.params = null;
        options.authenticated = true;         
        options.headers = {
            pageSize: params.pageSize,
            pageNumber: params.pageNumber
        }

        return request.get(options);
    }

    static create (params) {        
        let options = Object();

        options.url = UrlService.CONFIG;
        options.params = params;
        options.authenticated = true;

        return request.post(options);
    }

    static update (params) {        
        let options = Object();

        options.url = UrlService.CONFIG+"/"+params.id;
        options.params = params;
        options.authenticated = true;

        return request.put(options);
    }

    static delete (params) {        
        let options = Object();

        options.url = UrlService.CONFIG + "/" + params.key;
        options.params = null;
        options.authenticated = true;

        return request.delete(options);
    }
    
    static search (params) {
        let options = Object();
        options.url = UrlService.CONFIG_SEARCH+"/"+params.searchParam
        options.params = null
        options.authenticated = true;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }

        return request.get(options);
    }  
}

export default ConfigService;