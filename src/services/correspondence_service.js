import request from './request_service';
import UrlService from './url_service';

class CorrespondenceService {

    static getAllTemplates (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_TEMPLATE;
        options.params = null;

        options.headers = params;        

        options.authenticated = true;
    
        return request.get(options);
    }  
    
    // static getCorrespondenceParams () {
    //     let options = Object();
    //     options.url = UrlService.CONFIG+"/CORRESPONDENCE_TEMPLATE_PLACE_HOLDERS";
    //     options.params = null;
    //     options.authenticated = true;
    
    //     return request.get(options);
    // }  

    static createTemplate (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_TEMPLATE;
        options.params = params;
        options.authenticated = true;
    
        return request.post(options);
    }  
    
    static updateTemplate (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_TEMPLATE+"/"+params.id;
        options.params = params;
        options.authenticated = true;
    
        return request.put(options);
    }  

    static getTemplate (id) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_TEMPLATE+"/"+id;
        options.params = null;
        options.authenticated = true;
    
        return request.get(options);
    } 
    
    static delete (id) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_TEMPLATE+"/"+id;
        options.params = null;
        options.authenticated = true;
    
        return request.delete(options);
    }

    static getAllCorrespondences (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCES;
        options.params = null;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }        
        options.authenticated = true;
    
        return request.get(options);
    }

    static updateCorrespondence (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE+"/"+params.id;
        options.params = params;
        options.authenticated = true;
    
        return request.put(options);
    }

    static searchForCorrespondences (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_SEARCH;
        options.params = null;
        options.headers = params;
        options.authenticated = true;
    
        return request.get(options);
    }

    static createCorrespondence (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_GENERATE;
        options.params = params;
        options.authenticated = true;
    
        return request.post(options);
    }
    
    static searchforForms (params) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_SEARCH_CREATE+"/"+params.template;
        options.params = params;
        options.headers = {
            pageSize: params.pageSize,
            pageNumber: params.pageNumber
        }
        options.authenticated = true;
    
        return request.post(options);
    }
    
}

export default CorrespondenceService;