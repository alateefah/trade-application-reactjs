import request from './request_service';
import UrlService from './url_service';

class PaarService {

    static getShippingDocuments (params) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT_DETAILS_LIST;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber,
            'type': params.type,
            'status': params.status
        }  
        options.authenticated = true;

        return request.get(options);
    }

    static getDocumentTypes () {
        let options = Object();
        options.url = UrlService.DOCUMENT_TYPES;
        options.headers = {
            'pageSize': 100,
            'pageNumber': 1
        }  
        options.authenticated = true;

        return request.get(options);
    }

    static createShippingDocument (params) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL;
        options.params = params;
        options.headers = null  
        options.authenticated = true;

        return request.post(options);
    }

    static getShippingDocumentDetails (id) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL+"/"+id;
        options.params = null;
        options.headers = null  
        options.authenticated = true;

        return request.get(options);
    }

    static deleteShippingDocumentAttachment(id) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT+"/"+id;
        options.params = null;
        options.headers = null ;
        options.authenticated = true;

        return request.delete(options);
    }
    
    static updateShippingDocument (params) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL;
        options.params = params;
        options.headers = null  
        options.authenticated = true;

        return request.put(options);
    }

    static saveConsigmentNumber (params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL_CONSIGNMENT;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.post(options);
    }

    static NCSDocumentRequery (params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL_PAAR_REQUERY;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.post(options);
    }

    static NCSDocumentApproval (params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL_PAAR;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.post(options);
    }

    static getShippingDocumentDetailsByEventAndId(params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_DETAIL+"/"+params.id+"/"+params.event;
        options.params = null;
        options.headers = null ;
        options.authenticated = true;

        return request.get(options);
    }

    static requestDiscrepanciesActionsApproval (params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_EVENT_DISCREPANCIES;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.post(options);
    }

    static getEcdDocumentDetails (id) {
        let options = Object();
        
        options.url = UrlService.DOCUMENTS_ECD+"/"+id;
        options.params = null;
        options.headers = null ;
        options.authenticated = true;

        return request.get(options);
    }

    static uploadDiscrepancyFile (params) {
        let options = Object();
        
        options.url = UrlService.SHIPPING_DOCUMENT_DISCREPANCY_UPLOAD;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.put(options);
    }

    static getShippingDetailsByFormNumber (params) {
        let options = Object();
        
        options.url = UrlService.SEARCH_SHIPPING_DETAIL_BY_FORM_NUMBER+"/"+params.formNumber+"/search";
        options.params = null;
        options.headers = params ;
        options.authenticated = true;

        return request.get(options);
    }

    static createEcd (params) {        
        let options = Object();
        
        options.url = UrlService.ECD;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.post(options);
    }

    static acknowledgeReceipt (params) {
        let options = Object();
        
        options.url = UrlService.ECD;
        options.params = params;
        options.headers = null ;
        options.authenticated = true;

        return request.put(options);
    }
     
    static deleteEcd (id) {
        let options = Object();
        
        options.url = UrlService.ECD+"/"+id;
        options.params = null;
        options.headers = null;
        options.authenticated = true;

        return request.delete(options);
    }

    static updateEcdAttachment (params) {
        let options = Object();
        
        options.url = UrlService.ECD+"/"+params.id;
        options.params = params;
        options.headers = null;
        options.authenticated = true;

        return request.put(options);
    }

    static getDocumentForDetails (id) {
        let options = Object();
        options.url = UrlService.DOCUMENT_RELEASE+"/"+id;
        options.params = null;
        options.headers = null  
        options.authenticated = true;

        return request.get(options);
    }

    static verifyPaymentReference (reference) {
        let options = Object();
        options.url = UrlService.SHIPPING_DOCUMENT_VERIFY;
        options.params = null;
        options.headers = {
            paymentRef: reference
        }  
        options.authenticated = true;

        return request.get(options);
    }

    static addDutyPayment (params) {
        let options = Object();
        options.url = UrlService.DUTY;
        options.params = params;
        options.headers = null; 
        options.authenticated = true;

        return request.post(options);
    }

    static getDuty (id) {
        let options = Object();
        options.url = UrlService.DUTY+"/"+id;
        options.params = null;
        options.headers = null  
        options.authenticated = true;

        return request.get(options);
    }

    static acknowledgeDocumentPickup (params) {
        let options = Object();
        options.url = UrlService.DOCUMENT_RELEASE+"/"+params.id;
        options.params = null;
        options.headers = {
            recipient: params.recipient
        }  
        options.authenticated = true;

        return request.put(options);
    }
}

export default PaarService;