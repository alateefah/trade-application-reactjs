import request from './request_service';
import UrlService from './url_service';

class LoginService {

    static login (params) {
        let options = Object();
        options.url = UrlService.LOGIN;
        options.params = null;
        options.headers = {
            'X-AUTH-PASSWORD': params.password,
            'X-AUTH-USERNAME': params.username
        }
        return request.post(options);
    }   

}

export default LoginService;