import request from './request_service';
import UrlService from './url_service';

class FormMService {

    static getAll (params) {
        let options = Object();
        options.url = UrlService.GET_FORMS+"/IM";
        options.params = null;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }        
        options.authenticated = true;
    
        return request.get(options);
    }   

    static search (params) {
        let options = Object();
        options.url = UrlService.SEARCH_FORMS;
        options.params = params;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }        
        options.authenticated = true;
    
        return request.post(options);
    } 

    static getForm (params) {
        let options = Object();
        options.url = UrlService.FORM_M_DETAILS+"/"+params.formId;
        options.params = null;
        options.authenticated = true;
    
        return request.get(options);
    }

    
    static updateApplicant (params) {
        let options = Object();
        options.url = UrlService.FORM_DETAIL;
        options.params = params;
        options.authenticated = true;
    
        return request.put(options);
    }


    static getStatus () {
        let options = Object();
        options.url = UrlService.STATUS
        options.params = null;
        options.authenticated = true;
        options.headers = {
            pagesize: 200,
            pagenumber: 1,
            status: "status,goods_status,bank_status,attachments_status,processing_stage"
        }

        return request.get(options);
    }
   
    static getAvailableTemplateForForm (formId) {
        let options = Object();
        options.url = UrlService.CORRESPONDENCE_FORM+"/"+formId;
        options.params = null;
        options.authenticated = true;
    
        return request.get(options);
    }

    static generateLC (params) {
        let options = Object();
        options.url = UrlService.GENERATE_LC;
        options.params = params;
        options.authenticated = true;
    
        return request.post(options);
    }

    static amend (params) {
        let options = Object();
        options.url = UrlService.AMEND;
        options.params = params;        
        options.authenticated = true;
    
        return request.post(options);
    }


    static getAmendment (formId) {
        let options = Object();
        options.url = UrlService.AMENDMENTS+"/"+formId;
        options.params = null;        
        options.authenticated = true;
    
        return request.get(options);
    }
}

export default FormMService;