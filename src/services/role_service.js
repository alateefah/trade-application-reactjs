import request from './request_service';
import UrlService from './url_service';

class RoleService {

    static getAvailableRoles () {
        let options = Object();
        options.url = UrlService.ALL_ROLES;
        options.params = null;
        options.authenticated = true; 

        return request.get(options);
    }   

    static update (params) {
        let options = Object();
        options.url = UrlService.ALL_ROLES+'/'+params.id;
        options.params = params;
        options.authenticated = true; 

        return request.put(options);
    }
    static create (params) {
        let options = Object();
        options.url = UrlService.ALL_ROLES;
        options.params = params;
        options.authenticated = true; 

        return request.post(options);
    }

    static getAllPermissions () {
        let options = Object();
        options.url = UrlService.ALL_PERMISSIONS;
        options.params = null;
        options.authenticated = true; 

        return request.get(options);
    }

    static getRolePermissions (params) {
        let options = Object();
        options.url = UrlService.ROLE_PERMISSIONS+'/'+params.roleId;
        options.params = null;
        options.authenticated = true; 

        return request.get(options);
    }

    static assignPermissions (params) {
        let options = Object();
        options.url = UrlService.ROLE_PERMISSIONS+'/'+params.roleId;
        options.params = params.permissionIds;
        options.authenticated = true; 

        return request.post(options);
    }
    
}

export default RoleService;