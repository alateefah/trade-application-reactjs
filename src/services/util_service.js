class UtilService {

    static validAlphaNumericRegExp (text) {        
        let regExp = /^[a-zA-Z]{1}[\S]{4,}$/i

        return regExp.test(text)
    }

    static dualBoxSelectedMapper(response) {
        var values = response.map(function(item) {
            return item['id'];
        })
        
        return values;
    }

    static validEmail (email) {   
        var emailRegExp = /\S+@\S+\.\S+/;    
        if (!emailRegExp.test(email)) { return false; }
        return true;
    }

    static validAccountNumber (accountNumber) {
        var accountNumberRegExp = /^[0-9]{10}$/;
        if (!accountNumberRegExp.test(accountNumber)) { return false; } 
        return true;
    }

    static statusColor (status)  {
        let statuses = {"PENDING_VALIDATION": "orange", "IN_PROCESS": "orange", "CHARGES_COLLECTED": "blue", "VALIDATED": "blue",
            "APPROVED": "green", "ON_HOLD": "orange", "CANCELLED": "red", "COMPLETED": "green", "REJECTED": "red", "PENDING_APPROVAL": "orange",
            "CLOSED": "red"}
        
        return statuses[status];
    }

    static validName (name) {   
        var nameRegExp = /^[a-z ,.'-]+$/i;    
        if (!nameRegExp.test(name)) { return false; }
        return true;
    }

    static validNumberOnly (value) { 
        var regExp = /^\d+$/;    
        if (!regExp.test(value)) { return false; }
        return true;
    }
}

export default UtilService