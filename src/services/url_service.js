const UrlService =  {
    
    BASE_PATH : "/api/v1/ubatrade",
    
    // user management
    LOGIN: "/users/login",
    ALL_ROLES: "/roles",
    GET_BRANCHES: "/branches",
    FILTER_BRANCHES: "/branches/search",
    USER_TYPES: "/users/types",
    USERS: "/users",
    IMPORT_USER: "/users/ldap",
    CHANGE_PASSWORD: "/users/passwords",
    SET_PASSWORD: "/users/passwords",
    USERS_SEARCH: "/users/search",
    RESET_EMAIL: "/users/passwords/reset",
    ALL_PERMISSIONS: "/permissions",
    ROLE_PERMISSIONS: "/rolepermissions",
    LOGOUT: "/users/logout",
    RESEND_ACTIVATION_EMAIL: "/users/passwords/resend",
    UPDATE_ENABLE: "/users/enable",
    UNLOCK_USER: "/users/unlock",
    BRANCH: "/branch",

    //form
    GET_FORMS: "/forms",
    SEARCH_FORMS: "/form/search",
    FORM_DETAIL: "/formdetail",

    //app configuration
    GET_CONFIGS: "/app/configs",
    CONFIG: "/app/config",
    CONFIG_SEARCH:"/app/config/search",
    FORM_M_DETAILS: "/formminfo",   
    FORM_M_ATTACHMENTS: "/form/attachments",
    FORM_M_APPLICANT: "/formmapplicant",
    GENERATE_LC: "/posting",

    //audit
    GET_AUDITS: "/audits",
    SEARCH_AUDIT: "/audit/search",
    COMPARE_AUDIT: "/audit/compare/",

    //general
    APPROVAL: "/approval",
    STATUS: "/formm/status",
    ATTACHMENTS: "/attachments",
    COUNTRY: "/country",
    PAYMENT_MODES: "/payment/types",

    //correspondence
    CORRESPONDENCE: "/correspondence",
    CORRESPONDENCES: "/correspondences",
    CORRESPONDENCE_TEMPLATE: "/correspondence/templates",
    CORRESPONDENCE_GENERATE: "/correspondence/generate",
    CORRESPONDENCE_SEARCH: "/correspondence/search",
    CORRESPONDENCE_FORM: "/correspondence/forminfo",
    CORRESPONDENCE_SEARCH_CREATE: "/correspondence/search/create",

    //NXP
    SECTORIAL_CODE: "/sectors/categories",
    PACKAGES_TYPE: "/packages/types",
    UNITS_OF_MEASURMENT: "/measurements/units",
    HSCODES: "/hscodes",

    //PAAR processing
    SHIPPING_DOCUMENT_DETAILS: "/shippingdocumentdetails",
    SHIPPING_DOCUMENT_DETAILS_LIST: "/shippingdocumentdetails/list",
    DOCUMENT_TYPES: "/documenttypes",
    SHIPPING_DOCUMENT_DETAIL: "/shippingdocumentdetail",
    SHIPPING_DOCUMENT: "/shippingdocument",
    SHIPPING_DOCUMENT_DETAIL_CONSIGNMENT: "/shippingdocumentdetail/consignment",
    SHIPPING_DOCUMENT_DETAIL_PAAR_REQUERY: "/shippingdocumentdetail/paar/query",
    SHIPPING_DOCUMENT_DETAIL_PAAR: "/shippingdocumentdetail/paar",
    SHIPPING_DOCUMENT_EVENT_DISCREPANCIES: "/shippingdocumenteventdiscrepancies",
    DOCUMENTS_ECD: "/documents/ecd",
    SHIPPING_DOCUMENT_DISCREPANCY_UPLOAD: "/shippingdocumentdiscrepancy/requirement/upload",
    SEARCH_SHIPPING_DETAIL_BY_FORM_NUMBER: "/shippingdetails/forms/",
    ECD: "/documents/ecd",
    DOCUMENT_RELEASE: "/documentrelease",
    AMEND: "/amend",
    AMENDMENTS: "/amendments",
    INSPECTION_AGENT: "/inspectionagent",
    PORTS: "/portofdischarge",
    SHIPPING_DOCUMENT_VERIFY: "/shippingdocumentdetails/duty/verify",
    DUTY: "/shippingdocumentdetails/duty",
   
};

export default UrlService