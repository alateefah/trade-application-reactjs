import request from './request_service';
import UrlService from './url_service';

class UploadService {

    static getCountries () {
        let options = Object();
        options.url = UrlService.COUNTRY;
        options.authenticated = true;

        return request.get(options);
    }

    static getAppParameter (appParameter) {
        let options = Object();
        options.url = UrlService.CONFIG+"/"+appParameter;
        options.params = null;
        options.authenticated = true;
    
        return request.get(options);
    }    
    
    static getPaymentModes () {
        let options = Object();
        options.url = UrlService.PAYMENT_MODES;
        options.authenticated = true;

        return request.get(options);
    }

    static getPorts () {
        let options = Object();
        options.url = UrlService.PORTS;
        options.authenticated = true;
        options.headers = {
            'pageSize': 200,
            'pageNumber': 1
        } 
        return request.get(options);
    }  

    static getAllForms (params) {
        let options = Object();
        options.url = UrlService.GET_FORMS+params.formType;
        options.params = null;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }        
        options.authenticated = true;
    
        return request.get(options);
    }   

    static searchForForms (params) {
        let options = Object();
        options.url = UrlService.SEARCH_FORMS;
        options.params = params;
        options.headers = {
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }        
        options.authenticated = true;
    
        return request.post(options);
    } 

    static requestApproval (params) {
        let options = Object();
        options.url = UrlService.APPROVAL;
        options.params = params;
        options.authenticated = true;
    
        return request.post(options);
    }

    static getApproval (params) {
        let options = Object();
        options.url = UrlService.APPROVAL+"/"+params.parentId;
        options.params = null;
        if (!params.status || params.status === "") {
            options.headers = {
                recordId: params.recordId,
                type: params.type
            } 
        } else {
            options.headers = {
                recordId: params.recordId,
                type: params.type,
                approvalStatus: params.status
            } 
        }
        options.authenticated = true;
        return request.get(options);
    }
 
    static authorizeApproval (params) {
        let url = params.url ? "/"+params.url: "";
        let options = Object();
        options.url = UrlService.APPROVAL+"/"+params.approvalId + url;
        options.params = params;
        options.authenticated = true;
    
        return request.put(options);
    }

    static updateApprovers (params) {
        let options = Object();
        options.url = UrlService.APPROVAL+"/"+params.approvalId+"/approvers";
        options.params = params;
        options.authenticated = true;
    
        return request.put(options);
    }
    
    static upload (params) {        
        const formData = new FormData();
        formData.append('file', params.upload);

        let options = Object();
        options.url = UrlService.ATTACHMENTS;
        options.formData = true;
        options.params = formData;  
        options.authenticated = true;

        return request.post(options);
    }

    static deleteFile (fileName) {
        let options = Object();
        options.url = UrlService.ATTACHMENTS;
        options.headers = {
            filename: fileName
        }
        options.params = null;        
        options.authenticated = true;
    
        return request.delete(options);
    }
    
    static uploadAttachment (params) {        
        const formData = new FormData();
        formData.append('file', params.upload);

        let options = Object();
        options.url = UrlService.FORM_M_ATTACHMENTS;
        options.formData = true;
        options.params = formData;
        options.headers = {
            documentType: params.documentType,
            formInfoId: params.formInfoId
        }

        options.authenticated = true;
        return request.post(options);
    }

    static deleteAttachment (id) {        
        let options = Object();
        options.url = UrlService.FORM_M_ATTACHMENTS+"/"+id;
        options.params = null;

        options.authenticated = true;
        return request.delete(options);
    }
      
    static getScanningCompanies () {
        let options = Object();
        options.url = UrlService.INSPECTION_AGENT;
        options.authenticated = true;
        options.headers = {
            'pageSize': 200,
            'pageNumber': 1
        } 
        return request.get(options);
    }


}

export default UploadService;