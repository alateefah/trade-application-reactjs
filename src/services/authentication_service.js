var _ = require('lodash');

class AuthenticationService {
    
    static setSession (token, userId, authType, role, id, permissions) {
        
        this.setToken(token);
        this.setUserId(userId);
        this.setAuthType(authType);
        this.setRole(role);
        this.setId(id);
        this.setPermissions(permissions)

        if (this.isAdmin()) {
            this.goToUserPage();
        } else {
            this.goToDashboardPage();
        }

    }

    static removeSession () {
        
        this.setToken(null);
        this.setUserId(null);
        this.setAuthType(null);
        this.setRole(null);
        this.setId(null);
        this.setPermissions(null)

    }

    static hasSession () {
        
        return (localStorage.getItem('uba-trade:auth:token') !== null &&
            localStorage.getItem('uba-trade:auth:user_id') !== null &&
            localStorage.getItem('uba-trade:auth:user_role') !== null  &&
            localStorage.getItem('uba-trade:auth:id') !== null  &&
            localStorage.getItem('uba-trade:auth:user_auth_type') !== null &&
            localStorage.getItem('uba-trade:auth:permissions') !== null
        );

    }

    static getToken () { return localStorage.getItem('uba-trade:auth:token');    }

    static setToken (token) {

        if(_.isNull(token)) {
            localStorage.removeItem('uba-trade:auth:token');
        } else {
            localStorage.setItem('uba-trade:auth:token', token);
        }

    }
    
    static getId () { return localStorage.getItem('uba-trade:auth:id');  }
        
    static setId (id) {

        if (_.isNull(id)) {
            localStorage.removeItem('uba-trade:auth:id');
        } else {
            localStorage.setItem('uba-trade:auth:id', id);
        }

    }
        
    static getUserId () { return localStorage.getItem('uba-trade:auth:user_id');  }
        
    static setUserId (userId) {

        if (_.isNull(userId)) {
            localStorage.removeItem('uba-trade:auth:user_id');
        } else {
            localStorage.setItem('uba-trade:auth:user_id', userId);
        }

    }
        
    static getAuthType () { return localStorage.getItem('uba-trade:auth:user_auth_type'); }
        
    static setAuthType (authType) {

        if(_.isNull(authType)) {
            localStorage.removeItem('uba-trade:auth:user_auth_type');
        } else {
            localStorage.setItem('uba-trade:auth:user_auth_type', authType);
        }

    }
        
    static getRole () { return JSON.parse(localStorage.getItem('uba-trade:auth:user_role')); }
        
    static setRole (role) {

        if (_.isNull(role)) {
            localStorage.removeItem('uba-trade:auth:user_role');
        } else {
            localStorage.setItem('uba-trade:auth:user_role', JSON.stringify(role));
        }

    }

    static setPermissions (permission) {

        if (_.isNull(permission)) {
            localStorage.removeItem('uba-trade:auth:permissions');
        } else {
            localStorage.setItem('uba-trade:auth:permissions', JSON.stringify(permission));
        }

    }

    static getPermission (permissionId) { 
        let permissions = JSON.parse(localStorage.getItem('uba-trade:auth:permissions'))
        let permission = _.find(permissions, {name:  permissionId});
        return permission === undefined ? false : true  
    }
        
    static goToLoginPage () {   
        this.removeSession();
        return window.location.href = "#/";
    }

    static goToUserPage () { 
        return window.location.href = "#/users";
    }

    static goToDashboardPage () {   
        return window.location.href = "#/home";
    }

    static isAdmin () {
        return this.hasSession() && this.getRole().name === "ROLE_ADMIN";
    }

    static isLdap () {
        return this.hasSession() && this.getAuthType() !== "DB";
    }

    static hasPermission (permissionId) {
        return this.hasSession() && this.getPermission(permissionId);
    }

}

export default AuthenticationService;