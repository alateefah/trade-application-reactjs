import request from './request_service';
import UrlService from './url_service';

class UserService {

    static getUserTypes () {
        let options = Object();
        options.url = UrlService.USER_TYPES;
        options.params = null;
        options.authenticated = true;

        return request.get(options);
    }

    static getUsersList (pageNum, pageSize) {
        let options = Object();
        options.url = UrlService.USERS;
        options.params = null;
        options.authenticated = true;
        options.headers = {
            'pagesize': pageSize,
            'pagenumber': pageNum
        }

        return request.get(options);
    }

    static importUser(username) {
        let options = Object();
        options.url = UrlService.IMPORT_USER;
        options.params = null;
        options.authenticated = true;
        options.headers = {
            'username': username,
        }

        return request.get(options);
    }

    static createUser (params) {
        let options = Object();
        options.url = UrlService.USERS
        options.params = params
        options.authenticated = true;

        return request.post(options);
    }    
    
    static updatePassword (params) {
        let options = Object();
        options.url = UrlService.CHANGE_PASSWORD
        options.params = null
        options.authenticated = true;
        options.headers = {
            'X-AUTH-PASSWORD': params.newPassword,
            'X-AUTH-OLD-PASSWORD': params.oldPassword,
            'X-AUTH-USERNAME': params.username
        }

        return request.put(options);
    }

    static setPassword (params) {
        let options = Object();
        options.url = UrlService.SET_PASSWORD
        options.params = null
        options.authenticated = false;
        options.headers = {
            'X-AUTH-TOKEN': params.token,
            'X-AUTH-PASSWORD': params.password,
        }

        return request.post(options);
    }

    static search (params) {
        let options = Object();
        options.url = UrlService.USERS_SEARCH
        options.params = null
        options.authenticated = true;
        options.headers = {
            'key': params.searchText,
            'pageSize': params.pageSize,
            'pageNumber': params.pageNumber
        }

        return request.get(options);
    }  
    
    static sendResetPasswordEmail (params) {
        let options = Object();
        options.url = UrlService.RESET_EMAIL
        options.params = null
        options.authenticated = false;
        options.headers = {
            'X-AUTH-USERNAME': params.user
        }

        return request.post(options);
    }

    static logout() {
        let options = Object();
        options.url = UrlService.LOGOUT
        options.params = null
        options.authenticated = true;

        return request.post(options);        
    }

    static resendActivationEmail (params) {
        let options = Object();
        options.url = UrlService.RESEND_ACTIVATION_EMAIL
        options.params = null
        options.authenticated = true;
        options.headers = {
            'X-AUTH-USERNAME': params.username
        }

        return request.post(options);
    }
    
    static updateEnable (params) {
        let options = Object();
        options.url = UrlService.UPDATE_ENABLE
        options.params = params
        options.authenticated = true;

        return request.put(options);
    }

    static update (params) {
        let options = Object();
        options.url = UrlService.USERS
        options.params = params
        options.authenticated = true;

        return request.put(options);
    }

    static unlock (params) {
        let options = Object();
        options.url = UrlService.UNLOCK_USER
        options.params = params
        options.authenticated = true;

        return request.put(options);
    }
}

export default UserService;