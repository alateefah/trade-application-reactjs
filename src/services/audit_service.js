import request from './request_service';
import UrlService from './url_service';

class AuditService {

    static get (params) {
        let options = Object();

        options.url = UrlService.GET_AUDITS;
        options.params = null;
        options.authenticated = true; 
        
        options.headers = {
            pageSize: params.pageSize,
            pageNumber: params.pageNumber
        }

        return request.get(options);
    }

    static search (params) {
        let options = Object();

        options.url = UrlService.SEARCH_AUDIT;
        options.params = null;
        options.authenticated = true; 
        
        options.headers = {
            pageSize: params.pageSize,
            pageNumber: params.pageNumber,
            search: params.search,
            fromDate: params.startDate,
            toDate: params.endDate
        }

        return request.get(options);
    }

    static compare (params) {
        let options = Object();
        
        options.url = UrlService.COMPARE_AUDIT+params.resource+"/"+params.id+"/"+params.auditableId;
        options.params = null;
        options.authenticated = true; 
        options.headers = {
            pageSize: params.pageSize,
            pageNumber: params.pageNumber,
        }

        return request.get(options);
    }

}

export default AuditService;